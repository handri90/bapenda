<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Content extends CI_Controller {

	public function __construct()
        {
                parent::__construct();
        }

    public function index()
    {
        if($this->session->userdata('logged_in') == true){
            $this->listContent();
        }else{
            $this->load->view('login');
        }
    }

    public function listContent(){
        $data['list'] = $this->Content_model->getListMenu();
        $data['content'] = "district/content/listContent";
        $this->load->view('district/main',$data);
    }

    public function tambahContent(){
        $str ="";
        $this->menuRek($str,0,'-',"");
        $data['menu'] = "<select class='select2_single form-control' name='parent' tabindex='-1'><option value='0'>Root</option>".$str."</select>";
        $data['title'] = 'Tambah Content';
        $data['act'] = 'district/content/tambahContentAct';
        $data['content'] = "district/content/formContent";
        $this->load->view('district/main',$data);
    }

    public function menuRek(&$str,$parentid = 0,$prefix,$select){
        $menu = $this->Content_model->getMenuku($parentid);
        for($i=0;$i<count($menu);$i++){
            $sel = "";
            if($select == $menu[$i]['idMenu']){
                $sel = "selected";
            }
            $str .= "<option ".$sel." value='".$menu[$i]['idMenu']."'>".$prefix.$menu[$i]['menuName']."</option>";
            $this->menuRek($str,$menu[$i]['idMenu'],$prefix."-",$select);
        }
    }

    public function tambahContentAct(){
        $namamenu = $this->input->post("namamenu");
        $parent = $this->input->post("parent");
        $ordermenu = $this->input->post("ordermenu");
        $permalink = $this->input->post("permalink");
        $isi = htmlentities($this->input->post("isi"));
        $userId = $this->session->userdata("user_id");

        $slug = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $namamenu)));
        $cekSlug = $this->Content_model->cekSlugInputMenu($slug);
        if($cekSlug->jumlah > 0){
            $slug .= count($cekSlug);
        }

        $input = $this->Content_model->inputDataMenu($namamenu,$parent,$permalink,$isi,$userId,$slug,$ordermenu);
        if($input){
            $this->session->set_flashdata('message', 'Success');
        }else{
            $this->session->set_flashdata('message', 'Gagal');
        }
    	
		redirect('district/Content');
    }

    public function editContent($id){
        $data['list'] = $this->Content_model->getDataContentById($id);
        $str ="";
        $this->menuRek($str,0,'-',$data['list']->parentId);
        $data['menu'] = "<select class='select2_single form-control' name='parent' tabindex='-1'><option value='0'>Root</option>".$str."</select>";
        $data['title'] = 'Ubah Content';
        $data['act'] = 'district/content/ubahContentAct';
        $data['content'] = "district/content/formContent";
        $this->load->view('district/main',$data);
    }

    public function ubahContentAct(){
        $id = $this->input->post("idmenu");
        $namamenu = $this->input->post("namamenu");
        $parent = $this->input->post("parent");
        $isi = $this->input->post("isi");
        $permalink = $this->input->post("permalink");
        $ordermenu = $this->input->post("ordermenu");

        $slug = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $namamenu)));
        $cekSlug = $this->Content_model->cekSlugInputMenuUpdate($slug,$id);
        if($cekSlug->jumlah > 0){
            $slug .= $cekSlug->jumlah;
        }

        $input = $this->Content_model->updateDataMenu($namamenu,$parent,$permalink,$isi,$slug,$id,$ordermenu);
        if($input){
            $this->session->set_flashdata('message', 'Success');
        }else{
            $this->session->set_flashdata('message', 'Gagal');
        }
    	
		redirect('district/Content');
    }

    public function deleteContent($id){
        $cekChild = $this->Content_model->cekChildFromParent($id);
        if($cekChildFromParent->jml == 0){
            $input = $this->Content_model->deleteDataMenu($id);
        }
        if($input){
            $this->session->set_flashdata('message', 'Success');
        }else{
            $this->session->set_flashdata('message', 'Gagal');
        }
        redirect('district/Content');
    }
}

?>