<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lawang extends CI_Controller {

	public function __construct()
        {
                parent::__construct();
        }

    public function index()
    {
        if($this->session->userdata('logged_in') == true){
            $this->panelLawang();
        }else{
            $this->load->view('login');
        }
    }

    public function panelLawang(){
        $data['content'] = "district/dashboard";
        $this->load->view('district/main',$data);
    }
}

?>