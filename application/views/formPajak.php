<section id="news-single" class="section-small section-small-single">
      <div class="container">
        <div class="row">
          <div class="col-md-8">
          <h2><?php echo $titlePajak; ?></h2>
          <form>
          	
                            <div class="row">
                            
                            <?php
                            if(!empty($listkategorimblb)){
                            ?>
                            <div class="row-form">
                                    <div class="col-12 col-md-3">
                                        <div class="group">
                                            <label>Jenis</label>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <div class="group">
                                            <select class="form-control kategori">
                                            <option value="">-- PILIH --</option>
                                                <?php
                                                for($i=0;$i<count($listkategorimblb);$i++){
                                                    ?>
                                                    <option nilaitarifdasar="<?php echo $listkategorimblb[$i]['tarifDasarMBLB']; ?>" value="<?php echo $listkategorimblb[$i]['idKategoriMBLB']; ?>"><?php echo $listkategorimblb[$i]['jenisMBLB']; ?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            <div class="row-form">
                                    <div class="col-12 col-md-3">
                                        <div class="group">
                                            <label>Tarif Dasar</label>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <div class="group">
                                            <input readonly type="text" class="form-control hrgsatuan" placeholder="Tarif Dasar" >
                                            <span class="notifwarning notifwaktu"></span>
                                        </div>
                                    </div>
                                </div>
                            <div class="row-form">
                                    <div class="col-12 col-md-3">
                                        <div class="group">
                                            <label>Jumlah</label>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-5">
                                        <div class="group">
                                            <input required type="text" class="form-control onlynumber3 formatNumber jumlah" placeholder="Jumlah" />
                                            <span class="notifwarning notifluas"></span>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-4">
                                        <div class="group">
                                            <?php echo $labelSatuan; ?>
                                        </div>
                                    </div>
                                </div>
                            <?php
                            }
                            ?>
                            	<?php
                            if(!empty($listhiburan)){
                            ?>
                            <div class="row-form">
                                    <div class="col-12 col-md-3">
                                        <div class="group">
                                            <label>Keterangan</label>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <div class="group">
                                            <select class="form-control keterangan">
                                            <option value="">-- PILIH --</option>
                                                <?php
                                                for($i=0;$i<count($listhiburan);$i++){
                                                    ?>
                                                    <option nilaipersentase="<?php echo $listhiburan[$i]['persentase']; ?>" value="<?php echo $listhiburan[$i]['idPajakHiburan']; ?>"><?php echo $listhiburan[$i]['labelPersentaseHiburan']; ?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            <?php
                            }
                            ?>
                                <?php
                            	if(empty($isShowOpt)){
                                	?>
                            		<div class="row-form">
                                    <div class="col-12 col-md-3">
                                        <div class="group">
                                            <label>Jumlah</label>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-5">
                                        <div class="group">
                                            <input required type="text" class="form-control onlynumber3 formatNumber jumlah" placeholder="Jumlah" />
                                            <span class="notifwarning notifluas"></span>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-4">
                                        <div class="group">
                                            <?php echo $labelSatuan; ?>
                                        </div>
                                    </div>
                                </div>
                            		<?php
                                }
                            	?>
                            	<?php
                            if(empty($listkategorimblb)){
                            ?>
                            	<div class="row-form">
                                    <div class="col-12 col-md-3">
                                        <div class="group">
                                            <label><?php echo $labelHarga; ?></label>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <div class="group">
                                            <input required type="text" class="form-control onlynumber3 formatNumber hrgsatuan" placeholder="Harga Satuan" />
                                            <span class="notifwarning notifjumlah"></span>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                            	<?php
                            	if(empty($isShowOpt)){
                                	?>
                                <div class="row-form">
                                    <div class="col-12 col-md-3">
                                        <div class="group">
                                            <label>Jumlah Kena Pajak</label>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <div class="group">
                                            <input readonly type="text" class="form-control jmlkenapajak" placeholder="Jumlah Kena Pajak" >
                                            <span class="notifwarning notifwaktu"></span>
                                        </div>
                                    </div>
                                </div>
                            	<?php
                                }
                            	?>
                            	<div class="row-form">
                                    <div class="col-12 col-md-3">
                                        <div class="group">
                                            <label>Persentase</label>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <div class="group">
                                            <input readonly type="text" class="form-control persentase" value="<?php echo empty($listhiburan)?$persentase->persentase."%":""; ?>" >
                                            <span class="notifwarning notifwaktu"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row-form">
                                    <div class="col-12 col-md-3">
                                        <div class="group">
                                            <label>Total Pajak : </label>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <div class="group">
                                            <span class="result"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <a class="btn btn-primary btn-lg <?php echo $btnAct; ?>">Hitung</a>
                                </div>
                            </div>
                        </form>
          </div>
          <?php $this->load->view('sidebar'); ?>
        </div>
      </div>
    </section>