<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class KomponenLR extends CI_Controller {

	public function __construct()
        {
                parent::__construct();
        }

    public function index()
    {
        if($this->session->userdata('logged_in') == true){
            $this->listKomponenLR();
        }else{
            $this->load->view('login');
        }
    }

    public function listKomponenLR(){
        $data['list'] = $this->KomponenLR_model->getListKomponenLR();
        $data['content'] = "district/KomponenLR/listKomponenLR";
        $this->load->view('district/main',$data);
    }

    public function tambahKomponenLR(){
        $data['title'] = 'Tambah Komponen Luas Reklame';
        $data['act'] = 'district/KomponenLR/tambahKomponenLRAct';
        $data['content'] = "district/KomponenLR/formKomponenLR";
        $this->load->view('district/main',$data);
    }

    public function tambahKomponenLRAct(){
    	$nilaiawal = $this->input->post('nilaiawal');
    	$nilaiakhir = $this->input->post('nilaiakhir');
    	$bobot = $this->input->post('bobot');
        $notifInput = $this->KomponenLR_model->inputKomponenLR($nilaiawal,$nilaiakhir,$bobot);
        
        if($notifInput){
            $this->session->set_flashdata('message', 'Success');
        }else{
            $this->session->set_flashdata('message', 'Gagal');
        }
		redirect('district/KomponenLR');
    }
    
    public function editKomponenLR($id){
        $data['list'] = $this->KomponenLR_model->getKomponenLR($id);
        $data['title'] = 'Ubah Komponen Luas Reklame';
        $data['act'] = 'district/KomponenLR/ubahKomponenLRAct';
        $data['content'] = "district/KomponenLR/formKomponenLR";
        $this->load->view('district/main',$data);
    }

    public function ubahKomponenLRAct(){
        $nilaiawal = $this->input->post('nilaiawal');
    	$nilaiakhir = $this->input->post('nilaiakhir');
    	$bobot = $this->input->post('bobot');
        $idkomponen = $this->input->post('idkomponen');
        $notifInput = $this->KomponenLR_model->updateKomponenLR($nilaiawal,$nilaiakhir,$bobot,$idkomponen);
        
        if($notifInput){
            $this->session->set_flashdata('message', 'Success');
        }else{
            $this->session->set_flashdata('message', 'Gagal');
        }
		redirect('district/KomponenLR');
    }

    public function deleteKomponenLR($id){
    	$notifDelete = $this->KomponenLR_model->deleteKomponenLR($id);
        if($notifDelete){
            $this->session->set_flashdata('message', 'Success');
        }else{
            $this->session->set_flashdata('message', 'Gagal');
        }
        
		redirect('district/KomponenLR');        
    }
}

?>