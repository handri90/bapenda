<!-- page content -->
<div class="right_col" role="main">
          <div class="">

            <div class="row">

              <div class="col-md-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo $title; ?></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                    <form class="form-horizontal form-label-left" method="post" action="<?php echo base_url().$act; ?>">
                        
                      <div class="form-group">
                      <input type="hidden" name="idvolume" class="form-control" value="<?php echo !empty($list->idVolumePeruntukan)?$list->idVolumePeruntukan:""; ?>">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Volume Awal</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <input required type="text" name="volumeawal" class="form-control" placeholder="Volume Awal" value="<?php echo !empty($list->nilaiVolumePeruntukanAwal)?$list->nilaiVolumePeruntukanAwal:""; ?>">
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Volume Akhir</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <input required type="text" name="volumeakhir" class="form-control reqvolumeakhir" placeholder="Volume Akhir" value="<?php echo !empty($list->nilaiVolumePeruntukanAkhir)?$list->nilaiVolumePeruntukanAkhir:""; ?>">
                          <input type="checkbox" name="takterhingga" class="infinitycheck" value="oke"> <span style="font-size:20px;">&#8734;</span>
                        </div>
                      </div>
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                          <button type="button" class="btn btn-primary" id="backButton">Cancel</button>
                          <button type="reset" class="btn btn-primary">Reset</button>
                          <button type="submit" class="btn btn-success btnTambahFilePublikasi">Submit</button>
                        </div>
                      </div>

                    </form>
                  </div>
                </div>
              </div>
            </div>


            
          </div>
        </div>
        <!-- /page content -->