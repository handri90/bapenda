<!-- footer content -->
<footer>
          <div class="pull-right">
            Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  Konfirmasi Hapus Data
              </div>
              <div class="modal-body">
                  Apakah anda yakin ingin menghapus data ini?
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                  <a class="btn btn-danger btn-ok">Yes</a>
              </div>
          </div>
      </div>
  </div>

  <?php
    if($this->session->flashdata('message') != ''){
    ?>
    <div id="modal-container" class="five">
      <div class="modal-background">
        <div class="modal">
          <h2><?php echo $this->session->flashdata('message'); ?></h2>
        </div>
      </div>
    </div>
    <?php
    }
    ?>

    <!-- jQuery -->
    <script src="<?php echo base_url('assets/lawang/'); ?>vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo base_url('assets/lawang/'); ?>vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url('assets/lawang/'); ?>vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo base_url('assets/lawang/'); ?>vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="<?php echo base_url('assets/lawang/'); ?>vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- gauge.js -->
    <script src="<?php echo base_url('assets/lawang/'); ?>vendors/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="<?php echo base_url('assets/lawang/'); ?>vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="<?php echo base_url('assets/lawang/'); ?>vendors/iCheck/icheck.min.js"></script>
    <!-- Skycons -->
    <script src="<?php echo base_url('assets/lawang/'); ?>vendors/skycons/skycons.js"></script>
    <!-- Flot -->
    <script src="<?php echo base_url('assets/lawang/'); ?>vendors/Flot/jquery.flot.js"></script>
    <script src="<?php echo base_url('assets/lawang/'); ?>vendors/Flot/jquery.flot.pie.js"></script>
    <script src="<?php echo base_url('assets/lawang/'); ?>vendors/Flot/jquery.flot.time.js"></script>
    <script src="<?php echo base_url('assets/lawang/'); ?>vendors/Flot/jquery.flot.stack.js"></script>
    <script src="<?php echo base_url('assets/lawang/'); ?>vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="<?php echo base_url('assets/lawang/'); ?>vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="<?php echo base_url('assets/lawang/'); ?>vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="<?php echo base_url('assets/lawang/'); ?>vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="<?php echo base_url('assets/lawang/'); ?>vendors/DateJS/build/date.js"></script>
    <!-- JQVMap -->
    <script src="<?php echo base_url('assets/lawang/'); ?>vendors/jqvmap/dist/jquery.vmap.js"></script>
    <script src="<?php echo base_url('assets/lawang/'); ?>vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
    <script src="<?php echo base_url('assets/lawang/'); ?>vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="<?php echo base_url('assets/lawang/'); ?>vendors/moment/min/moment.min.js"></script>
    <script src="<?php echo base_url('assets/lawang/'); ?>vendors/bootstrap-daterangepicker/daterangepicker.js"></script>

    <!-- Datatables -->
    <script src="<?php echo base_url('assets/lawang/'); ?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url('assets/lawang/'); ?>vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo base_url('assets/lawang/'); ?>vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url('assets/lawang/'); ?>vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo base_url('assets/lawang/'); ?>vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo base_url('assets/lawang/'); ?>vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url('assets/lawang/'); ?>vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url('assets/lawang/'); ?>vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?php echo base_url('assets/lawang/'); ?>vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?php echo base_url('assets/lawang/'); ?>vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo base_url('assets/lawang/'); ?>vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?php echo base_url('assets/lawang/'); ?>vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="<?php echo base_url('assets/'); ?>ckeditor/ckeditor.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="<?php echo base_url('assets/lawang/'); ?>build/js/custom.min.js"></script>
    <script>
    CKEDITOR.replace( 'editor',
  {
    toolbarGroups: [{
          "name": "basicstyles",
          "groups": ["basicstyles"]
        },
        {
          "name": "links",
          "groups": ["links"]
        },
        {
          "name": "paragraph",
          "groups": ['list', 'indent', 'blocks', 'align', 'bidi', 'paragraph']
        },
        {
          "name": "document",
          "groups": ["mode"]
        },
        {
          "name": "insert",
          "groups": ["insert"]
        },
        {
          "name": "styles",
          "groups": ["styles"]
        },
        {
          "name": "about",
          "groups": ["about"]
        }
      ],
      // Remove the redundant buttons from toolbar groups defined above.
      removeButtons: 'Underline,Strike,Subscript,Superscript,Anchor,Styles,Specialchar,Flash,Source,CreateDiv,Save,NewPage,Preview,Print,About',
    filebrowserImageBrowseUrl : '<?php echo base_url('assets/kcfinder');?>'
  });
    </script>
    <script>
    function handleFileSelectBanner(evt) {
      document.getElementById('list').innerHTML = "";
      $('#savedImage').remove();
      $(".notifFile").html("");
      var files = evt.target.files;
      var j = 0;
      var separator2 = "";

      // Loop through the FileList and render image files as thumbnails.
      for (var i = 0, f; f = files[i]; i++) {
        if(i > 0){
          separator2 = '|';
        }

        // Only process image files.
        if (!f.type.match('image.*')) {
          continue;
        }

        var reader = new FileReader();

        // Closure to capture the file information.
        reader.onload = (function(theFile) {
          document.getElementById('tempFile').value += separator2+theFile.name;
          return function(e) {
            // Render thumbnail.
            var span = document.createElement('span');
            span.className = 'imageMask '+ j +'';
            span.innerHTML = 
            [
              '<div style="margin-top:10px;background-color:#caccd1;padding:10px;"><img class="thumb '+ j +'" style="height: 50%;width:100%; border: 1px solid #000;" src="', 
              e.target.result,
              '" alt="', escape(theFile.name), 
              '"/><!-- span class="deleteBtn" dek="iu" onClick="removeFunc(\'' + j + '\')">x</span --></div>'
            ].join('');
            j += 1;
            document.getElementById('list').insertBefore(span, null);
          };
        })(f);

        // Read in the image file as a data URL.
        reader.readAsDataURL(f);
      }
    }

  document.getElementById('inputImageBanner').addEventListener('change', handleFileSelectBanner, false);
    </script>
    <script>

  function handleFileSelect(evt) {
      document.getElementById('list').innerHTML = "";
      $('#savedImage').remove();
      $(".notifFile").html("");
      var files = evt.target.files;
      var j = 0;
      var separator2 = "";

      // Loop through the FileList and render image files as thumbnails.
      for (var i = 0, f; f = files[i]; i++) {
        if(i > 0){
          separator2 = '|';
        }

        // Only process image files.
        if (!f.type.match('image.*')) {
          continue;
        }

        var reader = new FileReader();

        // Closure to capture the file information.
        reader.onload = (function(theFile) {
          document.getElementById('tempFile').value += separator2+theFile.name;
          return function(e) {
            // Render thumbnail.
            var span = document.createElement('span');
            span.className = 'imageMask '+ j +'';
            span.innerHTML = 
            [
              '<div style="margin-top:17px;background-color:#caccd1;padding:25px;width:45%;float:left;margin-right:5px;position:relative;"><img class="thumb '+ j +'" style="height: 50%;width:100%; border: 1px solid #000; margin: 5px" src="', 
              e.target.result,
              '" alt="', escape(theFile.name), 
              '"/><input id="captionGambar" name="captionGambar[]" class="form-control" style="margin-left:5px;width:100%;"><!-- span class="deleteBtn" dek="iu" onClick="removeFunc(\'' + j + '\')">x</span --></div>'
            ].join('');
            j += 1;
            document.getElementById('list').insertBefore(span, null);
          };
        })(f);

        // Read in the image file as a data URL.
        reader.readAsDataURL(f);
      }
    }

  document.getElementById('inputImage').addEventListener('change', handleFileSelect, false);
    </script>

<script>

function handleFileSelectKepalaDaerah(evt) {
    document.getElementById('list').innerHTML = "";
    $('#savedImage').remove();
    $(".notifFile").html("");
    var files = evt.target.files;
    var j = 0;
    var separator2 = "";

    // Loop through the FileList and render image files as thumbnails.
    for (var i = 0, f; f = files[i]; i++) {
      if(i > 0){
        separator2 = '|';
      }

      // Only process image files.
      if (!f.type.match('image.*')) {
        continue;
      }

      var reader = new FileReader();

      // Closure to capture the file information.
      reader.onload = (function(theFile) {
        document.getElementById('tempFile').value += separator2+theFile.name;
        return function(e) {
          // Render thumbnail.
          var span = document.createElement('span');
          span.className = 'imageMask '+ j +'';
          span.innerHTML = 
          [
            '<div style="margin-top:17px;background-color:#caccd1;padding:25px;width:45%;float:left;margin-right:5px;position:relative;"><img class="thumb '+ j +'" style="height: 50%;width:100%; border: 1px solid #000; margin: 5px" src="', 
            e.target.result,
            '" alt="', escape(theFile.name), 
            '"/><!-- span class="deleteBtn" dek="iu" onClick="removeFunc(\'' + j + '\')">x</span --></div>'
          ].join('');
          j += 1;
          document.getElementById('list').insertBefore(span, null);
        };
      })(f);

      // Read in the image file as a data URL.
      reader.readAsDataURL(f);
    }
  }

document.getElementById('inputImageKepalaDaerah').addEventListener('change', handleFileSelectKepalaDaerah, false);
  </script>

<script type="text/javascript">
$(document).ready(function(){
  $(".deleteImage").click(function(){
    let jmlClassSaved = $(".deleteImage").length;
    let isExistFile = $("#list").html();
    if(isExistFile.length > 0 || jmlClassSaved > 1){
      let start = $(this);
      let fotoid = $(this).attr("fotopk");
          $.ajax({
                  type: "POST",
                  url: "<?php echo base_url(); ?>district/Gallery/deleteImage",
                  data:{fotoid:fotoid},
                  success: 
                    function(data){
                      if(data == 'sukses'){
                        start.parent().remove();
                      }
                    }
              });
    }else{
      alert("Foto tidak boleh kosong");
    }
  });

  $("#backButton").click(function(){
        window.history.back();
      });

      $(".btnBerita").click(function(){
        let isi = CKEDITOR.instances['editor'].getData();
        if(isi === ''){
          $(".notifIsi").html("Content Harus Diisi");
          return false;
        }else{
          $(".notifIsi").html("");
        }
      });

		$(".btnTambahFilePublikasi").click(function(){
        	
        	if($('#inputImage').val() != ''){
               if(document.getElementById('inputImage').files[0].size > 20000000){
        			$(".notiffileupload").html("File Tidak Boleh Lebih dari 10MB");
        			return false;
        		}
            }
        });

      $('body').on('keypress','.onlynumber',function(e){
        //if the letter is not digit then display error and don't type anything
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57) && e.which != 46) {
            //display error message
            return false;
                  
        }
      });

		$('body').on('keypress','.numbertelp',function(e){
        //if the letter is not digit then display error and don't type anything
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57) && e.which != 40 && e.which != 41 && e.which != 32) {
            //display error message
            return false;
                  
        }
      });

      $('body').on('keyup','.formatNumber',function(){
        let vValue = $(this).val();
        x = vValue.replace(/,/g, ""); // Strip out all commas
        x = x.split("").reverse().join("");
        x = x.replace(/.../g, function (e) {
            return e + ",";
        }); // Insert new commas
        x = x.split("").reverse().join("");
        x = x.replace(/^,/, ""); // Remove leading comma
        $(this).val(x);
      });

      $('#confirm-delete').on('show.bs.modal', function(e) {
          $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
      });

		$('.infinitycheck').change(function(){
        if($(this).prop('checked')) {
            $('.reqvolumeakhir').removeAttr('required');
        } else {
          $('.reqvolumeakhir').attr('required','required');
        }
      });

      setTimeout(function(){
        $("#modal-container").addClass('out');
      }, 1000);
});
    </script>
  </body>
</html>