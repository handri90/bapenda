<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class KategoriMBLB extends CI_Controller {

	public function __construct()
        {
                parent::__construct();
        }

    public function index()
    {
        if($this->session->userdata('logged_in') == true){
            $this->listKategoriMBLB();
        }else{
            $this->load->view('login');
        }
    }

	public function listKategoriMBLB(){
        $data['list'] = $this->KategoriMBLB_model->getListKategoriMBLB();
        $data['content'] = "district/KategoriMBLB/listKategoriMBLB";
        $this->load->view('district/main',$data);
    }

    public function tambahKategoriMBLB(){
        $data['title'] = 'Tambah Kategori MBLB';
    	$data['isShowLabel'] = "show";
        $data['act'] = 'district/KategoriMBLB/tambahKategoriMBLBAct';
        $data['content'] = "district/KategoriMBLB/formKategoriMBLB";
        $this->load->view('district/main',$data);
    }

    public function tambahKategoriMBLBAct(){
        $jenis = $this->input->post('jenis');
        $tarifdasar = str_replace(",","",$this->input->post('tarifdasar'));
        $notifInput = $this->KategoriMBLB_model->inputKategoriMBLB($jenis,$tarifdasar);
        
        if($notifInput){
            $this->session->set_flashdata('message', 'Success');
        }else{
            $this->session->set_flashdata('message', 'Gagal');
        }
		redirect('district/KategoriMBLB');
    }
    
    public function editKategoriMBLB($id){
        $data['list'] = $this->KategoriMBLB_model->getKategoriMBLB($id);
        $data['title'] = 'Ubah Kategori MBLB';
        $data['act'] = 'district/KategoriMBLB/ubahKategoriMBLBAct';
        $data['content'] = "district/KategoriMBLB/formKategoriMBLB";
        $this->load->view('district/main',$data);
    }

    public function ubahKategoriMBLBAct(){
    	$jenis = $this->input->post('jenis');
        $tarifdasar = str_replace(",","",$this->input->post('tarifdasar'));
        $idkategori = $this->input->post('idkategori');
        $notifInput = $this->KategoriMBLB_model->updateKategoriMBLB($jenis,$tarifdasar,$idkategori);
        
        if($notifInput){
            $this->session->set_flashdata('message', 'Success');
        }else{
            $this->session->set_flashdata('message', 'Gagal');
        }
		redirect('district/KategoriMBLB');
    }

	public function deleteKategoriMBLB($id){
        $input = $this->KategoriMBLB_model->deleteKategoriMBLB($id);
        redirect('district/KategoriMBLB');
    }
}

?>