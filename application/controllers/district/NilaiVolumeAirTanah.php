<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class NilaiVolumeAirTanah extends CI_Controller {

	public function __construct()
        {
                parent::__construct();
        }

    public function index()
    {
        if($this->session->userdata('logged_in') == true){
            $this->listNilaiVolumeAirTanah();
        }else{
            $this->load->view('login');
        }
    }

    public function listNilaiVolumeAirTanah(){
        $data['list'] = $this->NilaiVolumeAirTanah_model->getListNilaiVolumeAirTanah();
        $data['content'] = "district/NilaiVolumeAirTanah/listNilaiVolumeAirTanah";
        $this->load->view('district/main',$data);
    }

    public function tambahNilaiVolumeAirTanah(){
        $data['peruntukan'] = $this->NilaiVolumeAirTanah_model->getPeruntukan();
        $data['volume'] = $this->NilaiVolumeAirTanah_model->getVolume();
        $data['title'] = 'Tambah Volume Air Tanah';
        $data['act'] = 'district/NilaiVolumeAirTanah/tambahNilaiVolumeAirTanahAct';
        $data['content'] = "district/NilaiVolumeAirTanah/formNilaiVolumeAirTanah";
        $this->load->view('district/main',$data);
    }

    public function tambahNilaiVolumeAirTanahAct(){
        $peruntukan = $this->input->post('peruntukan');
        $volume = $this->input->post('volume');
        $nilaiperolehan = $this->input->post('nilaiperolehan');

        $getSameValue = $this->NilaiVolumeAirTanah_model->getSameValue($peruntukan,$volume);
         if(empty($getSameValue)){
            $notifInput = $this->NilaiVolumeAirTanah_model->inputNilaiVolume($peruntukan,$volume,$nilaiperolehan);
         }else{
             $notifInput = FALSE;
         }
        
        if($notifInput){
            $this->session->set_flashdata('message', 'Success');
        }else{
            $this->session->set_flashdata('message', 'Gagal');
        }
		redirect('district/NilaiVolumeAirTanah');
    }
    
    public function editNilaiVolumeAirTanah($id){
        $data['list'] = $this->NilaiVolumeAirTanah_model->getNilaiVolumeAirTanah($id);
        $data['peruntukan'] = $this->NilaiVolumeAirTanah_model->getPeruntukan();
        $data['volume'] = $this->NilaiVolumeAirTanah_model->getVolume();
        $data['title'] = 'Ubah Nilai & Volume Air Tanah';
        $data['act'] = 'district/NilaiVolumeAirTanah/ubahNilaiVolumeAirTanahAct';
        $data['content'] = "district/NilaiVolumeAirTanah/formNilaiVolumeAirTanah";
        $this->load->view('district/main',$data);
    }

    public function ubahNilaiVolumeAirTanahAct(){
        $peruntukan = $this->input->post('peruntukan');
        $volume = $this->input->post('volume');
        $nilaiperolehan = $this->input->post('nilaiperolehan');
         $idnilaivolume = $this->input->post('idnilaivolume');

         $getSameValue = $this->NilaiVolumeAirTanah_model->getSameValue($peruntukan,$volume);
         if(empty($getSameValue)){
            $notifInput = $this->NilaiVolumeAirTanah_model->updateNilaiVolume($peruntukan,$volume,$nilaiperolehan,$idnilaivolume);
         }else{
             $notifInput = FALSE;
         }
        
        if($notifInput){
            $this->session->set_flashdata('message', 'Success');
        }else{
            $this->session->set_flashdata('message', 'Gagal');
        }
		redirect('district/NilaiVolumeAirTanah');
    }

    public function deleteNilaiVolumeAirTanah($id){
    	$notifDelete = $this->NilaiVolumeAirTanah_model->deleteNilaiVolume($id);
        if($notifDelete){
            $this->session->set_flashdata('message', 'Success');
        }else{
            $this->session->set_flashdata('message', 'Gagal');
        }
        
		redirect('district/NilaiVolumeAirTanah');        
    }
}

?>