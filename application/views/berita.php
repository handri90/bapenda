<section id="news">
    <div class="container">
    <h3 class="pull-left">Berita</h3>
    <div class="clearfix"></div>
    <div class="row grid-pad">
    <?php
        for($i=0;$i<count($berita);$i++){
    ?>
    <div class="col-sm-4">
            <a href="<?php echo base_url().'berita/'.$berita[$i]['slugBerita']; ?>"><img src="<?php echo base_url().'fotoberita/'.$berita[$i]['foto']; ?>" alt="" class="img-responsive center-block">
            <h5><?php echo $berita[$i]['judul']; ?></h5></a>
            <p><?php 
            $berita[$i]['content']=strip_tags(html_entity_decode($berita[$i]['content']));
            $berita[$i]['content']= preg_replace('/\s+?(\S+)?$/', '', substr($berita[$i]['content'], 0, 101));
            echo $berita[$i]['content'].'...';
            ?></p><a href="<?php echo base_url().'berita/'.$berita[$i]['slugBerita']; ?>" class="btn btn-gray btn-xs">Selengkapnya</a>
        </div>
    <?php
        }
    ?>
    </div>
    </div>
</section>