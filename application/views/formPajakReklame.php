<section id="news-single" class="section-small section-small-single">
      <div class="container">
        <div class="row">
          <div class="col-md-8">
          <h2>Pajak Reklame</h2>
          <form>
                            <div class="row">
                                <div class="row-form">
                                    <div class="col-12 col-md-3">
                                        <div class="group">
                                            <label>Luas Reklame / m<sup>2</sup></label>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <div class="group">
                                            <input required type="text" class="form-control luasreklame onlynumber" placeholder="Luas Reklame" />
                                            <span class="notifwarning notifluas"></span>
                                        </div>
                                    </div>
                                </div>
                            	<div class="row-form">
                                    <div class="col-12 col-md-3">
                                        <div class="group">
                                            <label>Jumlah Sisi</label>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <div class="group">
                                            <input required type="text" class="form-control jmlsisi onlynumber2" placeholder="Jumlah Sisi" />
                                            <span class="notifwarning notifjumlah"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row-form">
                                    <div class="col-12 col-md-3">
                                        <div class="group">
                                            <label>Jenis Reklame</label>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <div class="group">
                                            <select class="form-control jenisreklame">
                                                <?php
                                                for($i=0;$i<count($jenisreklame);$i++){
                                                    ?>
                                                    <option value="<?php echo $jenisreklame[$i]['idJenisReklame']; ?>"><?php echo $jenisreklame[$i]['labelJenisReklame']; ?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row-form">
                                    <div class="col-12 col-md-3">
                                        <div class="group">
                                            <label>Tempat Pemasangan</label>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <div class="group">
                                            <select class="form-control tempatpemasangan">
                                            <?php
                                                for($i=0;$i<count($tempatpemasangan);$i++){
                                                    ?>
                                                    <option value="<?php echo $tempatpemasangan[$i]['idKomponenNJT']; ?>"><?php echo stripslashes($tempatpemasangan[$i]['labelKomponenNJT']); ?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row-form">
                                    <div class="col-12 col-md-3">
                                        <div class="group">
                                            <label>Selama</label>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-7">
                                        <div class="group">
                                            <input required type="text" class="form-control lamapemasangan" placeholder="Lama Pemasangan" >
                                            <span class="notifwarning notifwaktu"></span>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-2">
                                        <div class="group">
                                            / Bulan
                                        </div>
                                    </div>
                                </div>
                                <!-- <div class="col-12 col-md-5">
                                    <div class="group">
                                        <select class="form-control tempatpemasangan">
                                            <option value="bulan">Bulan</option>
                                            <option value="hari">Hari</option>
                                        </select>
                                    </div>
                                </div> -->
                                <div class="row-form">
                                    <div class="col-12 col-md-3">
                                        <div class="group">
                                            <label>Pajak Reklame : </label>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <div class="group">
                                            <span class="result"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <a class="btn btn-primary btn-lg calcpajakreklame">Hitung</a>
                                </div>
                            </div>
                        </form>
          </div>
          <?php $this->load->view('sidebar'); ?>
        </div>
      </div>
    </section>