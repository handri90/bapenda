<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class KategoriFilePublikasi extends CI_Controller {

	public function __construct()
        {
                parent::__construct();
        }

    public function index()
    {
        if($this->session->userdata('logged_in') == true){
            $this->listKategoriFilePublikasi();
        }else{
            $this->load->view('login');
        }
    }

    public function listKategoriFilePublikasi(){
        $data['list'] = $this->KategoriFilePublikasi_model->getListKategoriFilePublikasi();
        $data['content'] = "district/KategoriFilePublikasi/listKategoriFilePublikasi";
        $this->load->view('district/main',$data);
    }

    public function tambahKategoriFilePublikasi(){
        $data['title'] = 'Tambah Kategori Download';
        $data['act'] = 'district/KategoriFilePublikasi/tambahKategoriFilePublikasiAct';
        $data['content'] = "district/KategoriFilePublikasi/formKategoriFilePublikasi";
        $this->load->view('district/main',$data);
    }

    public function tambahKategoriFilePublikasiAct(){
        $namakategori = $this->input->post('namakategori');
        $slug = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $namakategori)));
        $cekJudul = $this->KategoriFilePublikasi_model->cekSlugKategori($namakategori);
        if($cekJudul->jumlah > 0){
            $slug .= $cekJudul->jumlah;
        }
        $notifInput = $this->KategoriFilePublikasi_model->inputKategoriFilePublikasi($namakategori,$slug);
        
        if($notifInput){
            $this->session->set_flashdata('message', 'Success');
        }else{
            $this->session->set_flashdata('message', 'Gagal');
        }
		redirect('district/KategoriFilePublikasi');
    }

    private function set_upload_options()
	{   
	    //upload an image options
	    $filename = md5(uniqid(rand(), true));
	    $config = array();
	    $config['upload_path'] = 'upload';
	    $config['allowed_types'] = 'gif|jpg|png|jpeg|pdf';
	    $config['file_name'] = $filename;

	    return $config;
    }
    
    public function editKategoriFilePublikasi($id){
        $data['list'] = $this->KategoriFilePublikasi_model->getKategoriFilePublikasiById($id);
        $data['title'] = 'Ubah Kategori Download';
        $data['act'] = 'district/KategoriFilePublikasi/ubahKategoriFilePublikasiAct';
        $data['content'] = "district/KategoriFilePublikasi/formKategoriFilePublikasi";
        $this->load->view('district/main',$data);
    }

    public function ubahKategoriFilePublikasiAct(){
        $namakategori = $this->input->post('namakategori');
        $idkategori = $this->input->post('idkategori');
        $slug = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $namakategori)));
        $cekJudul = $this->KategoriFilePublikasi_model->cekSlugKategoriUpdate($slug,$idkategori);
        if($cekJudul->jumlah > 0){
            $slug .= $cekJudul->jumlah;
        }
        $notifInput = $this->KategoriFilePublikasi_model->updateKategoriFilePublikasi($namakategori,$idkategori,$slug);
        
        if($notifInput){
            $this->session->set_flashdata('message', 'Success');
        }else{
            $this->session->set_flashdata('message', 'Gagal');
        }
		redirect('district/KategoriFilePublikasi');
    }

    public function deleteKategoriFilePublikasi($id){
    	$notifDelete = $this->KategoriFilePublikasi_model->deleteKategoriFilePublikasi($id);
        if($notifDelete){
            $this->session->set_flashdata('message', 'Success');
        }else{
            $this->session->set_flashdata('message', 'Gagal');
        }
        
		redirect('district/KategoriFilePublikasi');        
    }
}

?>