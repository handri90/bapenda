<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class BioDinas extends CI_Controller {

	public function __construct()
        {
                parent::__construct();
        }

    public function index()
    {
        if($this->session->userdata('logged_in') == true){
            $this->editBioDinas();
        }else{
            $this->load->view('login');
        }
    }
    
    public function editBioDinas(){
        $data['list'] = $this->BioDinas_model->getBioDinas();
        $data['title'] = 'Ubah Data Dinas';
        $data['act'] = 'district/BioDinas/ubahBioDinasAct';
        $data['content'] = "district/BioDinas/formBioDinas";
        $this->load->view('district/main',$data);
    }

    public function ubahBioDinasAct(){
        $alamat = $this->input->post('alamat');
    	$email = $this->input->post('email');
    	$telp = $this->input->post('telp');
    	$nama = $this->input->post('nama');
        $idbio = $this->input->post('idbio');
        $notifInput = $this->BioDinas_model->updateBioDinas($nama,$alamat,$email,$telp,$idbio);
        
        if($notifInput){
            $this->session->set_flashdata('message', 'Success');
        }else{
            $this->session->set_flashdata('message', 'Gagal');
        }
		redirect('district/BioDinas/editBioDinas');
    }
}

?>