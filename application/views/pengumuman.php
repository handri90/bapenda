<section id="news-single" class="section-small section-small-single">
    <div class="container">
    <h3 class="pull-left">Pengumuman</h3>
    <div class="clearfix"></div>
    <div class="row">
    <div class="content-page">
    <?php
        $no = 1;
        for($i=0;$i<count($pengumuman);$i++){
    ?>
        <div class="col-md-12 shad-univ">
        <span class="nomor-urut"><?php echo $no++; ?></span>
        <span class="content-ls-pengumuman"><h5><a href="<?php echo base_url().'pengumuman/'.$pengumuman[$i]['slug']; ?>" class="headline">
            <?php echo $pengumuman[$i]['judulPengumuman']; ?>
        </a></h5>
        <p><?php 
        $pengumuman[$i]['isiPengumuman']=strip_tags(html_entity_decode($pengumuman[$i]['isiPengumuman']));
        $pengumuman[$i]['isiPengumuman']= preg_replace('/\s+?(\S+)?$/', '', substr($pengumuman[$i]['isiPengumuman'], 0, 101));
        echo $pengumuman[$i]['isiPengumuman'].'...';
        ?></p></span>
        <span class="clearMobile"></span>
        </div>
        <?php
            }
        ?>
        </div>
        <?php $this->load->view('sidebar'); ?>
    </div>
    </div>
</section>
        