<!-- page content -->
<div class="right_col" role="main">
          <div class="">

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Pengumuman</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li><a href="<?php echo base_url(); ?>district/Pengumuman/tambahPengumuman" class="close-link badge bg-green"><i class="fa fa-plus"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <table id="datatable" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>No</th>
                          <th>Judul</th>
                          <th>Isi</th>
                          <th>File</th>
                          <th>Action</th>
                        </tr>
                      </thead>


                      <tbody>
                      <?php
                      $no=1;
                        for($i=0;$i<count($list);$i++){
                          ?>
                          <tr>
                            <td><?php echo $no++; ?></td>
                            <td><?php echo $list[$i]['judulPengumuman']; ?></td>
                            <td><?php 
                            $list[$i]['isiPengumuman']=strip_tags(html_entity_decode($list[$i]['isiPengumuman']));
                            $list[$i]['isiPengumuman']= preg_replace('/\s+?(\S+)?$/', '', substr($list[$i]['isiPengumuman'], 0, 101));
                            echo $list[$i]['isiPengumuman'].'...';
                            ?></td>
                            <td><a href="<?php echo base_url()."pengumuman/".$list[$i]['file']; ?>" download>Download</a></td>
                            <td><a class="btn btn-primary pull-right" style="margin-right: 5px;" href="<?php echo base_url().'district/Pengumuman/editPengumuman/'.$list[$i]['idPengumuman']; ?>"><i class="fa fa-pencil"></i> Ubah</a><a class="btn btn-primary pull-right" style="margin-right: 5px;" data-toggle="modal" data-target="#confirm-delete" href="#" data-href="<?php echo base_url().'district/Pengumuman/deletePengumuman/'.$list[$i]['idPengumuman']; ?>"><i class="fa fa-eraser"></i> Hapus</a></td>
                          </tr>
                          <?php
                        }
                      ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>