<!-- page content -->
<div class="right_col" role="main">
          <div class="">

            <div class="row">

              <div class="col-md-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo $title; ?></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                    <form class="form-horizontal form-label-left" method="post" action="<?php echo base_url().$act; ?>">
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Nama Dinas</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <input type="hidden" name="idbio" class="form-control" value="<?php echo !empty($list->idDinas)?$list->idDinas:""; ?>">
                          <input required type="text" name="nama" class="form-control" placeholder="Nama Dinas" value="<?php echo !empty($list->namaDinas)?$list->namaDinas:""; ?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Email</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                        <input required type="text" name="email" class="form-control" placeholder="Email" value="<?php echo !empty($list->emailDinas)?$list->emailDinas:""; ?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Telp</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <input required type="text" name="telp" class="form-control numbertelp" placeholder="Telp" value="<?php echo !empty($list->telpDinas)?$list->telpDinas:""; ?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Alamat</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <textarea name="alamat"><?php echo !empty($list->alamatDinas)?$list->alamatDinas:""; ?></textarea>
                        </div>
                      </div>
                      
                      
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                          <button type="button" class="btn btn-primary" id="backButton">Cancel</button>
                          <button type="reset" class="btn btn-primary">Reset</button>
                          <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                      </div>

                    </form>
                  </div>
                </div>
              </div>
            </div>


            
          </div>
        </div>
        <!-- /page content -->