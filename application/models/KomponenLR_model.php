<?php
class KomponenLR_model extends CI_Model {

        public function __construct()
        {

        }

        public function getListKomponenLR(){
                $query = $this->db->query("select * from komponenlr");
                return $query->result_array();
        }
        
        public function inputKomponenLR($nilaiawal,$nilaiakhir,$bobot){
            $query = $this->db->query("insert into komponenlr (nilaiAwalKomponenLR,nilaiAkhirKomponenLR,bobotKomponenLR) values ('".$nilaiawal."','".$nilaiakhir."',".$bobot.")");
    		return $query;
        }

        public function getKomponenLR($id){
          $query = $this->db->query("select * from komponenlr where idKomponenLR = ".$id);
    		  return $query->row();
        }

        public function updateKomponenLR($nilaiawal,$nilaiakhir,$bobot,$idkomponen){
          $query = $this->db->query("update komponenlr set nilaiAwalKomponenLR='".$nilaiawal."',bobotKomponenLR=".$bobot.",nilaiAkhirKomponenLR='".$nilaiakhir."' where idKomponenLR=".$idkomponen);
          return $query;
        }

        public function deleteKomponenLR($id){
          $query = $this->db->query("delete from komponenlr where idKomponenLR = ".$id);
          return $query;
        }
}
?>