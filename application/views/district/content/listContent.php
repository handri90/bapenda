<!-- page content -->
<div class="right_col" role="main">
          <div class="">

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Content</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li><a href="<?php echo base_url(); ?>district/Content/tambahContent" class="close-link badge bg-green"><i class="fa fa-plus"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    
                    <table id="datatable" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>No</th>
                          <th>Nama Menu</th>
                          <th>Content</th>
                          <th>Action</th>
                        </tr>
                      </thead>


                      <tbody>
                      <?php
                      $no=1;
                        for($i=0;$i<count($list);$i++){
                          ?>
                          <tr>
                            <td><?php echo $no++; ?></td>
                            <td><?php echo $list[$i]['menuName']; ?></td>
                            <td><?php 
                            $list[$i]['content']=strip_tags(html_entity_decode($list[$i]['content']));
                            $list[$i]['content']= preg_replace('/\s+?(\S+)?$/', '', substr($list[$i]['content'], 0, 101));
                            echo $list[$i]['content'].'...';
                            ?></td>
                            <td><a class="btn btn-primary pull-right" style="margin-right: 5px;" href="<?php echo base_url().'district/Content/editContent/'.$list[$i]['idMenu']; ?>"><i class="fa fa-pencil"></i> Ubah</a><a class="btn btn-primary pull-right" style="margin-right: 5px;" data-toggle="modal" data-target="#confirm-delete" href="#" data-href="<?php echo base_url().'district/Content/deleteContent/'.$list[$i]['idMenu']; ?>"><i class="fa fa-eraser"></i> Hapus</a></td>
                          </tr>
                          <?php
                        }
                      ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>