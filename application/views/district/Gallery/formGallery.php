<!-- page content -->
<div class="right_col" role="main">
          <div class="">

            <div class="row">

              <div class="col-md-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo $title; ?></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                    <form enctype="multipart/form-data" class="form-horizontal form-label-left" method="post" action="<?php echo base_url().$act; ?>">

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Nama Album</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <input type="hidden" name="idalbum" class="form-control" value="<?php echo !empty($list->idAlbum)?$list->idAlbum:""; ?>">
                          <input required type="text" name="namaalbum" class="form-control" placeholder="Nama Album" value="<?php echo !empty($list->namaAlbum)?$list->namaAlbum:""; ?>">
                        </div>
                      </div>
                      <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Foto</label>
                            <label class="btn btn-primary btn-upload btnUpgrade" for="inputImage" title="Upload image file">
                              <?php
                              $req = "";
                              if(empty($list->idAlbum)){
                                $req = "required";
                              }
                              ?>
                              <input multiple <?php echo $req; ?> type="file" class="sr-only" id="inputImage" name="userfile[]" accept="image/*">
                              <span class="docs-tooltip" data-toggle="tooltip" title="Import image with Blob URLs">
                                <span class="fa fa-upload"></span>
                              </span>
                            </label>
                            <input name="tempFile" id="tempFile" type="hidden" value="">
                            <input name="tempSavedFile" id="tempSavedFile" value="<?php echo isset($list->idFoto)?$list->idFoto:''; ?>" type="hidden">
                          </div>
                          <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">&nbsp;</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <output id="list"></output>
                          <?php
                          if(!empty($list->idAlbum)){
                            ?>
                            <output id="savedImage">
                              <span class='imgSaved'>
                              <?php
                              $listfoto = explode('|',$list->filefoto);
                              $caption = explode('|',$list->caption);
                              $idFoto = explode('|',$list->idFoto);
                              for($i=0;$i<count($listfoto);$i++){
                                ?>
                                  <div class="thumb<?php echo $i; ?>" style="margin-top:10px;background-color:#caccd1;padding:25px;width:45%;float:left;margin-right:5px;position:relative;"><img style="height: 50%;width:100%; border: 1px solid #000; margin: 5px" src="<?php echo base_url()."filegallery/".$listfoto[$i]; ?>" /><input value="<?php echo !empty($caption[$i])?$caption[$i]:""; ?>" id="captionGambar" name="captionGambarSaved[]" class="form-control" style="margin-left:5px;width:100%;"><span style="position:absolute;top:-3px;right:5px;font-size:20px;" class="deleteImage" fotopk="<?php echo $idFoto[$i]; ?>"><i class="fa fa-close"></i></span></div>
                                <?php
                              }
                              ?>
                            
                            </span>
                            </output>
                            <?php
                          }
                          ?>
                        </div>
                      </div>
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                          <button type="button" class="btn btn-primary" id="backButton">Cancel</button>
                          <button type="reset" class="btn btn-primary">Reset</button>
                          <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                      </div>

                    </form>
                  </div>
                </div>
              </div>
            </div>


            
          </div>
        </div>
        <!-- /page content -->