<!-- page content -->
<div class="right_col" role="main">
          <div class="">

            <div class="row">

              <div class="col-md-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo $title; ?></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                    <form enctype="multipart/form-data" class="form-horizontal form-label-left" method="post" action="<?php echo base_url().$act; ?>">

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Judul</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <input type="hidden" name="idDetailDownload" class="form-control" value="<?php echo !empty($list->idDetailKategoriDownload)?$list->idDetailKategoriDownload:""; ?>">
                          <input type="text" name="judul" class="form-control" placeholder="Judul" value="<?php echo !empty($list->judulDetailKategoriDownload)?$list->judulDetailKategoriDownload:""; ?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Kategori Download</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <select name="kategori" class="form-control">
                            <option value=""></option>
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">File
                        </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php
                        $req = "";
                        if(empty($list->idPengumuman)){
                            $req = "required";
                        }
                        ?>
                        <input <?php echo $req; ?> type="file" id="inputImage" name="userfile">
                        <input type="hidden" name="oldFile" value="<?php echo !empty($list->file)?$list->file:""; ?>">
                        </div>
                      </div>
                      
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                          <button type="button" class="btn btn-primary" id="backButton">Cancel</button>
                          <button type="reset" class="btn btn-primary">Reset</button>
                          <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                      </div>

                    </form>
                  </div>
                </div>
              </div>
            </div>


            
          </div>
        </div>
        <!-- /page content -->