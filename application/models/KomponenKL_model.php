<?php
class KomponenKL_model extends CI_Model {

        public function __construct()
        {

        }

        public function getListKomponenKL(){
                $query = $this->db->query("select * from komponenkl");
                return $query->result_array();
        }
        
        public function inputKomponenKL($nilaiawal,$nilaiakhir,$bobot){
            $query = $this->db->query("insert into komponenkl (nilaiAwalKomponenKL,nilaiAkhirKomponenKL,bobotKomponenKL) values ('".$nilaiawal."','".$nilaiakhir."',".$bobot.")");
    		return $query;
        }

        public function getKomponenKL($id){
          $query = $this->db->query("select * from komponenkl where idKomponenKL = ".$id);
    		  return $query->row();
        }

        public function updateKomponenKL($nilaiawal,$nilaiakhir,$bobot,$idkomponen){
          $query = $this->db->query("update komponenkl set nilaiAwalKomponenKL='".$nilaiawal."',bobotKomponenKL=".$bobot.",nilaiAkhirKomponenKL='".$nilaiakhir."' where idKomponenKL=".$idkomponen);
          return $query;
        }

        public function deleteKomponenKL($id){
          $query = $this->db->query("delete from komponenkl where idKomponenKL = ".$id);
          return $query;
        }
}
?>