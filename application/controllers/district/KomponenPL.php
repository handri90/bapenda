<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class KomponenPL extends CI_Controller {

	public function __construct()
        {
                parent::__construct();
        }

    public function index()
    {
        if($this->session->userdata('logged_in') == true){
            $this->listKomponenPL();
        }else{
            $this->load->view('login');
        }
    }

    public function listKomponenPL(){
        $data['list'] = $this->KomponenPL_model->getListKomponenPL();
        $data['content'] = "district/KomponenPL/listKomponenPL";
        $this->load->view('district/main',$data);
    }

    public function tambahKomponenPL(){
        $data['title'] = 'Tambah Komponen Peluang Melihat';
        $data['act'] = 'district/KomponenPL/tambahKomponenPLAct';
        $data['content'] = "district/KomponenPL/formKomponenPL";
        $this->load->view('district/main',$data);
    }

    public function tambahKomponenPLAct(){
    	$namakomponen = $this->input->post('namakomponen');
    	$bobot = $this->input->post('bobot');
        $notifInput = $this->KomponenPL_model->inputKomponenPL($namakomponen,$bobot);
        
        if($notifInput){
            $this->session->set_flashdata('message', 'Success');
        }else{
            $this->session->set_flashdata('message', 'Gagal');
        }
		redirect('district/KomponenPL');
    }
    
    public function editKomponenPL($id){
        $data['list'] = $this->KomponenPL_model->getKomponenPL($id);
        $data['title'] = 'Ubah Komponen Peluang Melihat';
        $data['act'] = 'district/KomponenPL/ubahKomponenPLAct';
        $data['content'] = "district/KomponenPL/formKomponenPL";
        $this->load->view('district/main',$data);
    }

    public function ubahKomponenPLAct(){
    	$namakomponen = $this->input->post('namakomponen');
    	$bobot = $this->input->post('bobot');
        $idkomponen = $this->input->post('idkomponen');
        $notifInput = $this->KomponenPL_model->updateKomponenPL($namakomponen,$bobot,$idkomponen);
        
        if($notifInput){
            $this->session->set_flashdata('message', 'Success');
        }else{
            $this->session->set_flashdata('message', 'Gagal');
        }
		redirect('district/KomponenPL');
    }

    public function deleteKomponenPL($id){
    	$notifDelete = $this->KomponenPL_model->deleteKomponenPL($id);
        if($notifDelete){
            $this->session->set_flashdata('message', 'Success');
        }else{
            $this->session->set_flashdata('message', 'Gagal');
        }
        
		redirect('district/KomponenPL');        
    }
}

?>