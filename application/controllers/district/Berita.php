<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Berita extends CI_Controller {

	public function __construct()
        {
                parent::__construct();
        }

    public function index()
    {
        if($this->session->userdata('logged_in') == true){
            $this->listBerita();
        }else{
            $this->load->view('login');
        }
    }

    public function listBerita(){
        $data['list'] = $this->Berita_model->getListBerita();
        $data['content'] = "district/Berita/listBerita";
        $this->load->view('district/main',$data);
    }

    public function tambahBerita(){
        $data['title'] = 'Tambah Berita';
        $data['act'] = 'district/Berita/tambahBeritaAct';
        $data['content'] = "district/Berita/formBerita";
        $this->load->view('district/main',$data);
    }

    public function tambahBeritaAct(){
    	$judul = $this->input->post('judul');
    	$isi = htmlentities($this->input->post('isi'));
    	$iduser = $this->session->userdata('user_id');
         $tanggalKegiatan = $this->input->post('tanggalkegiatan');
 	    $captionGambar = $this->input->post('captionGambar');         
        $notifInput = '';

            $date = date('Y:m:d H:i:s');
            $data['act'] = 'district/Berita/tambahBeritaAct';
            $slug = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $judul)));
            $cekJudul = $this->Berita_model->cekSlugInputBerita($judul);
            if($cekJudul->jumlah > 0){
                $slug .= $cekJudul->jumlah;
            }
            $this->upload->initialize($this->set_upload_options());
            $this->upload->do_upload();

            $file_data = $this->upload->data();
            $data['file_dir'] = $file_data['file_name'];
            $notifInput = $this->Berita_model->inputBerita($judul,$isi,$iduser,$date,$slug,$tanggalKegiatan,$data['file_dir'],$captionGambar[0]);
        if($notifInput){
            $this->session->set_flashdata('message', 'Success');
        }else{
            $this->session->set_flashdata('message', 'Gagal');
        }
    	
		redirect('district/Berita');
    }

    private function set_upload_options()
	{   
	    //upload an image options
	    $filename = md5(uniqid(rand(), true));
	    $config = array();
	    $config['upload_path'] = 'fotoberita';
	    $config['allowed_types'] = 'gif|jpg|png|jpeg';
	    $config['file_name'] = $filename;

	    return $config;
    }
    
    public function editBerita($id){
        $data['list'] = $this->Berita_model->getDataBeritaById($id);
        $data['title'] = 'Ubah Berita';
        $data['act'] = 'district/Berita/ubahBeritaAct';
        $data['content'] = "district/Berita/formBerita";
        $this->load->view('district/main',$data);
    }

    public function ubahBeritaAct(){
        $judul = $this->input->post('judul');
    	$isi = htmlentities($this->input->post('isi'));
    	$iduser = $this->session->userdata('user_id');
 	    $tanggalKegiatan = $this->input->post('tanggalkegiatan');
 	    $idberita = $this->input->post('idberita');
 	    $tempSavedFile = $this->input->post('tempSavedFile');
 	    $captionGambar = $this->input->post('captionGambar');
 	    $tempFile = $this->input->post('tempFile');
        $notifInput = '';

        $date = date('Y:m:d H:i:s');
        $data['act'] = 'district/Berita/tambahBeritaAct';
        $slug = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $judul)));
        $cekJudul = $this->Berita_model->cekSlugUpdate($slug,$idberita);
        if($cekJudul->jumlah > 0){
            $slug .= $cekJudul->jumlah;
        }

        if($tempFile != ''){
            $this->upload->initialize($this->set_upload_options());
            $this->upload->do_upload();

            $file_data = $this->upload->data();
            $data['file_dir'] = $file_data['file_name'];
            unlink("fotoberita/".$tempSavedFile);
        }else{
            $data['file_dir'] = $tempSavedFile;
        }
        $notifInput = $this->Berita_model->updateBerita($judul,$isi,$slug,$tanggalKegiatan,$captionGambar[0],$data['file_dir'],$idberita);
        
        if($notifInput){
            $this->session->set_flashdata('message', 'Success');
        }else{
            $this->session->set_flashdata('message', 'Gagal');
        }
    	
		redirect('district/Berita');
    }

    public function deleteBerita($id){
    	$notifDelete = $this->Berita_model->deleteBerita($id);
        if($notifDelete){
            $this->session->set_flashdata('message', 'Success');
        }else{
            $this->session->set_flashdata('message', 'Gagal');
        }
        
		redirect('district/Berita');        
    }
}

?>