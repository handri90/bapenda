<?php
class VolumeAirTanah_model extends CI_Model {

        public function __construct()
        {

        }

        public function getListVolumeAirTanah(){
                $query = $this->db->query("select * from volumeperuntukanairtanah");
                return $query->result_array();
        }

       public function inputVolume($volumeawal,$volumeakhir){
            $query = $this->db->query("insert into volumeperuntukanairtanah (nilaiVolumePeruntukanAwal,nilaiVolumePeruntukanAkhir) values (".$volumeawal.",".$volumeakhir.")");
                return $query;
        }

        public function getVolumeAirTanah($id){
            $query = $this->db->query("select * from volumeperuntukanairtanah where idVolumePeruntukan=".$id);
    		return $query->row();
        }

        public function updateVolume($volumeawal,$volumeakhir,$idvolume){
            $query = $this->db->query("update volumeperuntukanairtanah set nilaiVolumePeruntukanAwal=".$volumeawal.",nilaiVolumePeruntukanAkhir=".$volumeakhir." where idVolumePeruntukan=".$idvolume);
    		return $query;
        }

        public function deleteVolume($id){
            $query = $this->db->query("delete from volumeperuntukanairtanah where idVolumePeruntukan=".$id);
    		return $query;
        }
}
?>