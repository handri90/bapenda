<?php
class NilaiVolumeAirTanah_model extends CI_Model {

        public function __construct()
        {

        }

        public function getListNilaiVolumeAirTanah(){
                $query = $this->db->query("select * from nilaivolumeairtanah inner join peruntukanairtanah on idPeruntukanAirTanah=peruntukanAirTanahId inner join volumeperuntukanairtanah on idVolumePeruntukan=volumeAirTanahId");
                return $query->result_array();
        }

       public function inputNilaiVolume($peruntukan,$volume,$nilaiperolehan){
            $query = $this->db->query("insert into nilaivolumeairtanah (peruntukanAirTanahId,volumeAirTanahId,hargaNilaiVolumeAirTanah) values (".$peruntukan.",".$volume.",".$nilaiperolehan.")");
                return $query;
        }

        public function getNilaiVolumeAirTanah($id){
            $query = $this->db->query("select * from nilaivolumeairtanah where idNilaiVolumeAirTanah=".$id);
    		return $query->row();
        }

        public function updateNilaiVolume($peruntukan,$volume,$nilaiperolehan,$idnilaivolume){
            $query = $this->db->query("update nilaivolumeairtanah set peruntukanAirTanahId=".$peruntukan.",volumeAirTanahId=".$volume.",hargaNilaiVolumeAirTanah=".$nilaiperolehan." where idNilaiVolumeAirTanah=".$idnilaivolume);
    		return $query;
        }

        public function deleteNilaiVolume($id){
            $query = $this->db->query("delete from nilaivolumeairtanah where idNilaiVolumeAirTanah=".$id);
    		return $query;
        }

        public function getPeruntukan(){
            $query = $this->db->query("select * from peruntukanairtanah");
                return $query->result_array();
        }

        public function getVolume(){
            $query = $this->db->query("select * from volumeperuntukanairtanah");
                return $query->result_array();
        }

        public function getSameValue($peruntukan,$volume){
            $query = $this->db->query("select * from nilaivolumeairtanah where peruntukanAirTanahId=".$peruntukan." and volumeAirTanahId=".$volume);
                return $query->result_array();
        }
}
?>