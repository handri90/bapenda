<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Banner extends CI_Controller {

	public function __construct()
        {
                parent::__construct();
        }

    public function index()
    {
        if($this->session->userdata('logged_in') == true){
            $this->listBanner();
        }else{
            $this->load->view('login');
        }
    }

    public function listBanner(){
        $data['list'] = $this->Banner_model->getListBanner();
        $data['content'] = "district/Banner/listBanner";
        $this->load->view('district/main',$data);
    }

    public function tambahBanner(){
        $data['title'] = 'Tambah Banner';
        $data['act'] = 'district/Banner/tambahBannerAct';
        $data['content'] = "district/Banner/formBanner";
        $this->load->view('district/main',$data);
    }

    public function tambahBannerAct(){
        $data['act'] = 'district/Banner/tambahBannerAct';
        $ordermenu = $this->input->post("ordermenu");
        
        $this->upload->initialize($this->set_upload_options());
        $this->upload->do_upload();

        $file_data = $this->upload->data();
        $data['file_dir'] = $file_data['file_name'];
        $notifInput = $this->Banner_model->inputBanner($data['file_dir'],$ordermenu);

        if($notifInput){
            $this->session->set_flashdata('message', 'Success');
        }else{
            $this->session->set_flashdata('message', 'Gagal');
        }
    	
		redirect('district/Banner');
    }

    private function set_upload_options()
	{   
	    //upload an image options
	    $filename = md5(uniqid(rand(), true));
	    $config = array();
	    $config['upload_path'] = 'bannerhome';
	    $config['allowed_types'] = 'jpg|png|jpeg';
	    $config['file_name'] = $filename;

	    return $config;
    }
    
    public function editBanner($id){
        $data['list'] = $this->Banner_model->getBannerById($id);
        $data['title'] = 'Ubah Banner';
        $data['act'] = 'district/Banner/ubahBannerAct';
        $data['content'] = "district/Banner/formBanner";
        $this->load->view('district/main',$data);
    }

    public function ubahBannerAct(){
        $idbanner = $this->input->post('idbanner');
 	    $tempFile = $this->input->post('tempFile');
         $tempSavedFile = $this->input->post('tempSavedFile');
        $ordermenu = $this->input->post("ordermenu");

        if($tempFile != ''){
            $this->upload->initialize($this->set_upload_options());
            $this->upload->do_upload();

            $file_data = $this->upload->data();
            $data['file_dir'] = $file_data['file_name'];
            unlink("bannerhome/".$tempSavedFile);
        }else{
            $data['file_dir'] = $tempSavedFile;
        }
        $notifInput = $this->Banner_model->updateBanner($data['file_dir'],$ordermenu,$idbanner);
        
        if($notifInput){
            $this->session->set_flashdata('message', 'Success');
        }else{
            $this->session->set_flashdata('message', 'Gagal');
        }
    	
		redirect('district/Banner');
    }

    public function deleteBanner($id){
    	$notifDelete = $this->Banner_model->deleteBanner($id);
        if($notifDelete){
            $this->session->set_flashdata('message', 'Success');
        }else{
            $this->session->set_flashdata('message', 'Gagal');
        }
        
		redirect('district/Banner');        
    }

    public function changeStatusBanner($id){
        $notifChange = $this->Banner_model->changeStatusBanner($id);
        if($notifChange){
            $this->session->set_flashdata('message', 'Success');
        }else{
            $this->session->set_flashdata('message', 'Gagal');
        }
        
		redirect('district/Banner');
    }
}

?>