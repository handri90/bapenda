<section id="portfolio" class="portfolio-wide">
      <div class="container text-center">
        <div class="row"> 
          <div class="col-sm-10 col-sm-offset-1">
            <h3>Gallery</h3>
          </div>
        </div>
      </div>
      <div class="container-fluid">
        <div id="grid" class="row portfolio-items">
        <?php
                for($i=0;$i<count($album);$i++){
                    $filename = explode('|',$album[$i]['filename']);
                    ?>
          <div class="col-sm-3">
            <div class="portfolio-item"><a href="<?php echo base_url('album/').$album[$i]['slug']; ?>"><img src="<?php echo base_url('filegallery/').$filename[0]; ?>" alt="">
                <div class="portfolio-overlay">
                  <div class="caption">
                    <h5><?php echo $album[$i]['namaAlbum']; ?></span>
                  </div>
                </div></a>
            </div>
          </div>
          <?php
          }
          ?>
        </div>
      </div>
    </section>