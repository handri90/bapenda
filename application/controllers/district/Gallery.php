<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gallery extends CI_Controller {

	public function __construct()
        {
                parent::__construct();
        }

    public function index()
    {
        if($this->session->userdata('logged_in') == true){
            $this->listGallery();
        }else{
            $this->load->view('login');
        }
    }

    public function listGallery(){
        $data['list'] = $this->Gallery_model->getListGallery();
        $data['content'] = "district/Gallery/listGallery";
        $this->load->view('district/main',$data);
    }

    public function tambahGallery(){
        $data['title'] = 'Tambah Gallery';
        $data['act'] = 'district/Gallery/tambahGalleryAct';
        $data['content'] = "district/Gallery/formGallery";
        $this->load->view('district/main',$data);
    }

    public function tambahGalleryAct(){
        $judul = $this->input->post('namaalbum');
        $iduser = $this->session->userdata('user_id');       
        $date = date('Y:m:d H:i:s');
        $data['act'] = 'district/Gallery/tambahGalleryAct';
        $cekJudul = $this->Gallery_model->cekSlugAlbumFoto($judul);
        $slug = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $judul)));
        if($cekJudul->jumlah > 0){
            $slug .= $cekJudul->jumlah;
        }

        $notifInput = $this->Gallery_model->inputAlbumfoto($judul,$slug,$date,$iduser);
        
        $files = $_FILES;
        $cpt = count($_FILES['userfile']['name']);
        for($i=0; $i<$cpt; $i++)
        { 
            $_FILES['userfile']['name']= $files['userfile']['name'][$i];
            $_FILES['userfile']['type']= $files['userfile']['type'][$i];
            $_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
            $_FILES['userfile']['error']= $files['userfile']['error'][$i];
            $_FILES['userfile']['size']= $files['userfile']['size'][$i];
            $this->upload->initialize($this->set_upload_options());
            $this->upload->do_upload();

            $file_data = $this->upload->data();
            $data['file_dir'] = $file_data['file_name'];
            $data['date_uploaded'] = date('Y-m-d H:i:s');
            $captionGambar = $this->input->post('captionGambar');
            $this->Gallery_model->inputImage($notifInput,$data['file_dir'],$captionGambar[$i]);
        }
        if($notifInput){
            $this->session->set_flashdata('message', 'Success');
        }else{
            $this->session->set_flashdata('message', 'Gagal');
        }
    	
		redirect('district/Gallery');
    }

    private function set_upload_options()
	{   
	    //upload an image options
	    $filename = md5(uniqid(rand(), true));
	    $config = array();
	    $config['upload_path'] = 'filegallery';
	    $config['allowed_types'] = 'gif|jpg|png|jpeg';
	    $config['file_name'] = $filename;

	    return $config;
    }
    
    public function editGallery($id){
        $data['list'] = $this->Gallery_model->getDataGalleryById($id);
        $data['title'] = 'Ubah Gallery';
        $data['act'] = 'district/Gallery/ubahGalleryAct';
        $data['content'] = "district/Gallery/formGallery";
        $this->load->view('district/main',$data);
    }

    public function ubahGalleryAct(){
        $judul = $this->input->post('namaalbum');
        $idalbum = $this->input->post('idalbum');
        $captionGambarSaved = $this->input->post('captionGambarSaved');
        $tempSavedFile = explode("|",$this->input->post('tempSavedFile'));
    	$iduser = $this->session->userdata('user_id');           
        $date = date('Y:m:d H:i:s');
        $data['act'] = 'district/Gallery/tambahBeritaAct';
        $slug = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $judul)));
        $cekJudul = $this->Gallery_model->cekSlugAlbumFotoUpdate($slug,$idalbum);
        if($cekJudul->jumlah > 0){
            $slug .= $cekJudul->jumlah;
        }

        $notifInput = $this->Gallery_model->updateAlbumfoto($judul,$slug,$idalbum);
        for($i=0;$i<count($tempSavedFile);$i++){
            $updateCaption = $this->Gallery_model->updateCaptionFotoSaved($tempSavedFile[$i],$captionGambarSaved[$i]);
        }
        
        $files = $_FILES;
        $cpt = count($_FILES['userfile']['name']);
        if(!empty($_FILES['userfile']['name'][0])){
            for($i=0; $i<$cpt; $i++)
            { 
                $_FILES['userfile']['name']= $files['userfile']['name'][$i];
                $_FILES['userfile']['type']= $files['userfile']['type'][$i];
                $_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
                $_FILES['userfile']['error']= $files['userfile']['error'][$i];
                $_FILES['userfile']['size']= $files['userfile']['size'][$i];
                $this->upload->initialize($this->set_upload_options());
                $this->upload->do_upload();

                $file_data = $this->upload->data();
                $data['file_dir'] = $file_data['file_name'];
                $data['date_uploaded'] = date('Y-m-d H:i:s');
                $captionGambar = $this->input->post('captionGambar');
                $this->Gallery_model->inputImage($idalbum,$data['file_dir'],$captionGambar[$i]);
            }
        }
        if($notifInput){
            $this->session->set_flashdata('message', 'Success');
        }else{
            $this->session->set_flashdata('message', 'Gagal');
        }
    	
		redirect('district/Gallery');
    }

    public function deleteImage(){
        $idfoto = $this->input->post('fotoid');
        $notifDelete = $this->Gallery_model->deleteImage($idfoto);
        if($notifDelete){
            echo 'sukses';
        }else{
            echo 'gagal';
        }
    }

    public function deleteGallery($id){
    	$notifDelete = $this->Gallery_model->deleteGallery($id);
        if($notifDelete){
            $this->session->set_flashdata('message', 'Success');
        }else{
            $this->session->set_flashdata('message', 'Gagal');
        }
        
		redirect('district/Gallery');        
    }
}

?>