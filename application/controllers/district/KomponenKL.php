<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class KomponenKL extends CI_Controller {

	public function __construct()
        {
                parent::__construct();
        }

    public function index()
    {
        if($this->session->userdata('logged_in') == true){
            $this->listKomponenKL();
        }else{
            $this->load->view('login');
        }
    }

    public function listKomponenKL(){
        $data['list'] = $this->KomponenKL_model->getListKomponenKL();
        $data['content'] = "district/KomponenKL/listKomponenKL";
        $this->load->view('district/main',$data);
    }

    public function tambahKomponenKL(){
        $data['title'] = 'Tambah Komponen Lebar Jalan';
        $data['act'] = 'district/KomponenKL/tambahKomponenKLAct';
        $data['content'] = "district/KomponenKL/formKomponenKL";
        $this->load->view('district/main',$data);
    }

    public function tambahKomponenKLAct(){
    	$nilaiawal = $this->input->post('nilaiawal');
    	$nilaiakhir = $this->input->post('nilaiakhir');
    	$bobot = $this->input->post('bobot');
        $notifInput = $this->KomponenKL_model->inputKomponenKL($nilaiawal,$nilaiakhir,$bobot);
        
        if($notifInput){
            $this->session->set_flashdata('message', 'Success');
        }else{
            $this->session->set_flashdata('message', 'Gagal');
        }
		redirect('district/KomponenKL');
    }
    
    public function editKomponenKL($id){
        $data['list'] = $this->KomponenKL_model->getKomponenKL($id);
        $data['title'] = 'Ubah Komponen Lebar Jalan';
        $data['act'] = 'district/KomponenKL/ubahKomponenKLAct';
        $data['content'] = "district/KomponenKL/formKomponenKL";
        $this->load->view('district/main',$data);
    }

    public function ubahKomponenKLAct(){
        $nilaiawal = $this->input->post('nilaiawal');
    	$nilaiakhir = $this->input->post('nilaiakhir');
    	$bobot = $this->input->post('bobot');
        $idkomponen = $this->input->post('idkomponen');
        $notifInput = $this->KomponenKL_model->updateKomponenKL($nilaiawal,$nilaiakhir,$bobot,$idkomponen);
        
        if($notifInput){
            $this->session->set_flashdata('message', 'Success');
        }else{
            $this->session->set_flashdata('message', 'Gagal');
        }
		redirect('district/KomponenKL');
    }

    public function deleteKomponenKL($id){
    	$notifDelete = $this->KomponenKL_model->deleteKomponenKL($id);
        if($notifDelete){
            $this->session->set_flashdata('message', 'Success');
        }else{
            $this->session->set_flashdata('message', 'Gagal');
        }
        
		redirect('district/KomponenKL');        
    }
}

?>