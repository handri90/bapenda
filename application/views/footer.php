<!-- Footer Section-->
<section class="section-small footer bg-gray">
      <div class="container">
        <div class="row">
          <div class="col-sm-6">
            <p><img src="<?php echo base_url().'assets/img/logofooter.png'; ?>" ></p>
            <p><?php echo $datadinas->namaDinas; ?></p>
            <h5><i class="fa fa-map-marker fa-fw fa-lg"></i> <?php echo $datadinas->alamatDinas; ?>
            </h5>
            <h5><i class="fa fa-envelope fa-fw fa-lg"></i> <?php echo $datadinas->emailDinas; ?>
            </h5>
            <h5><i class="fa fa-phone fa-fw fa-lg"></i> <?php echo $datadinas->telpDinas; ?>
            </h5>
          </div>
          <div class="col-sm-4">
            <h5>Link Cepat</h5>
            <?php echo $tautancepat; ?>
          </div>
          <div class="col-sm-2">
             <!-- Histats.com  (div with counter) --><div id="histats_counter"></div>
<!-- Histats.com  START  (aync)-->
<script type="text/javascript">var _Hasync= _Hasync|| [];
_Hasync.push(['Histats.start', '1,4297965,4,403,118,80,00011111']);
_Hasync.push(['Histats.fasi', '1']);
_Hasync.push(['Histats.track_hits', '']);
(function() {
var hs = document.createElement('script'); hs.type = 'text/javascript'; hs.async = true;
hs.src = ('//s10.histats.com/js15_as.js');
(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(hs);
})();</script>
<noscript><a href="/" target="_blank"><img  src="//sstatic1.histats.com/0.gif?4297965&101" alt="website statistics" border="0"></a></noscript>
<!-- Histats.com  END  -->
          </div>
        </div>
      </div>
    </section>
    <section class="section-smalls footer">
      <div class="container">
        <div class="row">
          <div class="col-sm-12">
            <h6>2019 Bapenda Kotawaringin Barat</a>
            </h6>
          </div>
        </div>
      </div>
    </section>
    <!-- jQuery-->
    <script src="<?php echo base_url(); ?>assets/js/jquery-1.12.4.min.js"></script>
    <script type='text/javascript' src='<?php echo base_url(); ?>assets/js/unitegallery.min.js'></script>
    <script type='text/javascript' src='<?php echo base_url(); ?>assets/js/ug-theme-tilesgrid.js'></script>
    <!-- Bootstrap Core JavaScript-->
    <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
    <!-- Plugin JavaScript-->
    <script src="<?php echo base_url(); ?>assets/js/jquery.easing.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.countdown.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/device.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/form.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.placeholder.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.shuffle.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.parallax.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.circle-progress.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.swipebox.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/smoothscroll.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/wow.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.smartmenus.js"></script>
        <!-- Google Maps API Key - Use your own API key to enable the map feature. More information on the Google Maps API can be found at https://developers.google.com/maps/-->
<!--         <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB3E86i8mx1BZDlAaLcknh_mWl4F70i4os"></script> -->
        <script src="<?php echo base_url(); ?>assets/js/map.js"></script>
    <!-- Custom Theme JavaScript-->
    <script src="<?php echo base_url(); ?>assets/js/universal.js"></script>
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script>
        $(document).ready(function () {

$('.navbar .dropdown-item.dropdown').on('click', function (e) {
    var $el = $(this).children('.dropdown-toggle');
    if ($el.length > 0 && $(e.target).hasClass('dropdown-toggle')) {
        var $parent = $el.offsetParent(".dropdown-menu");
        $(this).parent("li").toggleClass('open');

        if (!$parent.parent().hasClass('navbar-nav')) {
            if ($parent.hasClass('show')) {
                $parent.removeClass('show');
                $el.next().removeClass('show');
                $el.next().css({"top": -999, "left": -999});
            } else {
                $parent.parent().find('.show').removeClass('show');
                $parent.addClass('show');
                $el.next().addClass('show');
                $el.next().css({"top": $el[0].offsetTop, "left": $parent.outerWidth() - 4});
            }
            e.preventDefault();
            e.stopPropagation();
        }
        return;
    }
});

$('.navbar .dropdown').on('hidden.bs.dropdown', function () {
    $(this).find('li.dropdown').removeClass('show open');
    $(this).find('ul.dropdown-menu').removeClass('show open');
});

$(".calcpajakreklame").click(function(){
    let luasreklame=$(".luasreklame").val();
    let jenisreklame=$(".jenisreklame").val();
    let tempatpemasangan=$(".tempatpemasangan").val();
    let lamapemasangan=$(".lamapemasangan").val();
	let jmlsisi=$(".jmlsisi").val();

    if(luasreklame === ''){
        $(".notifluas").html("Luas Reklame Harus Diisi!");
        return false;
    }else{
        $(".notifluas").html("");
    }

	if(jmlsisi === ''){
        $(".notifjumlah").html("Jumlah Sisi Harus Diisi!");
        return false;
    }else{
        $(".notifjumlah").html("");
    }

    if(lamapemasangan === ''){
        $(".notifwaktu").html("Lama Pemasangan Harus Diisi!");
        return false;
    }else{
        $(".notifwaktu").html("");
    }


    $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>/Frontend/pajakReklameAct", 
            data:{luasreklame:luasreklame,jenisreklame:jenisreklame,tempatpemasangan:tempatpemasangan,lamapemasangan:lamapemasangan,jmlsisi:jmlsisi},
            success: 
            function(data){
            $(".result").html(data);
            }
    });
});
        
        $('.calcpajakair').click(function(){
        	let peruntukan=$('.peruntukan').val();
        	let jmlvolume=$('.jmlvolume').val();
        	let persentase=$('.persentase').val();
        	persentase = persentase.replace("%","");
        
        	$.ajax({
            	type: "POST",
            	url: "<?php echo base_url(); ?>/Frontend/pajakAirTanahAct", 
            	data:{peruntukan:peruntukan,jmlvolume:jmlvolume,persentase:persentase},
            	dataType : 'json',
                async : true,
            	success: 
            	function(data){
                	$(".hargam3").val('Rp '+formatNumber(data.hargapermeter));
            		$(".result").html('Rp '+formatNumber(data.hasil));
            	}
    		});
        	
        });
        
        $('.calcpajak').click(function(){
        	let jml=$('.jumlah').val();
        	jml = jml.replace(/,/g,"");
            let hrgsatuan=$('.hrgsatuan').val();
        	hrgsatuan = hrgsatuan.replace(/,/g,"");
            let persentase=$('.persentase').val();
        	persentase = persentase.replace("%","");
        	let jmlknapajak = jml*hrgsatuan;
            let result = (persentase/100)*(jml*hrgsatuan);
        	$('.jmlkenapajak').val('Rp '+formatNumber(jmlknapajak));
        	$('.result').html('Rp '+formatNumber(result));
        });
        
        $('.calcpajak2').click(function(){
            let hrgsatuan=$('.hrgsatuan').val();
        	hrgsatuan = hrgsatuan.replace(/,/g,"");
            let persentase=$('.persentase').val();
        	persentase = persentase.replace("%","");
            let result = (persentase/100)*hrgsatuan;
        	$('.result').html('Rp '+formatNumber(result));
        });
        

jQuery("#gallery").unitegallery();  

$('body').on('keypress','.onlynumber',function(e){
        //if the letter is not digit then display error and don't type anything
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57) && e.which != 46) {
            //display error message
            return false;
                  
        }
      });
        
        $('body').on('keypress','.onlynumber3',function(e){
        //if the letter is not digit then display error and don't type anything
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            //display error message
            return false;
                  
        }
      });
        
        $('body').on('keyup','.formatNumber',function(){
        let vValue = $(this).val();
        x = vValue.replace(/,/g, ""); // Strip out all commas
        x = x.split("").reverse().join("");
        x = x.replace(/.../g, function (e) {
            return e + ",";
        }); // Insert new commas
        x = x.split("").reverse().join("");
        x = x.replace(/^,/, ""); // Remove leading comma
        $(this).val(x);
      });
        
        $('body').on('change','.keterangan',function(){
        let element = $(this).find('option:selected'); 
        let vValue = element.attr("nilaipersentase"); 
        $(".persentase").val(vValue+"%");
      });
        
        $('body').on('change','.kategori',function(){
        let element = $(this).find('option:selected'); 
        let vValue = element.attr("nilaitarifdasar"); 
        $(".hrgsatuan").val(formatNumber(vValue));
      });
        
        function formatNumber(e){
        let	number_string = e.toString(),
          sisa 	= number_string.length % 3,
          rupiah 	= number_string.substr(0, sisa),
          ribuan 	= number_string.substr(sisa).match(/\d{3}/g);
            
        if (ribuan) {
          separator = sisa ? ',' : '';
          rupiah += separator + ribuan.join(',');
        }

        return rupiah;
      }

});
    </script>
  </body>
</html>