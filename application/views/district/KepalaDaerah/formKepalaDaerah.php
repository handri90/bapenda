<!-- page content -->
<div class="right_col" role="main">
          <div class="">

            <div class="row">

              <div class="col-md-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo $title; ?></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                    <form enctype="multipart/form-data" class="form-horizontal form-label-left" method="post" action="<?php echo base_url().$act; ?>">

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Nama</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <input type="hidden" name="idkd" class="form-control" value="<?php echo !empty($list->idKepalaDaerah)?$list->idKepalaDaerah:""; ?>">
                          <input required type="text" name="nama" class="form-control" placeholder="Nama" value="<?php echo !empty($list->namaKepalaDaerah)?$list->namaKepalaDaerah:""; ?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Jabatan</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <input required type="text" name="jabatan" class="form-control" placeholder="Jabatan" value="<?php echo !empty($list->jabatanKepalaDaerah)?$list->jabatanKepalaDaerah:""; ?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Order</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                        <input required type="text" name="ordermenu" class="form-control" placeholder="Order Menu" value="<?php echo !empty($list->orderKepalaDaerah)?$list->orderKepalaDaerah:""; ?>">                          
                        </div>
                      </div>
                      <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Foto</label>
                            <label class="btn btn-primary btn-upload btnUpgrade" for="inputImageKepalaDaerah" title="Upload image file">
                              <?php
                              $req = "";
                              if(empty($list->idKepalaDaerah)){
                                $req = "required";
                              }
                              ?>
                              <input <?php echo $req; ?> type="file" class="sr-only" id="inputImageKepalaDaerah" name="userfile" accept="image/*">
                              <span class="docs-tooltip" data-toggle="tooltip" title="Import image with Blob URLs">
                                <span class="fa fa-upload"></span>
                              </span>
                            </label>
                            <input name="tempFile" id="tempFile" type="hidden" value="">
                            <input name="tempSavedFile" id="tempSavedFile" value="<?php echo isset($list->filenameKepalaDaerah)?$list->filenameKepalaDaerah:''; ?>" type="hidden">
                          </div>
                          <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">&nbsp;</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <output id="list">
                          </output>
                          <?php
                          if(!empty($list->idKepalaDaerah)){
                            ?>
                            <output id="savedImage">
                            <span class='imgSaved'><div style="margin-top:10px;background-color:#caccd1;padding:10px;"><img style="height: 50%;width:50%; border: 1px solid #000; margin: 5px" src="<?php echo base_url()."kepaladaerah/".$list->filenameKepalaDaerah; ?>" /></div></span>
                            </output>
                            <?php
                          }
                          ?>
                        </div>
                      </div>
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                          <button type="button" class="btn btn-primary" id="backButton">Cancel</button>
                          <button type="reset" class="btn btn-primary">Reset</button>
                          <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                      </div>

                    </form>
                  </div>
                </div>
              </div>
            </div>


            
          </div>
        </div>
        <!-- /page content -->