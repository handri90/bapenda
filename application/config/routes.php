<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'Frontend';
$route['page/(:any)'] = 'Frontend/showSingleMenu/$1';
$route['berita'] = 'Frontend/showBerita';
$route['berita/(:any)'] = 'Frontend/showDetailBerita/$1';
$route['pengumuman'] = 'Frontend/showPengumuman';
$route['pengumuman/(:any)'] = 'Frontend/showDetailPengumuman/$1';
$route['download/(:any)'] = 'Frontend/showDownload/$1';
$route['calcpajakreklame'] = 'Frontend/hitungPajakReklame';
$route['calcpajakrestoran'] = 'Frontend/hitungPajakRestoran';
$route['calcpajakhotel'] = 'Frontend/hitungPajakHotel';
$route['calcpajakhiburan'] = 'Frontend/hitungPajakHiburan';
$route['calcpajakparkir'] = 'Frontend/hitungPajakParkir';
$route['calcpajaksarangburungwalet'] = 'Frontend/hitungPajakSarangBurungWalet';
$route['calcpajakairbawahtanah'] = 'Frontend/hitungPajakAirBawahTanah';
$route['calcpajakmblb'] = 'Frontend/hitungPajakMBLB';
$route['gallery'] = 'Frontend/showAlbum';
$route['album/(:any)'] = 'Frontend/detailAlbum/$1';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
