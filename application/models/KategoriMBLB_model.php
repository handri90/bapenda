<?php
class KategoriMBLB_model extends CI_Model {

        public function __construct()
        {

        }

		public function getListKategoriMBLB(){
        	$query = $this->db->query("select * from kategorimblb");
    		  return $query->result_array();
        }

        public function getKategoriMBLB($id){
          $query = $this->db->query("select * from kategorimblb where idKategoriMBLB=".$id);
    		  return $query->row();
        }

		public function inputKategoriMBLB($jenis,$tarifdasar){
        	$query = $this->db->query("insert into kategorimblb (jenisMBLB,tarifDasarMBLB) values ('".$jenis."',".$tarifdasar.")");
          return $query;
        }

        public function updateKategoriMBLB($jenis,$tarifdasar,$idkategori){
          $query = $this->db->query("update kategorimblb set jenisMBLB='".$jenis."', tarifDasarMBLB=".$tarifdasar." where idKategoriMBLB=".$idkategori);
          return $query;
        }

		public function deleteKategoriMBLB($id){
        	$query = $this->db->query("delete from kategorimblb where idKategoriMBLB=".$id);
          return $query;
        }
}
?>