<?php
class UploadFilePublikasi_model extends CI_Model {

        public function __construct()
        {

        }

        public function getListUploadFilePublikasi(){
                $query = $this->db->query("select * from detailfilepublikasi inner join kategorifilepublikasi on idKategoriFilePublikasi=kategoriFilePublikasiId order by idDetailFilePublikasi");
                return $query->result_array();
        }

        public function getKategoriDownload(){
            $query = $this->db->query("select * from kategorifilepublikasi order by idKategoriFilePublikasi");
                return $query->result_array();
        }

       public function inputFilePublikasi($judul,$kategori,$file){
            $query = $this->db->query("insert into detailfilepublikasi (judulDetailFilePublikasi,file,kategoriFilePublikasiId) values ('".$judul."','".$file."',".$kategori.")");
                return $query;
        }

        public function getFilePublikasiById($id){
            $query = $this->db->query("select * from detailfilepublikasi where idDetailFilePublikasi=".$id);
    		return $query->row();
        }

        public function updateFilePublikasi($judul,$kategori,$file,$iddetailpublikasi){
            $query = $this->db->query("update detailfilepublikasi set judulDetailFilePublikasi='".$judul."',file='".$file."',kategoriFilePublikasiId=".$kategori." where idDetailFilePublikasi=".$iddetailpublikasi);
    		return $query;
        }

        public function deleteFilePublikasi($id){
            $query = $this->db->query("select file from detailfilepublikasi where idDetailFilePublikasi = ".$id);
            $result = $query->row();
            unlink("publikasi/".$result->file);
            $query = $this->db->query("delete from detailfilepublikasi where idDetailFilePublikasi=".$id);
    		return $query;
        }
}
?>