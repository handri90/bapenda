<?php
class PersentaseHiburan_model extends CI_Model {

        public function __construct()
        {

        }

		public function getListPersentaseHiburan(){
        	$query = $this->db->query("select * from pajakhiburan");
    		  return $query->result_array();
        }

        public function getPersentaseHiburan($id){
          $query = $this->db->query("select idPajakHiburan as idpersentase,persentase,labelPersentaseHiburan from pajakhiburan where idPajakHiburan=".$id);
    		  return $query->row();
        }

		public function inputPersentaseHiburan($keterangan,$persentase){
        	$query = $this->db->query("insert into pajakhiburan (persentase,labelPersentaseHiburan) values (".$persentase.",'".$keterangan."')");
          return $query;
        }

        public function updatePersentaseHiburan($label,$persentase,$id){
          $query = $this->db->query("update pajakhiburan set labelPersentaseHiburan='".$label."', persentase=".$persentase." where idPajakHiburan=".$id);
          return $query;
        }

		public function deletePersentaseHiburan($id){
        	$query = $this->db->query("delete from pajakhiburan where idPajakHiburan=".$id);
          return $query;
        }
}
?>