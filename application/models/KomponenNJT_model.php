<?php
class KomponenNJT_model extends CI_Model {

        public function __construct()
        {

        }

        public function getListKomponenNJT(){
                $query = $this->db->query("select * from komponennjt");
                return $query->result_array();
        }
        
        public function inputKomponenNJT($namakomponen,$kategorisr,$kategoripl,$kategorikl,$bobot){
            $query = $this->db->query("insert into komponennjt (labelKomponenNJT,nilaiKomponenNJT,komponenSRid,komponenPLid,komponenKLid) values ('".$namakomponen."',".$bobot.",".$kategorisr.",".$kategoripl.",".$kategorikl.")");
    		return $query;
        }

        public function getKomponenNJT($id){
          $query = $this->db->query("select * from komponennjt where idKomponenNJT = ".$id);
    		  return $query->row();
        }

        public function updateKomponenNJT($namakomponen,$kategorisr,$kategoripl,$kategorikl,$bobot,$idkomponen){
          $query = $this->db->query("update komponennjt set labelKomponenNJT='".$namakomponen."',nilaiKomponenNJT=".$bobot.",komponenSRid=".$kategorisr.",komponenPLid=".$kategoripl.",komponenKLid=".$kategorikl." where idKomponenNJT=".$idkomponen);
          return $query;
        }

        public function deleteKomponenNJT($id){
          $query = $this->db->query("delete from komponennjt where idKomponenNJT = ".$id);
          return $query;
        }

        public function getAllKomponenSR(){
          $query = $this->db->query("select * from komponensr");
                return $query->result_array();
        }

        public function getAllKomponenPL(){
          $query = $this->db->query("select * from komponenpl");
                return $query->result_array();
        }

        public function getAllKomponenKL(){
          $query = $this->db->query("select * from komponenkl");
                return $query->result_array();
        }
}
?>