<!-- page content -->
<div class="right_col" role="main">
          <div class="">

            <div class="row">

              <div class="col-md-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo $title; ?></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                    <form enctype="multipart/form-data" class="form-horizontal form-label-left" method="post" action="<?php echo base_url().$act; ?>">

                    
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Tempat Pemasangan</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <input type="hidden" name="idkomponen" class="form-control" value="<?php echo !empty($list->idKomponenNJT)?$list->idKomponenNJT:""; ?>">
                          <input required type="text" name="namakomponen" class="form-control" placeholder="Tempat Pemasangan" value="<?php echo !empty($list->labelKomponenNJT)?stripslashes($list->labelKomponenNJT):""; ?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Nominal</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <input required type="text" name="bobot" class="form-control formatNumber onlynumber" placeholder="Nominal" value="<?php echo !empty($list->nilaiKomponenNJT)?nominal($list->nilaiKomponenNJT):""; ?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Kategori Sasaran Reklame 
                        </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <select name="kategorisr" class="form-control">
                            <?php
                            for($i=0;$i<count($komponensr);$i++){
                            	$selected ="";
                            	if($list->komponenSRid == $komponensr[$i]['idKomponenSR']){
                                	$selected = "selected";
                            	}else{
                                	$selected ="";
                                }
                                ?>
                                <option <?php echo $selected; ?> value="<?php echo $komponensr[$i]['idKomponenSR']; ?>"><?php echo $komponensr[$i]['labelKomponenSR']; ?></option>
                                <?php
                            }
                            ?>
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Peluang Melihat 
                        </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <select name="kategoripl" class="form-control">
                            <?php
                            for($i=0;$i<count($komponenpl);$i++){
                            	$selected ="";
                            	if($list->komponenPLid == $komponenpl[$i]['idKomponenPL']){
                                	$selected = "selected";
                            	}else{
                                	$selected ="";
                                }
                                ?>
                                <option <?php echo $selected; ?> value="<?php echo $komponenpl[$i]['idKomponenPL']; ?>"><?php echo $komponenpl[$i]['labelKomponenPL']; ?></option>
                                <?php
                            }
                            ?>
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Kelas / Lebar Jalan 
                        </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <select name="kategorikl" class="form-control">
                            <?php
                            for($i=0;$i<count($komponenkl);$i++){
                            	$selected ="";
                            	if($list->komponenPLid == $komponenpl[$i]['idKomponenPL']){
                                	$selected = "selected";
                            	}else{
                                	$selected ="";
                                }
                                ?>
                                <option <?php echo $selected; ?> value="<?php echo $komponenkl[$i]['idKomponenKL']; ?>"><?php echo $komponenkl[$i]['nilaiAwalKomponenKL']." - ".$komponenkl[$i]['nilaiAkhirKomponenKL']." M2 (Bobot ".$komponenkl[$i]['bobotKomponenKL'].")"; ?></option>
                                <?php
                            }
                            ?>
                          </select>
                        </div>
                      </div>
                      
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                          <button type="button" class="btn btn-primary" id="backButton">Cancel</button>
                          <button type="reset" class="btn btn-primary">Reset</button>
                          <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                      </div>

                    </form>
                  </div>
                </div>
              </div>
            </div>


            
          </div>
        </div>
        <!-- /page content -->