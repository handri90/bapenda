<?php
class Pengumuman_model extends CI_Model {

        public function __construct()
        {

        }

        public function getListPengumuman(){
                $query = $this->db->query("select * from pengumuman order by idPengumuman desc");
                return $query->result_array();
        }

        public function cekSlugPengumuman($judul){
            $query = $this->db->query("select count(*) as jumlah from pengumuman where judulPengumuman = '".$judul."'");
    		return $query->row();
        }

        public function inputPengumuman($judul,$isi,$iduser,$slug,$filename){
            $query = $this->db->query("insert into pengumuman (judulPengumuman,isiPengumuman,file,slug,tanggalInputPengumuman,userId) values ('".$judul."','".$isi."','".$filename."','".$slug."',now(),".$iduser.")");
                return $query;
        }

        public function getDataPengumumanById($id){
            $query = $this->db->query("select idPengumuman,judulPengumuman,isiPengumuman,file from pengumuman where idPengumuman=".$id);
    		return $query->row();
        }

        public function cekSlugPengumumanUbah($slug,$idPengumuman){
            $query = $this->db->query("select count(*) as jumlah from pengumuman where slug = '".$slug."' and idPengumuman!=".$idPengumuman);
    		return $query->row();
        }

        public function ubahPengumuman($judul,$isi,$slug,$filename,$idPengumuman){
            $query = $this->db->query("update pengumuman set judulPengumuman='".$judul."',isiPengumuman='".$isi."',file='".$filename."',slug='".$slug."' where idPengumuman=".$idPengumuman);
    		return $query;
        }

        public function deletePengumuman($id){
            $query = $this->db->query("select file from pengumuman where idPengumuman = ".$id);
            $result = $query->row();
            unlink("filepengumuman/".$result->file);
            $query = $this->db->query("delete from pengumuman where idPengumuman=".$id);
    		return $query;
        }
}
?>