<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PersentaseRestoran extends CI_Controller {

	public function __construct()
        {
                parent::__construct();
        }

    public function index()
    {
        if($this->session->userdata('logged_in') == true){
            $this->editPersentaseRestoran();
        }else{
            $this->load->view('login');
        }
    }
    
    public function editPersentaseRestoran(){
        $data['list'] = $this->PersentaseRestoran_model->getPersentaseRestoran();
        $data['title'] = 'Ubah Persentase Pajak Restoran';
        $data['labelpersentase'] = 'Pajak Restoran';
        $data['act'] = 'district/PersentaseRestoran/ubahPersentaseRestoranAct';
        $data['content'] = "district/PersentasePajak/formPersentase";
        $this->load->view('district/main',$data);
    }

    public function ubahPersentaseRestoranAct(){
        $persentase = $this->input->post('persentase');
        $notifInput = $this->PersentaseRestoran_model->updatePersentaseRestoran($persentase);
        
        if($notifInput){
            $this->session->set_flashdata('message', 'Success');
        }else{
            $this->session->set_flashdata('message', 'Gagal');
        }
		redirect('district/PersentaseRestoran/editPersentaseRestoran');
    }
}

?>