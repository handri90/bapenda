<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PersentaseHiburan extends CI_Controller {

	public function __construct()
        {
                parent::__construct();
        }

    public function index()
    {
        if($this->session->userdata('logged_in') == true){
            $this->listPersentaseHiburan();
        }else{
            $this->load->view('login');
        }
    }

	public function listPersentaseHiburan(){
        $data['list'] = $this->PersentaseHiburan_model->getListPersentaseHiburan();
        $data['content'] = "district/PersentasePajak/listPersentase";
        $this->load->view('district/main',$data);
    }

    public function tambahPersentaseHiburan(){
        $data['title'] = 'Tambah Persentase Hiburan';
    	$data['isShowLabel'] = "show";
        $data['act'] = 'district/PersentaseHiburan/tambahPersentaseHiburanAct';
        $data['content'] = "district/PersentasePajak/formPersentase";
        $this->load->view('district/main',$data);
    }

    public function tambahPersentaseHiburanAct(){
        $keterangan = $this->input->post('keterangan');
        $persentase = $this->input->post('persentase');
        $notifInput = $this->PersentaseHiburan_model->inputPersentaseHiburan($keterangan,$persentase);
        
        if($notifInput){
            $this->session->set_flashdata('message', 'Success');
        }else{
            $this->session->set_flashdata('message', 'Gagal');
        }
		redirect('district/PersentaseHiburan');
    }
    
    public function editPersentaseHiburan($id){
    	$data['isShowLabel'] = "show";
        $data['list'] = $this->PersentaseHiburan_model->getPersentaseHiburan($id);
        $data['title'] = 'Ubah Persentase Pajak Hiburan';
        $data['labelpersentase'] = 'Pajak Hiburan';
        $data['act'] = 'district/PersentaseHiburan/ubahPersentaseHiburanAct';
        $data['content'] = "district/PersentasePajak/formPersentase";
        $this->load->view('district/main',$data);
    }

    public function ubahPersentaseHiburanAct(){
    	$keterangan = $this->input->post('keterangan');
        $persentase = $this->input->post('persentase');
        $idpersentase = $this->input->post('idpersentase');
        $notifInput = $this->PersentaseHiburan_model->updatePersentaseHiburan($keterangan,$persentase,$idpersentase );
        
        if($notifInput){
            $this->session->set_flashdata('message', 'Success');
        }else{
            $this->session->set_flashdata('message', 'Gagal');
        }
		redirect('district/PersentaseHiburan');
    }

	public function deletePersentaseHiburan($id){
        $input = $this->PersentaseHiburan_model->deletePersentaseHiburan($id);
        redirect('district/PersentaseHiburan');
    }
}

?>