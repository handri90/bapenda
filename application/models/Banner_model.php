<?php
class Banner_model extends CI_Model {

        public function __construct()
        {

        }

        public function getListBanner(){
                $query = $this->db->query("select * from bannerhome");
                return $query->result_array();
        }

        

        public function inputBanner($file,$ordermenu){
            $query = $this->db->query("insert into bannerhome (filename,orderbanner) values ('".$file."',".$ordermenu.")");
    		    return $query;
        }

        public function getBannerById($id){
          $query = $this->db->query("select * from bannerhome where idBannerHome=".$id);
    		  return $query->row();
        }

        public function updateBanner($file,$ordermenu,$idbanner){
          $query = $this->db->query("update bannerhome set filename='".$file."',orderbanner=".$ordermenu." where idBannerHome=".$idbanner);
          return $query;
        }

        public function deleteBanner($id){
          $query = $this->db->query("select filename from bannerhome where idBannerHome = ".$id);
          $result = $query->row();
          unlink("bannerhome/".$result->filename);
          $query = $this->db->query("delete from bannerhome where idBannerHome = ".$id);
          return $query;
        }

        public function changeStatusBanner($id){
          $query = $this->db->query("select status from bannerhome where idBannerHome=".$id);
          $checkStatus = $query->row();
          $val = 0;
          if($checkStatus->status == '0'){
            $val = '1';
          }else{
            $val = '0';
          }
          $query = $this->db->query("update bannerhome set status='".$val."' where idBannerHome=".$id);
          return $query;
        }
}
?>