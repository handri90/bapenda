<section id="news-single" class="section-small section-small-single">
      <div class="container">
        <div class="row">
          <div class="col-md-8">
          <h2>Pajak Air Bawah Tanah</h2>
          <form>
                            <div class="row">
                                <div class="row-form">
                                    <div class="col-12 col-md-3">
                                        <div class="group">
                                            <label>Peruntukan</label>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <div class="group">
                                            <select class="form-control peruntukan">
                                                <?php
                                                for($i=0;$i<count($peruntukan);$i++){
                                                    ?>
                                                    <option value="<?php echo $peruntukan[$i]['idPeruntukanAirTanah']; ?>"><?php echo $peruntukan[$i]['labelPeruntukanAirTanah']; ?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            	<div class="row-form">
                                    <div class="col-12 col-md-3">
                                        <div class="group">
                                            <label>Jumlah Volume Air</label>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <div class="group">
                                            <input required type="text" class="form-control jmlvolume onlynumber3" placeholder="Jumlah Volume Air" />
                                            <span class="notifwarning notifjumlah"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row-form">
                                    <div class="col-12 col-md-3">
                                        <div class="group">
                                        <label>Harga / m<sup>3</sup></label>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <div class="group">
                                            <input readonly type="text" class="form-control hargam3" placeholder="Harga / m3" >
                                            <span class="notifwarning notifwaktu"></span>
                                        </div>
                                    </div>
                                </div>
                            	<div class="row-form">
                                    <div class="col-12 col-md-3">
                                        <div class="group">
                                            <label>Persentase</label>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <div class="group">
                                            <input readonly type="text" class="form-control persentase" value="<?php echo !empty($persentase->persentase)?$persentase->persentase."%":""; ?>" >
                                            <span class="notifwarning notifwaktu"></span>
                                        </div>
                                    </div>
                                </div>
                                <!-- <div class="col-12 col-md-5">
                                    <div class="group">
                                        <select class="form-control tempatpemasangan">
                                            <option value="bulan">Bulan</option>
                                            <option value="hari">Hari</option>
                                        </select>
                                    </div>
                                </div> -->
                                <div class="row-form">
                                    <div class="col-12 col-md-3">
                                        <div class="group">
                                            <label>Total : </label>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <div class="group">
                                            <span class="result"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <a class="btn btn-primary btn-lg calcpajakair">Hitung</a>
                                </div>
                            </div>
                        </form>
          </div>
          <?php $this->load->view('sidebar'); ?>
        </div>
      </div>
    </section>