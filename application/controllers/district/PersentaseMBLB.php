<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PersentaseMBLB extends CI_Controller {

	public function __construct()
        {
                parent::__construct();
        }

    public function index()
    {
        if($this->session->userdata('logged_in') == true){
            $this->editPersentaseMBLB();
        }else{
            $this->load->view('login');
        }
    }
    
    public function editPersentaseMBLB(){
        $data['list'] = $this->PersentaseMBLB_model->getPersentaseMBLB();
        $data['title'] = 'Ubah Persentase Pajak Mineral Bukan Logam dan Batuan';
        $data['labelpersentase'] = 'Pajak Hotel';
        $data['act'] = 'district/PersentaseMBLB/ubahPersentaseMBLBAct';
        $data['content'] = "district/PersentasePajak/formPersentase";
        $this->load->view('district/main',$data);
    }

    public function ubahPersentaseMBLBAct(){
        $persentase = $this->input->post('persentase');
        $notifInput = $this->PersentaseMBLB_model->updatePersentaseMBLB($persentase);
        
        if($notifInput){
            $this->session->set_flashdata('message', 'Success');
        }else{
            $this->session->set_flashdata('message', 'Gagal');
        }
		redirect('district/PersentaseMBLB/editPersentaseMBLB');
    }
}

?>