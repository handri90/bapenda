<section id="news-single" class="section-small section-small-single">
      <div class="container">
        <div class="row">
          <div class="col-md-8">
            <h4><?php echo $single->judul; ?></h4>
            <div id="carousel-news" class="carousel slide carousel-fade">
            <img src="<?php echo base_url().'fotoberita/'.$single->foto; ?>">
            </div>
            <?php echo html_entity_decode($single->content); ?>
          </div>
          <?php $this->load->view('sidebar'); ?>
        </div>
      </div>
    </section>