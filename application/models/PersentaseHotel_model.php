<?php
class PersentaseHotel_model extends CI_Model {

        public function __construct()
        {

        }

        public function getPersentaseHotel(){
          $query = $this->db->query("select * from pajakhotel");
    		  return $query->row();
        }

        public function updatePersentaseHotel($persentase){
          $query = $this->db->query("update pajakhotel set persentase=".$persentase);
          return $query;
        }
}
?>