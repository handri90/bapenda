<?php
class Content_model extends CI_Model {

        public function __construct()
        {

        }

        public function getListMenu(){
                $query = $this->db->query("select * from menu");
                return $query->result_array();
        }

        public function getMenuku($parent)
	{
                $query = $this->db->query("select idMenu,menuName,ifnull(a.jmlChild,0) as jmlChild,b.parentId,slug,link from menu as b left join (select count(*) as jmlChild,parentId from menu group by parentId) as a on a.parentId=idMenu where b.parentId=".$parent." order by orderMenu asc");
                return $query->result_array();
        }
        
        public function inputDataMenu($namamenu,$parent,$permalink,$isi,$userId,$slug,$ordermenu){
                $query = $this->db->query("insert into menu (menuName,parentId,content,link,userId,slug,orderMenu) values ('".$namamenu."',".$parent.",'".$isi."','".$permalink."',".$userId.",'".$slug."',".$ordermenu.")");
                return $query;
        }

        public function cekSlugInputMenu($slug){
                $query = $this->db->query("select count(*) as jumlah from menu where slug = '".$slug."'");
    		return $query->row();
        }

        public function cekSlugInputMenuUpdate($slug,$id){
                $query = $this->db->query("select count(*) as jumlah from menu where slug = '".$slug."' and idMenu!=".$id);
    		return $query->row();
        }

        public function getDataContentById($id){
                $query = $this->db->query("select * from menu where idMenu = '".$id."'");
    		return $query->row();
        }

        public function updateDataMenu($namamenu,$parent,$permalink,$isi,$slug,$id,$ordermenu){
                $query = $this->db->query("update menu set menuName='".$namamenu."',parentId=".$parent.",link='".$permalink."',content='".$isi."',slug='".$slug."',orderMenu=".$ordermenu." where idMenu=".$id);
	        return $query;
        }

        public function cekChildFromParent($id){
                $query = $this->db->query("select count(*) as jml from menu where parentId = ".$id);
    		return $query->row();
        }

        public function deleteDataMenu($id){
                $query = $this->db->query("delete from menu where idMenu=".$id);
	        return $query;
        }

        public function getSingleMenu($slug){
                $query = $this->db->query("select * from menu where slug = '".$slug."'");
    		return $query->row();
        }

        public function getKategoriDownload(){
                $query = $this->db->query("select * from kategorifilepublikasi");
    		return $query->result_array();
        }

        public function getAllNews(){
                $query = $this->db->query("select * from berita");
    		return $query->result_array();
        }

        public function getSingleNews($slug){
                $query = $this->db->query("select * from berita where slugBerita='".$slug."'");
                return $query->row(); 
        }

        public function getAllPengumuman(){
                $query = $this->db->query("select * from pengumuman");
    		return $query->result_array();
        }

        public function getSinglePengumuman($slug){
                $query = $this->db->query("select * from pengumuman where slug='".$slug."'");
                return $query->row(); 
        }

        public function getDownload($slug){
                $query = $this->db->query("select * from detailfilepublikasi inner join kategorifilepublikasi on idKategoriFilePublikasi=kategoriFilePublikasiId where slug='".$slug."'");
                return $query->result_array();
        }

        public function getKategoriDownloadBySlug($slug){
                $query = $this->db->query("select * from kategorifilepublikasi where slug='".$slug."'");
                return $query->row();
        }

        public function getJenisReklame(){
                $query = $this->db->query("select * from jenisreklame");
                return $query->result_array();
        }

        public function getTempatPemasangan(){
                $query = $this->db->query("select * from komponennjt");
                return $query->result_array();
        }

        public function getBobotSrPlKl($tempatpemasangan){
                $query = $this->db->query("select nilaiKomponenNJT,bobotKomponenSR,bobotKomponenPL,bobotKomponenKL from komponennjt inner join komponensr on idKomponenSR=komponenSRid inner join komponenpl on idKomponenPL=komponenPLid inner join komponenkl on idKomponenKL=komponenKLid where idKomponenNJT =".$tempatpemasangan);
                return $query->row();
        }

        public function getBobotLr($luasReklame){
                $query = $this->db->query("select * from komponenlr where '".$luasReklame."' between nilaiAwalKomponenLR and nilaiAkhirKomponenLR");
                return $query->row();
        }

        public function getPriceJenisReklame($jenisreklame){
                $query = $this->db->query("select * from jenisreklame where idJenisReklame=".$jenisreklame);
                return $query->row();
        }

        public function getAlbum(){
                $query = $this->db->query("select idAlbum,namaAlbum,slug,group_concat(filename separator '|') as filename from albumfoto inner join foto on idAlbum=albumIdAlbum group by idAlbum order by tanggalUpload desc");
                return $query->result_array();
        }

        public function getFoto($slug){
                $query = $this->db->query("select * from albumfoto inner join foto on idALbum=albumIdAlbum where slug='".$slug."'");
                return $query->result_array();
        }

        public function getAlbumHome(){
                $query = $this->db->query("select idAlbum,namaAlbum,slug,group_concat(filename separator '|') as filename from albumfoto inner join foto on idAlbum=albumIdAlbum group by idAlbum order by tanggalUpload desc limit 3");
                return $query->result_array();
        }

        public function getAllNewsHome(){
                $query = $this->db->query("select * from berita order by tanggalBerita desc limit 10");
    		return $query->result_array();
        }

        public function getPengumumanHome(){
                $query = $this->db->query("select * from pengumuman order by tanggalPengumuman desc limit 5");
                return $query->result_array();
        }

        public function updateReadCount($jml,$didBerita){
                $query = $this->db->query("update berita set readCount=".$jml." where idBerita=".$didBerita);
                return $query;
        }

        public function getBeritaTerpopuler(){
                $query = $this->db->query("select * from berita order by readCount desc limit 10");
                return $query->result_array();
        }

        public function getBanner(){
                $query = $this->db->query("select * from bannerhome where status='1' order by orderbanner");
                return $query->result_array();   
        }

        public function getQuotes(){
                $query = $this->db->query("select * from quotes where isActive = '1'");
                return $query->row();       
        }

        public function getKepalaDaerah(){
                $query = $this->db->query("select * from kepaladaerah where isActive = '1' order by orderKepalaDaerah");
                return $query->result_array();
        }

        public function getBeritaTerbaru(){
                $query = $this->db->query("select * from berita order by tanggalBerita desc limit 10");
                return $query->result_array();
        }
		public function getDataDinas(){
                $query = $this->db->query("select * from biodinas");
                return $query->row();
        }

		public function getPersentasePajakRestoran(){
        	$query = $this->db->query("select * from pajakrestoran");
                return $query->row();
        }

		public function getPersentasePajakHotel(){
    		$query = $this->db->query("select * from pajakhotel");
                return $query->row();
    	}

		public function getPersentasePajakHiburan(){
        	$query = $this->db->query("select * from pajakhiburan");
                return $query->row();
        }

		public function getPersentasePajakParkir(){
        	$query = $this->db->query("select * from pajakparkir");
                return $query->row();
        }
		
		public function getPersentasePajakSarangBurungWalet(){
        	$query = $this->db->query("select * from pajaksarangburungwalet");
                return $query->row();
        }

		public function getPersentasePajakAirTanah(){
        	$query = $this->db->query("select * from pajakairtanah");
                return $query->row();
        }

		public function getPeruntukan(){
        	$query = $this->db->query("select * from peruntukanairtanah");
                return $query->result_array();
        }

		public function getNilaiVolume($peruntukan,$jmlvolume){
        	$query = $this->db->query("select * from nilaivolumeairtanah inner join volumeperuntukanairtanah on idVolumePeruntukan=volumeAirTanahId where peruntukanAirTanahId = ".$peruntukan." and ".$jmlvolume." between nilaiVolumePeruntukanAwal and nilaiVolumePeruntukanAkhir");
                return $query->row();
        }

		public function getMaxNilaiByPeruntukan($peruntukan){
        	$query = $this->db->query("select * from nilaivolumeairtanah inner join volumeperuntukanairtanah on idVolumePeruntukan=volumeAirTanahId where peruntukanAirTanahId = ".$peruntukan." order by nilaiVolumePeruntukanAkhir desc limit 1");
                return $query->row();
        }

		public function getNilaiVolumeMax($peruntukan,$jmlvolume){
        	$query = $this->db->query("select * from nilaivolumeairtanah inner join volumeperuntukanairtanah on idVolumePeruntukan=volumeAirTanahId where peruntukanAirTanahId = ".$peruntukan." and nilaiVolumePeruntukanAwal <= ".$jmlvolume." and nilaiVolumePeruntukanAkhir IS NULL");
                return $query->row();
        }

		public function getPersentaseHiburan(){
        	$query = $this->db->query("select * from pajakhiburan order by labelPersentaseHiburan");
                return $query->result_array();
        }

		public function getkategoriMBLB(){
        	$query = $this->db->query("select * from kategorimblb order by jenisMBLB");
                return $query->result_array();
        }

		public function getPersentasePajakMBLB(){
        	$query = $this->db->query("select * from pajakmblb");
                return $query->row();
        }
}	
?>