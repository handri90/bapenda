<?php
class KomponenSR_model extends CI_Model {

        public function __construct()
        {

        }

        public function getListKomponenSR(){
                $query = $this->db->query("select * from komponensr");
                return $query->result_array();
        }
        
        public function inputKomponenSR($namakomponen,$bobot){
            $query = $this->db->query("insert into komponensr (labelKomponenSR,bobotKomponenSR) values ('".$namakomponen."',".$bobot.")");
    		return $query;
        }

        public function getKomponenSR($id){
          $query = $this->db->query("select * from komponensr where idKomponenSR = ".$id);
    		  return $query->row();
        }

        public function updateKomponenSR($namakomponen,$bobot,$idkomponen){
          $query = $this->db->query("update komponensr set labelKomponenSR='".$namakomponen."',bobotKomponenSR=".$bobot." where idKomponenSR=".$idkomponen);
          return $query;
        }

        public function deleteKomponenSR($id){
          $query = $this->db->query("delete from komponensr where idKomponenSR = ".$id);
          return $query;
        }
}
?>