<?php
class KomponenPL_model extends CI_Model {

        public function __construct()
        {

        }

        public function getListKomponenPL(){
                $query = $this->db->query("select * from komponenpl");
                return $query->result_array();
        }
        
        public function inputKomponenPL($namakomponen,$bobot){
            $query = $this->db->query("insert into komponenpl (labelKomponenPL,bobotKomponenPL) values ('".$namakomponen."',".$bobot.")");
    		return $query;
        }

        public function getKomponenPL($id){
          $query = $this->db->query("select * from komponenpl where idKomponenPL = ".$id);
    		  return $query->row();
        }

        public function updateKomponenPL($namakomponen,$bobot,$idkomponen){
          $query = $this->db->query("update komponenpl set labelKomponenPL='".$namakomponen."',bobotKomponenPL=".$bobot." where idKomponenPL=".$idkomponen);
          return $query;
        }

        public function deleteKomponenPL($id){
          $query = $this->db->query("delete from komponenpl where idKomponenPL = ".$id);
          return $query;
        }
}
?>