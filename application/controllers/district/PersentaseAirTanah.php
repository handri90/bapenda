<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PersentaseAirTanah extends CI_Controller {

	public function __construct()
        {
                parent::__construct();
        }

    public function index()
    {
        if($this->session->userdata('logged_in') == true){
            $this->editPersentaseAirTanah();
        }else{
            $this->load->view('login');
        }
    }
    
    public function editPersentaseAirTanah(){
        $data['list'] = $this->PersentaseAirTanah_model->getPersentaseAirTanah();
        $data['title'] = 'Ubah Persentase Air Tanah';
        $data['act'] = 'district/PersentaseAirTanah/ubahPersentaseAirTanahAct';
        $data['content'] = "district/PersentasePajak/formPersentase";
        $this->load->view('district/main',$data);
    }

    public function ubahPersentaseAirTanahAct(){
        $persentase = $this->input->post('persentase');
        $notifInput = $this->PersentaseAirTanah_model->updatePersentaseAirTanah($persentase);
        
        if($notifInput){
            $this->session->set_flashdata('message', 'Success');
        }else{
            $this->session->set_flashdata('message', 'Gagal');
        }
		redirect('district/PersentaseAirTanah/editPersentaseAirTanah');
    }
}

?>