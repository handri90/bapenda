<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PersentaseSarangBurungWalet extends CI_Controller {

	public function __construct()
        {
                parent::__construct();
        }

    public function index()
    {
        if($this->session->userdata('logged_in') == true){
            $this->editPersentaseSarangBurungWalet();
        }else{
            $this->load->view('login');
        }
    }
    
    public function editPersentaseSarangBurungWalet(){
        $data['list'] = $this->PersentaseSarangBurungWalet_model->getPersentaseSarangBurungWalet();
        $data['title'] = 'Ubah Persentase Pajak Sarang Burung Walet';
        $data['labelpersentase'] = 'Pajak Sarang Burung Walet';
        $data['act'] = 'district/PersentaseSarangBurungWalet/ubahPersentaseSarangBurungWaletAct';
        $data['content'] = "district/PersentasePajak/formPersentase";
        $this->load->view('district/main',$data);
    }

    public function ubahPersentaseSarangBurungWaletAct(){
        $persentase = $this->input->post('persentase');
        $notifInput = $this->PersentaseSarangBurungWalet_model->updatePersentaseSarangBurungWalet($persentase);
        
        if($notifInput){
            $this->session->set_flashdata('message', 'Success');
        }else{
            $this->session->set_flashdata('message', 'Gagal');
        }
		redirect('district/PersentaseSarangBurungWalet/editPersentaseSarangBurungWalet');
    }
}

?>