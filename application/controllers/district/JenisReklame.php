<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class JenisReklame extends CI_Controller {

	public function __construct()
        {
                parent::__construct();
        }

    public function index()
    {
        if($this->session->userdata('logged_in') == true){
            $this->listJenisReklame();
        }else{
            $this->load->view('login');
        }
    }

    public function listJenisReklame(){
        $data['list'] = $this->JenisReklame_model->getListJenisReklame();
        $data['content'] = "district/JenisReklame/listJenisReklame";
        $this->load->view('district/main',$data);
    }

    public function tambahJenisReklame(){
        $data['satuan'] = $this->JenisReklame_model->getListSatuanWaktu();
        $data['title'] = 'Tambah Jenis Reklame';
        $data['act'] = 'district/JenisReklame/tambahJenisReklameAct';
        $data['content'] = "district/JenisReklame/formJenisReklame";
        $this->load->view('district/main',$data);
    }

    public function tambahJenisReklameAct(){
    	$satuan = $this->input->post('satuan');
    	$namakomponen = $this->input->post('namakomponen');
        $bobot = str_replace(',','',$this->input->post('bobot'));
        $notifInput = $this->JenisReklame_model->inputJenisReklame($satuan,$namakomponen,$bobot);
        
        if($notifInput){
            $this->session->set_flashdata('message', 'Success');
        }else{
            $this->session->set_flashdata('message', 'Gagal');
        }
		redirect('district/JenisReklame');
    }
    
    public function editJenisReklame($id){
        $data['satuan'] = $this->JenisReklame_model->getListSatuanWaktu();
        $data['list'] = $this->JenisReklame_model->getJenisReklame($id);
        $data['title'] = 'Ubah  Jenis Reklame';
        $data['act'] = 'district/JenisReklame/ubahJenisReklameAct';
        $data['content'] = "district/JenisReklame/formJenisReklame";
        $this->load->view('district/main',$data);
    }

    public function ubahJenisReklameAct(){
        $satuan = $this->input->post('satuan');
    	$namakomponen = $this->input->post('namakomponen');
    	$bobot = str_replace(',','',$this->input->post('bobot'));
        $idkomponen = $this->input->post('idkomponen');
        $notifInput = $this->JenisReklame_model->updateJenisReklame($satuan,$namakomponen,$bobot,$idkomponen);
        
        if($notifInput){
            $this->session->set_flashdata('message', 'Success');
        }else{
            $this->session->set_flashdata('message', 'Gagal');
        }
		redirect('district/JenisReklame');
    }

    public function deleteJenisReklame($id){
    	$notifDelete = $this->JenisReklame_model->deleteJenisReklame($id);
        if($notifDelete){
            $this->session->set_flashdata('message', 'Success');
        }else{
            $this->session->set_flashdata('message', 'Gagal');
        }
		redirect('district/JenisReklame');        
    }
}

?>