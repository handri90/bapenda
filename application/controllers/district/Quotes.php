<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Quotes extends CI_Controller {

	public function __construct()
        {
                parent::__construct();
        }

    public function index()
    {
        if($this->session->userdata('logged_in') == true){
            $this->listQuotes();
        }else{
            $this->load->view('login');
        }
    }

    public function listQuotes(){
        $data['list'] = $this->Quotes_model->getListQuotes();
        $data['content'] = "district/Quotes/listQuotes";
        $this->load->view('district/main',$data);
    }

    public function tambahQuotes(){
        $data['title'] = 'Tambah Quotes';
        $data['act'] = 'district/Quotes/tambahQuotesAct';
        $data['content'] = "district/Quotes/formQuotes";
        $this->load->view('district/main',$data);
    }

    public function tambahQuotesAct(){
    	$isi = htmlentities($this->input->post('isi'));

        $notifInput = $this->Quotes_model->inputQuotes($isi);
        if($notifInput){
            $this->session->set_flashdata('message', 'Success');
        }else{
            $this->session->set_flashdata('message', 'Gagal');
        }
    	
		redirect('district/Quotes');
    }
    
    public function editQuotes($id){
        $data['list'] = $this->Quotes_model->getDataQuotesById($id);
        $data['title'] = 'Ubah Quotes';
        $data['act'] = 'district/Quotes/ubahQuotesAct';
        $data['content'] = "district/Quotes/formQuotes";
        $this->load->view('district/main',$data);
    }

    public function ubahQuotesAct(){
        $isi = htmlentities($this->input->post('isi'));
    	$idquot = $this->input->post('idquot');

        $notifInput = $this->Quotes_model->updateQuotes($isi,$idquot);
        
        if($notifInput){
            $this->session->set_flashdata('message', 'Success');
        }else{
            $this->session->set_flashdata('message', 'Gagal');
        }
    	
		redirect('district/Quotes');
    }

    public function deleteQuotes($id){
    	$notifDelete = $this->Quotes_model->deleteQuotes($id);
        if($notifDelete){
            $this->session->set_flashdata('message', 'Success');
        }else{
            $this->session->set_flashdata('message', 'Gagal');
        }
        
		redirect('district/Quotes');        
    }

    public function changeStatus($id){
        $notifDelete = $this->Quotes_model->changeStatusQuotes($id);
        if($notifDelete){
            $this->session->set_flashdata('message', 'Success');
        }else{
            $this->session->set_flashdata('message', 'Gagal');
        }
        
		redirect('district/Quotes');
    }
}

?>