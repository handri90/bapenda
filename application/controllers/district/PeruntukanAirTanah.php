<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PeruntukanAirTanah extends CI_Controller {

	public function __construct()
        {
                parent::__construct();
        }

    public function index()
    {
        if($this->session->userdata('logged_in') == true){
            $this->listPeruntukanAirTanah();
        }else{
            $this->load->view('login');
        }
    }

    public function listPeruntukanAirTanah(){
        $data['list'] = $this->PeruntukanAirTanah_model->getListPeruntukanAirTanah();
        $data['content'] = "district/PeruntukanAirTanah/listPeruntukanAirTanah";
        $this->load->view('district/main',$data);
    }

    public function tambahPeruntukanAirTanah(){
        $data['title'] = 'Tambah Kategori Download';
        $data['act'] = 'district/PeruntukanAirTanah/tambahPeruntukanAirTanahAct';
        $data['content'] = "district/PeruntukanAirTanah/formPeruntukanAirTanah";
        $this->load->view('district/main',$data);
    }

    public function tambahPeruntukanAirTanahAct(){
        $labelperuntukan = $this->input->post('labelperuntukan');
        $notifInput = $this->PeruntukanAirTanah_model->inputPeruntukanAirTanah($labelperuntukan);
        
        if($notifInput){
            $this->session->set_flashdata('message', 'Success');
        }else{
            $this->session->set_flashdata('message', 'Gagal');
        }
		redirect('district/PeruntukanAirTanah');
    }

    private function set_upload_options()
	{   
	    //upload an image options
	    $filename = md5(uniqid(rand(), true));
	    $config = array();
	    $config['upload_path'] = 'upload';
	    $config['allowed_types'] = 'gif|jpg|png|jpeg|pdf';
	    $config['file_name'] = $filename;

	    return $config;
    }
    
    public function editPeruntukanAirTanah($id){
        $data['list'] = $this->PeruntukanAirTanah_model->getPeruntukanAirTanahById($id);
        $data['title'] = 'Ubah Kategori Download';
        $data['act'] = 'district/PeruntukanAirTanah/ubahPeruntukanAirTanahAct';
        $data['content'] = "district/PeruntukanAirTanah/formPeruntukanAirTanah";
        $this->load->view('district/main',$data);
    }

    public function ubahPeruntukanAirTanahAct(){
        $labelperuntukan = $this->input->post('labelperuntukan');
        $idperuntukan = $this->input->post('idperuntukan');
        $notifInput = $this->PeruntukanAirTanah_model->updatePeruntukanAirTanah($labelperuntukan,$idperuntukan);
        
        if($notifInput){
            $this->session->set_flashdata('message', 'Success');
        }else{
            $this->session->set_flashdata('message', 'Gagal');
        }
		redirect('district/PeruntukanAirTanah');
    }

    public function deletePeruntukanAirTanah($id){
    	$notifDelete = $this->PeruntukanAirTanah_model->deletePeruntukanAirTanah($id);
        if($notifDelete){
            $this->session->set_flashdata('message', 'Success');
        }else{
            $this->session->set_flashdata('message', 'Gagal');
        }
        
		redirect('district/PeruntukanAirTanah');        
    }
}

?>