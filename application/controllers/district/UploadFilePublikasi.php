<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UploadFilePublikasi extends CI_Controller {

	public function __construct()
        {
                parent::__construct();
        }

    public function index()
    {
        if($this->session->userdata('logged_in') == true){
            $this->listUploadFilePublikasi();
        }else{
            $this->load->view('login');
        }
    }

    public function listUploadFilePublikasi(){
        $data['list'] = $this->UploadFilePublikasi_model->getListUploadFilePublikasi();
        $data['content'] = "district/UploadFilePublikasi/listUploadFilePublikasi";
        $this->load->view('district/main',$data);
    }

    public function tambahUploadFilePublikasi(){
        $data['kategori'] = $this->UploadFilePublikasi_model->getKategoriDownload();
        $data['title'] = 'Tambah Upload File Publikasi';
        $data['act'] = 'district/UploadFilePublikasi/tambahUploadFilePublikasiAct';
        $data['content'] = "district/UploadFilePublikasi/formUploadFilePublikasi";
        $this->load->view('district/main',$data);
    }

    public function tambahUploadFilePublikasiAct(){
    	$judul = $this->input->post('judul');
        $kategori = $this->input->post('kategori');
        $this->upload->initialize($this->set_upload_options());
        $this->upload->do_upload();

        $file_data = $this->upload->data();
        $data['file_dir'] = $file_data['file_name'];
        $notifInput = $this->UploadFilePublikasi_model->inputFilePublikasi($judul,$kategori,$data['file_dir']);
        
        if($notifInput){
            $this->session->set_flashdata('message', 'Success');
        }else{
            $this->session->set_flashdata('message', 'Gagal');
        }
		redirect('district/UploadFilePublikasi');
    }

    private function set_upload_options()
	{   
	    //upload an image options
	    $filename = md5(uniqid(rand(), true));
	    $config = array();
	    $config['upload_path'] = 'publikasi';
	    $config['allowed_types'] = 'gif|jpg|png|jpeg|pdf|word';
	    $config['file_name'] = $filename;

	    return $config;
    }
    
    public function editFilePublikasi($id){
        $data['kategori'] = $this->UploadFilePublikasi_model->getKategoriDownload();
        $data['list'] = $this->UploadFilePublikasi_model->getFilePublikasiById($id);
        $data['title'] = 'Ubah File Publikasi';
        $data['act'] = 'district/UploadFilePublikasi/ubahUploadFilePublikasiAct';
        $data['content'] = "district/UploadFilePublikasi/formUploadFilePublikasi";
        $this->load->view('district/main',$data);
    }

    public function ubahUploadFilePublikasiAct(){
        $judul = $this->input->post('judul');
        $kategori = $this->input->post('kategori');
        $iddetailpublikasi = $this->input->post('iddetailpublikasi');
 	    $oldFile = $this->input->post('oldFile');

        if(!empty($_FILES['userfile']['name'])){
            $this->upload->initialize($this->set_upload_options());
            $this->upload->do_upload();

            $file_data = $this->upload->data();
            $data['file_dir'] = $file_data['file_name'];
            unlink('publikasi/'.$oldFile);
        }else{
            $data['file_dir'] = $oldFile;
        }
        $notifInput = $this->UploadFilePublikasi_model->updateFilePublikasi($judul,$kategori,$data['file_dir'],$iddetailpublikasi);
        
        if($notifInput){
            $this->session->set_flashdata('message', 'Success');
        }else{
            $this->session->set_flashdata('message', 'Gagal');
        }
		redirect('district/UploadFilePublikasi');
    }

    public function deleteUploadFilePublikasi($id){
    	$notifDelete = $this->UploadFilePublikasi_model->deleteFilePublikasi($id);
        if($notifDelete){
            $this->session->set_flashdata('message', 'Success');
        }else{
            $this->session->set_flashdata('message', 'Gagal');
        }
        
		redirect('district/UploadFilePublikasi');        
    }
}

?>