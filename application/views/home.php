    <!-- Intro Slider-->
    <header id="Carousel-intro" data-ride="carousel" class="intro carousel carousel-big slide carousel-fade">
      <!-- Indicators-->

      <?php
        if($isShowBanner == 'active'){
            $listDot = "";
            $classStart="";
            $listImage = "";
            $classImage ="";
            for($i=0;$i<count($banner);$i++){
                if($i == 0){
                    $classStart = "class='active'";
                    $classImage = "active";
                }else{
                    $classStart = "";      
                    $classImage = "";
                }
                $listDot .= '<li data-target="#Carousel-intro" data-slide-to="'.$i.'" '.$classStart.'></li>';
                $listImage .= '<div class="item '.$classImage.'">
                    <div style="background-image:url(\''.base_url().'bannerhome/'.$banner[$i]['filename'].'\');" class="fill">
                    </div>
                </div>';
            }

            ?>
            <ol class="carousel-indicators">
                <?php echo $listDot; ?>
            </ol>
            <div class="carousel-inner">
                <?php echo $listImage; ?>
            </div>
            <a href="#Carousel-intro" data-slide="prev" class="left carousel-control"><span class="icon-prev"></span></a><a href="#Carousel-intro" data-slide="next" class="right carousel-control"><span class="icon-next"></span></a>
            <?php
        }
        ?>
    </header>

    <section class="quote section-small bg-img3 text-center">
      <div class="overlay"></div>
      <div class="container">
        <div class="row">
          <div class="col-sm-8 col-sm-offset-2">
            <?php echo !empty($quotes->descQuotes)?html_entity_decode($quotes->descQuotes):""; ?>
          </div>
        </div>
      </div>
    </section>
    
    <section id="news">
      <div class="container">
        <h3 class="pull-left">Berita</h3>
        <div class="pull-right">
          <h5><a href="<?php echo base_url(); ?>berita">Berita Lainnya</a></h5>
        </div>
        <div class="clearfix"></div>
        <div class="row grid-pad">
        <?php
            for($i=0;$i<count($berita);$i++){
        ?>
        <div class="col-sm-4">
              <a href="<?php echo base_url().'berita/'.$berita[$i]['slugBerita']; ?>"><img src="<?php echo base_url().'fotoberita/'.$berita[$i]['foto']; ?>" alt="" class="img-responsive center-block">
              <h5><?php echo $berita[$i]['judul']; ?></h5></a>
                <p><?php 
                $berita[$i]['content']=strip_tags(html_entity_decode($berita[$i]['content']));
                $berita[$i]['content']= preg_replace('/\s+?(\S+)?$/', '', substr($berita[$i]['content'], 0, 101));
                echo $berita[$i]['content'].'...';
                ?></p><a href="<?php echo base_url().'berita/'.$berita[$i]['slugBerita']; ?>" class="btn btn-gray btn-xs">Selengkapnya</a>
          </div>
        <?php
            }
        ?>
        </div>
      </div>
    </section>
    <!-- Portfolio-->
    <section id="portfolio" class="bg-gray portfolio-wide">
      <div class="container text-center">
        <div class="row">
          <div class="col-sm-10 col-sm-offset-1">
            <h3>Gallery</h3>
          </div>
        </div>
      </div>
      <div class="container">
        <div id="grid" class="row portfolio-items">
        <?php
            for($i=0;$i<count($album);$i++){
            $filename = explode('|',$album[$i]['filename']);
        ?>
          <div class="col-sm-3">
            <div class="portfolio-item"><a href="<?php echo base_url('album/').$album[$i]['slug']; ?>"><img width="260" height="160" src="<?php echo base_url('filegallery/').$filename[0]; ?>" alt="">
                <div class="portfolio-overlay">
                  <div class="caption">
                    <h5><?php echo $album[$i]['namaAlbum']; ?></span>
                  </div>
                </div></a></div>
          </div>
          <?php
            }
        ?>
        </div>
      </div>
    </section>
    <div class="section-small action bg-gray text-center"><a href="<?php echo base_url().'gallery'; ?>" class="btn btn-dark-border">Open Gallery</a></div>
    <!-- Team Section-->
    <section id="team" class="bg-white text-center">
      <div class="container">
        <h3>KEPALA DAERAH</h3>
        <div class="row">
          <?php
          for($i =0;$i<count($kepaladaerah);$i++){
            ?>
            <div class="col-md-6 col-sm-6">
              <p><img src="<?php echo base_url().'kepaladaerah/'.$kepaladaerah[$i]['filenameKepalaDaerah']; ?>" alt="" class="img-responsive center-block"></p>
              <h2 class="classic"><?php echo $kepaladaerah[$i]['namaKepalaDaerah']; ?></h2>
              <h6><?php echo $kepaladaerah[$i]['jabatanKepalaDaerah']; ?></h6>
            </div>
            <?php
          }
          ?>
        </div>
      </div>
    </section>
    <!-- <section id="testimonials">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-md-offset-3">
            <h3 class="text-center">Testimonials</h3>
            <div id="carousel-testimonials" data-ride="carousel" class="carousel slide carousel-fade">
              <ol class="carousel-indicators">
                <li data-target="#carousel-testimonials" data-slide-to="0" class="active"></li>
                <li data-target="#carousel-testimonials" data-slide-to="1"></li>
                <li data-target="#carousel-testimonials" data-slide-to="2"></li>
              </ol>
              <div role="listbox" class="carousel-inner">
                <div class="item active"><img src="<?php echo base_url(); ?>assets/img/testimonials/1.jpg" alt="" class="center-block">
                  <div class="carousel-caption">
                    <h2 class="classic">Richard Kindall</h2>
                    <h5 class="no-pad">Thank you very much, the template is great! Very concise and simple.</h5>
                  </div>
                </div>
                <div class="item"><img src="<?php echo base_url(); ?>assets/img/testimonials/2.jpg" alt="" class="center-block">
                  <div class="carousel-caption">
                    <h2 class="classic">David Cohen</h2>
                    <h5 class="no-pad">Adorable minimalist theme! Should be working well as a canvas for any upcoming project.</h5>
                  </div>
                </div>
                <div class="item"><img src="<?php echo base_url(); ?>assets/img/testimonials/3.jpg" alt="" class="center-block">
                  <div class="carousel-caption">
                    <h2 class="classic">Jane Navarro</h2>
                    <h5 class="no-pad">Amazing and SUPER easy to incorporate, looks like a great theme, full functionality indeed</h5>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section> -->
    <!-- <section id="services" class="bg-img4 text-center">
      <div class="overlay"></div>
      <div class="container text-center">
        <div class="row">
          <div class="col-lg-8 col-lg-offset-2">
            <h3>Our Services</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla convallis pulvinar vestibulum. Doneceleifend, sem sed dictum. Lorem ipsum dolor sit amet, consectetur adipiscing elits</p>
          </div>
        </div>
        <div class="row">
          <div data-wow-delay=".2s" class="col-lg-3 col-sm-6 wow fadeIn">
            <a href="#"><h4><i class="icon icon-big ion-ios-analytics-outline"></i> ANALITICS</h4></a>
            <p>Lorem ipsum dolor sit amet. Con eleifend sem sed dictum mattis sectetur elit. Nulla convallis pul.</p>
          </div>
          <div data-wow-delay=".4s" class="col-lg-3 col-sm-6 wow fadeIn">
            <h4><i class="icon icon-big ion-ios-pie-outline"></i> DESIGN</h4>
            <p>Lorem ipsum dolor sit amet. Con eleifend sem sed dictum mattis sectetur elit. Nulla convallis pul.</p>
          </div>
          <div data-wow-delay=".6s" class="col-lg-3 col-sm-6 wow fadeIn">
            <h4><i class="icon icon-big ion-ios-stopwatch-outline"></i> CONSULTING</h4>
            <p>Lorem ipsum dolor sit amet. Con eleifend sem sed dictum mattis sectetur elit. Nulla convallis pul.</p>
          </div>
          <div data-wow-delay=".8s" class="col-lg-3 col-sm-6 wow fadeIn">
            <h4><i class="icon icon-big ion-ios-game-controller-b-outline"></i> ADVERTISING</h4>
            <p>Lorem ipsum dolor sit amet. Con eleifend sem sed dictum mattis sectetur elit. Nulla convallis pul.</p>
          </div>
          <div data-wow-delay="1s" class="col-lg-3 col-sm-6 wow fadeIn">
            <h4><i class="icon icon-big icon ion-ios-infinite-outline"></i>BRANDING</h4>
            <p>Lorem ipsum dolor sit amet. Con eleifend sem sed dictum mattis sectetur elit. Nulla convallis pul.</p>
          </div>
          <div data-wow-delay="1.2s" class="col-lg-3 col-sm-6 wow fadeIn">
            <h4><i class="icon icon-big ion-ios-stopwatch-outline"></i>DEVELOPMENT</h4>
            <p>Lorem ipsum dolor sit amet. Con eleifend sem sed dictum mattis sectetur elit. Nulla convallis pul.</p>
          </div>
          <div data-wow-delay="1.4s" class="col-lg-3 col-sm-6 wow fadeIn">
            <h4><i class="icon icon-big ion-ios-monitor-outline"></i>OPTIMIZATION</h4>
            <p>Lorem ipsum dolor sit amet. Con eleifend sem sed dictum mattis sectetur elit. Nulla convallis pul.</p>
          </div>
          <div data-wow-delay="1.6s" class="col-lg-3 col-sm-6 wow fadeIn">
            <h4><i class="icon icon-big ion-ios-settings"></i>CUSTOMIZATION</h4>
            <p>Lorem ipsum dolor sit amet. Con eleifend sem sed dictum mattis sectetur elit. Nulla convallis pul.</p>
          </div>
        </div>
      </div>
    </section> -->
    <!-- Action video-->
    <!-- <section class="section-small bg-img2 text-center">
      <div class="overlay"></div>
      <div class="container">
        <div class="row">
          <div class="col-sm-8 col-sm-offset-2"><a href="https://vimeo.com/155463374" class="swipebox-video"><i class="icon icon-big ion-ios-videocam-outline"></i></a>
            <h2>Watch <span class="bold">Video</span>
            </h2>
            <p>A business has to be involving, it has to be fun, and it has to exercise your creative instincts. Start where you are. Use what you have. Do what you can.</p>
          </div>
        </div>
      </div>
    </section> -->
    