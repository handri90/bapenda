<?php
class PersentaseAirTanah_model extends CI_Model {

        public function __construct()
        {

        }

        public function getPersentaseAirTanah(){
          $query = $this->db->query("select * from pajakairtanah");
    		  return $query->row();
        }

        public function updatePersentaseAirTanah($persentase){
          $query = $this->db->query("update pajakairtanah set persentase=".$persentase);
          return $query;
        }
}
?>