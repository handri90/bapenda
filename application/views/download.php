<section id="news-single" class="section-small section-small-single">
    <div class="container">
        <h3 class="pull-left"><?php echo $single->namaKategoriFilePublikasi; ?></h3>
        <div class="clearfix"></div>
            <div class="row">
                <div class="content-page">
                <?php
                    $no = 1;
                    for($i=0;$i<count($download);$i++){
                ?>
                <div class="col-md-12 shad-univ">
                    <span class="nomor-urut"><?php echo $no++; ?></span>
                    <span class="content-ls-pengumuman"><h5><?php echo $download[$i]['judulDetailFilePublikasi']; ?></h5>
                    <p><a target="_blank" class="btn btn-primary btn-lg" href="<?php echo base_url().'publikasi/'.$download[$i]['file']; ?>">Download</a></p></span>
                <span class="clearMobile"></span>
                </div>
                <?php
                    }
                ?>
                </div>
        <?php $this->load->view('sidebar'); ?>
        </div>
    </div>
</section>