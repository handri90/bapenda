<!-- page content -->
<div class="right_col" role="main">
          <div class="">

            <div class="row">

              <div class="col-md-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo $title; ?></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                    <form enctype="multipart/form-data" class="form-horizontal form-label-left" method="post" action="<?php echo base_url().$act; ?>">

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Judul</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <input type="hidden" name="idberita" class="form-control" value="<?php echo !empty($list->idBerita)?$list->idBerita:""; ?>">
                          <input required type="text" name="judul" class="form-control" placeholder="Judul" value="<?php echo !empty($list->judul)?$list->judul:""; ?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal Kegiatan</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <input id="single_cal2" type="text" name="tanggalkegiatan" class="form-control" placeholder="Tanggal Kegiatan" value="<?php echo !empty($list->tanggalBerita)?$list->tanggalBerita:""; ?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Content
                        </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <textarea required class="form-control" id="editor" name="isi">
                          <?php echo !empty($list->content)?$list->content:""; ?>
                          </textarea>
                          <span class="notifwarning notifIsi"></span>
                        </div>
                      </div>
                      <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Foto</label>
                            <label class="btn btn-primary btn-upload btnUpgrade" for="inputImage" title="Upload image file">
                              <?php
                              $req = "";
                              if(empty($list->idBerita)){
                                $req = "required";
                              }
                              ?>
                              <input <?php echo $req; ?> type="file" class="sr-only" id="inputImage" name="userfile" accept="image/*">
                              <span class="docs-tooltip" data-toggle="tooltip" title="Import image with Blob URLs">
                                <span class="fa fa-upload"></span>
                              </span>
                            </label>
                            <input name="tempFile" id="tempFile" type="hidden" value="">
                            <input name="tempSavedFile" id="tempSavedFile" value="<?php echo isset($list->foto)?$list->foto:''; ?>" type="hidden">
                          </div>
                          <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">&nbsp;</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <output id="list">
                          </output>
                          <?php
                          if(!empty($list->idBerita)){
                            ?>
                            <output id="savedImage">
                            <span class='imgSaved'><div style="margin-top:10px;background-color:#caccd1;padding:10px;"><img style="height: 50%;width:50%; border: 1px solid #000; margin: 5px" src="<?php echo base_url()."fotoberita/".$list->foto; ?>" /><input value="<?php echo !empty($list->caption)?$list->caption:""; ?>" id="captionGambar" name="captionGambar[]" class="form-control" style="margin-left:5px;width:50%;"></div></span>
                            </output>
                            <?php
                          }
                          ?>
                        </div>
                      </div>
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                          <button type="button" class="btn btn-primary" id="backButton">Cancel</button>
                          <button type="reset" class="btn btn-primary">Reset</button>
                          <button type="submit" class="btn btn-success btnBerita">Submit</button>
                        </div>
                      </div>

                    </form>
                  </div>
                </div>
              </div>
            </div>


            
          </div>
        </div>
        <!-- /page content -->