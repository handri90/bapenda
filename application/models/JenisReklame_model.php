<?php
class JenisReklame_model extends CI_Model {

        public function __construct()
        {

        }

        public function getListJenisReklame(){
                $query = $this->db->query("select * from jenisreklame inner join satuanwaktu on satuanWaktuId=idSatuanWaktu");
                return $query->result_array();
        }
        
        public function inputJenisReklame($satuan,$namakomponen,$bobot){
            $query = $this->db->query("insert into jenisreklame (labelJenisReklame,nilaiJenisReklame,satuanWaktuId) values ('".$namakomponen."',".$bobot.",".$satuan.")");
    		return $query;
        }

        public function getJenisReklame($id){
          $query = $this->db->query("select * from jenisreklame where idJenisReklame = ".$id);
    		  return $query->row();
        }

        public function updateJenisReklame($satuan,$namakomponen,$bobot,$idkomponen){
          $query = $this->db->query("update jenisreklame set labelJenisReklame='".$namakomponen."',nilaiJenisReklame=".$bobot.",satuanWaktuId=".$satuan." where idJenisReklame=".$idkomponen);
          return $query;
        }

        public function deleteJenisReklame($id){
          $query = $this->db->query("delete from jenisreklame where idJenisReklame = ".$id);
          return $query;
        }

        public function getListSatuanWaktu(){
            $query = $this->db->query("select * from satuanwaktu");
    		  return $query->result_array();
        }
}
?>