<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Frontend extends CI_Controller {
	public function index()
	{
		$this->home();
    }
    
    public function home(){
        $data['isShowBanner'] = 'active';
        $data['classNav'] = '';
    	$data['datadinas'] = $this->Content_model->getDataDinas();
        $data['album'] = $this->Content_model->getAlbumHome();
        $data['berita'] = $this->Content_model->getAllNewsHome();
        $data['pengumuman'] = $this->Content_model->getPengumumanHome();
        $data['beritaterpopuler']=$this->Content_model->getBeritaTerpopuler();
        $data['banner']=$this->Content_model->getBanner();
        $data['quotes']=$this->Content_model->getQuotes();
        $data['kepaladaerah']=$this->Content_model->getKepalaDaerah();
        $data['tautancepat'] = $this->tautanCepat();
        $data['menu'] =  $this->menu();
        $data['content'] = "home";
        $this->load->view('main',$data);
    }

    public function menu(){
        $str ="";
        $filedownload = $this->Content_model->getKategoriDownload();
        $strDown = "";
        for($i=0;$i<count($filedownload);$i++){
            $strDown .= '<li><a href="'.base_url().'download/'.$filedownload[$i]['slug'].'">'.$filedownload[$i]['namaKategoriFilePublikasi'].'</a></li>';
        }
        $addedMenu = '<li>
                        <a>Publikasi <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="'.base_url().'berita">Berita</a></li>
                            <li><a href="'.base_url().'pengumuman">Pengumuman</a></li>'.$strDown.'
                        </ul>
                    </li>
                    <li>
                        <a>Kalkulator Pajak <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="'.base_url().'calcpajakreklame">Pajak Reklame</a></li>
                            <li><a href="'.base_url().'calcpajakrestoran">Pajak Restoran</a></li>
                            <li><a href="'.base_url().'calcpajakhotel">Pajak Hotel</a></li>
                            <li><a href="'.base_url().'calcpajakhiburan">Pajak Hiburan</a></li>
                            <li><a href="'.base_url().'calcpajakparkir">Pajak Parkir</a></li>
                            <li><a href="'.base_url().'calcpajaksarangburungwalet">Pajak Sarang Burung Walet</a></li>
                            <li><a href="'.base_url().'calcpajakairbawahtanah">Pajak Air Bawah Tanah</a></li>
                            <li><a href="'.base_url().'calcpajakmblb">Pajak Mineral Bukan Logam dan Batuan</a></li>
                        </ul>
                    </li>
                    <li><a href="'.base_url().'gallery">Gallery</a></li>';
        return  "<ul class='nav navbar-nav navbar-left'><li class='hidden'><a href='#page-top'></a></li>".$this->menuRek(0).$addedMenu."</ul>";
    }

    public function menuRek($parentid = 0){
        $str = "";
        $menu = $this->Content_model->getMenuku($parentid);
        for($i=0;$i<count($menu);$i++){
            $classLi = "";
            $attr = "";
            $href = "";
            $child = "";
            if($parentid == 0){
                if($menu[$i]['jmlChild'] == '0'){
                    $href = !empty($menu[$i]['link'])?"target='_blank' href='".$menu[$i]['link']."'":"href='".base_url().'page/'.$menu[$i]['slug']."'";
                    $childspan = '';
                    $child = "";
                }else{
                    $href = "href='#'";
                    $childspan = '<span class="caret"></span>';
                    $child = "<ul class='dropdown-menu'>".$this->menuRek($menu[$i]['idMenu'])."</ul>";
                }
            }else{
                if($menu[$i]['jmlChild'] == '0'){
                    $href = !empty($menu[$i]['link'])?"target='_blank' href='".$menu[$i]['link']."'":"href='".base_url().'page/'.$menu[$i]['slug']."'";
                    $childspan = '';     
                    $child = "";              
                }else{
                    $href = "href='#'";
                    $childspan = '<span class="angle-right"></span><span class="caret"></span>';
                    $child = "<ul class='dropdown-menu'>".$this->menuRek($menu[$i]['idMenu'])."</ul>";                    
                }
            }
            $str .= "<li><a ".$href.">".$menu[$i]['menuName']." ".$childspan."</a>";
            $str .= $child;
            $str .= "</li>";
        }
        return $str;
    }

    public function tautanCepat(){
        $filedownload = $this->Content_model->getKategoriDownload();
        $strDown = "";
        for($i=0;$i<count($filedownload);$i++){
            $strDown .= '<h6 class="no-pad"><a href="'.base_url().'download/'.$filedownload[$i]['slug'].'">'.$filedownload[$i]['namaKategoriFilePublikasi'].'</a></h6>';
        }
        return '<h6 class="no-pad"><a href="'.base_url().'berita">Berita</a></h6>
        <h6 class="no-pad"><a href="'.base_url().'pengumuman">Pengumuman</a></h6>'.$strDown;
    }

    public function showSingleMenu($id){
        $data['classNav'] = 'top-nav-collapse';
        $data['tautancepat'] = $this->tautanCepat();
        $data['isShowBanner'] = 'failed';
    	$data['datadinas'] = $this->Content_model->getDataDinas();
        $data['beritaterpopuler']=$this->Content_model->getBeritaTerpopuler();
        $data['beritaterbaru']=$this->Content_model->getBeritaTerbaru();
        $data['single'] = $this->Content_model->getSingleMenu($id);
        $data['menu'] =  $this->menu();
        $data['content'] = "singlePage";
        $this->load->view('main',$data);
    }

    public function showBerita(){
        $data['classNav'] = 'top-nav-collapse';
        $data['isShowBanner'] = 'failed';
    	$data['datadinas'] = $this->Content_model->getDataDinas();
        $data['beritaterpopuler']=$this->Content_model->getBeritaTerpopuler();
        $data['beritaterbaru']=$this->Content_model->getBeritaTerbaru();
        $data['tautancepat'] = $this->tautanCepat();
        $data['berita'] = $this->Content_model->getAllNews();
        $data['menu'] =  $this->menu();
        $data['content'] = "berita";
        $this->load->view('main',$data);
    }

    public function showDetailBerita($slug){
        $data['beritaterpopuler']=$this->Content_model->getBeritaTerpopuler();
        $data['beritaterbaru']=$this->Content_model->getBeritaTerbaru();
        $data['tautancepat'] = $this->tautanCepat();
        $data['classNav'] = 'top-nav-collapse';
        $data['isShowBanner'] = 'failed';
    	$data['datadinas'] = $this->Content_model->getDataDinas();
        $data['single'] = $this->Content_model->getSingleNews($slug);
        $jml = $data['single']->readCount;
        $this->Content_model->updateReadCount(++$jml,$data['single']->idBerita);
        $data['menu'] =  $this->menu();
        $data['content'] = "singleNews";
        $this->load->view('main',$data);
    }

    public function showPengumuman(){
        $data['isShowBanner'] = 'failed';
        $data['classNav'] = 'top-nav-collapse';
        $data['tautancepat'] = $this->tautanCepat();
    	$data['datadinas'] = $this->Content_model->getDataDinas();
        $data['beritaterpopuler']=$this->Content_model->getBeritaTerpopuler();
        $data['beritaterbaru']=$this->Content_model->getBeritaTerbaru();
        $data['pengumuman'] = $this->Content_model->getAllPengumuman();
        $data['menu'] =  $this->menu();
        $data['content'] = "pengumuman";
        $this->load->view('main',$data);
    }

    public function showDetailPengumuman($slug){
        $data['isShowBanner'] = 'failed';
        $data['classNav'] = 'top-nav-collapse';
        $data['tautancepat'] = $this->tautanCepat();
    	$data['datadinas'] = $this->Content_model->getDataDinas();
        $data['beritaterpopuler']=$this->Content_model->getBeritaTerpopuler();
        $data['beritaterbaru']=$this->Content_model->getBeritaTerbaru();
        $data['single'] = $this->Content_model->getSinglePengumuman($slug);
        $data['menu'] =  $this->menu();
        $data['content'] = "singlePengumuman";
        $this->load->view('main',$data);
    }

    public function showDownload($slug){
        $data['isShowBanner'] = 'failed';
        $data['classNav'] = 'top-nav-collapse';
    	$data['datadinas'] = $this->Content_model->getDataDinas();
        $data['tautancepat'] = $this->tautanCepat();
        $data['beritaterpopuler']=$this->Content_model->getBeritaTerpopuler();
        $data['beritaterbaru']=$this->Content_model->getBeritaTerbaru();
        $data['single'] = $this->Content_model->getKategoriDownloadBySlug($slug);
        $data['download'] = $this->Content_model->getDownload($slug);
        $data['menu'] =  $this->menu();
        $data['content'] = "download";
        $this->load->view('main',$data);
    }

    public function hitungPajakReklame(){
        $data['isShowBanner'] = 'failed';
        $data['classNav'] = 'top-nav-collapse';
        $data['tautancepat'] = $this->tautanCepat();
    	$data['datadinas'] = $this->Content_model->getDataDinas();
        $data['jenisreklame'] = $this->Content_model->getJenisReklame();
        $data['beritaterpopuler']=$this->Content_model->getBeritaTerpopuler();
        $data['beritaterbaru']=$this->Content_model->getBeritaTerbaru();
        $data['tempatpemasangan'] = $this->Content_model->getTempatPemasangan();
        $data['menu'] =  $this->menu();
        $data['content'] = "formPajakReklame";
    	$data['btnAct'] = "calcpajak";
        $this->load->view('main',$data);
    }

	public function hitungPajakRestoran(){
    	$data['isShowBanner'] = 'failed';
        $data['classNav'] = 'top-nav-collapse';
    	$data['labelSatuan'] = 'Paket / Org. Pelanggan';
    	$data['titlePajak'] = 'Pajak Restoran';
    	$data['labelHarga'] = 'Harga Satuan';
        $data['tautancepat'] = $this->tautanCepat();
    	$data['datadinas'] = $this->Content_model->getDataDinas();
        $data['beritaterpopuler']=$this->Content_model->getBeritaTerpopuler();
        $data['beritaterbaru']=$this->Content_model->getBeritaTerbaru();
    	$data['persentase']=$this->Content_model->getPersentasePajakRestoran();
        $data['menu'] =  $this->menu();
    	$data['btnAct'] = "calcpajak";
        $data['content'] = "formPajak";
        $this->load->view('main',$data);
    }

	public function hitungPajakHotel(){
    	$data['isShowBanner'] = 'failed';
        $data['classNav'] = 'top-nav-collapse';
    	$data['labelSatuan'] = 'Paket / Org';
    	$data['titlePajak'] = 'Pajak Hotel';
    	$data['labelHarga'] = 'Harga Satuan';
        $data['tautancepat'] = $this->tautanCepat();
    	$data['datadinas'] = $this->Content_model->getDataDinas();
        $data['beritaterpopuler']=$this->Content_model->getBeritaTerpopuler();
        $data['beritaterbaru']=$this->Content_model->getBeritaTerbaru();
    	$data['persentase']=$this->Content_model->getPersentasePajakHotel();
        $data['menu'] =  $this->menu();
    	$data['btnAct'] = "calcpajak";
        $data['content'] = "formPajak";
        $this->load->view('main',$data);
    }

	public function hitungPajakHiburan(){
    	$data['isShowBanner'] = 'failed';
        $data['classNav'] = 'top-nav-collapse';
    	$data['labelSatuan'] = 'Lembar';
    	$data['titlePajak'] = 'Pajak Hiburan';
    	$data['labelHarga'] = 'Harga Satuan';
        $data['tautancepat'] = $this->tautanCepat();
        $data['listhiburan'] = $this->Content_model->getPersentaseHiburan();
    	$data['datadinas'] = $this->Content_model->getDataDinas();
        $data['beritaterpopuler']=$this->Content_model->getBeritaTerpopuler();
        $data['beritaterbaru']=$this->Content_model->getBeritaTerbaru();
    	$data['persentase']=$this->Content_model->getPersentasePajakHiburan();
        $data['menu'] =  $this->menu();
    	$data['btnAct'] = "calcpajak";
        $data['content'] = "formPajak";
        $this->load->view('main',$data);
    }

	public function hitungPajakParkir(){
    	$data['isShowBanner'] = 'failed';
        $data['classNav'] = 'top-nav-collapse';
    	$data['labelSatuan'] = 'Lembar';
    	$data['titlePajak'] = 'Pajak Parkir';
    	$data['btnAct'] = "calcpajak2";
    	$data['isShowOpt'] = 'failed';
    	$data['labelHarga'] = 'Total Penerimaan Pembayaran';
        $data['tautancepat'] = $this->tautanCepat();
    	$data['datadinas'] = $this->Content_model->getDataDinas();
        $data['beritaterpopuler']=$this->Content_model->getBeritaTerpopuler();
        $data['beritaterbaru']=$this->Content_model->getBeritaTerbaru();
    	$data['persentase']=$this->Content_model->getPersentasePajakParkir();
        $data['menu'] =  $this->menu();
        $data['content'] = "formPajak";
        $this->load->view('main',$data);
    }

	public function hitungPajakSarangBurungWalet(){
    	$data['isShowBanner'] = 'failed';
        $data['classNav'] = 'top-nav-collapse';
    	$data['labelSatuan'] = '';
    	$data['titlePajak'] = 'Pajak Sarang Burung Walet';
    	$data['btnAct'] = "calcpajak2";
    	$data['isShowOpt'] = 'failed';
    	$data['labelHarga'] = 'Total Penjualan';
        $data['tautancepat'] = $this->tautanCepat();
    	$data['datadinas'] = $this->Content_model->getDataDinas();
        $data['beritaterpopuler']=$this->Content_model->getBeritaTerpopuler();
        $data['beritaterbaru']=$this->Content_model->getBeritaTerbaru();
    	$data['persentase']=$this->Content_model->getPersentasePajakSarangBurungWalet();
        $data['menu'] =  $this->menu();
        $data['content'] = "formPajak";
        $this->load->view('main',$data);
    }

	public function hitungPajakMBLB(){
    	$data['isShowBanner'] = 'failed';
    	$data['isShowOpt'] = 'false';
        $data['classNav'] = 'top-nav-collapse';
    	$data['labelSatuan'] = 'm<sup>3</sup>';
    	$data['titlePajak'] = 'Pajak Mineral Bukan Logam dan Batuan';
    	$data['btnAct'] = "calcpajak";
    	$data['labelHarga'] = 'Harga Satuan';
        $data['tautancepat'] = $this->tautanCepat();
        $data['listkategorimblb'] = $this->Content_model->getkategoriMBLB();
    	$data['datadinas'] = $this->Content_model->getDataDinas();
        $data['beritaterpopuler']=$this->Content_model->getBeritaTerpopuler();
        $data['beritaterbaru']=$this->Content_model->getBeritaTerbaru();
    	$data['persentase']=$this->Content_model->getPersentasePajakMBLB();
        $data['menu'] =  $this->menu();
        $data['content'] = "formPajak";
        $this->load->view('main',$data);
    }

	public function hitungPajakAirBawahTanah(){
    	$data['isShowBanner'] = 'failed';
        $data['classNav'] = 'top-nav-collapse';
    	$data['tautancepat'] = $this->tautanCepat();
    	$data['datadinas'] = $this->Content_model->getDataDinas();
        $data['beritaterpopuler']=$this->Content_model->getBeritaTerpopuler();
        $data['beritaterbaru']=$this->Content_model->getBeritaTerbaru();
    	$data['persentase']=$this->Content_model->getPersentasePajakAirTanah();
    	$data['peruntukan']=$this->Content_model->getPeruntukan();
        $data['menu'] =  $this->menu();
        $data['content'] = "formPajakAirBawahTanah";
        $this->load->view('main',$data);
    }

	public function pajakAirTanahAct(){
    	$peruntukan = $this->input->post('peruntukan');
        $jmlvolume = $this->input->post('jmlvolume');
        $persentase = $this->input->post('persentase');
    
    	$getMaxNilaiByPeruntukan = $this->Content_model->getMaxNilaiByPeruntukan($peruntukan);
    
    	if($jmlvolume > $getMaxNilaiByPeruntukan->nilaiVolumePeruntukanAkhir){
        	$getNilaiVolume = $this->Content_model->getNilaiVolumeMax($peruntukan,$jmlvolume);
        }else{
        	$getNilaiVolume = $this->Content_model->getNilaiVolume($peruntukan,$jmlvolume);
        }
    
    	
    	$result = $persentase/100*($jmlvolume*$getNilaiVolume->hargaNilaiVolumeAirTanah);
    	echo json_encode(array('hasil'=>$result,'hargapermeter'=>$getNilaiVolume->hargaNilaiVolumeAirTanah));
    }

    public function pajakReklameAct(){
        $luasReklame = $this->input->post('luasreklame');
        $jenisreklame = $this->input->post('jenisreklame');
        $tempatpemasangan = $this->input->post('tempatpemasangan');
        $lamapemasangan = $this->input->post('lamapemasangan');
    	$jmlsisi = $this->input->post('jmlsisi');

        $srplkl = $this->Content_model->getBobotSrPlKl($tempatpemasangan);
        $lr = $this->Content_model->getBobotLr($luasReklame);
        $hargaJenisReklame = $this->Content_model->getPriceJenisReklame($jenisreklame);

        $nsplr = (($srplkl->bobotKomponenSR+$lr->bobotKomponenLR+($srplkl->bobotKomponenPL*$jmlsisi)+$srplkl->bobotKomponenKL)/100)*$srplkl->nilaiKomponenNJT;
        $nsr = $nsplr+($luasReklame*$hargaJenisReklame->nilaiJenisReklame*$lamapemasangan);
        $pajakReklame = (25/100)*$nsr;
        echo "Rp ".nominal($pajakReklame);
    }

    public function showAlbum(){
        $data['isShowBanner'] = 'failed';
        $data['classNav'] = 'top-nav-collapse';
        $data['tautancepat'] = $this->tautanCepat();
    	$data['datadinas'] = $this->Content_model->getDataDinas();
        $data['beritaterbaru']=$this->Content_model->getBeritaTerbaru();
        $data['beritaterpopuler']=$this->Content_model->getBeritaTerpopuler();
        $data['album'] = $this->Content_model->getAlbum();
        $data['menu'] =  $this->menu();
        $data['content'] = "gallery";
        $this->load->view('main',$data);
    }

    public function detailAlbum($slug){
        $data['isShowBanner'] = 'failed';
        $data['classNav'] = 'top-nav-collapse';
        $data['tautancepat'] = $this->tautanCepat();
    	$data['datadinas'] = $this->Content_model->getDataDinas();
        $data['beritaterpopuler']=$this->Content_model->getBeritaTerpopuler();
        $data['beritaterbaru']=$this->Content_model->getBeritaTerbaru();
        $data['foto'] = $this->Content_model->getFoto($slug);
        $data['menu'] =  $this->menu();
        $data['content'] = "foto";
        $this->load->view('main',$data);
    }
}
