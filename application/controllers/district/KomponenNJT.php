<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class KomponenNJT extends CI_Controller {

	public function __construct()
        {
                parent::__construct();
        }

    public function index()
    {
        if($this->session->userdata('logged_in') == true){
            $this->listKomponenNJT();
        }else{
            $this->load->view('login');
        }
    }

    public function listKomponenNJT(){
        $data['list'] = $this->KomponenNJT_model->getListKomponenNJT();
        $data['content'] = "district/KomponenNJT/listKomponenNJT";
        $this->load->view('district/main',$data);
    }

    public function tambahKomponenNJT(){
        $data['komponensr'] = $this->KomponenNJT_model->getAllKomponenSR();
        $data['komponenpl'] = $this->KomponenNJT_model->getAllKomponenPL();
        $data['komponenkl'] = $this->KomponenNJT_model->getAllKomponenKL();
        $data['title'] = 'Tambah Komponen NJT';
        $data['act'] = 'district/KomponenNJT/tambahKomponenNJTAct';
        $data['content'] = "district/KomponenNJT/formKomponenNJT";
        $this->load->view('district/main',$data);
    }

    public function tambahKomponenNJTAct(){
    	$namakomponen = addslashes($this->input->post('namakomponen'));
    	$kategorisr = $this->input->post('kategorisr');
    	$kategoripl = $this->input->post('kategoripl');
    	$kategorikl = $this->input->post('kategorikl');
    	$bobot = str_replace(',','',$this->input->post('bobot'));
        $notifInput = $this->KomponenNJT_model->inputKomponenNJT($namakomponen,$kategorisr,$kategoripl,$kategorikl,$bobot);
        
        if($notifInput){
            $this->session->set_flashdata('message', 'Success');
        }else{
            $this->session->set_flashdata('message', 'Gagal');
        }
		redirect('district/KomponenNJT');
    }
    
    public function editKomponenNJT($id){
        $data['list'] = $this->KomponenNJT_model->getKomponenNJT($id);
    	$data['komponensr'] = $this->KomponenNJT_model->getAllKomponenSR();
        $data['komponenpl'] = $this->KomponenNJT_model->getAllKomponenPL();
        $data['komponenkl'] = $this->KomponenNJT_model->getAllKomponenKL();
        $data['title'] = 'Ubah Komponen NJT';
        $data['act'] = 'district/KomponenNJT/ubahKomponenNJTAct';
        $data['content'] = "district/KomponenNJT/formKomponenNJT";
        $this->load->view('district/main',$data);
    }

    public function ubahKomponenNJTAct(){
        $namakomponen = addslashes($this->input->post('namakomponen'));
        $kategorisr = $this->input->post('kategorisr');
    	$kategoripl = $this->input->post('kategoripl');
    	$kategorikl = $this->input->post('kategorikl');
    	$bobot = str_replace(',','',$this->input->post('bobot'));
        $idkomponen = $this->input->post('idkomponen');
        $notifInput = $this->KomponenNJT_model->updateKomponenNJT($namakomponen,$kategorisr,$kategoripl,$kategorikl,$bobot,$idkomponen);
        
        if($notifInput){
            $this->session->set_flashdata('message', 'Success');
        }else{
            $this->session->set_flashdata('message', 'Gagal');
        }
		redirect('district/KomponenNJT');
    }

    public function deleteKomponenNJT($id){
    	$notifDelete = $this->KomponenNJT_model->deleteKomponenNJT($id);
        if($notifDelete){
            $this->session->set_flashdata('message', 'Success');
        }else{
            $this->session->set_flashdata('message', 'Gagal');
        }
        
		redirect('district/KomponenNJT');        
    }
}

?>