<section id="news-single" class="section-small section-small-single">
      <div class="container">
        <div class="row">
          <div class="col-md-8">
            <h4><?php echo $single->judulPengumuman; ?></h4>
            <?php echo html_entity_decode($single->isiPengumuman); ?>
            <?php
                $fileext = pathinfo(base_url().'filepengumuman/'.$single->file);
                if($fileext['extension'] == 'pdf'){
                    ?>
                    <object
                        data="<?php echo base_url().'filepengumuman/'.$single->file; ?>"
                        type="application/pdf"
                        class="obj-print-preview">
                        </object>
                    <a target="_blank" class="btn btn-primary btn-lg" href="<?php echo base_url().'filepengumuman/'.$single->file; ?>">Download</a>
                    <?php
                }else{
                    ?>
                    <img src="<?php echo base_url().'filepengumuman/'.$single->file; ?>" width="100%">
                    <?php
                }
            ?>
          </div>
          <?php $this->load->view('sidebar'); ?>
        </div>
      </div>
    </section>