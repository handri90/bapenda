<section id="news-single" class="section-small section-small-single">
      <div class="container">
        <div class="row">
          <div class="col-md-8">
            <?php echo html_entity_decode($single->content); ?>
          </div>
          <?php $this->load->view('sidebar'); ?>
        </div>
      </div>
    </section>