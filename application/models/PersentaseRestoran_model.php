<?php
class PersentaseRestoran_model extends CI_Model {

        public function __construct()
        {

        }

        public function getPersentaseRestoran(){
          $query = $this->db->query("select * from pajakrestoran");
    		  return $query->row();
        }

        public function updatePersentaseRestoran($persentase){
          $query = $this->db->query("update pajakrestoran set persentase=".$persentase);
          return $query;
        }
}
?>