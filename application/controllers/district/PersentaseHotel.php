<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PersentaseHotel extends CI_Controller {

	public function __construct()
        {
                parent::__construct();
        }

    public function index()
    {
        if($this->session->userdata('logged_in') == true){
            $this->editPersentaseHotel();
        }else{
            $this->load->view('login');
        }
    }
    
    public function editPersentaseHotel(){
        $data['list'] = $this->PersentaseHotel_model->getPersentaseHotel();
        $data['title'] = 'Ubah Persentase Pajak Hotel';
        $data['labelpersentase'] = 'Pajak Hotel';
        $data['act'] = 'district/PersentaseHotel/ubahPersentaseHotelAct';
        $data['content'] = "district/PersentasePajak/formPersentase";
        $this->load->view('district/main',$data);
    }

    public function ubahPersentaseHotelAct(){
        $persentase = $this->input->post('persentase');
        $notifInput = $this->PersentaseHotel_model->updatePersentaseHotel($persentase);
        
        if($notifInput){
            $this->session->set_flashdata('message', 'Success');
        }else{
            $this->session->set_flashdata('message', 'Gagal');
        }
		redirect('district/PersentaseHotel/editPersentaseHotel');
    }
}

?>