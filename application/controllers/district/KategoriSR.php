<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class KategoriSR extends CI_Controller {

	public function __construct()
        {
                parent::__construct();
        }

    public function index()
    {
        if($this->session->userdata('logged_in') == true){
            $this->listKategoriSR();
        }else{
            $this->load->view('login');
        }
    }

    public function listKategoriSR(){
        $data['list'] = $this->KategoriSR_model->getListKategoriSR();
        $data['content'] = "district/KategoriSR/listKategoriSR";
        $this->load->view('district/main',$data);
    }

    public function tambahKategoriSR(){
        $data['title'] = 'Tambah Kategori Sasaran Reklame';
        $data['act'] = 'district/KategoriSR/tambahKategoriSRAct';
        $data['content'] = "district/KategoriSR/formKategoriSR";
        $this->load->view('district/main',$data);
    }

    public function tambahKategoriSRAct(){
    	$namakategori = $this->input->post('namakategori');
        $notifInput = $this->KategoriSR_model->inputKategoriSR($namakategori);
        
        if($notifInput){
            $this->session->set_flashdata('message', 'Success');
        }else{
            $this->session->set_flashdata('message', 'Gagal');
        }
		redirect('district/KategoriSR');
    }
    
    public function editKategoriSR($id){
        $data['list'] = $this->KategoriSR_model->getKategoriSR($id);
        $data['title'] = 'Ubah Kategori Sasaran Reklame';
        $data['act'] = 'district/KategoriSR/ubahKategoriSRAct';
        $data['content'] = "district/KategoriSR/formKategoriSR";
        $this->load->view('district/main',$data);
    }

    public function ubahKategoriSRAct(){
        $namakategori = $this->input->post('namakategori');
        $idkategori = $this->input->post('idkategori');
        $notifInput = $this->KategoriSR_model->updateKategoriSR($namakategori,$idkategori);
        
        if($notifInput){
            $this->session->set_flashdata('message', 'Success');
        }else{
            $this->session->set_flashdata('message', 'Gagal');
        }
		redirect('district/KategoriSR');
    }

    public function deleteKategoriSR($id){
    	$notifDelete = $this->KategoriSR_model->deleteKategoriSR($id);
        if($notifDelete){
            $this->session->set_flashdata('message', 'Success');
        }else{
            $this->session->set_flashdata('message', 'Gagal');
        }
        
		redirect('district/KategoriSR');        
    }
}

?>