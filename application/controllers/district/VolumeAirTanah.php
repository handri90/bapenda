<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class VolumeAirTanah extends CI_Controller {

	public function __construct()
        {
                parent::__construct();
        }

    public function index()
    {
        if($this->session->userdata('logged_in') == true){
            $this->listVolumeAirTanah();
        }else{
            $this->load->view('login');
        }
    }

    public function listVolumeAirTanah(){
        $data['list'] = $this->VolumeAirTanah_model->getListVolumeAirTanah();
        $data['content'] = "district/VolumeAirTanah/listVolumeAirTanah";
        $this->load->view('district/main',$data);
    }

    public function tambahVolumeAirTanah(){
        $data['title'] = 'Tambah Volume Air Tanah';
        $data['act'] = 'district/VolumeAirTanah/tambahVolumeAirTanahAct';
        $data['content'] = "district/VolumeAirTanah/formVolumeAirTanah";
        $this->load->view('district/main',$data);
    }

    public function tambahVolumeAirTanahAct(){
        $volumeawal = $this->input->post('volumeawal');
        $volumeakhir = $this->input->post('volumeakhir');
        $takterhingga = $this->input->post('takterhingga');
        if(!empty($takterhingga)){
            $volumeakhir = 'NULL';
        }
        $notifInput = $this->VolumeAirTanah_model->inputVolume($volumeawal,$volumeakhir);
        
        if($notifInput){
            $this->session->set_flashdata('message', 'Success');
        }else{
            $this->session->set_flashdata('message', 'Gagal');
        }
		redirect('district/VolumeAirTanah');
    }
    
    public function editVolumeAirTanah($id){
        $data['list'] = $this->VolumeAirTanah_model->getVolumeAirTanah($id);
        $data['title'] = 'Ubah Volume Air Tanah';
        $data['act'] = 'district/VolumeAirTanah/ubahVolumeAirTanahAct';
        $data['content'] = "district/VolumeAirTanah/formVolumeAirTanah";
        $this->load->view('district/main',$data);
    }

    public function ubahVolumeAirTanahAct(){
        $volumeawal = $this->input->post('volumeawal');
        $volumeakhir = $this->input->post('volumeakhir');
         $idvolume = $this->input->post('idvolume');
         
         $notifInput = $this->VolumeAirTanah_model->updateVolume($volumeawal,$volumeakhir,$idvolume);
        
        if($notifInput){
            $this->session->set_flashdata('message', 'Success');
        }else{
            $this->session->set_flashdata('message', 'Gagal');
        }
		redirect('district/VolumeAirTanah');
    }

    public function deleteVolumeAirTanah($id){
    	$notifDelete = $this->VolumeAirTanah_model->deleteVolume($id);
        if($notifDelete){
            $this->session->set_flashdata('message', 'Success');
        }else{
            $this->session->set_flashdata('message', 'Gagal');
        }
        
		redirect('district/VolumeAirTanah');        
    }
}

?>