<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengumuman extends CI_Controller {

	public function __construct()
        {
                parent::__construct();
        }

    public function index()
    {
        if($this->session->userdata('logged_in') == true){
            $this->listPengumuman();
        }else{
            $this->load->view('login');
        }
    }

    public function listPengumuman(){
        $data['list'] = $this->Pengumuman_model->getListPengumuman();
        $data['content'] = "district/Pengumuman/listPengumuman";
        $this->load->view('district/main',$data);
    }

    public function tambahPengumuman(){
        $data['title'] = 'Tambah Pengumuman';
        $data['act'] = 'district/Pengumuman/tambahPengumumanAct';
        $data['content'] = "district/Pengumuman/formPengumuman";
        $this->load->view('district/main',$data);
    }

    public function tambahPengumumanAct(){
        $judul = $this->input->post('judul');
    	$isi = htmlentities($this->input->post('isi'));
    	$iduser = $this->session->userdata('user_id');
        //  $tanggalKegiatan = $this->input->post('tanggalkegiatan');     
        $notifInput = '';

            $date = date('Y:m:d H:i:s');
            $data['act'] = 'district/Pengumuman/tambahPengumumanAct';
            $slug = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $judul)));
            $cekJudul = $this->Pengumuman_model->cekSlugPengumuman($judul);
            if($cekJudul->jumlah > 0){
                $slug .= $cekJudul->jumlah;
            }
            $this->upload->initialize($this->set_upload_options());
            $this->upload->do_upload();

            $file_data = $this->upload->data();
            $data['file_dir'] = $file_data['file_name'];
            $notifInput = $this->Pengumuman_model->inputPengumuman($judul,$isi,$iduser,$slug,$data['file_dir']);
        if($notifInput){
            $this->session->set_flashdata('message', 'Success');
        }else{
            $this->session->set_flashdata('message', 'Gagal');
        }
    	
		redirect('district/Pengumuman');
    }

    private function set_upload_options()
	{   
	    //upload an image options
	    $filename = md5(uniqid(rand(), true));
	    $config = array();
	    $config['upload_path'] = 'filepengumuman';
	    $config['allowed_types'] = 'gif|jpg|png|jpeg|pdf';
	    $config['file_name'] = $filename;

	    return $config;
    }

    public function editPengumuman($id){
        $data['list'] = $this->Pengumuman_model->getDataPengumumanById($id);
        $data['title'] = 'Ubah Pengumuman';
        $data['act'] = 'district/Pengumuman/ubahPengumumanAct';
        $data['content'] = "district/Pengumuman/formPengumuman";
        $this->load->view('district/main',$data);
    }

    public function ubahPengumumanAct(){
        $judul = $this->input->post('judul');
    	$isi = htmlentities($this->input->post('isi'));
    	$iduser = $this->session->userdata('user_id');
        $idPengumuman = $this->input->post('idPengumuman');
        //  $tanggalKegiatan = $this->input->post('tanggalkegiatan');     
         $oldFile = $this->input->post('oldFile');     
        $notifInput = '';

            $date = date('Y:m:d H:i:s');
            $data['act'] = 'district/Pengumuman/tambahPengumumanAct';
            $slug = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $judul)));
            $cekSlug = $this->Pengumuman_model->cekSlugPengumumanUbah($slug,$idPengumuman);
            if($cekSlug->jumlah > 0){
                $slug .= $cekSlug->jumlah;
            }
            if(!empty($_FILES['userfile']['name'])){
                $this->upload->initialize($this->set_upload_options());
                $this->upload->do_upload();

                $file_data = $this->upload->data();
                $data['file_dir'] = $file_data['file_name'];
                unlink('filepengumuman/'.$oldFile);
            }else{
                $data['file_dir'] = $oldFile;
            }
            $notifInput = $this->Pengumuman_model->ubahPengumuman($judul,$isi,$slug,$data['file_dir'],$idPengumuman);
        if($notifInput){
            $this->session->set_flashdata('message', 'Success');
        }else{
            $this->session->set_flashdata('message', 'Gagal');
        }
    	
		redirect('district/Pengumuman');
    }

    public function deletePengumuman($id){
        $input = $this->Pengumuman_model->deletePengumuman($id);
        redirect('district/Pengumuman');
    }
}

?>