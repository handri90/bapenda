<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class KepalaDaerah extends CI_Controller {

	public function __construct()
        {
                parent::__construct();
        }

    public function index()
    {
        if($this->session->userdata('logged_in') == true){
            $this->listKepalaDaerah();
        }else{
            $this->load->view('login');
        }
    }

    public function listKepalaDaerah(){
        $data['list'] = $this->KepalaDaerah_model->getListKepalaDaerah();
        $data['content'] = "district/KepalaDaerah/listKepalaDaerah";
        $this->load->view('district/main',$data);
    }

    public function tambahKepalaDaerah(){
        $data['title'] = 'Tambah Kepala Daerah';
        $data['act'] = 'district/KepalaDaerah/tambahKepalaDaerahAct';
        $data['content'] = "district/KepalaDaerah/formKepalaDaerah";
        $this->load->view('district/main',$data);
    }

    public function tambahKepalaDaerahAct(){
    	$nama = $this->input->post('nama');
        $jabatan = $this->input->post('jabatan');     
        $ordermenu = $this->input->post("ordermenu");
        $data['act'] = 'district/KepalaDaerah/tambahKepalaDaerahAct';
        $this->upload->initialize($this->set_upload_options());
        $this->upload->do_upload();

        $file_data = $this->upload->data();
        $data['file_dir'] = $file_data['file_name'];
        $notifInput = $this->KepalaDaerah_model->inputKepalaDaerah($nama,$jabatan,$data['file_dir'],$ordermenu);
        if($notifInput){
            $this->session->set_flashdata('message', 'Success');
        }else{
            $this->session->set_flashdata('message', 'Gagal');
        }
    	
		redirect('district/KepalaDaerah');
    }

    private function set_upload_options()
	{   
	    //upload an image options
	    $filename = md5(uniqid(rand(), true));
	    $config = array();
	    $config['upload_path'] = 'kepaladaerah';
	    $config['allowed_types'] = 'gif|jpg|png|jpeg';
	    $config['file_name'] = $filename;

	    return $config;
    }
    
    public function editKepalaDaerah($id){
        $data['list'] = $this->KepalaDaerah_model->getDataKepalaDaerahById($id);
        $data['title'] = 'Ubah Kepala Daerah';
        $data['act'] = 'district/KepalaDaerah/ubahKepalaDaerahAct';
        $data['content'] = "district/KepalaDaerah/formKepalaDaerah";
        $this->load->view('district/main',$data);
    }

    public function ubahKepalaDaerahAct(){
        $nama = $this->input->post('nama');
        $jabatan = $this->input->post('jabatan');
 	    $idKepalaDaerah = $this->input->post('idkd');
 	    $tempSavedFile = $this->input->post('tempSavedFile');
         $tempFile = $this->input->post('tempFile');
         $ordermenu = $this->input->post("ordermenu");
        $notifInput = '';

        $data['act'] = 'district/KepalaDaerah/tambahKepalaDaerahAct';
        if($tempFile != ''){
            $this->upload->initialize($this->set_upload_options());
            $this->upload->do_upload();

            $file_data = $this->upload->data();
            $data['file_dir'] = $file_data['file_name'];
            unlink("kepaladaerah/".$tempSavedFile);
        }else{
            $data['file_dir'] = $tempSavedFile;
        }
        $notifInput = $this->KepalaDaerah_model->updateKepalaDaerah($nama,$jabatan,$data['file_dir'],$ordermenu,$idKepalaDaerah);
        
        if($notifInput){
            $this->session->set_flashdata('message', 'Success');
        }else{
            $this->session->set_flashdata('message', 'Gagal');
        }
    	
		redirect('district/KepalaDaerah');
    }

    public function deleteKepalaDaerah($id){
    	$notifDelete = $this->KepalaDaerah_model->deleteKepalaDaerah($id);
        if($notifDelete){
            $this->session->set_flashdata('message', 'Success');
        }else{
            $this->session->set_flashdata('message', 'Gagal');
        }
        
		redirect('district/KepalaDaerah');        
    }

    public function changeStatusKepalaDaerah($id){
        $notifChange = $this->KepalaDaerah_model->changeStatusKepalaDaerah($id);
        if($notifChange){
            $this->session->set_flashdata('message', 'Success');
        }else{
            $this->session->set_flashdata('message', 'Gagal');
        }
        
		redirect('district/KepalaDaerah');  
    }
}

?>