<?php
class KategoriFilePublikasi_model extends CI_Model {

        public function __construct()
        {

        }

        public function getListKategoriFilePublikasi(){
                $query = $this->db->query("select * from kategorifilepublikasi");
                return $query->result_array();
        }
        
        public function inputKategoriFilePublikasi($namakategori,$slug){
            $query = $this->db->query("insert into kategorifilepublikasi (namaKategoriFilePublikasi,slug) values ('".$namakategori."','".$slug."')");
    		return $query;
        }

        public function getKategoriFilePublikasiById($id){
          $query = $this->db->query("select * from kategorifilepublikasi where idKategoriFilePublikasi = ".$id);
    		  return $query->row();
        }

        public function updateKategoriFilePublikasi($namakategori,$idkategori,$slug){
          $query = $this->db->query("update kategorifilepublikasi set namaKategoriFilePublikasi='".$namakategori."',slug='".$slug."' where idKategoriFilePublikasi=".$idkategori);
          return $query;
        }

        public function deleteKategoriFilePublikasi($id){
          $query = $this->db->query("delete from kategorifilepublikasi where idKategoriFilePublikasi = ".$id);
          return $query;
        }

        public function cekSlugKategori($namakategori){
          $query = $this->db->query("select count(*) as jumlah from kategorifilepublikasi where namaKategoriFilePublikasi = '".$namakategori."'");
          return $query->row();
        }

        public function cekSlugKategoriUpdate($slug,$id){
          $query = $this->db->query("select count(*) as jumlah from kategorifilepublikasi where slug = '".$slug."' and idKategoriFilePublikasi!=".$id);
          return $query->row();
        }
}
?>