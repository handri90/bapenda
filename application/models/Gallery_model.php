<?php
class Gallery_model extends CI_Model {

        public function __construct()
        {

        }

        public function getListGallery(){
                $query = $this->db->query("select idAlbum,namaAlbum,count(albumIdAlbum) as jmlFoto from albumfoto inner join foto on idAlbum=albumIdAlbum group by idAlbum");
                return $query->result_array();
        }

        public function cekSlugAlbumFoto($judul)
		{
            $query = $this->db->query("select count(*) as jumlah from albumfoto where namaAlbum = '".$judul."'");
    		return $query->row();
        }
        
        public function inputAlbumfoto($judul,$slug,$date,$iduser){
            $query = $this->db->query("insert into albumfoto (namaAlbum,tanggalUpload,userId,slug) values ('".$judul."','".$date."',".$iduser.",'".$slug."')");
    		return $this->db->insert_id();
        }

        public function inputImage($notifInput,$file,$caption){
            $query = $this->db->query("insert into foto (filename,albumIdAlbum,caption) values ('".$file."',".$notifInput.",'".$caption."')");
    		return $query;
        }

        public function getDataGalleryById($id){
          $query = $this->db->query("select idAlbum,namaAlbum,group_concat(filename order by idFoto separator '|') as filefoto,group_concat(caption order by idFoto separator '|') as caption,group_concat(idFoto order by idFoto separator '|') as idFoto from albumfoto inner join foto on idAlbum=albumIdAlbum where idAlbum = ".$id." group by idAlbum");
    		  return $query->row();
        }

        public function updateAlbumfoto($judul,$slug,$idalbum){
          $query = $this->db->query("update albumfoto set namaAlbum='".$judul."',slug='".$slug."' where idAlbum=".$idalbum);
          return $query;
        }

        public function updateCaptionFotoSaved($idFoto,$caption){
          $query = $this->db->query("update foto set caption='".$caption."' where idFoto=".$idFoto);
          return $query;
        }

        public function deleteGallery($id){
          $query = $this->db->query("select filename from foto where albumIdAlbum = ".$id);
          $result = $query->result_array();
          for($i=0;$i<count($result);$i++){
            unlink("filegallery/".$result[$i]['filename']);
          }
          $query = $this->db->query("delete from foto where albumIdAlbum = ".$id);
          $query = $this->db->query("delete from albumfoto where idAlbum = ".$id);
          return $query;
        }

        public function deleteImage($idfoto){
          $query = $this->db->query("select filename from foto where idFoto = ".$idfoto);
          $result = $query->row();
          unlink("filegallery/".$result->filename);
          $query = $this->db->query("delete from foto where idFoto = ".$idfoto);
          return $query;
        }
        
        public function cekSlugAlbumFotoUpdate($slug,$idalbum){
          $query = $this->db->query("select count(*) as jumlah from albumfoto where slug = '".$slug."' and idAlbum !=".$idalbum);
	        return $query->row();
        }
}
?>