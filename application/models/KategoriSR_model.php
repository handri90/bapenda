<?php
class KategoriSR_model extends CI_Model {

        public function __construct()
        {

        }

        public function getListKategoriSR(){
                $query = $this->db->query("select * from kategorisr");
                return $query->result_array();
        }
        
        public function inputKategoriSR($namakategori){
            $query = $this->db->query("insert into kategorisr (labelKategoriSR) values ('".$namakategori."')");
    		return $query;
        }

        public function getKategoriSR($id){
          $query = $this->db->query("select * from kategorisr where idKategoriSR = ".$id);
    		  return $query->row();
        }

        public function updateKategoriSR($namakategori,$idkategori){
          $query = $this->db->query("update kategorisr set labelKategoriSR='".$namakategori."' where idKategoriSR=".$idkategori);
          return $query;
        }

        public function deleteKategoriSR($id){
          $query = $this->db->query("delete from kategorisr where idKategoriSR = ".$id);
          return $query;
        }
}
?>