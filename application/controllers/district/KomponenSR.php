<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class KomponenSR extends CI_Controller {

	public function __construct()
        {
                parent::__construct();
        }

    public function index()
    {
        if($this->session->userdata('logged_in') == true){
            $this->listKomponenSR();
        }else{
            $this->load->view('login');
        }
    }

    public function listKomponenSR(){
        $data['list'] = $this->KomponenSR_model->getListKomponenSR();
        $data['content'] = "district/KomponenSR/listKomponenSR";
        $this->load->view('district/main',$data);
    }

    public function tambahKomponenSR(){
        // $data['kategori'] = $this->KategoriSR_model->getListKategoriSR();
        $data['title'] = 'Tambah Komponen Sasaran Reklame';
        $data['act'] = 'district/KomponenSR/tambahKomponenSRAct';
        $data['content'] = "district/KomponenSR/formKomponenSR";
        $this->load->view('district/main',$data);
    }

    public function tambahKomponenSRAct(){
    	// $kategori = $this->input->post('kategori');
    	$namakomponen = $this->input->post('namakomponen');
    	$bobot = $this->input->post('bobot');
        $notifInput = $this->KomponenSR_model->inputKomponenSR($namakomponen,$bobot);
        
        if($notifInput){
            $this->session->set_flashdata('message', 'Success');
        }else{
            $this->session->set_flashdata('message', 'Gagal');
        }
		redirect('district/KomponenSR');
    }
    
    public function editKomponenSR($id){
        // $data['kategori'] = $this->KategoriSR_model->getListKategoriSR();
        $data['list'] = $this->KomponenSR_model->getKomponenSR($id);
        $data['title'] = 'Ubah Komponen Sasaran Reklame';
        $data['act'] = 'district/KomponenSR/ubahKomponenSRAct';
        $data['content'] = "district/KomponenSR/formKomponenSR";
        $this->load->view('district/main',$data);
    }

    public function ubahKomponenSRAct(){
        // $kategori = $this->input->post('kategori');
    	$namakomponen = $this->input->post('namakomponen');
    	$bobot = $this->input->post('bobot');
        $idkomponen = $this->input->post('idkomponen');
        $notifInput = $this->KomponenSR_model->updateKomponenSR($namakomponen,$bobot,$idkomponen);
        
        if($notifInput){
            $this->session->set_flashdata('message', 'Success');
        }else{
            $this->session->set_flashdata('message', 'Gagal');
        }
		redirect('district/KomponenSR');
    }

    public function deleteKomponenSR($id){
    	$notifDelete = $this->KomponenSR_model->deleteKomponenSR($id);
        if($notifDelete){
            $this->session->set_flashdata('message', 'Success');
        }else{
            $this->session->set_flashdata('message', 'Gagal');
        }
        
		redirect('district/KomponenSR');        
    }
}

?>