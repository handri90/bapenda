<div class="col-md-3 col-md-3-universal col-md-offset-universal">
            <p class="text-center"><img src="<?php echo base_url().'assets/img/logofooter.png'; ?>" ></p>
            <p><?php echo $datadinas->namaDinas; ?></p>
            <h6><i class="fa fa-map-marker fa-fw fa-lg"></i> <?php echo $datadinas->alamatDinas; ?>
            </h6>
            <h6><i class="fa fa-envelope fa-fw fa-lg"></i> <?php echo $datadinas->emailDinas; ?>
            </h6>
            <h6><i class="fa fa-phone fa-fw fa-lg"></i> <?php echo $datadinas->telpDinas; ?>
            </h6>
            <ul role="tablist" class="nav nav-tabs">
              <li role="presentation" class="active"><a href="#tab1" aria-controls="tab1" role="tab" data-toggle="tab">Populer</a></li>
              <li role="presentation"><a href="#tab2" aria-controls="tab2" role="tab" data-toggle="tab">Terbaru</a></li>
            </ul>
            <!-- Tab panes-->
            <div class="tab-content">
              <div id="tab1" role="tabpanel" class="tab-pane fade in active">
                  <?php
                  for($i=0;$i<count($beritaterpopuler);$i++){
                      ?>
                      <div class="item-tab-sidebar">
                        <img src="<?php echo base_url().'fotoberita/'.$beritaterpopuler[$i]['foto']; ?>" >
                        <a href="<?php echo base_url().'berita/'.$beritaterpopuler[$i]['slugBerita']; ?>"><?php echo $beritaterpopuler[$i]['judul']; ?></a>
                        <span class="clearqu"></span>
                    </div>
                      <?php
                  }
                  ?>
              </div>
              <div id="tab2" role="tabpanel" class="tab-pane fade">
                <?php
                  for($i=0;$i<count($beritaterbaru);$i++){
                      ?>
                      <div class="item-tab-sidebar">
                        <img src="<?php echo base_url().'fotoberita/'.$beritaterbaru[$i]['foto']; ?>" >
                        <a href="<?php echo base_url().'berita/'.$beritaterbaru[$i]['slugBerita']; ?>"><?php echo $beritaterbaru[$i]['judul']; ?></a>
                        <span class="clearqu"></span>
                    </div>
                      <?php
                  }
                  ?>
              </div>
            </div>
          </div>