<?php
class Berita_model extends CI_Model {

        public function __construct()
        {

        }

        public function getListBerita(){
                $query = $this->db->query("select idBerita,judul,content,status,readCount,tanggalPublish,tanggalModified,slugBerita,userId,foto,DATE_FORMAT(tanggalBerita, '%Y-%m-%d') as tanggalBerita from berita order by idBerita desc");
                return $query->result_array();
        }

        public function cekSlugInputBerita($slug)
		{
            $query = $this->db->query("select count(*) as jumlah from berita where judul = '".$slug."'");
    		return $query->row();
        }
        
        public function inputBerita($judul,$isi,$iduser,$date,$slug,$tanggalKegiatan,$filefoto,$captionGambar){
            $query = $this->db->query("insert into berita (judul,content,tanggalPublish,tanggalBerita,slugBerita,userId,foto,caption) values ('".$judul."','".$isi."','".$date."',concat(STR_TO_DATE('".$tanggalKegiatan."', '%m/%d/%Y'),' ',curtime()),'".$slug."',".$iduser.",'".$filefoto."','".$captionGambar."')");
    		return $query;
        }

        public function getDataBeritaById($id){
          $query = $this->db->query("select idBerita,judul,content,status,readCount,tanggalPublish,tanggalModified,slugBerita,userId,foto,DATE_FORMAT(tanggalBerita, '%m/%d/%Y') as tanggalBerita,caption from berita where idBerita = ".$id);
    		  return $query->row();
        }

        public function updateBerita($judul,$isi,$slug,$tanggalKegiatan,$captionGambar,$foto,$idberita){
          $query = $this->db->query("update berita set judul='".$judul."',content='".$isi."',tanggalModified=now(),slugBerita='".$slug."',foto='".$foto."',tanggalBerita=concat(STR_TO_DATE('".$tanggalKegiatan."', '%m/%d/%Y')),caption='".$captionGambar."' where idBerita=".$idberita);
          return $query;
        }

        public function deleteBerita($id){
          $query = $this->db->query("select foto from berita where idBerita = ".$id);
          $result = $query->row();
          unlink("fotoberita/".$result->foto);
          $query = $this->db->query("delete from berita where idBerita = ".$id);
          return $query;
        }

        public function cekSlugUpdate($slug,$idBerita)
				{
			        $query = $this->db->query("select count(*) as jumlah from berita where slugBerita = '".$slug."' and idBerita !=".$idBerita);
	        return $query->row();
				}
}
?>