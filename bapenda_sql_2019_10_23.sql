-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Oct 23, 2019 at 06:25 PM
-- Server version: 5.5.60-MariaDB
-- PHP Version: 7.1.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bapenda`
--

-- --------------------------------------------------------

--
-- Table structure for table `albumfoto`
--

CREATE TABLE `albumfoto` (
  `idAlbum` int(11) UNSIGNED NOT NULL,
  `namaAlbum` text,
  `tanggalUpload` date DEFAULT NULL,
  `userId` int(11) UNSIGNED NOT NULL,
  `slug` text NOT NULL,
  `status` enum('1','0') NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `albumfoto`
--

INSERT INTO `albumfoto` (`idAlbum`, `namaAlbum`, `tanggalUpload`, `userId`, `slug`, `status`) VALUES
(8, 'Album1', '2019-08-05', 1, 'album1', '0'),
(9, 'Album1', '2019-08-05', 1, 'album11', '0'),
(10, 'Album 1', '2019-08-05', 1, 'album-1', '0'),
(11, 'Keluarga Besar Badan Pendapatan Daerah Kotawaringin Barat', '2019-08-06', 4, 'keluarga-besar-badan-pendapatan-daerah-kotawaringin-barat', '0'),
(12, 'Keluarga Besar Badan Pendapatan', '2019-08-06', 4, 'keluarga-besar-badan-pendapatan', '0'),
(13, 'Keluarga Besar Badan Pendapatan', '2019-08-06', 4, 'keluarga-besar-badan-pendapatan1', '0'),
(14, 'Keluarga Besar Badan Pendapatan', '2019-08-06', 4, 'keluarga-besar-badan-pendapatan2', '0'),
(15, 'Keluarga Besar Badan Pendapatan', '2019-08-06', 4, 'keluarga-besar-badan-pendapatan3', '0'),
(16, 'Keluarga Besar Badan Pendapatan', '2019-08-06', 4, 'keluarga-besar-badan-pendapatan4', '0'),
(17, 'Keluarga Besar Badan Pendapatan', '2019-08-06', 4, 'keluarga-besar-badan-pendapatan5', '0'),
(18, 'Album 1', '2019-08-06', 1, 'album-11', '0'),
(19, 'Album 2', '2019-08-06', 1, 'album-2', '0'),
(21, 'Keluarga Besar Badan Pendapatan', '2019-08-07', 4, 'keluarga-besar-badan-pendapatan1', '0'),
(22, 'Keluarga Besar Badan Pendapatan', '2019-08-07', 4, 'keluarga-besar-badan-pendapatan1', '0');

-- --------------------------------------------------------

--
-- Table structure for table `bannerhome`
--

CREATE TABLE `bannerhome` (
  `idBannerHome` int(11) UNSIGNED NOT NULL,
  `filename` varchar(100) DEFAULT NULL,
  `status` enum('1','0') DEFAULT '0',
  `orderbanner` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bannerhome`
--

INSERT INTO `bannerhome` (`idBannerHome`, `filename`, `status`, `orderbanner`) VALUES
(5, '6fe38e74efbe02d283689e45bd2b378c.jpeg', '0', 1),
(6, '9f1388639f9b4587985fc89f295ba110.jpeg', '0', 2),
(7, '0e352195ccc3afd705bd7b35566aaa35.png', '1', 3),
(8, '439a94b0d092de03bd3d1fba673b13da.png', '1', 4),
(12, 'c99f43c721b408c8b1c74f0ef9ebe940.jpeg', '0', 1);

-- --------------------------------------------------------

--
-- Table structure for table `berita`
--

CREATE TABLE `berita` (
  `idBerita` int(11) UNSIGNED NOT NULL,
  `judul` varchar(200) DEFAULT NULL,
  `content` text,
  `status` enum('1','0') DEFAULT '0',
  `readCount` int(11) DEFAULT NULL,
  `tanggalPublish` datetime DEFAULT NULL,
  `tanggalModified` datetime DEFAULT NULL,
  `slugBerita` varchar(300) DEFAULT NULL,
  `userId` int(11) UNSIGNED NOT NULL,
  `foto` varchar(200) DEFAULT NULL,
  `tanggalBerita` datetime DEFAULT NULL,
  `caption` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `berita`
--

INSERT INTO `berita` (`idBerita`, `judul`, `content`, `status`, `readCount`, `tanggalPublish`, `tanggalModified`, `slugBerita`, `userId`, `foto`, `tanggalBerita`, `caption`) VALUES
(5, 'Kejar Potensi Pajak, Bapenda Kobar Lakukan Pendataan Wajib Pajak Baru', '&lt;p style=&quot;margin-left:0cm; margin-right:0cm&quot;&gt;&lt;strong&gt;Bapenda Kobar&lt;/strong&gt; - &amp;nbsp;Bertempat di lokasi perkebunan kelapa sawit PT. Agro Menara Rahmat, Desa Runtu, Kecamatan Arut Selatan, pada Rabu (17/7), Badan Pendapatan Daerah (Bapenda) Kabupaten Kobar melaksanakan koordinasi dan konfirmasi mengenai objek pajak daerah, yaitu PBB P-2 (Pajak Daerah) dan penjelasan objek pajak yang termasuk PBB P-3 (Pajak Pusat yang meliputi Perhutanan, Perkebunan dan Pertambangan).&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0cm; margin-right:0cm&quot;&gt;Dalam kegiatan tersebut pihak PT. Agro Menara Rachmat diwakili oleh Patra Rika Agustian selaku Manager Produksi menjelaskan bahwa ada 2 unit usaha yang dikelola yaitu Perkebunan Kelapa Sawit (AMR 1) dan Usaha Peternakan Sapi (AMR 2) yang berada pada Izin Usaha Perkebunan Budidaya (Integrasi Sawit dan Sapi).&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0cm; margin-right:0cm&quot;&gt;&amp;ldquo;Usaha peternakan sapi PT. Agro Menara Rachmat berdasarkan Surat Izin Usaha Perdagangan (SIUP) Besar Nomor : 79/AC.1.7/31.75/1.824.27/e/2018 tanggal 12 Juli 2018 dan Izin Mendirikan Bangunan (IMB) Nomor : 066/0055/DPM-PTSP/III/IMB/2017 tanggal 9 Mei 2017 dengan luas bangunan 19.743,85 M&amp;sup2; dan luas tanah 7.448,56 Ha,&amp;rdquo; jelas Patra.&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0cm; margin-right:0cm&quot;&gt;Kepala Bapenda Kobar Molta Dena, SE, MA pada kesempatan ini sekaligus melakukan peninjauan ke lapangan usaha peternakan PT. Agro Menara Rachmat yang terdapat bangunan antara lain perkantoran, gudang, rumah timbang, 4 buah kandang sapi yang permanen dengan konstruksi baja ringan, kandang biasa dan lain-lain.&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0cm; margin-right:0cm&quot;&gt;&amp;ldquo;Usaha lokasi peternakan sapi PT. Agro Menara Rachmat ini merupakan potensi untuk dijadikan objek pajak PBB-P2 dan selain itu terdapat juga pengunaan air tanah dari sumur bor sehingga dapat menjadi potensi penerimaan pajak dari Pajak Air Tanah,&amp;rdquo; kata Molta.&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0cm; margin-right:0cm&quot;&gt;Hasil peninjauan lapangan ini akan disampaikan ke Kantor Pusat PT. Agro Menara Rachmat di Jakarta sebagai bahan tindak lanjut, potensi mana saja yang akan didaftarkan menjadi objek pajak daerah. &lt;strong&gt;(gino sriyanto/bapenda kobar)&lt;/strong&gt;&lt;/p&gt;\r\n', '0', 15, '2019-07-31 16:23:04', NULL, 'kejar-potensi-pajak-bapenda-kobar-lakukan-pendataan-wajib-pajak-baru', 1, '06e53ec6497074c10c82f84694755fae.jpg', '2019-07-23 16:23:04', 'Kepala Bapenda Kobar Molta Dena, SE, MA didampingi Kepala Bidang Pendaftaran, Pendataan, Pengolahan Data dan Dokumentasi Ir. Agus Abdullah, saat peninjauan lapangan usaha peternakan PT. Agro Menara Rachmat, Desa Runtu, Kec. Arsel, Rabu (17/7/2019). (bapenda kobar)'),
(6, 'Optimalisasi PBB, Bapenda Ikut Serta Kegiatan BBGRM Ke-16 dan HKG Ke- 47', '&lt;p style=&quot;margin-left:0cm; margin-right:0cm; text-align:justify&quot;&gt;&lt;strong&gt;Bapenda Kobar -&lt;/strong&gt; &amp;nbsp;Dalam Rangka Optimalisasi PBB, Badan Pendapatan Daerah (Bapenda) Kabupaten Kotawaringin Barat (Kobar) ikut serta membuka stand pembayaran PBB pada acara Peringatan Bulan Bhakti Gotong Royong (BB-GRM) ke-16 dan Hari Kesatuan Gerak PKK (HKG) ke- 47 Tingkat Kobar di Desa Kumpai Batu Atas, Kecamatan Arut Selatan, pada Kamis (18/7).&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0cm; margin-right:0cm&quot;&gt;Kegiatan ini dihadiri oleh Bupati, Wakil Bupati Kobar, Ketua TP-PKK Kabupaten Kobar, Unsur Forkopimda, Sekda Kobar,kepala SKPD serta organisasi wanita se-Kobar. Dalam kegiatan kali ini, tema yang diusung adalah &amp;ldquo;Aktualisasi Gerakan Membangun Kotawaringin Barat Menuju Kejayaan Dengan Kerja Nyata dan Ikhlas&amp;rdquo;.&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0cm; margin-right:0cm&quot;&gt;Sebelum kegiatan tersebut Kepala Bapenda Kobar Molta Dena, SE, MA telah menyampaikan Pengumuman Nomor : 973/354/BAPENDA.IV tentang Pelaksanaan Kegiatan Penerimaan Pembayaran PBB-P2 Tahun 2019 di Desa Kumpai Batu Atas Kecamatan Arut Selatan yang ditujukan kepada masyarakat Desa Kumpai Batu Atas, Desa Kumpai Batu Bawah, Desa Tanjung Terantang, Desa Tanjung Putri dan Desa Pasir Panjang.&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0cm; margin-right:0cm&quot;&gt;&amp;ldquo;Kegiatan Penerimaan Pembayaran PBB-P2 ini melibatkan Bank Persepsi untuk memudahkan masyarakat dalam membayar kewajibannya sehingga tidak perlu jauh-jauh ke Kota Pangkalan Bun dan pada akhirnya akan menaikan jumlah pendapatan yang diterima daerah,&amp;rdquo; kata Molta.&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0cm; margin-right:0cm&quot;&gt;Setelah selesai pembukaan, Bupati Kobar Hj. Nurhidayah, SH, MH berkunjung ke stand pembayaran PBB-P2 Bapenda Kobar, diikuti oleh masyarakat yang membayar kewajiban pajaknya.&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0cm; margin-right:0cm; text-align:justify&quot;&gt;Kegiatan serupa diagendakan berlanjut menyasar seluruh Kelurahan dan Desa lainnya yang dipandang perlu dilakukan upaya jemput bola. &lt;strong&gt;(bapenda kobar)&lt;/strong&gt;&lt;/p&gt;\r\n', '0', 10, '2019-07-31 16:24:18', '2019-07-31 16:29:57', 'optimalisasi-pbb-bapenda-ikut-serta-kegiatan-bbgrm-ke-16-dan-hkg-ke--47', 1, '1d632fe1cd51295a3de4ce3716717d73.jpg', '2019-07-18 00:00:00', 'Bupati Kobar saat mengunjungi stand pembayaran PBB-P2 Bapenda Kobar pada BBGRM ke-16 dan HKG ke-47 Tingkat Kobar, Kamis (18/7). (bapenda kobar)'),
(7, 'Rapat Evaluasi PAD, Beberapa SKPD Ajukan Kenaikan Target PAD Tahun 2019', '&lt;p style=&quot;margin-left:0cm; margin-right:0cm&quot;&gt;&lt;strong&gt;Bapenda Kobar -&lt;/strong&gt; &amp;nbsp;Badan Pendapatan Daerah (Bapenda) Kabupaten Kotawaringin Barat (Kobar) menggelar Rapat Koordinasi dalam rangka evaluasi dan penetapan perubahan target penerimaan PAD Tahun Anggaran 2019, yang bertempat di ruang rapat Bupati Kobar, Kamis (27/06), yang dipimpin oleh Wakil Bupati, Ahmadi Riansyah dan dihadiri oleh seluruh SKPD Pengelola PAD di Kobar.&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0cm; margin-right:0cm&quot;&gt;Dalam sambutannya Wabup Kobar mengatakan bahwa rapat evaluasi dan penetapan perubahan target penerimaan PAD Tahun Anggaran 2019 ini dilakukan untuk mengetahui sejauh mana SKPD Pengelola PAD mencapai&amp;nbsp;realisasi penerimaan sebagaimana yang telah ditargetkan dan usulan perubahan target penerimaan PAD sampai dengan akhir tahun.&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0cm; margin-right:0cm&quot;&gt;&amp;ldquo;Untuk kepentingan penyusunan Dokumen KUA/PPAS APBD-P 2019 tetap konsisten dengan apa yang sudah kita sepakati bersama dengan DPRD dalam Dokumen APBD Murni 2019, kecuali memang ada ketentuan atau peraturan dari Pemerintah Pusat yang menyebabkan pungutan itu tidak bisa lagi dilakukan,&amp;rdquo; kata Ahmadi dihadapan SKPD Pengelola PAD.&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0cm; margin-right:0cm&quot;&gt;Pada saat rapat juga dilakukan paparan oleh Kepala Bapenda Kobar Molta Dena, SE, MA yang menyampaikan bahwa ada beberapa SKPD yang mengajukan kenaikan, penurunan dan tetap pada target APBD Murni Tahun Anggaran 2019.&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0cm; margin-right:0cm&quot;&gt;&amp;ldquo;SKPD yang mengajukan kenaikan target adalah Dinas Kesehatan, Dinas Lingkungan Hidup dan Kecamatan Pangkalan Lada, penurunan target adalah Dinas Perikanan, Dinas Peternakan dan Kesehatan Hewan, Dinas Tenaga Kerja dan Transmigrasi, Dinas Kependudukan dan Catatan Sipil dan Kecamatan Arut Selatan, sedangkan SKPD lainnya tetap,&amp;rdquo; kata Molta.&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0cm; margin-right:0cm&quot;&gt;Hasil rapat evaluasi dan koordinasi ini nantinya direkap dan disampaikan kepada Tim Anggaran Pemerintah Daerah (TAPD) Kobar sebagai bahan penyusunan dokumen KUA/PPAS APBD-P 2019 yang selanjutnya akan dibahas bersama DPRD Kobar. &lt;strong&gt;(gino sriyanto/ bapenda kobar)&lt;/strong&gt;&lt;/p&gt;\r\n', '0', 17, '2019-07-31 16:25:25', NULL, 'rapat-evaluasi-pad-beberapa-skpd-ajukan-kenaikan-target-pad-tahun-2019', 1, '90899d35a5d658f97ad0987fa28005b9.jpeg', '2019-06-27 16:25:25', 'Rapat Evaluasi dan Penetapan Perubahan Target Penerimaan PAD Tahun Anggaran 2019, di ruang rapat Bupati Kobar, Kamis (27/06/19). (bapenda kobar)'),
(8, 'Sosialiasasi Dan Pembelajaran Aplikasi Sistem PAD Kobar', '&lt;p style=&quot;margin-left:0cm; margin-right:0cm&quot;&gt;&lt;strong&gt;Bapenda Kobar -&lt;/strong&gt; Bertempat di kantor Badan Pendapatan Daerah (Bapenda) Kabupaten Kotawaringin Barat (Kobar) pada Jumat (12/7) pukul 13.30 WIB, Bapenda Kobar melalui Bidang Pengawasan, Pemeriksaan dan Pengembangan PAD melaksanakan kegiatan Sosialisasi dan Pembelajaran dari Tim Analisa dan Pembuatan Aplikasi Sistem PAD Dinas Komunikasi dan Informatika (Diskominfo) Kota Cirebon bagi pegawai, staf dan pejabat lingkup Bapenda Kobar dengan narasumber Sutiono, ST dan Iid Anwar Hidayat, S.Kom.&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0cm; margin-right:0cm&quot;&gt;Sosialisasi yang dibuka oleh Sekretaris Bapenda Kobar, Elly Rosdiannie, S.Hut ini dilakukan untuk mendapatkan masukan-masukan dari peserta kegiatan agar Aplikasi Sistem PAD yang akan diberlakukan di Kobar ini menjadi sempurna dan mudah dipergunakan, baik dari wajib pajak maupun petugas pajak.&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0cm; margin-right:0cm&quot;&gt;&amp;ldquo;Pembangunan Aplikasi Sistem PAD ini merupakan kelanjutan dari Perjanjian Kerjasama antara Bapenda Kobar dengan Diskominfo Kota Cirebon,&amp;rdquo; ujar Elly.&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0cm; margin-right:0cm&quot;&gt;Dalam kegiatan ini banyak masukan-masukan yang diberikan kepada Tim Analisa dan Pembuatan Aplikasi Sistem PAD Diskominfo Kota Cirebon sehingga sepulangnya ke tempat asal akan segera merubah hal-hal yang sudah disepakati bersama.&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0cm; margin-right:0cm&quot;&gt;&amp;ldquo;Aplikasi Sistem PAD yang dibangun sama persis seperti di Kota Cirebon namun dengan masukan yang ada akan disesuaikan dengan kondisi di Kobar,&amp;rdquo; ungkap Sutiono.&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0cm; margin-right:0cm&quot;&gt;Diakhir kegiatan Kepala Bidang Pengawasan, Pemeriksaan dan Pengembangan PAD Bapenda Kobar, Nuriasih, SP, MM yang menyampaikan bahwa Aplikasi Sistem PAD yang diberi nama Sistem Informasi Manajemen Pendapatan Daerah ini akan segera diberlakukan setelah perbaikan dari Tim Diskominfo Kota Cirebon pada tahun ini juga. &lt;strong&gt;(gino sriyanto/bapenda kobar)&lt;/strong&gt;&lt;/p&gt;\r\n', '0', 12, '2019-07-31 16:26:35', '2019-07-31 16:29:45', 'sosialiasasi-dan-pembelajaran-aplikasi-sistem-pad-kobar', 1, '1792b5d045b4f855e833592e5ed38309.jpg', '2019-07-12 00:00:00', ''),
(9, 'Temu Sharing Pengawasan Pelaksanaan Perda Sarang Burung Walet antara Komisi II DPRD Kapuas dan Pemkab Kobar', '&lt;p style=&quot;margin-left:0cm; margin-right:0cm; text-align:justify&quot;&gt;&lt;strong&gt;Bapenda Kobar -&lt;/strong&gt; Bertempat di Ruang Rapat Bupati Kobar, Jumat (12/7) Pemerintah Kabupaten (Pemkab) Kotawaringin Barat (Kobar) menerima kunjungan Komisi II Dewan Perwakilan Rakyat Daerah (DPRD) Kabupaten Kapuas. Kunjungan Komisi II DPRD Kapuas ini dalam rangka temu sharing pengawasan pelaksanaan Perda Sarang Burung Walet di Kobar.&amp;nbsp;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0cm; margin-right:0cm&quot;&gt;Kunjungan Komisi II DPRD Kabupaten Kapuas ini disambut langsung oleh Wakil Bupati Kobar yang didampingi Kepala Badan Pendapatan Daerah (Bapenda), Kepala Dinas Peternakan dan Kesehatan Hewan, Satpol PP Kobar, Dinas Penanaman Modal dan Pelayanan Terpadu Satu Pintu, Dinas Pekerjaan Umum dan Penataan Ruang, Kabag Hukum, Balai Karantina Pertanian Kelas II Wilayah Kerja Pangkalan Bun dan instansi lainnya.&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0cm; margin-right:0cm&quot;&gt;Melalui kunjungannya ini, Komisi II DPRD Kapuas mengetahui secara langsung hal-hal yang dianggap perlu dalam pengawasan pelaksanaan Perda Sarang Burung Walet, diantaranya pendataan tempat usaha sarang burung walet, proses perijinan, pendapatan pajak, pengelolaan konflik sekitar gedung walet, penertiban tempat usaha dan lain sebagainya terkait pengelolaan sarang burung walet.&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0cm; margin-right:0cm&quot;&gt;Wakil Bupati Kobar, Ahmadi Riansyah dalam sambutannya menyampaikan bahwa pengawasan pelaksanaan Perda Sarang Burung Walet telah dilakukan berbagai upaya salah satunya melalui pembentukan Tim Yustisi yang diketuai oleh Wakil Bupati dengan didukung oleh unsur DPRD, Kepolisian, Kejaksanaan, Balai Karantina, Satpol PP, Bapenda dan SKPD terkait lainnya lingkup Pemkab Kobar.&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0cm; margin-right:0cm&quot;&gt;&amp;ldquo;Tugas Tim Yustisi ini adalah mendata, mendaftarkan, menagih, memberikan peringatan bahkan penyegelan bila dipandang perlu,&amp;rdquo; ungkap Ahmadi.&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0cm; margin-right:0cm&quot;&gt;Dalam acara temu sharing ini juga dilakukan pemaparan oleh Kepala Bapenda Kobar, Molta Dena yang berisi Time Schedule pelaksanaan, tahapan-tahanpan yang dilakukan dan foto-foto kegiatan pelaksanaan Tim Yustisi.&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0cm; margin-right:0cm&quot;&gt;&amp;rdquo;Pada Tahun Anggaran 2018, sebelum pelaksanaan Tim Yustisi sekira bulan Agustus 2018, pendapatan dari Pajak Sarang Burung Walet baru mencapai Rp.64.000.000. Dan setelah dilaksanakan Tim Yustisi terealisasi Rp.1.166.826.000, namun pencapaian ini masih jauh dari target yang diberikan sebesar Rp.5.000.000.000 atau baru mencapai &amp;plusmn; 23,34 %,&amp;ldquo; jelas Molta.&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0cm; margin-right:0cm&quot;&gt;Diakhir pertemuan, Ketua Komisi II DPRD Kapuas Murniati, SE yang mewakili 10 orang anggota yang hadir menyampaikan ucapan terima kasih kepada Pemkab Kobar yang bersedia dengan ramah dan penuh kekeluargaan menyambut dan berbagi pengalaman dalam pengawasan pelaksanaan Perda Sarang Burung Walet ini, sehingga dengan bekal ini akan dijadikan contoh untuk pelaksanaan di Kabupaten Kapuas. &lt;strong&gt;(bapenda kobar)&lt;/strong&gt;&lt;/p&gt;\r\n', '0', 9, '2019-07-31 16:27:49', '2019-07-31 16:29:34', 'temu-sharing-pengawasan-pelaksanaan-perda-sarang-burung-walet-antara-komisi-ii-dprd-kapuas-dan-pemkab-kobar', 1, 'f9bde0c1d6c6c5f3b6f4c33693f9ed7f.jpg', '2019-07-12 00:00:00', ''),
(10, 'Mudahkan Wajib Pajak, Pemkab Kobar Bangun Sistem Aplikasi Pembayaran Pajak Online', '&lt;p style=&quot;margin-left:0cm; margin-right:0cm&quot;&gt;&lt;strong&gt;Bapenda Kobar -&lt;/strong&gt; Dalam rangka mempermudah pembayaran pajak oleh wajib pajak, Pemerintah Kabupaten (Pemkab) Kotawaringin Barat (Kobar) melalui Badan Pendapatan Daerah (Bapenda) bekerja sama dengan Dinas Komunikasi Informatika dan Statistik Kota Cirebon melaksanakan penandatanganan perjanjian kerja sama tentang pelaksanaan kegiatan pemeliharaan dan pengembangan sistem informasi pendapatan daerah, setelah sebelumnya dilakukan Kesepahaman Bersama antara Pemkab Kobar dengan Pemkot Cirebon dalam rangka pengembangan sistem informasi pendapatan daerah.&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0cm; margin-right:0cm&quot;&gt;Penandatanganan Perjanjian Kerjasama ini dilakukan oleh Kepala Bapenda Kobar, Molta Dena dan dari pihak Pemkot Cirebon yang diwakili Kepala Diskominfosta Kota Cirebon, Iing Daiman di Kantor Sekretariat Daerah Kota Cirebon, Jumat (17/5).&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0cm; margin-right:0cm&quot;&gt;&amp;ldquo;Penandatangan Perjanjian Kerjasama kali ini merupakan kelanjutan dari 2 (dua) perjanjian yang telah disepakati tahun sebelumnya,&amp;ldquo; tegas Molta Dena. Hal ini menjadikan Kabupaten Kobar mengadopsi sepenuhnya aplikasi online pengelolaan pajak daerah yang ada di Kota Cirebon.&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0cm; margin-right:0cm&quot;&gt;Pada Tahun 2018 telah selesai dibangun aplikasi online pembayaran PBB (e-PBB) dan akan dilakukan pemeliharaan tahun berikutnya. Sedangkan di tahun 2019 ini akan dikembangkan aplikasi online pembayaran Bea Perolehan Hak atas Tanah dan Bangunan (e-BPHTB) dan pajak daerah lainnya.&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0cm; margin-right:0cm; text-align:justify&quot;&gt;Pada akhirnya, melalui aplikasi ini diharapkan dapat mempermudah dan mengefektifkan pengelolaan pajak daerah, baik dari wajib pajak maupun pemerintah daerah. &lt;strong&gt;(gino sriyanto/bapenda kobar)&lt;/strong&gt;&lt;/p&gt;\r\n', '0', 19, '2019-07-31 16:29:26', NULL, 'mudahkan-wajib-pajak-pemkab-kobar-bangun-sistem-aplikasi-pembayaran-pajak-online', 1, '31a1fb2ea81df8f8952a89c30180b6b3.jpeg', '2019-05-20 16:29:26', 'Pemkab Kobar melalui Bapenda yang diwakili oleh Kepala Bapenda, Molta Dena usai menandatangani kerjasama pemeliharaan dan pengembangan sistem informasi pendapatan daerah dengan Diskominfosta yang diwakili Kepala Diskominfosta Kota Cirebon, Iing Daiman di Kantor Sekretariat Daerah Kota Cirebon, Jumat (17/5). (bapenda kobar)');

-- --------------------------------------------------------

--
-- Table structure for table `biodinas`
--

CREATE TABLE `biodinas` (
  `idDinas` int(11) UNSIGNED NOT NULL,
  `alamatDinas` text,
  `emailDinas` varchar(100) DEFAULT NULL,
  `telpDinas` char(20) DEFAULT NULL,
  `statusDinas` enum('1','0') DEFAULT '0',
  `namaDinas` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `biodinas`
--

INSERT INTO `biodinas` (`idDinas`, `alamatDinas`, `emailDinas`, `telpDinas`, `statusDinas`, `namaDinas`) VALUES
(1, 'Jalan Sutan Syahrir No. 22 Pangkalan Bun Kode Pos 74112 Fax ( 0532 ) 28052', 'bapenda.kobar@gmail.com', '( 0532 ) 21064 28414', '0', 'Badan Pendapatan Daerah Kabupaten Kotawaringin Barat');

-- --------------------------------------------------------

--
-- Table structure for table `detailfilepublikasi`
--

CREATE TABLE `detailfilepublikasi` (
  `idDetailFilePublikasi` int(11) UNSIGNED NOT NULL,
  `judulDetailFilePublikasi` varchar(100) DEFAULT NULL,
  `file` varchar(100) DEFAULT NULL,
  `type` char(20) DEFAULT NULL,
  `kategoriFilePublikasiId` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `detailfilepublikasi`
--

INSERT INTO `detailfilepublikasi` (`idDetailFilePublikasi`, `judulDetailFilePublikasi`, `file`, `type`, `kategoriFilePublikasiId`) VALUES
(14, 'Peraturan Daerah Tentang Pajak Restoran', '743d34b957c8919d9957b273afedef07.pdf', NULL, 4),
(15, 'Peraturan Daerah Tentang Pajak Hotel', '0ef69f9ce16d885b27a47d3e16a4f8f6.pdf', NULL, 4),
(16, 'Peraturan Daerah Tentang Pajak Air Tanah', '6dcdb7605174a6f032f963ec105b814b.pdf', NULL, 4),
(17, 'Peraturan Daerah Tentang Pajak Mineral Bukan Logam dan Batuan', '2cded076c82f03465eb0661a97ff8c45.pdf', NULL, 4),
(18, 'Peraturan Daerah Tentang Pajak Hiburan', 'f53ef66812d6c2cc469f5ef15677ba1f.pdf', NULL, 4),
(19, 'Peraturan Daerah Tentang Pajak Sarang Burung Walet', '2e963c90e3412acc451b16736f91709d.pdf', NULL, 4),
(20, 'Peraturan Daerah Tentang Pajak Penerangan Jalan ( PPJ )', '28c0b064aa933b0c20f2ade7f09e4fa8.pdf', NULL, 4),
(21, 'Peraturan Daerah Tentang Pajak Parkir', '6f40b4e6da0d6b51b34b2ecd0cb0adb2.pdf', NULL, 4),
(28, 'Peraturan Daerah Tentang Pajak Reklame', '0f67acdedf02504e71db5fbf62ba09d2.pdf', NULL, 4),
(29, 'Peraturan Daerah Tentang Pajak Bea Perolehan Hak Atas Tanah dan Bangunan ( BPHTB )', '4b55f49e65483e0ccfe19f8fdd9e0899.pdf', NULL, 4),
(30, 'Peraturan Daerah Tentang Pajak Bumi dan Bangunan Sektor Pedesaan dan Perkotaan ( PBB-P2 )', 'c49ecce66acf0ebf5f0f397fee8c68df.pdf', NULL, 4),
(33, 'Peraturan Bupati Kotawaringin Barat Nomor 18 Tahun 2011', '7dbb884a4ecc1e85c62a4029763ef184.pdf', NULL, 2),
(34, 'Peraturan Bupati Kotawaringin Barat Nomor 19 Tahun 2011 ', '33fb8bf8df489a99972a66a728cb2318.pdf', NULL, 2),
(35, 'Peraturan Bupati Kotawaringin Barat Nomor 20 Tahun 2011 ', '0f4716b54aa921dfeb5f876ef18d593a.pdf', NULL, 2),
(36, 'Peraturan Bupati Kotawaringin Barat Nomor 21 Tahun 2011 ', '5de5303a1c6d854ec7ba2c17627d08c8.pdf', NULL, 2),
(37, 'Peraturan Bupati Kotawaringin Barat Nomor 22 Tahun 2011 ', '24985b76a151420b2acfb3eb5ab5deee.pdf', NULL, 2),
(38, 'Peraturan Bupati Kotawaringin Barat Nomor 33 Tahun 2011 ', '59b3a13a094bed0c589dabe6ffa5a74b.pdf', NULL, 2),
(39, 'Peraturan Bupati Kotawaringin Barat Nomor 34 Tahun 2011 ', '6ed2c8d54bd4612f977029c7702c00b2.pdf', NULL, 2),
(40, 'Peraturan Bupati Kotawaringin Barat Nomor 35 Tahun 2011 ', 'bf448c908801cdaa844f59f6c4efc8a3.pdf', NULL, 2),
(41, 'Peraturan Bupati Kotawaringin Barat Nomor 36 Tahun 2011 ', '4cf8e8614e1d93b16f36c967c9936ebb.pdf', NULL, 2),
(42, 'Peraturan Bupati Kotawaringin Barat Nomor 46 Tahun 2011 ', '136ba394e84e55f585652369468ffa7b.pdf', NULL, 2),
(43, 'Peraturan Bupati Kotawaringin Barat Nomor 1 Tahun 2012 ', '2ceed4b38c05473192d33bb229c400d6.pdf', NULL, 2),
(44, 'Peraturan Bupati Kotawaringin Barat Nomor 3 Tahun 2014 ', '49108a238221ed11072df55a93305db7.pdf', NULL, 2),
(45, 'Peraturan Bupati Kotawaringin Barat Nomor 4 Tahun 2014 ', '89c0760827c098192ff24760f457dcda.pdf', NULL, 2),
(46, 'Peraturan Bupati Kotawaringin Barat Nomor 5 Tahun 2014', 'e8ffa1801061011410c16b865e868f91.pdf', NULL, 2),
(47, 'Peraturan Bupati Kotawaringin Barat Nomor 16 Tahun 2014', '51da3480006f85f9e77e81ac4f583731.pdf', NULL, 2),
(48, 'Peraturan Bupati Kotawaringin Barat Nomor 26 Tahun 2014', 'e2b7e3b1b341db08b938eee641596540.pdf', NULL, 2),
(49, 'Peraturan Bupati Kotawaringin Barat Nomor 36 Tahun 2015 ', '63472042aaf9712b185f79c663d471e0.pdf', NULL, 2),
(50, 'Peraturan Bupati Kotawaringin Barat Nomor 37 Tahun 2015', '191b2b8cc0de9c3241e5b0f5ab69d4a2.pdf', NULL, 2),
(53, 'Formulir Pendaftaran Pajak PBB - P2', '219a552b07cc4789793200e5d8a5147b.pdf', NULL, 2),
(54, 'Formulir Permohonan Pembetulan Pajak PBB - P2', 'b3d0959d1c057a84eff8469d9b3adb36.pdf', NULL, 2),
(55, 'Formulir Permohonan Mutasi Pajak PBB - P2', '12282996fbcf8f7a5aa1a0a20975ca87.pdf', NULL, 2),
(56, 'Surat Pemberitahuan Masa Pajak Pengambilan Dan Pengolahan Mineral Bukan Logam dan Batuan', '8497237530825279aca463b2b99281d3.pdf', NULL, 2),
(57, 'Surat Pemberitahuan Pajak Daerah', '12a926ab33759f5d76ee838ba8688bcd.pdf', NULL, 2),
(58, 'Laporan Realisasi Badan Pendapatan Daerah Tahun 2010', '052d6ed17d6a6c5cb34a5eed3661b53c.pdf', NULL, 2),
(60, 'Laporan Realisasi Badan Pendapatan Daerah Tahun 2012', 'f93a8043c578b50e588031f43821c0c0.pdf', NULL, 2),
(61, 'Laporan Realisasi Badan Pendapatan Daerah Tahun 2013', 'f0bc9c7f58d48b5307f135b01d7d42c8.pdf', NULL, 2),
(62, 'Laporan Realisasi Badan Pendapatan Daerah Tahun 2014', '2bdeba1790ab69495223524434d031c5.pdf', NULL, 2),
(63, 'Laporan Realisasi Badan Pendapatan Daerah Tahun 2015', '35ff38a30a700bfac3cfc1a00a3c8f30.pdf', NULL, 2),
(64, 'Laporan Realisasi Badan Pendapatan Daerah Tahun 2016', '32ec5bf04e8bc5cb10b3cdb439df8216.pdf', NULL, 2);

-- --------------------------------------------------------

--
-- Table structure for table `foto`
--

CREATE TABLE `foto` (
  `idFoto` int(11) UNSIGNED NOT NULL,
  `filename` varchar(200) DEFAULT NULL,
  `albumIdAlbum` int(11) UNSIGNED NOT NULL,
  `caption` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `foto`
--

INSERT INTO `foto` (`idFoto`, `filename`, `albumIdAlbum`, `caption`) VALUES
(34, 'c8699154484b122fca42cefb0b67aeab.jpeg', 22, '');

-- --------------------------------------------------------

--
-- Table structure for table `jenisreklame`
--

CREATE TABLE `jenisreklame` (
  `idJenisReklame` int(11) UNSIGNED NOT NULL,
  `labelJenisReklame` varchar(100) DEFAULT NULL,
  `nilaiJenisReklame` int(11) DEFAULT NULL,
  `satuanWaktuId` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jenisreklame`
--

INSERT INTO `jenisreklame` (`idJenisReklame`, `labelJenisReklame`, `nilaiJenisReklame`, `satuanWaktuId`) VALUES
(2, 'Papan / Billboard / Megatron', 30000, 1),
(3, 'Kain / Plastik', 40000, 1),
(4, 'Melekat / Stiker', 70, 1),
(5, 'Selebaran', 50, 1);

-- --------------------------------------------------------

--
-- Table structure for table `kategorifilepublikasi`
--

CREATE TABLE `kategorifilepublikasi` (
  `idKategoriFilePublikasi` int(11) UNSIGNED NOT NULL,
  `namaKategoriFilePublikasi` varchar(100) DEFAULT NULL,
  `slug` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kategorifilepublikasi`
--

INSERT INTO `kategorifilepublikasi` (`idKategoriFilePublikasi`, `namaKategoriFilePublikasi`, `slug`) VALUES
(2, 'Download Center', 'download-center'),
(4, 'Produk Hukum', 'produk-hukum'),
(5, 'Sosialisasi Pajak', 'sosialisasi-pajak'),
(6, 'Daftar Peraturan Bupati Kotawaringin Barat Di Lingkungan Badan Pendapatan Daerah ', 'daftar-peraturan-bupati-kotawaringin-barat-di-lingkungan-badan-pendapatan-daerah-');

-- --------------------------------------------------------

--
-- Table structure for table `kategorimblb`
--

CREATE TABLE `kategorimblb` (
  `idKategoriMBLB` int(11) UNSIGNED NOT NULL,
  `jenisMBLB` varchar(100) DEFAULT NULL,
  `tarifDasarMBLB` int(11) DEFAULT NULL,
  `persentaseTarifDasarMBLB` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kategorimblb`
--

INSERT INTO `kategorimblb` (`idKategoriMBLB`, `jenisMBLB`, `tarifDasarMBLB`, `persentaseTarifDasarMBLB`) VALUES
(1, 'Asbes', 54350, NULL),
(2, 'Batu Tulis', 13000, NULL),
(3, 'Batu Kapur', 20450, NULL),
(4, 'Batu Apung', 39500, NULL),
(5, 'Bentonik', 28350, NULL),
(6, 'Dolomit', 13500, NULL),
(7, 'Felspar', 70500, NULL),
(8, 'Garam Batu (Helite)', 46850, NULL),
(9, 'Granit', 42180, NULL),
(10, 'Granit/Andesit (Agregat Kasar)', 130000, NULL),
(11, 'Granit/Andesit (Agregat Halus)', 150000, NULL),
(12, 'Granit/Andesit (Batu Belah)', 65500, NULL),
(13, 'Granit/Andesit (Batu Gunung/Quarry)', 65500, NULL),
(14, 'Granit/Andesit (Batu Kali)', 75000, NULL),
(15, 'Granit/Andesit (Batu Pecah 0.5 - 1 cm)', 150000, NULL),
(16, 'Granit/Andesit (Batu Pecah 1 - 2 cm)', 100000, NULL),
(17, 'Granit/Andesit (Batu Pecah 2 - 3 cm)', 150000, NULL),
(18, 'Granit/Andesit (Batu Pecah 3 - 5 cm)', 130000, NULL),
(19, 'Granit/Andesit (Batu Pecah 5 - 7 cm)', 130000, NULL),
(20, 'Granit/Andesit (Batu Pecah 7 - 10 cm)', 127000, NULL),
(21, 'Granit/Andesit (Batu Pecah 10 - 15/10 - 12 cm)', 125000, NULL),
(22, 'Granit/Andesit (Laterit)', 50000, NULL),
(23, 'Granit/Andesit (Pasir Ayak untuk Beton)', 12500, NULL),
(24, 'Granit/Andesit (Pasir)', 11400, NULL),
(25, 'Granit/Andesit (Tanah Urug Biasa)', 12500, NULL),
(26, 'Granit/Andesit (Tanah Urug Subur)', 35000, NULL),
(27, 'Granit/Andesit (Tanah Bahan Timbunan Pilihan)', 15000, NULL),
(28, 'Gips', 45550, NULL),
(29, 'Kalsit', 15180, NULL),
(30, 'Kaolin', 60750, NULL),
(31, 'Leusit', 63780, NULL),
(32, 'Magnesit', 63780, NULL),
(33, 'Mika', 63780, NULL),
(34, 'Marmer', 75930, NULL),
(35, 'Nitrat', 50625, NULL),
(36, 'Opsiden', 5400, NULL),
(37, 'Oker', 42280, NULL),
(38, 'Pasir dan Kerikil', 35000, NULL),
(39, 'Zirkon', 1000, NULL),
(40, 'Pasir Kuarsa', 60750, NULL),
(41, 'Perlit', 5400, NULL),
(42, 'Phospat', 55680, NULL),
(43, 'Talk', 63780, NULL),
(44, 'Tanah Serap (Fullers Earth)', 45750, NULL),
(45, 'Tanah Diotomi', 45750, NULL),
(46, 'Tanah Liat', 15000, NULL),
(47, 'Tawas (Alum)', 50625, NULL),
(48, 'Tras', 14000, NULL),
(49, 'Yarosit', 63780, NULL),
(50, 'Zeolit', 45562, NULL),
(51, 'Basal', 45562, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `kategorisr`
--

CREATE TABLE `kategorisr` (
  `idKategoriSR` int(11) UNSIGNED NOT NULL,
  `labelKategoriSR` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `kepaladaerah`
--

CREATE TABLE `kepaladaerah` (
  `idKepalaDaerah` int(11) UNSIGNED NOT NULL,
  `namaKepalaDaerah` varchar(100) DEFAULT NULL,
  `jabatanKepalaDaerah` varchar(100) DEFAULT NULL,
  `isActive` enum('1','0') DEFAULT '0',
  `orderKepalaDaerah` int(11) DEFAULT NULL,
  `filenameKepalaDaerah` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kepaladaerah`
--

INSERT INTO `kepaladaerah` (`idKepalaDaerah`, `namaKepalaDaerah`, `jabatanKepalaDaerah`, `isActive`, `orderKepalaDaerah`, `filenameKepalaDaerah`) VALUES
(4, 'Hj. Nurhidayah, S.H., M.H.', 'Bupati', '1', 1, '67d4b151719cc544ac7ad9403678e121.jpg'),
(5, 'Ahmadi Riansyah', 'Wakil Bupati', '1', 2, 'ad25b7d70772a8c023e3ed0c3ce19e44.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `komponenkl`
--

CREATE TABLE `komponenkl` (
  `idKomponenKL` int(11) UNSIGNED NOT NULL,
  `nilaiAwalKomponenKL` int(11) DEFAULT NULL,
  `nilaiAkhirKomponenKL` int(11) DEFAULT NULL,
  `bobotKomponenKL` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `komponenkl`
--

INSERT INTO `komponenkl` (`idKomponenKL`, `nilaiAwalKomponenKL`, `nilaiAkhirKomponenKL`, `bobotKomponenKL`) VALUES
(2, 25, 30, 20),
(3, 20, 24, 17),
(4, 15, 19, 14),
(5, 10, 14, 10),
(6, 5, 9, 7),
(7, 1, 4, 4);

-- --------------------------------------------------------

--
-- Table structure for table `komponenlr`
--

CREATE TABLE `komponenlr` (
  `idKomponenLR` int(11) UNSIGNED NOT NULL,
  `nilaiAwalKomponenLR` decimal(6,2) DEFAULT NULL,
  `bobotKomponenLR` int(11) DEFAULT NULL,
  `nilaiAkhirKomponenLR` decimal(6,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `komponenlr`
--

INSERT INTO `komponenlr` (`idKomponenLR`, `nilaiAwalKomponenLR`, `bobotKomponenLR`, `nilaiAkhirKomponenLR`) VALUES
(3, '45.51', 30, '55.50'),
(4, '35.51', 25, '45.50'),
(5, '25.51', 20, '35.50'),
(6, '15.51', 15, '25.50'),
(7, '10.51', 10, '15.50'),
(8, '6.51', 8, '10.50'),
(9, '3.51', 6, '6.50'),
(10, '1.51', 4, '3.50'),
(11, '0.51', 2, '1.50'),
(12, '0.00', 1, '0.50');

-- --------------------------------------------------------

--
-- Table structure for table `komponennjt`
--

CREATE TABLE `komponennjt` (
  `idKomponenNJT` int(11) UNSIGNED NOT NULL,
  `labelKomponenNJT` varchar(100) DEFAULT NULL,
  `nilaiKomponenNJT` int(11) DEFAULT NULL,
  `komponenSRid` int(11) UNSIGNED NOT NULL,
  `komponenPLid` int(11) UNSIGNED NOT NULL,
  `komponenKLid` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `komponennjt`
--

INSERT INTO `komponennjt` (`idKomponenNJT`, `labelKomponenNJT`, `nilaiKomponenNJT`, `komponenSRid`, `komponenPLid`, `komponenKLid`) VALUES
(3, 'Kawasan Bundaran Pancasila', 10000000, 11, 2, 6),
(4, 'Simpang Tiga R. Santrek - P. Antasari', 9000000, 6, 4, 6),
(5, 'Simpang Empat Udan Said - P. Antasari', 9000000, 6, 3, 6),
(8, 'Pasar Indra Kencana', 8000000, 5, 6, 6),
(9, 'Pasar Indra Sari', 8000000, 5, 3, 6),
(10, 'Simpang Tiga A. Yani - Udan Said', 8000000, 6, 4, 6),
(11, 'Bundaran Pramuka ( Bukit Raja )', 8000000, 6, 4, 6),
(12, 'Jl. Gerilya - Bundaran', 7500000, 12, 5, 6),
(13, 'Bundaran Pangkalan Lima', 7500000, 12, 4, 6),
(14, 'Pasar Cempaka Kumai', 7500000, 5, 5, 6),
(15, 'Jl. Pangeran Antasari', 6000000, 6, 6, 6),
(16, 'Jl. Rangga Santrek', 6000000, 6, 6, 6),
(17, 'Jl. Pra Kusuma Yudha', 6000000, 6, 6, 6),
(18, 'Jl. Belimbing Manis', 6000000, 6, 5, 6),
(19, 'Jl. Suradilaga', 6000000, 6, 5, 6),
(20, 'Jl. Udan Said', 5500000, 6, 5, 6),
(21, 'Jl. Bendahara Kumai', 5000000, 6, 5, 6),
(23, 'jl. Gerilya Kumai', 5000000, 6, 5, 5),
(24, 'Jl. Abdul Aziz Kumai', 4500000, 6, 5, 6),
(25, 'Pelabuhan Laut', 4000000, 7, 5, 6),
(26, 'Bandar Udara', 3000000, 7, 5, 5),
(27, 'kawasan wisata', 3000000, 12, 5, 6),
(28, 'Terminal Angkutan Umum', 2500000, 7, 5, 5),
(29, 'Jl. Sutan Syahrir', 2500000, 8, 5, 5),
(30, 'LIK - Pasir Panjang', 2000000, 9, 5, 6),
(31, 'komplek KPR BTN dan Sejenisnya', 2000000, 10, 5, 6),
(32, 'Tempat Lainnya', 1500000, 12, 5, 6),
(33, 'Jl. H.M Rafi\'i ( Bundaran Pancasila - Bundaran Kecil )', 2500000, 8, 5, 5),
(34, 'Taman Hiburan Daang Ilung', 1750000, 12, 5, 6);

-- --------------------------------------------------------

--
-- Table structure for table `komponenpl`
--

CREATE TABLE `komponenpl` (
  `idKomponenPL` int(11) UNSIGNED NOT NULL,
  `labelKomponenPL` varchar(100) DEFAULT NULL,
  `bobotKomponenPL` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `komponenpl`
--

INSERT INTO `komponenpl` (`idKomponenPL`, `labelKomponenPL`, `bobotKomponenPL`) VALUES
(2, 'Simpang Lima', 25),
(3, 'Simpang Empat', 20),
(4, 'Simpang Tiga', 15),
(5, 'Dua Arah', 10),
(6, 'Satu Arah', 5);

-- --------------------------------------------------------

--
-- Table structure for table `komponensr`
--

CREATE TABLE `komponensr` (
  `idKomponenSR` int(11) UNSIGNED NOT NULL,
  `labelKomponenSR` varchar(100) DEFAULT NULL,
  `bobotKomponenSR` int(11) DEFAULT NULL,
  `kategoriIdKategoriSR` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `komponensr`
--

INSERT INTO `komponensr` (`idKomponenSR`, `labelKomponenSR`, `bobotKomponenSR`, `kategoriIdKategoriSR`) VALUES
(5, 'Pusat Perdagangan', 25, 0),
(6, 'Kawasan Perdagangan', 22, 0),
(7, 'Kawasan Khusus', 19, 0),
(8, 'Daerah Perkantoran', 15, 0),
(9, 'Kawasan Industri', 3, 0),
(10, 'Kawasan Perumahan', 5, 0),
(11, 'Kawasan Terbuka', 1, 0),
(12, 'Kawasan Campuran', 11, 0);

-- --------------------------------------------------------

--
-- Table structure for table `leveluser`
--

CREATE TABLE `leveluser` (
  `idLevelUser` int(11) UNSIGNED NOT NULL,
  `namaLevelUser` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `leveluser`
--

INSERT INTO `leveluser` (`idLevelUser`, `namaLevelUser`) VALUES
(1, 'superadmin');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `idMenu` int(11) UNSIGNED NOT NULL,
  `menuName` varchar(200) DEFAULT NULL,
  `parentId` int(11) NOT NULL DEFAULT '0',
  `slug` varchar(200) DEFAULT NULL,
  `status` enum('1','0') DEFAULT '1',
  `content` text,
  `userId` int(11) UNSIGNED NOT NULL,
  `link` varchar(200) DEFAULT NULL,
  `orderMenu` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`idMenu`, `menuName`, `parentId`, `slug`, `status`, `content`, `userId`, `link`, `orderMenu`) VALUES
(18, 'Profil Bapenda', 35, 'profil-bapenda', '1', '<p style=\"margin-right:0cm\">Badan Pengelolaan Pendapatan Daerah (BAPENDA) Kabupaten Kotawaringin Barat adalah suatu Satuan Kerja Perangkat Daerah di lingkungan Pemerintah Kabupaten Kotawaringin Barat yang dibentuk berdasarkan Peraturan Daerah Nomor 06 Tahun 2016 Kabupaten Kotawaringin Barat Tentang Pembentukan dan Susunan Perangkat Daerah Kabupaten Kotawaringin Barat&nbsp;&nbsp;dan juga Berdasarkan Peraturan&nbsp;Bupati&nbsp;Kotawaringin Barat No 70 Tahun 2016 Tentang Kedudukan,Susunan Organisasi, Tugas dan Fungsi Badan Pendapatan Daerah Kabupaten Kotawaringin Barat. Badan Pendapatan Daerah&nbsp;merupakan unsur penunjang urusan pemerintahan yang melaksanakan fungsi penunjang keuangan di bidang pendapatan daerah, dipimpin oleh seorang Kepala Badan yang berkedudukan dibawah dan bertanggung jawab kepada Bupati melalui Sekretaris Daerah.<br />\r\nBapenda memiliki peran yang strategis, yakni di satu sisi merupakan pengelola pajak daerah, di sisi lain merupakan koordinator pendapatan daerah yang ikut bertanggung jawab atas keberhasilan penerimaan pendapatan daerah secara keseluruhan.<br />\r\nKantor Badan Pengelolaan Pendapatan Daerah terletak di Jalan Sutan Syahrir no.22 Pangkalan Bun,Kabupaten Kotawaringin Barat,Provinsi kalimantan Tengah.<br />\r\nJenis jenis pajak yang diterima dan dikelola oleh BAPENDA adalah :</p>\r\n\r\n<p style=\"margin-left:0cm; margin-right:0cm\">1. Pajak Hotel</p>\r\n\r\n<p style=\"margin-left:0cm; margin-right:0cm\">2. Pajak Restoran</p>\r\n\r\n<p style=\"margin-left:0cm; margin-right:0cm\">3. Pajak Hiburan</p>\r\n\r\n<p style=\"margin-left:0cm; margin-right:0cm\">4. Pajak Sarang Burung Walet</p>\r\n\r\n<p style=\"margin-left:0cm; margin-right:0cm\">5. Pajak Mineral Bukan Logam dan Batuan</p>\r\n\r\n<p style=\"margin-left:0cm; margin-right:0cm\">6. Pajak Air Tanah</p>\r\n\r\n<p style=\"margin-left:0cm; margin-right:0cm\">7. Pajak Penerangan Jalan ( PPJ )</p>\r\n\r\n<p style=\"margin-left:0cm; margin-right:0cm\">8. Pajak Reklame</p>\r\n\r\n<p style=\"margin-left:0cm; margin-right:0cm\">9. Pajak Parkir</p>\r\n\r\n<p style=\"margin-left:0cm; margin-right:0cm\">10. Bea Perolehan Hak Atas Tanah dan Bangunan ( BPHTB )</p>\r\n\r\n<p style=\"margin-left:0cm; margin-right:0cm\">11. Pajak Bumi dan Bangunan Sektor Pedesaan dan Perkotaan ( PBB - P2 )</p>\r\n\r\n<p style=\"margin-left:0cm; margin-right:0cm\">Bapenda Kabupaten Kotawaringin Barat berupaya selalu memberikan pelayanan terbaik kepada masyarakat dalam hal pelayanan pajak dan retribusi daerah.</p>\r\n\r\n<ol>\r\n	<li style=\"list-style-type:none\">\r\n	<ol>\r\n		<li><strong>Tugas, Fungsi dan &nbsp;Struktur Organisasi</strong></li>\r\n	</ol>\r\n	</li>\r\n</ol>\r\n\r\n<p style=\"margin-left:0cm; margin-right:0cm\">Sejalan dengan Peraturan Pemerintah Nomor 18 Tahun 2016 tentang Perangkat Daerah (Lembaran Negara Republik Indonesia Tahun 2016 Nomor 114), Peraturan Daerah Kabupaten Kotawaringin Barat&nbsp; Nomor&nbsp; 6 Tahun 2016 Tentang&nbsp; Pembentukan dan Susunan Perangkat&nbsp; Daerah Kabupaten Kotawaringin Barat&nbsp; yang&nbsp; didukung dengan Perbup No. 70 Kedudukan, Susunan Organisasi, Tugas Dan Fungsi Badan Pendapatan Daerah Kabupaten Kotawaringin Barat.</p>\r\n\r\n<p style=\"margin-left:0cm; margin-right:0cm\">Susunan organisasi Badan Pendapatan Daerah terdiri atas:</p>\r\n\r\n<ol>\r\n	<li>Kepala Badan.</li>\r\n	<li>Sekretaris terdiri dari:</li>\r\n</ol>\r\n\r\n<ol>\r\n	<li>Sub Bagian Perencanaan dan Pengendalian Program;</li>\r\n	<li>Sub Bagian Keuangan;</li>\r\n	<li>Sub Bagian Umum, Kepegawaian dan Perlengkapan.</li>\r\n</ol>\r\n\r\n<ol start=\"3\">\r\n	<li>Bidang terdiri dari :</li>\r\n</ol>\r\n\r\n<ol style=\"list-style-type:lower-alpha\">\r\n	<li>Bidang Pendaftaran, Pendataan, Pengolahan Data dan Dokumentasi, terdiri dari :</li>\r\n</ol>\r\n\r\n<ol>\r\n	<li>Sub Bidang Pendaftaran, Pendataan dan Penilaian;</li>\r\n	<li>Sub Bidang Pengolahan Data dan Dokumentasi;</li>\r\n</ol>\r\n\r\n<ol start=\"2\" style=\"list-style-type:lower-alpha\">\r\n	<li>Bidang Perhitungan, Penetapan dan Keberatan, terdiri dari :</li>\r\n</ol>\r\n\r\n<ol>\r\n	<li>Sub BidangPerhitungan dan Penetapan Pajak Daerah;</li>\r\n	<li>Sub Bidang Perhitungan dan Penetapan PBB-P2 dan &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; BPHTB;</li>\r\n	<li>Sub Bidang Penanganan Keberatan.</li>\r\n</ol>\r\n\r\n<ol start=\"3\" style=\"list-style-type:lower-alpha\">\r\n	<li>Bidang Penagihan, Pembukuan dan Pelaporan, terdiri dari :</li>\r\n</ol>\r\n\r\n<ol>\r\n	<li>Sub Bidang Penagihan Pajak Daerah;</li>\r\n	<li>Sub Bidang Penagihan PBB-P2 dan BPHTB;</li>\r\n	<li>Sub Bidang Pembukuan dan Pelaporan.</li>\r\n</ol>\r\n\r\n<ol start=\"4\" style=\"list-style-type:lower-alpha\">\r\n	<li>Bidang Pengawasan, Pemeriksaan dan Pengembangan, terdiri dari :</li>\r\n</ol>\r\n\r\n<ol>\r\n	<li>Sub Bidang Monitoring dan Pengawasan PAD;</li>\r\n	<li>Sub Bidang Pemeriksaan dan Penyidikan Pajak Daerah;</li>\r\n	<li>Sub Bidang Evaluasi dan Pengembangan PAD.</li>\r\n</ol>\r\n\r\n<ol start=\"4\">\r\n	<li>Unit Pelaksana Teknis Badan (UPTB).</li>\r\n	<li>Kelompok Jabatan Fungsional.</li>\r\n</ol>\r\n\r\n<p style=\"margin-left:0cm; margin-right:0cm\">&nbsp;</p>\r\n\r\n<p style=\"margin-left:0cm; margin-right:0cm\"><strong>Tugas Pokok</strong> : membantu Bupati dalam melaksanakan urusan Pemerintahan Daerah di Bidang Pendapatan Daerah berdasarkan Azas Otonomi dan Tugas Pembantuan.</p>\r\n\r\n<p style=\"margin-left:0cm; margin-right:0cm\">&nbsp;</p>\r\n\r\n<p style=\"margin-left:0cm; margin-right:0cm\"><strong>Fungsi Badan Pendapatan Daerah</strong> :</p>\r\n\r\n<ol>\r\n	<li style=\"list-style-type:none\">\r\n	<ol style=\"list-style-type:lower-alpha\">\r\n		<li>Perencanaan dan perumusan program, kegiatan pendapatan daerah sesuai visi dan misi Kepala Daerah;</li>\r\n		<li>Pelaksanaan pendaftaran, pendataan, pengolahan, pendokumentasian data&nbsp; Pajak Daerah dan BPHTB;</li>\r\n		<li>Pelaksanaan perhitungan, penetapan, penagihan, penanganan keberatan, pembukuan dan pelaporan;</li>\r\n		<li>Pelaksanaan monitoring, pengawasan, pemeriksaan, penyidikan, evaluasi dan pengembangan;</li>\r\n		<li>Pelaksanaan koordinasi dengan pihak-pihak terkait untuk kelancaran tugas di lapangan;</li>\r\n		<li>Penyelenggaraan ketatausahaan Badan;</li>\r\n		<li>Pembinaan kegiatan UPTD;</li>\r\n		<li>Pelaksanaan tugas-tugas lain yang diberikan oleh Bupati sesuai dengan tugas fungsinya.</li>\r\n	</ol>\r\n	</li>\r\n</ol>\r\n\r\n<p style=\"margin-left:0cm; margin-right:0cm\">&nbsp;</p>\r\n\r\n<p style=\"margin-left:0cm; margin-right:0cm\">Untuk melaksanakan fungsi sebagaimana dimaksud dalam Pasal 6, Badan Pendapatan Daerah mempunyai kewenangan sebagai berikut :</p>\r\n\r\n<ol style=\"list-style-type:lower-alpha\">\r\n	<li>Menetapkan kebijakan pengelolaan pendapatan daerah;</li>\r\n	<li>Melaksanakan pendaftaran, pendataan, pengolahan, pendokumentasian data&nbsp; Pajak Daerah dan BPHTB;</li>\r\n	<li>Melaksanakan perhitungan, penetapan, penagihan, penanganan keberatan, pembukuan dan pelaporan;</li>\r\n	<li>Pelaksanaan monitoring, pengawasan, pemeriksaan, penyidikan, evaluasi dan pengembangan;</li>\r\n	<li>Melaksanakan koordinasi dengan pihak-pihak terkait untuk kelancaran tugas di lapangan;</li>\r\n	<li>Menyelenggarakan ketatausahaan Badan;</li>\r\n	<li>Melakukan pembinaan kegiatan UPTD; dan</li>\r\n	<li>Melaksanaan tugas-tugas lain yang diberikan dan dilimpahkan oleh Bupati.</li>\r\n</ol>\r\n\r\n<p style=\"margin-left:0cm; margin-right:0cm\">&nbsp;</p>\r\n\r\n<p style=\"margin-left:0cm; margin-right:0cm\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>\r\n', 1, '', NULL),
(21, 'Visi Misi', 35, 'visi-misi', '1', '<p><strong>Visi</strong> :</p>\r\n\r\n<ul>\r\n	<li><strong><span style=\"font-size:12.0pt\">&ldquo;</span><span style=\"font-size:12.0pt\">Gerakan Membangun Kotawaringin Barat Menuju Kejayaan </span>&nbsp;<span style=\"font-size:12.0pt\">Dengan</span> <span style=\"font-size:12.0pt\">Kerja</span> <span style=\"font-size:12.0pt\">N</span><span style=\"font-size:12.0pt\">y</span><span style=\"font-size:12.0pt\">ata</span><span style=\"font-size:12.0pt\"> d</span><span style=\"font-size:12.0pt\">an</span> <span style=\"font-size:12.0pt\">I</span><span style=\"font-size:12.0pt\">khl</span><span style=\"font-size:12.0pt\">a</span><span style=\"font-size:12.0pt\">s</span><span style=\"font-size:12.0pt\">&rdquo;</span> </strong><span style=\"font-size:12.0pt\">dan sebagai perwujudan dari bagian rencana strategis Badan pendapatan Daerah Kabupaten Kotawaringin Barat</span><span style=\"font-size:12.0pt\">.</span></li>\r\n</ul>\r\n\r\n<p><br />\r\n<strong>Misi :</strong></p>\r\n\r\n<ul>\r\n	<li><span style=\"font-size:11pt\"><span style=\"font-family:Calibri,sans-serif\"><span style=\"font-size:12.0pt\">Memperkuat tata pemerintahan yang bersih, efektif, demokratis dan transparan;</span></span></span></li>\r\n	<li><span style=\"font-size:11pt\"><span style=\"font-family:Calibri,sans-serif\"><span style=\"font-size:12.0pt\">Meningkatkan kwalitas hidup manusia melalui pendidikan, kesehatan dan olah raga;</span></span></span></li>\r\n	<li><span style=\"font-size:11pt\"><span style=\"font-family:Calibri,sans-serif\"><span style=\"font-size:12.0pt\">Mendorong penguatan kemandirian ekonomi yang berbasis pada pertanian dalam arti luas, kelautan, industri serta pengelolaan potensi daerah dan sumber energi </span><span style=\"font-size:12.0pt\">&nbsp;melalui infrastruktur dan </span><span style=\"font-size:12.0pt\">memperhatikan lingkungan hidup;</span></span></span></li>\r\n	<li><span style=\"font-size:11pt\"><span style=\"font-family:Calibri,sans-serif\"><span style=\"font-size:12.0pt\">Meningkatkan kualitas kehidupan beragama dan bermasyarakat;</span></span></span></li>\r\n	<li><span style=\"font-size:11pt\"><span style=\"font-family:Calibri,sans-serif\"><span style=\"font-size:12.0pt\">Mewujudkan kondisi masyarakat yang aman, tentram dan dinamis;</span></span></span></li>\r\n	<li><span style=\"font-size:11pt\"><span style=\"font-family:Calibri,sans-serif\"><span style=\"font-size:12.0pt\">Melestarikan situs budaya, kesenian lokal dan masyarakat lainnya guna meningkatkan kunjungan wisata;</span></span></span></li>\r\n</ul>\r\n', 1, '', NULL),
(22, 'Kepala Bapenda', 35, 'kepala-bapenda', '1', '<div>\r\n<div>\r\n<div>Assalamualaikum Wr. Wb.</div>\r\n\r\n<div>Selamat Datang di Website Badan Pengelola Pendapatan Daerah Kabupaten Kotawaringin Barat, Puji Syukur atas segala nikmat dan karunia yang telah kita terima hingga saat ini. Bapenda selaku Koordinator SKPD Pemungut Pajak Daerah&nbsp;dan BPHTB selalu akan berusaha memberikan Pelayanan yang terbaik. Inovasi dan kreatifitas akan selalu kita kembangkan demi tercapainya standar pelayanan yang prima.<br />\r\n<br />\r\nEra globalisasi yang sangat berkembang cepat ini, kita selalu dituntut dengan kecepatan informasi, dengan adanya website Bapenda ini nantinya diharapkan sangat membantu masyarakat dalam hal mengenai informasi, bahkan bukan hanya informasi, tetapi juga berhubungan dengan Pelayanan Pajak seperti pembayaran pajak secara online, menghitung pajak secara online dan lain-lain kedepannya akan selalu kita kembangkan.<br />\r\n<br />\r\nakhir kata semoga website ini bermanfaat bagi seluruh lapisan masyarakat yang ingin memperoleh informasi seputar pajak-pajak daerah.<br />\r\n<br />\r\nwasalamualaikum wr. wb.</div>\r\n</div>\r\n</div>\r\n', 1, '', 2),
(23, 'Tupoksi', 35, 'tupoksi', '1', '', 1, '', NULL),
(24, 'Sekretariat', 23, 'sekretariat', '1', '<p style=\"margin-left:0in; margin-right:0in; text-align:justify\"><strong>Sekretariat</strong> merupakan unsur pelaksana Badan Pendapatan Daerah yang dipimpin oleh Sekretaris dan mempunyai tugas pokok merencanakan, menyusun, melaksanakan, mengatur, mengevaluasi dan melaporkan pelaksanaan pelayanan umum, kepegawaian dan perlengkapan, keuangan, perencanaan dan pengendalian program, serta mengkoordinasikan penyelenggaraan, evaluasi dan pelaporan tugas bidang secara terpadu.</p>\r\n\r\n<p style=\"margin-left:0in; margin-right:0in\"><strong>Pasal 11</strong></p>\r\n\r\n<p style=\"margin-left:0in; margin-right:0in; text-align:justify\">Untuk melaksanakan tugas sebagaimana dimaksud dalam Pasal 10,&nbsp; Sekretaris menyelenggarakan fungsi&nbsp; :</p>\r\n\r\n<ol>\r\n	<li style=\"list-style-type:none\">\r\n	<ol style=\"list-style-type:lower-alpha\">\r\n		<li style=\"text-align:justify\">Mengkoordinasikan penyusunan rencana program kegiatan dan anggaran SKPD;</li>\r\n		<li style=\"text-align:justify\">Mengatur pelaksanaan urusan umum, kepegawaian dan perlengkapan SKPD ;</li>\r\n		<li style=\"text-align:justify\">Mengatur pelaksanaan administrasi pengelolaan keuangan SKPD;</li>\r\n		<li style=\"text-align:justify\">Menyusun evaluasi dan pelaporan kegiatan SKPD ;</li>\r\n		<li style=\"text-align:justify\">Mengkoordinasikan dan membina pelaksanaan tugas bidang secara terpadu ; dan</li>\r\n	</ol>\r\n	</li>\r\n</ol>\r\n\r\n<p style=\"margin-left:0in; margin-right:0in; text-align:justify\">f.&nbsp; pelaksanaan tugas lain yang diberikan oleh Kepala Badan Pendapatan Daerah.</p>\r\n\r\n<p style=\"margin-left:0in; margin-right:0in\">&nbsp;</p>\r\n\r\n<p style=\"margin-left:0in; margin-right:0in\"><strong>Pasal 12</strong></p>\r\n\r\n<p style=\"margin-left:0in; margin-right:0in; text-align:justify\">Sekretaris terdiri dari :</p>\r\n\r\n<ol>\r\n	<li style=\"text-align:justify\">Sub Bagian Perencanaan dan Pengendalian Program</li>\r\n	<li style=\"text-align:justify\">Sub Bagian Keuangan</li>\r\n	<li style=\"text-align:justify\">Sub Bagian Umum, Kepegawaian,&nbsp; dan Perlengkapan</li>\r\n</ol>\r\n\r\n<p style=\"margin-left:0in; margin-right:0in\">&nbsp;</p>\r\n\r\n<p style=\"margin-left:0in; margin-right:0in; text-align:center\">&nbsp;</p>\r\n\r\n<p style=\"margin-left:0in; margin-right:0in; text-align:center\">&nbsp;</p>\r\n\r\n<p style=\"margin-left:0in; margin-right:0in; text-align:center\">&nbsp;</p>\r\n\r\n<p style=\"margin-left:0in; margin-right:0in; text-align:center\"><strong>Paragraf 1</strong></p>\r\n\r\n<p style=\"margin-left:0in; margin-right:0in; text-align:center\"><strong>Sub Bagian Perencanaan dan Pengendalian Program</strong></p>\r\n\r\n<p style=\"margin-left:0in; margin-right:0in; text-align:center\">&nbsp;</p>\r\n\r\n<p style=\"margin-left:0in; margin-right:0in; text-align:center\"><strong>Pasal 13</strong></p>\r\n\r\n<p style=\"margin-left:0in; margin-right:0in; text-align:center\">&nbsp;</p>\r\n\r\n<ol>\r\n	<li style=\"list-style-type:none\">\r\n	<ol>\r\n		<li style=\"list-style-type:none\">\r\n		<ol>\r\n			<li style=\"list-style-type:none\">\r\n			<ol>\r\n				<li style=\"text-align:justify\">Sub Bagian Perencanaan dan Pengendalian Program merupakan unsur pelaksana Sekretariat, yang dipimpin oleh Kepala Sub Bagian mempunyai tugas pokok menyiapkan dan menghimpun data dari bidang sebagai bahan penyusunan program dan anggaran Badan pendapatan daerah, serta menghimpun bahan penyusunan pelaporan.</li>\r\n			</ol>\r\n			</li>\r\n		</ol>\r\n		</li>\r\n	</ol>\r\n	</li>\r\n</ol>\r\n\r\n<p style=\"margin-left:0.5in; margin-right:0in; text-align:justify\">&nbsp;</p>\r\n\r\n<ol start=\"0\" style=\"list-style-type:lower-alpha\">\r\n	<li style=\"list-style-type:none\">\r\n	<ol>\r\n		<li style=\"list-style-type:none\">\r\n		<ol>\r\n			<li style=\"list-style-type:none\">\r\n			<ol start=\"2\">\r\n				<li style=\"text-align:justify\">Kepala Sub Bagian Perencanaan dan Pengendalian Program menyelenggarakan fungsi&nbsp; :</li>\r\n			</ol>\r\n			</li>\r\n		</ol>\r\n		</li>\r\n	</ol>\r\n	</li>\r\n	<li style=\"text-align:justify\">Menyiapkan bahan dan data penyusunan program dan anggaran;</li>\r\n	<li style=\"text-align:justify\">Menghimpun data dari semua bidang sebagai bahan dalam penyusunan program dan anggaran ;</li>\r\n	<li style=\"text-align:justify\">Pelaksanaan penyusunan program dan anggaran;</li>\r\n	<li style=\"text-align:justify\">Menyelenggarakan pelaporan dinas ; dan</li>\r\n	<li style=\"text-align:justify\">Pembinaan, pengendalian, monitoring, evaluasi dan pelaporan pelaksanaan tugas ; dan</li>\r\n	<li style=\"text-align:justify\">melaksanakan tugas lain yang diberikan oleh&nbsp; Sekretaris secara berkala dalam rangka kelancaran pelaksanaan tugas kesekretariatan.</li>\r\n</ol>\r\n\r\n<p style=\"margin-left:0in; margin-right:0in; text-align:center\">&nbsp;</p>\r\n\r\n<p style=\"margin-left:0in; margin-right:0in; text-align:center\"><strong>Paragraf 2</strong></p>\r\n\r\n<p style=\"margin-left:0in; margin-right:0in; text-align:center\"><strong>Sub Bagian Keuangan</strong></p>\r\n\r\n<p style=\"margin-left:0in; margin-right:0in; text-align:center\">&nbsp;</p>\r\n\r\n<p style=\"margin-left:0in; margin-right:0in; text-align:center\"><strong>Pasal 14</strong></p>\r\n\r\n<ol>\r\n	<li style=\"text-align:justify\">Sub Bagian Keuangan merupakan unsur pelaksana Sekretariat, yang dipimpin oleh Kepala Sub Bagian mempunyai tugas pokok menyelenggarakan penyusunan rencana, pengelolaan dan pengendalian keuangan, melaksanakan penatausahaan dan pelaporan keuangan</li>\r\n</ol>\r\n\r\n<p style=\"margin-left:0.5in; margin-right:0in; text-align:justify\">&nbsp;</p>\r\n\r\n<ol start=\"2\">\r\n	<li style=\"text-align:justify\">Kepala Sub Bagian Keuangan menyelenggarakan fungsi :</li>\r\n</ol>\r\n\r\n<p style=\"margin-left:0in; margin-right:0in; text-align:justify\">a. &nbsp; Menyusun rencana kegiatan anggaran keuangan.</p>\r\n\r\n<p style=\"margin-left:0in; margin-right:0in; text-align:justify\">b. &nbsp; Melaksanakan pengelolaan dan pengendalian keuangan.</p>\r\n\r\n<p style=\"margin-left:0in; margin-right:0in; text-align:justify\">c. &nbsp; Menghimpun, mengklarifikasi serta mengolah data dan bahan analisa pelaksanaan anggaran, pembukuan,</p>\r\n\r\n<p style=\"margin-left:0in; margin-right:0in; text-align:justify\">d. &nbsp; Perbendaharaan dan verifikasi.</p>\r\n\r\n<p style=\"margin-left:0in; margin-right:0in; text-align:justify\">e. &nbsp; Melaksanakan akuntansi keuangan; dan</p>\r\n\r\n<p style=\"margin-left:0in; margin-right:0in; text-align:justify\">f. &nbsp;&nbsp; Melaksanakan tugas lain yang diberikan oleh Sekretaris secara berkala dalam rangka kelancaran pelaksanaan tugas kesekretariatan.</p>\r\n\r\n<p style=\"margin-left:0in; margin-right:0in; text-align:justify\">&nbsp;</p>\r\n\r\n<p style=\"margin-left:0in; margin-right:0in; text-align:center\">&nbsp;</p>\r\n\r\n<p style=\"margin-left:0in; margin-right:0in; text-align:center\">&nbsp;</p>\r\n\r\n<p style=\"margin-left:0in; margin-right:0in; text-align:center\">&nbsp;</p>\r\n\r\n<p style=\"margin-left:0in; margin-right:0in; text-align:center\">&nbsp;</p>\r\n\r\n<p style=\"margin-left:0in; margin-right:0in; text-align:center\">&nbsp;</p>\r\n\r\n<p style=\"margin-left:0in; margin-right:0in; text-align:center\"><strong>Paragraf 3</strong></p>\r\n\r\n<p style=\"margin-left:0in; margin-right:0in; text-align:center\"><strong>Sub Bagian Umum, Kepegawaian dan Perlengkapan </strong></p>\r\n\r\n<p style=\"margin-left:0in; margin-right:0in; text-align:center\">&nbsp;</p>\r\n\r\n<p style=\"margin-left:0in; margin-right:0in; text-align:center\"><strong>Pasal 15</strong></p>\r\n\r\n<ol>\r\n	<li style=\"text-align:justify\">Sub Bagian Umum, Kepegawaian, dan Perlengkapan merupakan unsur pelaksana Sekretariat, yang dipimpin oleh Kepala Sub Bagian mempunyai tugas pokok menyelenggarakan pelayanan umum, administrasi perkantoran, kepegawaian, kehumasan dan protokol serta perlengkapan.</li>\r\n</ol>\r\n\r\n<p style=\"margin-left:0.5in; margin-right:0in; text-align:justify\">&nbsp;</p>\r\n\r\n<ol start=\"2\">\r\n	<li style=\"text-align:justify\">Kepala Sub Bagian Umum, Kepegawaian,&nbsp; dan Perlengkapan, menyelenggarakan fungsi :&nbsp;&nbsp;</li>\r\n</ol>\r\n\r\n<p style=\"margin-left:0in; margin-right:0in; text-align:justify\">a. Menyelenggarakan pelayanan umum, tata usaha dan surat menyurat.</p>\r\n\r\n<p style=\"margin-left:0in; margin-right:0in; text-align:justify\">b.&nbsp;&nbsp; Melakukan perencanaan kebutuhan, pengelolaan dan pengendalian, perlengkapan, perbekalan serta sarana dan prasarana</p>\r\n\r\n<p style=\"margin-left:0in; margin-right:0in; text-align:justify\">c. &nbsp; Melaksanakan urusan kepegawaian.</p>\r\n\r\n<p style=\"margin-left:0in; margin-right:0in; text-align:justify\">d. &nbsp; Melaksanakan urusan kehumasan dan keprotokolan.</p>\r\n\r\n<p style=\"margin-left:0in; margin-right:0in; text-align:justify\">e. &nbsp; Melaksanakan urusan perpustakaan dan kearsipan.</p>\r\n\r\n<p style=\"margin-left:0in; margin-right:0in; text-align:justify\">f. &nbsp;&nbsp; Menyiapkan bahan peraturan perundang-undangan; dan</p>\r\n\r\n<p style=\"margin-left:0in; margin-right:0in; text-align:justify\">g. Melaksanakan tugas lain yang diberikan oleh&nbsp; Sekretaris secara berkala dalam rangka kelancaran pelaksanaan tugas kesekretariatan.</p>\r\n\r\n<p style=\"margin-left:0in; margin-right:0in\">&nbsp;</p>\r\n', 1, '', NULL),
(25, 'Bidang Pendaftaran, Pendataan, Pengolahan Data & Dokumentasi', 23, 'bidang-pendaftaran-pendataan-pengolahan-data-dokumentasi', '1', '<div>\r\n<div>\r\n<div style=\"text-align:justify\">\r\n<p style=\"margin-left:0in; margin-right:0in; text-align:center\"><strong>Bidang Penda</strong><strong>ftaran, Pendataan, Pengolahan Data dan Dokumentasi</strong></p>\r\n\r\n<p style=\"margin-left:0in; margin-right:0in; text-align:center\">&nbsp;</p>\r\n\r\n<p style=\"margin-left:0in; margin-right:0in; text-align:center\"><strong>Pasal 16</strong></p>\r\n\r\n<p style=\"margin-left:0in; margin-right:0in\">&nbsp;</p>\r\n\r\n<p style=\"margin-left:0in; margin-right:0in; text-align:justify\">Bidang Pendaftaran, Pendataan, Pengolahan Data dan Dokumentasi merupakan unsur pelaksana Badan Pendapatan Daerah yang dipimpin oleh Kepala Bidang dan mempunyai tugas pokok membantu Kepala Badan dalam membina, merencanakan dan mendistribusikan kegiatan pendaftaran, pendataan dan pengolahan data dan dokumentasi wajib pajak daerah sesuai dengan ketentuan perundang-undangan yang berlaku.</p>\r\n\r\n<p style=\"margin-left:0in; margin-right:0in; text-align:justify\">&nbsp;</p>\r\n\r\n<p style=\"margin-left:0in; margin-right:0in; text-align:justify\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>Pasal 17</strong></p>\r\n\r\n<p style=\"margin-left:0in; margin-right:0in; text-align:justify\">&nbsp;</p>\r\n\r\n<p style=\"margin-left:0in; margin-right:0in; text-align:justify\">Untuk melaksanakan tugas sebagaimana dimaksud dalam pasal 16, Kepala Bidang Pendaftaran, Pendataan, Pengolahan Data dan Dokumentasi dalam melaksanakan tugasmenyelenggarakan fungsi :</p>\r\n\r\n<ol style=\"list-style-type:lower-alpha\">\r\n	<li style=\"text-align:justify\">Perumusan rencana kegiatan Bidang Pendaftaran, Pendataan, Pengolahan Data dan Dokumentasi;</li>\r\n	<li style=\"text-align:justify\">Penyiapan petunjuk pelaksanaan Bidang Pendaftaran, Pendataan, Pengolahan Data dan Dokumentasi;</li>\r\n	<li style=\"text-align:justify\">Pelaksanaan Pendaftaran, Pendataan, Pengolahan Data dan Dokumentasi; dan</li>\r\n	<li style=\"text-align:justify\">Pelaksanaan tugas lain yang diberikan oleh atasan sesuai dengan tugas dan fungsinya.</li>\r\n</ol>\r\n\r\n<p style=\"margin-left:0.5in; margin-right:0in; text-align:justify\">&nbsp;</p>\r\n\r\n<p style=\"margin-left:0in; margin-right:0in; text-align:center\"><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Pasal 18</strong></p>\r\n\r\n<p style=\"margin-left:0in; margin-right:0in; text-align:center\">&nbsp;</p>\r\n\r\n<p style=\"margin-left:0in; margin-right:0in; text-align:justify\">Bidang Pendaftaran, Pendataan, Pengolahan Data dan Dokumentasi terdiri dari :</p>\r\n\r\n<p style=\"margin-left:0.5in; margin-right:0in; text-align:justify\">1. Sub Bidang Pendaftaran, Pendataan dan Penilaian;</p>\r\n\r\n<p style=\"margin-left:0.5in; margin-right:0in; text-align:justify\">2. Sub Bidang Pengolahan Data dan Dokumentasi.</p>\r\n\r\n<p style=\"margin-left:0in; margin-right:0in; text-align:center\">&nbsp;</p>\r\n\r\n<p style=\"margin-left:0in; margin-right:0in; text-align:center\"><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Paragraf 1</strong></p>\r\n\r\n<p style=\"margin-left:0in; margin-right:0in; text-align:center\"><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </strong><strong>Sub Bidang Pendaftaran</strong><strong>, </strong><strong>Pendataan dan Penilaian</strong></p>\r\n\r\n<p style=\"margin-left:0in; margin-right:0in; text-align:center\">&nbsp;</p>\r\n\r\n<p style=\"margin-left:0in; margin-right:0in; text-align:center\"><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </strong><strong>&nbsp;&nbsp;&nbsp; </strong><strong>Pasal 19</strong></p>\r\n\r\n<p style=\"margin-left:0in; margin-right:0in; text-align:center\">&nbsp;</p>\r\n\r\n<ol>\r\n	<li style=\"text-align:justify\">Sub Bidang Pendaftaran, Pendataan dan Penilaian merupakan unsur pelaksana Bidang Pendaftaran, Pendataan, Pengolahan Data dan Dokumentasi yang dipimpin oleh Kepala Sub Bidang mempunyai tugas pokok membantu Kepala Bidang dalam menyiapkan bahan Pendaftaran, Pendataan dan Penilaian.</li>\r\n</ol>\r\n\r\n<p style=\"margin-left:0.5in; margin-right:0in; text-align:justify\">&nbsp;</p>\r\n\r\n<ol start=\"2\">\r\n	<li style=\"text-align:justify\">Kepala Sub Bidang Pendaftaran, Pendataan dan Penilaian, menyelenggarakan fungsi :<strong> </strong>\r\n\r\n	<ol style=\"list-style-type:lower-alpha\">\r\n		<li style=\"text-align:justify\">Penyusunan Daftar Induk Wajib Pajak Daerah Daerah dengan pemberikan NPWPD kepada Wajib Pajak Daerah, potensi pajak untuk bahan perencanaan penerimaan;</li>\r\n		<li style=\"text-align:justify\">Penerimaan dan penelitian isian SPT Wajib Pajak;</li>\r\n		<li style=\"text-align:justify\">Pelaksanaan pendaftaran terhadap Wajib Pajak Daerah.</li>\r\n		<li style=\"text-align:justify\">Pelaksanaan legalisasi Tanda Lunas Bayar Pajak kepada Wajib Pajak;</li>\r\n		<li style=\"text-align:justify\">Penghimpunan serta pencatatan data objek pajak daerah;</li>\r\n		<li style=\"text-align:justify\">Pelaksanakan kegiatan pendistribusian dan menerima kembali formulir pendaftaran yang telah diisi oleh wajib pajak daerah;</li>\r\n		<li style=\"text-align:justify\">Pelaksanaaan serta pencatatan nama dan alamat calon wajib pajak daerah dalam daftar wajib pajak daerah;</li>\r\n		<li style=\"text-align:justify\">pemeliharaan daftar inventaris wajib pajak daerah;</li>\r\n		<li style=\"text-align:justify\" value=\"1\">Pendataan objek dan Subjek Pajak Daerah;</li>\r\n		<li style=\"text-align:justify\" value=\"10\">Pengarsipan surat perpajakan daerah yang berkaitan dengan pendataan;</li>\r\n		<li style=\"text-align:justify\">Pengelolaan data objek dan subjek pajak;</li>\r\n		<li style=\"text-align:justify\" value=\"50\">Pelaksanaan pemutakhiran data terhadap objek dan Subjek Pajak Daerah;</li>\r\n		<li style=\"text-align:justify\" value=\"1000\">Pengecekan kelapangan data objek PBB-P2;</li>\r\n		<li style=\"text-align:justify\" value=\"14\">Penentuan ZNT objek PBB-P2;</li>\r\n		<li style=\"text-align:justify\">Pelaksanaan penaksiran objek PBB-P2</li>\r\n		<li style=\"text-align:justify\">Melaksanakan tugas lain yang diberikan oleh Kepala Bidang secara berkala dalam rangka kelancaran pelaksanaan tugas Bidang Pendaftaran, Pendataan, Pengolahan Data dan Dokumentasi.</li>\r\n	</ol>\r\n	</li>\r\n</ol>\r\n\r\n<p style=\"margin-left:0in; margin-right:0in\"><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </strong></p>\r\n\r\n<p style=\"margin-left:0in; margin-right:0in; text-align:center\"><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </strong></p>\r\n\r\n<p style=\"margin-left:0in; margin-right:0in; text-align:center\">&nbsp;</p>\r\n\r\n<p style=\"margin-left:0in; margin-right:0in; text-align:center\">&nbsp;</p>\r\n\r\n<p style=\"margin-left:0in; margin-right:0in; text-align:center\">&nbsp;</p>\r\n\r\n<p style=\"margin-left:0in; margin-right:0in; text-align:center\">&nbsp;</p>\r\n\r\n<p style=\"margin-left:0in; margin-right:0in; text-align:center\">&nbsp;</p>\r\n\r\n<p style=\"margin-left:0in; margin-right:0in; text-align:center\"><strong>Paragraf 2</strong></p>\r\n\r\n<p style=\"margin-left:0in; margin-right:0in; text-align:center\"><strong>Sub Bidang Pengolahan Data dan Dokumentasi</strong></p>\r\n\r\n<p style=\"margin-left:0in; margin-right:0in; text-align:center\">&nbsp;</p>\r\n\r\n<p style=\"margin-left:0in; margin-right:0in; text-align:center\"><strong>Pasal 20</strong></p>\r\n\r\n<p style=\"margin-left:0in; margin-right:0in; text-align:justify\">&nbsp;</p>\r\n\r\n<ol>\r\n	<li style=\"text-align:justify\">Sub Bidang Pengolahan Data dan Dokumentasi merupakan unsur pelaksana Bidang Pendaftaran, Pendataan, Pengolahan Data dan Dokumentasi yang dipimpin oleh Kepala Sub Bidang mempunyai tugas pengolahan data dan dokumentasi Pajak Daerah dan BPHTB.</li>\r\n</ol>\r\n\r\n<p style=\"margin-left:0.5in; margin-right:0in; text-align:justify\">&nbsp;</p>\r\n\r\n<ol start=\"2\">\r\n	<li style=\"text-align:justify\">Kepala Sub Bidang Pengolahan Data dan Dokumentasi, menyelenggarakan fungsi :<strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </strong></li>\r\n</ol>\r\n\r\n<ol style=\"list-style-type:lower-alpha\">\r\n	<li style=\"text-align:justify\">Perumusan rencana kegiatan Pengolahan Data dan Dokumentasi;</li>\r\n	<li style=\"text-align:justify\">Penyusunan petunjuk pelaksanaan Pengolahan Data dan Dokumentasi;</li>\r\n	<li style=\"text-align:justify\">Pengolahan data objek dan subjek pajak;</li>\r\n	<li style=\"text-align:justify\">Penyusunan data target penerimaanPendapatan Asli Daerah sesuai potensi yang ada;</li>\r\n	<li style=\"text-align:justify\">Pembuatan dan pemeliharaan daftar induk wajib pajak daerah; dan</li>\r\n	<li style=\"text-align:justify\">Pendokumentasian data Pajak Daerah; dan</li>\r\n	<li style=\"text-align:justify\">Melaksanakan tugas lain yang diberikan oleh Kepala Bidang secara berkala dalam rangka kelancaran pelaksanaan tugas Bidang Pendaftaran, Pendataan, Pengolahan Data dan Dokumentasi.</li>\r\n</ol>\r\n\r\n<p style=\"margin-left:0in; margin-right:0in\">&nbsp;</p>\r\n</div>\r\n</div>\r\n</div>\r\n', 1, '', NULL),
(26, 'JDIH Kobar', 0, 'jdih-kobar', '1', '', 1, 'http://jdih.kotawaringinbaratkab.go.id/', 2),
(28, 'Kepala Badan ', 23, 'kepala-badan-', '1', '<p style=\"margin-left:80px; margin-right:0in; text-align:center\"><strong>RINCIAN TUGAS POKOK DAN FUNGSI JABATAN</strong></p>\r\n\r\n<p style=\"margin-left:80px; margin-right:0in; text-align:center\"><strong>&nbsp;PADA BADAN PENDAPATAN DAERAH</strong></p>\r\n\r\n<p style=\"margin-left:0in; margin-right:0in; text-align:center\">&nbsp;</p>\r\n\r\n<p style=\"margin-left:0in; margin-right:0in; text-align:center\"><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Bagian </strong><strong>Kesatu</strong></p>\r\n\r\n<p style=\"margin-left:0in; margin-right:0in; text-align:center\"><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Kepala Badan</strong></p>\r\n\r\n<p style=\"margin-left:0in; margin-right:0in; text-align:center\">&nbsp;</p>\r\n\r\n<p style=\"margin-left:0in; margin-right:0in; text-align:center\"><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Pasal 8</strong></p>\r\n\r\n<p style=\"margin-left:0in; margin-right:0in; text-align:justify\">&nbsp;</p>\r\n\r\n<p style=\"margin-left:0in; margin-right:0in; text-align:justify\">Badan Pendapatan Daerah merupakan unsur penunjang Pemerintah Kabupaten, yang dipimpin oleh Kepala Badan dan mempunyai tugas pokok memimpin, membina, mengoordinasikan dan mengawasi penyelenggaraan Urusan Pemerintahan Daerah di bidang pendapatan daerah sesuai dengan ketentuan perundang-undangan yang berlaku.</p>\r\n\r\n<p style=\"margin-left:0in; margin-right:0in; text-align:justify\">&nbsp;</p>\r\n\r\n<p style=\"margin-left:0in; margin-right:0in; text-align:center\"><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Pasal 9</strong></p>\r\n\r\n<p style=\"margin-left:0in; margin-right:0in; text-align:center\">&nbsp;</p>\r\n\r\n<p style=\"margin-left:0in; margin-right:0in; text-align:justify\">Untuk melaksanakan tugas sebagaimana dimaksud dalam Pasal 8, Kepala Badan Pendapatan Daerah, menyelenggarakan fungsi :</p>\r\n\r\n<ol style=\"list-style-type:lower-alpha\">\r\n	<li style=\"text-align:justify\">Perumusan dan pelaksanan kebijakan teknis di bidang perpajakan daerah;</li>\r\n	<li style=\"text-align:justify\">Pengkajian, analisa, evaluasi dan pengembangan ketentuan peraturan perundang-undangan perpajakan daerah, retribusi daerah, dan lain-lain pendapatan asli daerah;</li>\r\n	<li style=\"text-align:justify\">Pelaksanaan pengelolaan pajak daerah dan BPHTB;</li>\r\n	<li style=\"text-align:justify\">Pelaksanaan perhitungan, penetapan, penagihan, penanganan keberatan, pembukuan dan pelaporan;</li>\r\n	<li style=\"text-align:justify\">Pelaksanaan koordinasi dalam rangka optimalisasi penerimaan pajak daerah, retribusi daerah, dana transfer, dan lain-lain pendapatan daerah;</li>\r\n	<li style=\"text-align:justify\">Pelaksanaan pembinaan, monitoring, pengawasan, pemeriksaan dan penyidikan;</li>\r\n	<li style=\"text-align:justify\">Melaksanakan tugas lainnya berdasarkan kuasa yang dilimpahkan oleh Bupati;</li>\r\n</ol>\r\n\r\n<p style=\"margin-left:0cm; margin-right:0cm\"><strong>&nbsp; &nbsp; &nbsp;</strong></p>\r\n', 4, '', NULL),
(29, 'Bidang Perhitungan, Penetapan dan Keberatan', 23, 'bidang-perhitungan-penetapan-dan-keberatan', '1', '&lt;p style=&quot;margin-left:0in; margin-right:0in; text-align:center&quot;&gt;&lt;strong&gt;Bidang &lt;/strong&gt;&lt;strong&gt;Perhitungan, Penetapan dan Keberatan&lt;/strong&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in; text-align:center&quot;&gt;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in; text-align:center&quot;&gt;&lt;strong&gt;Pasal 21&lt;/strong&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in; text-align:justify&quot;&gt;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in; text-align:justify&quot;&gt;Bidang Perhitungan, Penetapan dan Keberatan merupakan unsur pelaksana Badan Pendapatan Daerah yang dipimpin oleh Kepala Bidang dan mempunyai tugas pokok membantu Kepala Badan dalam membina, merencanakan, mengkoordinasikan, perhitungan, penetapan, dan&amp;nbsp; penanganan keberatan wajib pajak daerah dan BPHTB sesuai dengan ketentuan perundang-undangan yang berlaku.&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in; text-align:justify&quot;&gt;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in; text-align:center&quot;&gt;&lt;strong&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; Pasal 22&lt;/strong&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in; text-align:center&quot;&gt;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in; text-align:justify&quot;&gt;Untuk melaksanakan tugas sebagaimana dimaksud dalam pasal 21, Kepala Bidang Perhitungan, Penetapan dan Keberatan dalam melaksanakan tugas menyelenggarakan fungsi :&lt;/p&gt;\r\n\r\n&lt;ol style=&quot;list-style-type:lower-alpha&quot;&gt;\r\n	&lt;li style=&quot;text-align:justify&quot;&gt;Perencanaan operasional pelaksanaan program kerja Bidang Perhitungan, Penetapan dan Penanganan Keberatan;&lt;/li&gt;\r\n	&lt;li style=&quot;text-align:justify&quot;&gt;Pelaksanaan perhitungan, penetapan pajak daerah dan BPHTB;&lt;/li&gt;\r\n	&lt;li style=&quot;text-align:justify&quot;&gt;Penanganan keberatan Wajib Pajak Daerah dan BPHTB;&lt;/li&gt;\r\n	&lt;li style=&quot;text-align:justify&quot;&gt;Pelaksanaan Rekonsiliasi Ketetapan dengan Penyetoran Pajak Daerah dan BPHTB; dan&lt;/li&gt;\r\n	&lt;li style=&quot;text-align:justify&quot;&gt;Pelaksanaan tugas lain yang diberikan oleh atasan sesuai dengan tugas dan fungsinya.&lt;/li&gt;\r\n&lt;/ol&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in&quot;&gt;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in; text-align:center&quot;&gt;&lt;strong&gt;Pasal 23&lt;/strong&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in; text-align:center&quot;&gt;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in&quot;&gt;Bidang Perhitungan, Penetapan dan Keberatan, terdiri dari :&lt;/p&gt;\r\n\r\n&lt;ol&gt;\r\n	&lt;li style=&quot;text-align:justify&quot;&gt;Sub Bidang Perhitungan dan Penetapan Pajak Daerah dan Retribusi Daerah;&lt;/li&gt;\r\n	&lt;li style=&quot;text-align:justify&quot;&gt;Sub Bidang Perhitungan dan Penetapan PBB-P2 dan BPHTB;&lt;/li&gt;\r\n	&lt;li style=&quot;text-align:justify&quot;&gt;Sub Bidang Penanganan Keberatan.&lt;/li&gt;\r\n&lt;/ol&gt;\r\n\r\n&lt;p style=&quot;margin-left:0.5in; margin-right:0in&quot;&gt;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in; text-align:center&quot;&gt;&lt;strong&gt;Paragraf 1&lt;/strong&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0.5in; margin-right:0in; text-align:center&quot;&gt;&lt;strong&gt;Sub Bidang Perhitungan dan Penetapan Pajak Daerah &lt;/strong&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in; text-align:center&quot;&gt;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in; text-align:center&quot;&gt;&lt;strong&gt;Pasal 24&lt;/strong&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in; text-align:center&quot;&gt;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;ol&gt;\r\n	&lt;li style=&quot;text-align:justify&quot;&gt;Sub Bidang Perhitungan dan Penetapan Pajak Daerah merupakan unsur pelaksana Bidang Perhitungan, Penetapan dan Keberatan yang dipimpin oleh Kepala Sub Bidang mempunyai tugas pokok membantu Kepala Bidang dalam menyiapkan bahan perhitungan dan penetapan pajak daerah selain PBB-P2 dan BPHTB.&lt;/li&gt;\r\n&lt;/ol&gt;\r\n\r\n&lt;p style=&quot;margin-left:0.5in; margin-right:0in; text-align:justify&quot;&gt;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;ol start=&quot;2&quot;&gt;\r\n	&lt;li style=&quot;text-align:justify&quot;&gt;Kepala Sub Bidang Perhitungan dan Penetapan Pajak Daerah menyelenggarakan fungsi:&lt;/li&gt;\r\n&lt;/ol&gt;\r\n\r\n&lt;ol style=&quot;list-style-type:lower-alpha&quot;&gt;\r\n	&lt;li style=&quot;text-align:justify&quot;&gt;Penyusunan kegiatan program Sub Bidang Perhitungan dan Penetapan Pajak Daerah selaian PBB-P2 dan BPHTB;&lt;/li&gt;\r\n	&lt;li style=&quot;text-align:justify&quot;&gt;Perhitungan dan penetapan pajak daerah selain PBB-P2 dan BPHTB;&lt;/li&gt;\r\n	&lt;li style=&quot;text-align:justify&quot;&gt;Legalisasi Tanda Lunas Pembayaran Pajak kepada Wajib Pajak Daerah selain PBB-P2 dan BPHTB;&lt;/li&gt;\r\n	&lt;li style=&quot;text-align:justify&quot;&gt;Pembuatan laporan dalam pelaksanaan perhitungan dan penetapan pajak daerah selain PBB, P2 dan BPHTB; dan&lt;/li&gt;\r\n	&lt;li style=&quot;text-align:justify&quot;&gt;Melaksanakan tugas lain yang diberikan oleh Kepala Bidang secara berkala dalam rangka kelancaran pelaksanaan tugas Bidang Perhitungan, Penetapan dan Keberatan.&lt;/li&gt;\r\n&lt;/ol&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in; text-align:center&quot;&gt;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in; text-align:center&quot;&gt;&lt;strong&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; Paragraf 2&lt;/strong&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0.5in; margin-right:0in&quot;&gt;&lt;strong&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp; Sub Bidang Perhitungan dan Penetapan PBB-P2 dan BPHTB&lt;/strong&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in&quot;&gt;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in; text-align:center&quot;&gt;&lt;strong&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; Pasal 25&lt;/strong&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in; text-align:center&quot;&gt;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;ol&gt;\r\n	&lt;li style=&quot;text-align:justify&quot;&gt;Sub Bidang Perhitungan dan Penetapan PBB- P2 dan BPHTB merupakan unsur pelaksana Bidang Perhitungan, Penetapan dan Keberatan yang dipimpin oleh Kepala Sub Bidang mempunyai tugas pokok membantu Kepala Bidang dalam menyiapkan bahan perhitungan dan penetapan PBB &amp;ndash; P2 dan BPHTB.&lt;/li&gt;\r\n&lt;/ol&gt;\r\n\r\n&lt;p style=&quot;margin-left:0.5in; margin-right:0in; text-align:justify&quot;&gt;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;ol start=&quot;2&quot;&gt;\r\n	&lt;li style=&quot;text-align:justify&quot;&gt;Kepala Sub Bidang Perhitungan dan Penetapan PBB- P2 dan BPHTB, menyelenggarakan fungsi:&lt;/li&gt;\r\n&lt;/ol&gt;\r\n\r\n&lt;ol style=&quot;list-style-type:lower-alpha&quot;&gt;\r\n	&lt;li style=&quot;text-align:justify&quot;&gt;Penyusunan kegiatan program Sub Bidang Perhitungan dan Penetapan PBB dan BPHTB;&lt;/li&gt;\r\n	&lt;li style=&quot;text-align:justify&quot;&gt;Perhitungan dan penetapan PBB dan BPHTB;&lt;/li&gt;\r\n	&lt;li style=&quot;text-align:justify&quot;&gt;Verifikasi SSPD BPHTB;&lt;/li&gt;\r\n	&lt;li style=&quot;text-align:justify&quot;&gt;Pembuatan laporan dalam pelaksanaan perhitungan dan penetapan PBB- dan P2 dan BPHTB; dan&lt;/li&gt;\r\n	&lt;li style=&quot;text-align:justify&quot;&gt;Melaksanakan tugas lain yang diberikan oleh Kepala Bidang secara berkala dalam rangka kelancaran pelaksanaan tugas Bidang Perhitungan, Penetapan dan Keberatan.&lt;/li&gt;\r\n&lt;/ol&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in; text-align:center&quot;&gt;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in; text-align:center&quot;&gt;&lt;strong&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; Paragraf 3&lt;/strong&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in; text-align:center&quot;&gt;&lt;strong&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; &lt;/strong&gt;&lt;strong&gt;Sub Bidang Penanganan Keberatan&lt;/strong&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in; text-align:center&quot;&gt;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in; text-align:center&quot;&gt;&lt;strong&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; Pasal 26&lt;/strong&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in; text-align:center&quot;&gt;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;ol&gt;\r\n	&lt;li style=&quot;text-align:justify&quot;&gt;Sub Bidang Penanganan Keberatan merupakan unsur pelaksana Bidang Perhitungan, Penetapan dan Keberatan yang dipimpin oleh Kepala Sub Bidang mempunyai tugas pokok membantu Kepala Bidang menyiapkan dokumen pengajuan keberatan dari Wajib Pajak.&lt;/li&gt;\r\n&lt;/ol&gt;\r\n\r\n&lt;p style=&quot;margin-left:0.5in; margin-right:0in; text-align:justify&quot;&gt;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;ol start=&quot;2&quot;&gt;\r\n	&lt;li style=&quot;text-align:justify&quot;&gt;Kepala Sub Bidang Penanganan Keberatan, menyelenggarakan fungsi :&lt;/li&gt;\r\n&lt;/ol&gt;\r\n\r\n&lt;ol style=&quot;list-style-type:lower-alpha&quot;&gt;\r\n	&lt;li style=&quot;text-align:justify&quot;&gt;Penyusunan perencanaan kegiatan program kerja Sub Bidang Penanganan Keberatan;&lt;/li&gt;\r\n	&lt;li style=&quot;text-align:justify&quot;&gt;Pelaksanaan Pengecekan dan pengotrolan untuk pelaksanaan Sub Bidang Penanganan Keberatan;&lt;/li&gt;\r\n	&lt;li style=&quot;text-align:justify&quot;&gt;Penyiapan data untuk bahan penanganan keberatan;&lt;/li&gt;\r\n	&lt;li style=&quot;text-align:justify&quot;&gt;Melakukan Verifikasi lapangan terhadap pengajuan keberatan;&lt;/li&gt;\r\n	&lt;li style=&quot;text-align:justify&quot;&gt;Pembuatan laporan hasil Keberatan yang sudah dilakukan untuk bahan lebih lanjut; dan&lt;/li&gt;\r\n	&lt;li style=&quot;text-align:justify&quot;&gt;Melaksanakan tugas lain yang diberikan oleh Kepala Bidang secara berkala dalam rangka kelancaran pelaksanaan tugas Bidang Perhitungan, Penetapan dan Keberatan.&lt;/li&gt;\r\n&lt;/ol&gt;\r\n', 4, '', NULL);
INSERT INTO `menu` (`idMenu`, `menuName`, `parentId`, `slug`, `status`, `content`, `userId`, `link`, `orderMenu`) VALUES
(33, 'Bidang Penagihan, Pembukuan dan Pelaporan', 23, 'bidang-penagihan-pembukuan-dan-pelaporan1', '1', '&lt;p style=&quot;margin-left:0in; margin-right:0in&quot;&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;strong&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Bidang Penagihan, Pembukuan dan Pelaporan&lt;/span&gt;&lt;/span&gt;&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in&quot;&gt;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in&quot;&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;strong&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; &lt;/strong&gt;&lt;strong&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Pasal &lt;/span&gt;&lt;/span&gt;&lt;/strong&gt;&lt;strong&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;27&lt;/span&gt;&lt;/span&gt;&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in&quot;&gt;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in&quot;&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Bidang &lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Penagihan, Pembukuan dan Pelaporan&lt;/span&gt;&lt;/span&gt; &lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;merupakan unsur pelaksana &lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Badan &lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Pendapatan Daerah&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt; yang dipimpin oleh Kepala Bidang dan mempunyai tugas pokok&lt;/span&gt;&lt;/span&gt; &lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;membantu Kepala Badan dalam penyusunan rencana pendapatan, Pembinaan Teknis Pemungutan, Pemantauan dan penggalian peningkatan Pendapatan Daerah, Pelaksanaan pembukuan dan pelaporan realisasi penerimaan dan tunggakan Pajak Daerah dan BPHTB&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;.&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in&quot;&gt;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in&quot;&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;strong&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; &lt;/strong&gt;&lt;strong&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Pasal &lt;/span&gt;&lt;/span&gt;&lt;/strong&gt;&lt;strong&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;28&lt;/span&gt;&lt;/span&gt;&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in&quot;&gt;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in&quot;&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Untuk melaksanakan tugas sebagaimana dimaksud dalam pasal &lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;27&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;,&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt; Kepala &lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Bidang &lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Penagihan, Pembukuan dan Pelaporan&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt; dalam melaksanakan tugas menyelenggarakan fungsi:&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;ol style=&quot;list-style-type:lower-alpha&quot;&gt;\r\n	&lt;li&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Perumusan rencana kegiatan Bidang &lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Penagihan, Pembukuan dan Pelaporan;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/li&gt;\r\n	&lt;li&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Penyiapan petunjuk pelaksanaan &lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Bidang Penagihan, Pembukuan dan Pelaporan;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/li&gt;\r\n	&lt;li&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Pelaksanaan penagihan Pajak Daerah yang telah melampaui masa / jatuh tempo;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/li&gt;\r\n	&lt;li&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Pengumpulan serta pengolahan data sumber-sumber penerimaan daerah lainnya diluar pajak daerah dan retribusi daerah;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/li&gt;\r\n	&lt;li&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Pengelolaan piutang pajak daerah&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;; dan&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/li&gt;\r\n	&lt;li&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Pelaksanaan tugas lain yang diberikan oleh atasan sesuai dengan tugas dan fungsinya.&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/li&gt;\r\n&lt;/ol&gt;\r\n\r\n&lt;p style=&quot;margin-left:0.5in; margin-right:0in&quot;&gt;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0.5in; margin-right:0in&quot;&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;strong&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; Pasal 29&lt;/span&gt;&lt;/span&gt;&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in&quot;&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Bidang &lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Penagihan, Pembukuan dan Pelaporan,&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt; terdiri dari:&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;ol&gt;\r\n	&lt;li style=&quot;list-style-type:none&quot;&gt;\r\n	&lt;ol&gt;\r\n		&lt;li style=&quot;list-style-type:none&quot;&gt;\r\n		&lt;ol&gt;\r\n			&lt;li&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Sub Bidang Penagihan Pajak&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt; Daerah&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/li&gt;\r\n			&lt;li&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Sub Bidang Penagihan PBB-P2 dan BPHTB&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/li&gt;\r\n			&lt;li&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Sub Bidang Pembukuan dan Pelaporan&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;.&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/li&gt;\r\n		&lt;/ol&gt;\r\n		&lt;/li&gt;\r\n	&lt;/ol&gt;\r\n	&lt;/li&gt;\r\n&lt;/ol&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in&quot;&gt;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in&quot;&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;strong&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; Paragraf 1&lt;/span&gt;&lt;/span&gt;&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in&quot;&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;strong&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; &lt;/strong&gt;&lt;strong&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Sub Bidang Penagihan Pajak&lt;/span&gt;&lt;/span&gt;&lt;/strong&gt;&lt;strong&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt; Daerah&lt;/span&gt;&lt;/span&gt;&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in&quot;&gt;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in&quot;&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;strong&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; Pasal 30&lt;/span&gt;&lt;/span&gt;&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in&quot;&gt;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;ol&gt;\r\n	&lt;li&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Sub Bidang Penagihan Pajak Daerah &lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;merupakan unsur pelaksana Bidang Penagihan, Pembukuan dan Pelaporan yang dipimpin oleh Kepala Sub Bidang mempunyai tugas pokok membantu Kepala Bidang &lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;dalam menyiapkan bahan perencanaan&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt; Penagihan Pajak Daerah.&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/li&gt;\r\n&lt;/ol&gt;\r\n\r\n&lt;p style=&quot;margin-left:0.5in; margin-right:0in&quot;&gt;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;ol start=&quot;2&quot;&gt;\r\n	&lt;li&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Kepala Sub Bidang Penagihan Pajak&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt; Daerah&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;, menyelenggarakan fungsi :&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/li&gt;\r\n&lt;/ol&gt;\r\n\r\n&lt;ol style=&quot;list-style-type:lower-alpha&quot;&gt;\r\n	&lt;li&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Penyusunan kegiatan penagihan Pajak daerah;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/li&gt;\r\n	&lt;li&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Penerbitan Surat Tagihan Pajak Daerah;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/li&gt;\r\n	&lt;li&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Pengadministrasian dokumen tagihan Pajak daerah&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;; dan&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/li&gt;\r\n	&lt;li&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Melaksanakan tugas lain yang diberikan oleh Kepala Bidang secara berkala dalam rangka kelancaran pelaksanaan tugas &lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Bidang &lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Penagihan, Pembukuan dan Pelaporan.&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/li&gt;\r\n&lt;/ol&gt;\r\n\r\n&lt;p style=&quot;margin-left:0.5in; margin-right:0in&quot;&gt;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in&quot;&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;strong&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; Paragraf 2&lt;/span&gt;&lt;/span&gt;&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in&quot;&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;strong&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; &lt;/strong&gt;&lt;strong&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Sub Bidang Penagihan PBB-P2 dan BPHTB&lt;/span&gt;&lt;/span&gt;&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in&quot;&gt;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in&quot;&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;strong&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; &lt;/strong&gt;&lt;strong&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp; &lt;/strong&gt;&lt;strong&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Pasal 31&lt;/span&gt;&lt;/span&gt;&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in&quot;&gt;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;ol&gt;\r\n	&lt;li&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Sub Bidang Penagihan PBB-P2 dan BPHTB &lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;merupakan unsur pelaksana Bidang Penagihan, Pembukuan dan Pelaporan yang dipimpin oleh Kepala Sub Bidang mempunyai tugas pokok membantu Kepala Bidang&amp;nbsp; &lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;dalam menyiapkan bahan perencanaan&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt; Penagihan PBB-P2 dan BPHTB.&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/li&gt;\r\n&lt;/ol&gt;\r\n\r\n&lt;p style=&quot;margin-left:0.5in; margin-right:0in&quot;&gt;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;ol start=&quot;2&quot;&gt;\r\n	&lt;li&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Kepala Sub Bidang Penagihan PBB-P2 dan BPHTB&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;, menyelenggarakan fungsi :&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/li&gt;\r\n&lt;/ol&gt;\r\n\r\n&lt;ol style=&quot;list-style-type:lower-alpha&quot;&gt;\r\n	&lt;li&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Penyusunan kegiatan penagihan PBB-P2 dan BPHTB;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/li&gt;\r\n	&lt;li&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Penerbitan Surat Tagihan PBB-P2 dan BPHTB;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/li&gt;\r\n	&lt;li&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Pengadministrasian dokumen tagihan PBB-P2 dan BPHTB&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;; dan&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/li&gt;\r\n	&lt;li&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Melaksanakan tugas lain yang diberikan oleh Kepala Bidang secara berkala dalam rangka kelancaran pelaksanaan tugas &lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Bidang &lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Penagihan, Pembukuan dan Pelaporan&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/li&gt;\r\n&lt;/ol&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in&quot;&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; &lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in&quot;&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;strong&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; &lt;/strong&gt;&lt;strong&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; &lt;/strong&gt;&lt;strong&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Paragraf 3&lt;/span&gt;&lt;/span&gt;&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in&quot;&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;strong&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; Sub Bidang Pembukuan dan Pelaporan&lt;/span&gt;&lt;/span&gt;&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in&quot;&gt;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in&quot;&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;strong&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; &amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; Pasal 32&lt;/span&gt;&lt;/span&gt;&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in&quot;&gt;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;ol&gt;\r\n	&lt;li&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Sub Bidang Pembukuan dan Pelaporan &lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;merupakan unsur pelaksana Bidang Penagihan, Pembukuan dan Pelaporan yang dipimpin oleh Kepala Sub Bidang mempunyai tugas pokok membantu Kepala Bidang dalam melakukan Penyiapan Administrasi Pembukuan dan Pelaporan, tentang hasil dari penagihan Pajak daerah, BPHTB, Retribusi daerah dan lain-lain pendapatan daerah.&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/li&gt;\r\n&lt;/ol&gt;\r\n\r\n&lt;p style=&quot;margin-left:0.5in; margin-right:0in&quot;&gt;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;ol start=&quot;2&quot;&gt;\r\n	&lt;li&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Kepala Sub Bidang Pembukuan dan Pelaporan&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;, menyelenggarakan fungsi &lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;:&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/li&gt;\r\n&lt;/ol&gt;\r\n\r\n&lt;ol style=&quot;list-style-type:lower-alpha&quot;&gt;\r\n	&lt;li&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Penyusunan rencana program dan kegiatan Pembukuan dan Pelaporan;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/li&gt;\r\n	&lt;li&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Pelaksanaan pembukuan dan pelaporan Pajak, BPHTB, Retribusi, dan lain-lain pendapatan ke dalam Buku penerimaan;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/li&gt;\r\n	&lt;li&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Pembuatan Laporan Realisasi Penerimaan Pajak, BPHTB, Retribusi, dan lain-lain pendapatan atas dasar penetapan, penerimaan, tunggakan per jenis Pajak Daerah, BPHTB, Retribusi Daerah dan lain-lain pendapatan daerah;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/li&gt;\r\n	&lt;li&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Pembuatan laporan realisasi penerimaan secara periodik yaitu : Bulanan, Triwulanan, Semesteran dan Tahunan&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;; dan &lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/li&gt;\r\n	&lt;li&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Melaksanakan tugas lain yang diberikan oleh Kepala Bidang secara berkala dalam rangka kelancaran pelaksanaan tugas &lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Bidang &lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Penagihan, Pembukuan dan Pelaporan&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/li&gt;\r\n&lt;/ol&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in&quot;&gt;&amp;nbsp;&lt;/p&gt;\r\n', 4, '', NULL);
INSERT INTO `menu` (`idMenu`, `menuName`, `parentId`, `slug`, `status`, `content`, `userId`, `link`, `orderMenu`) VALUES
(34, 'Bidang Pengawasan, Pemeriksaan dan Pengembangan', 23, 'bidang-pengawasan-pemeriksaan-dan-pengembangan', '1', '&lt;p style=&quot;margin-left:0in; margin-right:0in&quot;&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;strong&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Bidang Pengawasan, Pemeriksaan dan Pengembangan&lt;/span&gt;&lt;/span&gt;&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in&quot;&gt;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in&quot;&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;strong&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Pasal 33&lt;/span&gt;&lt;/span&gt;&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in&quot;&gt;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in&quot;&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Bidang Pengawasan, Pemeriksaan dan Pengembangan&lt;/span&gt;&lt;/span&gt; &lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;merupakan unsur pelaksana &lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Badan &lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Pendapatan Daerah&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt; yang dipimpin oleh Kepala Bidang dan mempunyai tugas pokok membantu Kepala Badan dalam menyiapkan bahan perencanaan, pelaksanaan, koordinasi, pengaturan, pembinaan, evaluasi, pengembangan, monitoring, pengawasan, Pemeriksaan dan penyidikan.&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in&quot;&gt;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in&quot;&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;strong&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Pasal 34&lt;/span&gt;&lt;/span&gt;&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in&quot;&gt;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in&quot;&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Untuk melaksanakan tugas sebagaimana dimaksud dalam pasal &lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;33&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;,&lt;/span&gt;&lt;/span&gt; &lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Kepala &lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Bidang Pengawasan, Pemeriksaan dan Pengembangan&lt;/span&gt;&lt;/span&gt; &lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;dalam&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt; melaksanakan tugasmenyelenggarakan fungsi :&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;ol style=&quot;list-style-type:lower-alpha&quot;&gt;\r\n	&lt;li&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Penyusunan rumusan kebijakan teknis pada Bidang Pengawasan, Pemeriksaan dan Pengembangan&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/li&gt;\r\n	&lt;li&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Penyusunan rencana kegiatan dan program kerja Bidang Pengawasan, Pemeriksaan dan Pengembangan&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/li&gt;\r\n	&lt;li&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Pelaksanaan monitoring dan pengawasan terhadap Wajib Pajak, PBB-P2 dan BPHTB, Retribusi dan Lain-lain Pendapatan Asli Daerah&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/li&gt;\r\n	&lt;li&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Pelaksanaan pemeriksaan dan penyidikan terhadap Wajib Pajak, PBB-P2 dan BPHTB, Retribusi dan Lain-lain Pendapatan Asli Daerah&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/li&gt;\r\n	&lt;li&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Pelaksanaan evaluasi dan pengembangan terhadap produk hukum daerah dan kebijakan di bidang pendapatan daerah&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/li&gt;\r\n	&lt;li&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Pelaksanaan koordinasi dengan lembaga/badan/instansi dan pihak terkait untuk kelancaran pelaksanaan tugas&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/li&gt;\r\n	&lt;li&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Pembagian tugas, pemberian petunjuk dan arahan kepada para Kepala Sub Bidang agar dalam melaksanakan tupoksinya terarah dan sesuai dengan ketentuan yang berlaku&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/li&gt;\r\n	&lt;li&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Pelaksanaan koordinasi dengan pihak-pihak terkait untuk kelancaran tugas dilapangan&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/li&gt;\r\n	&lt;li value=&quot;1&quot;&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Pelaksanaan pembinaan, pengawasan dan evaluasi terhadap bawahan dalam melaksanakan tugasnya&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/li&gt;\r\n	&lt;li value=&quot;10&quot;&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Pelaksanaan tugas lain yang diberikan oleh Kepala Badan sesuai dengan ketentuan yang berlaku&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;;&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt; dan &lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/li&gt;\r\n	&lt;li&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Pelaksanaan tugas lain yang diberikan oleh atasan sesuai dengan tugas dan fungsinya.&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/li&gt;\r\n&lt;/ol&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in&quot;&gt;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in&quot;&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;strong&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Pasal 35&lt;/span&gt;&lt;/span&gt;&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in&quot;&gt;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in&quot;&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Bidang Pengawasan, Pemeriksaan dan Pengembangan, terdiri dari :&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;ol&gt;\r\n	&lt;li&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Sub Bidang Monitoring dan Pengawasan&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/li&gt;\r\n	&lt;li&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Sub Bidang Pemeriksaan dan Penyidikan&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/li&gt;\r\n	&lt;li&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Sub Bidang Evaluasi dan Pengembangan&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/li&gt;\r\n&lt;/ol&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in&quot;&gt;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in&quot;&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;strong&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Paragraf 1&lt;/span&gt;&lt;/span&gt;&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in&quot;&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;strong&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Sub Bidang Monitoring dan Pengawasan&lt;/span&gt;&lt;/span&gt;&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in&quot;&gt;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in&quot;&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;strong&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Pasal 36&lt;/span&gt;&lt;/span&gt;&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in&quot;&gt;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;ol&gt;\r\n	&lt;li&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Sub Bidang Monitoring dan Pengawasan&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt; merupakan unsur pelaksana Bidang Pengawasan, Pemeriksaan dan Pengembangan yang dipimpin oleh Kepala Sub Bidang mempunyai tugas pokok membantu Kepala Bidang dalam menyiapkan bahan perencanaan, koordinasi, pembinaan, pengaturan, pelaksanaan monitoring dan pengawasan, evaluasi dan pelaporan pada Sub Bidang Monitoring dan Pengawasan. &lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/li&gt;\r\n&lt;/ol&gt;\r\n\r\n&lt;p style=&quot;margin-left:0.5in; margin-right:0in&quot;&gt;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;ol start=&quot;2&quot;&gt;\r\n	&lt;li&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Kepala &lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Sub Bidang Monitoring dan Pengawasan&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;,&lt;/span&gt;&lt;/span&gt; &lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;&amp;nbsp;menyelenggarakan&lt;/span&gt;&lt;/span&gt; &lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;fungsi :&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/li&gt;\r\n&lt;/ol&gt;\r\n\r\n&lt;ol style=&quot;list-style-type:lower-alpha&quot;&gt;\r\n	&lt;li&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Penyusunan rencana kegiatan dan program kerja pada Sub Bidang Monitoring dan Pengawasan&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/li&gt;\r\n	&lt;li&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Penyiapan, pengumpulan bahan dan data serta penggalian informasi dan keterangan dari berbagai sumber&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/li&gt;\r\n	&lt;li&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Pelaksanaan monitoring dan pengawasan terhadap Wajib Pajak, PBB-P2 dan BPHTB, Retribusi dan Lain-lain Pendapatan Asli Daerah&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/li&gt;\r\n	&lt;li&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Pelaksanaan koordinasi dengan lembaga/badan/instansi dan pihak terkait hingga ke tingkat RT/RW untuk kelancaran pelaksanaan tugas&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/li&gt;\r\n	&lt;li&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Pelaksanaan pembinaan, pengawasan dan evaluasi terhadap pelaksaan pemungutan Pajak Daerah dan BPHTB&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/li&gt;\r\n	&lt;li&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Melaksanakan tugas lain yang diberikan oleh Kepala Bidang secara berkala dalam rangka kelancaran pelaksanaan tugas &lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Bidang &lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Pengawasan, Pemeriksaan dan Pengembangan&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;.&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/li&gt;\r\n&lt;/ol&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in&quot;&gt;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in&quot;&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;strong&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Paragraf 2&lt;/span&gt;&lt;/span&gt;&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in&quot;&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;strong&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Sub Bidang Pemeriksaan dan Penyidikan&lt;/span&gt;&lt;/span&gt;&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in&quot;&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;strong&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Pasal 37&lt;/span&gt;&lt;/span&gt;&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;ol&gt;\r\n	&lt;li&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Sub Bidang Pemeriksaan dan Penyidikan&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt; merupakan unsur pelaksana Bidang Pengawasan, Pemeriksaan dan Pengembangan yang dipimpin oleh Kepala Sub Bidang mempunyai tugas pokok membantu Kepala Bidang dalam menyiapkan bahan perencanaan, koordinasi, pembinaan, pengaturan, pelaksanaan pemeriksaan dan penyidikan, evaluasi serta pelaporan pada Sub Bidang Pemeriksaan dan Penyidikan.&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/li&gt;\r\n&lt;/ol&gt;\r\n\r\n&lt;p style=&quot;margin-left:0.5in; margin-right:0in&quot;&gt;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;ol start=&quot;2&quot;&gt;\r\n	&lt;li&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Kepala &lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Sub Bidang Pemeriksaan dan Penyidikan&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;, menyelenggarakan fungsi &lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;:&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/li&gt;\r\n&lt;/ol&gt;\r\n\r\n&lt;ol style=&quot;list-style-type:lower-alpha&quot;&gt;\r\n	&lt;li&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Penyusunan rencana kegiatan dan program kerja pada Sub Bidang Pemeriksaan dan Penyidikan&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/li&gt;\r\n	&lt;li&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Pembagian tugas dan pemberian arahan kepada staf/pelaksana di lingkungan Sub Bidang Pemeriksaan dan Penyidikan sesuai dengan bidang tugasnya agar pelaksanaan tugas terarah dan sesuai dengan ketentuan yang berlaku&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/li&gt;\r\n	&lt;li&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Penyiapan, pengumpulan bahan dan data serta penggalian informasi dan keterangan dari berbagai sumber&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/li&gt;\r\n	&lt;li&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Pelaksanaan tugas pemeriksaan dan penyidikan terhadap Wajib Pajak, PBB-P2 dan BPHTB, Retribusi dan Lain-lain Pendapatan Asli Daerah&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/li&gt;\r\n	&lt;li&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Pelaksanaan koordinasi dengan lembaga/badan/instansi dan pihak terkait hingga ke tingkat RT/RW untuk kelancaran pelaksanaan tugas&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/li&gt;\r\n	&lt;li&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Pelaksanaan pembinaan, pengawasan dan evaluasi terhadap staf/pelaksana dalam melaksanakan tugasnya&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/li&gt;\r\n	&lt;li&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Pelaksanaan tugas lain yang diberikan oleh atasan sesuai dengan ketentuan yang berlaku&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;;&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt; dan &lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/li&gt;\r\n	&lt;li&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Melaksanakan tugas lain yang diberikan oleh Kepala Bidang secara berkala dalam rangka kelancaran pelaksanaan tugas &lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Bidang &lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Pengawasan, Pemeriksaan dan Pengembangan&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;.&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/li&gt;\r\n&lt;/ol&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in&quot;&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;strong&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; &lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in&quot;&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;strong&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Paragraf 3&lt;/span&gt;&lt;/span&gt;&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0.5in; margin-right:0in&quot;&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;strong&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Sub Bidang Evaluasi dan Pengembangan&lt;/span&gt;&lt;/span&gt;&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in&quot;&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;strong&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Pasal 38&lt;/span&gt;&lt;/span&gt;&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;ol&gt;\r\n	&lt;li&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Sub Bidang Evaluasi dan Pengembangan&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt; merupakan unsur pelaksana Bidang Pengawasan, Pemeriksaan dan Pengembangan yang dipimpin oleh Kepala Sub Bidang mempunyai tugas pokok membantu Kepala Bidang dalam menyiapkan bahan perencanaan, koordinasi, pembinaan, pengaturan, pelaksanaan evaluasi/kajian dan pengembangan terhadap produk hukum daerah dan kebijakan dibidang perpajakan/retribusi daerah/lain-lain pendapatan asli daerah serta pelaporan pada Sub Bidang Evaluasi dan Pengembangan.&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/li&gt;\r\n	&lt;li&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Kepala &lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Sub Bidang Evaluasi dan Pengembangan&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;, menyelenggarakan fungsi :&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/li&gt;\r\n&lt;/ol&gt;\r\n\r\n&lt;ol style=&quot;list-style-type:lower-alpha&quot;&gt;\r\n	&lt;li&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Penyusunan rencana kegiatan dan program kerja pada Sub Bidang Evaluasi dan Pengembangan&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/li&gt;\r\n	&lt;li&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Pembagian tugas dan pemberian arahan kepada staf/pelaksana di lingkungan Sub Bidang Evaluasi dan Pengembangan sesuai dengan bidang tugasnya agar pelaksanaan tugas terarah dan sesuai dengan ketentuan yang berlaku&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;;&amp;nbsp; &lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/li&gt;\r\n	&lt;li&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Penyiapan, pengumpulan bahan dan data produk hukum pusat/nasional/daerah maupun dari berbagai sumber lainnya&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/li&gt;\r\n	&lt;li&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Pelaksanaan tugas evaluasi/kajian dan pengembangan terhadap produk hukum daerah dan kebijakan dibidang perpajakan/retribusi /lain-lain pendapatan asli daerah&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/li&gt;\r\n	&lt;li&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Pelaksanaan koordinasi dengan lembaga/badan/instansi dan pihak terkait hingga ke tingkat RT/RW untuk kelancaran pelaksanaan tugas&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/li&gt;\r\n	&lt;li&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Pelaksanaan pembinaan, pengawasan dan evaluasi terhadap staf/pelaksana dalam melaksanakan tugasnya&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/li&gt;\r\n	&lt;li&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Pelaksanaan tugas lain yang diberikan oleh atasan sesuai dengan ketentuan yang berlaku&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;; &lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/li&gt;\r\n	&lt;li&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Melaksanakan tugas lain yang diberikan oleh Kepala Bidang secara berkala dalam rangka kelancaran pelaksanaan tugas &lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Bidang &lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Pengawasan, Pemeriksaan dan Pengembangan&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;.&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/li&gt;\r\n&lt;/ol&gt;\r\n\r\n&lt;p style=&quot;margin-left:0.5in; margin-right:0in&quot;&gt;&amp;nbsp;&lt;/p&gt;\r\n', 4, '', NULL),
(35, 'Profil Badan', 0, 'profil-badan1', '1', '', 4, '', NULL);
INSERT INTO `menu` (`idMenu`, `menuName`, `parentId`, `slug`, `status`, `content`, `userId`, `link`, `orderMenu`) VALUES
(36, 'Struktur Organisasi', 35, 'struktur-organisasi', '1', '<p style=\"margin-left:0in; margin-right:0in\"><span style=\"font-size:11pt\"><span style=\"font-family:Calibri,sans-serif\"><span style=\"font-size:12.0pt\">&nbsp;</span></span></span></p>\r\n\r\n<p style=\"margin-left:0in; margin-right:0in\"><span style=\"font-size:11pt\"><span style=\"font-family:Calibri,sans-serif\"><span style=\"font-size:12.0pt\">&nbsp;<strong>Struktur Organisasi Badan Pendapatan Daerah</strong></span></span></span></p>\r\n\r\n<p style=\"margin-left:0in; margin-right:0in\"><span style=\"font-size:11pt\"><span style=\"font-family:Calibri,sans-serif\"><img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAABI8AAAMOCAYAAACXvibxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAK/1SURBVHhe7d09kvw+euD5Osr4HaGINpVnaGfa6QtMpLPtyewLbBsbm+3LHNmKGKMcGasDyF5j6ibr5PLBCwmAAPiSIIoAvp8ISJVMEgDB5yFIVP3+/fUGAAAAAAAAElg8AgAAAAAAQBKLRwAAAAAAAEhi8QgAAAAAAABJLB4BAAAAAAAgicUjAAAAAAAAJAWLR9/v59fX+2suj/frx3zl+H5+vR+xL94/79djOf75bTY7osd+P99fj9d0tPHzej8yxy/9e049Vhunn52+BscfPmZnfz49l+lLb7zmvglz3PJdWMcFY61+XsYkOm6OaP2T6HFB3QAAAAAAoA3RxSO7EPHzevgLDYpdYAoXE8xixryKIZ/DfRLHqoUcZyEiteCi9rPHTvU/Td/U9sxC0Kljtvrz4bmo8XHHSw516guOU9fCPY8rxlr9bMYkNW6zXP2R49y6AQAAAABAMw4vHqltz5davHDWPYJFg7j8sbIQYRYXEgsuegElsgChjo8vBJ05Zk9/Pj2XVfuKM/7hceqz2V8de8FYO20kx81I1Z88zu0/AAAAAABoxsY/WwsXKPRfvMhigVokcBaW1F/NrP5KyZU+dl5IeU3/X+/gL5zMnP65X4YLMd7xJ47Z7M/n56IXWcLx1X1VfxUUHOfuf9lYq5/tmCTGTcnUnzrOqxsAAAAAALQi+5dHqxd+tfBgFzz8ff0FDb244C0gZI6dFzSmg6We58tfOFlR+0v9pj7neCVYeFGOHLPVH/X9Z+eyd/FI91nK0tfLxjq2wKP2l/qdvubqt8LjYnUDAAAAAIDbyy8emc/6v4/jLFK4xSxirBdDzP56VSJ7rLugYduU71cLEh6nb97xk9jikbLzmGx/Cp1L2L5w+7Dqz1LHZWOdXOBxxm2rfo9zXLJuAAAAAABwZ/nFI3fRIvby720zCxHzwc6Cxtax4UJKsFgym455LjstfVV1Lft7iytnjsn1x+235W7bey6r8ZJdnUWYoH/qu3nB6KKxdvebfk6PW6b+I8cBAAAAAIDb2/hvHtm/NpE1hunz6q9L3L94EeHxerFg89hwQcN8N1drmYWOsG9K8N3SpbPHxPtT7FyUYLzcetVii3ucaSNcQJpLgbF2F3gS47av/vVxLB4BAAAAANCmYPEIAAAAAAAAWLB4BAAAAAAAgCQWjwAAAAAAAJDE4hEAAAAAAACSWDwCAAAAAABAEotHAAAAAAAASGLxCAAAAAAAAEksHgEAAAAAACCJxSMAAAAAAAAksXgEAAAAAACAJBaPAAAAAAAAkBQsHn2/n19f76+5PN6vH/OV4/v59X6EX/y83g/v2OdU21r02O/n++vxes9bTV3PsALZz23D7hBun4pqI1fvp/11j111NHbcz/v1cI5xi9vHyUdjVPK8pqL2WdUZaXcSrTPWT9VOpF9qXyfmTH/COqPt5GJX1eN8dvsUnnOJa5kaR9m++xrmxiFs347lZzEm2/z9ZEz9e8B6n3hd9tzcPszn6Z7f5pj453SuDqvk/S1Wf+zY/dcEAAAAAO4qunhkX4p+Xo/IC459AQte/t2XuYl6yVwtECSODV+OUy9/aj9zrNlHHaO2R14Ec/V+3N9IP2aJ46xUf5UPx+jj84r0K2hLxcXeOmP9dMfP5fVd1+ePq0iNrd7u9dHGbnhebp/cvpjtH1/L1Diq7UevYTgOZjHCOSh6jVN9UBLnFB4T9nfVF5GoKzg3L2bc88uOSXiu8vloHS7dV69Ph+5vifOZJY61stcEAAAAAO7r8OKR2vZ8qZc67+XMfZlTdF3ui2byWPPyN79YpV7+1H72xcx5sUy9lOXq/bi/kX4YyeOsVH8nH4/Rx+cV6VfY1qqNTJ2xfnrj53DqVQsikb/MSI9tJnbD83L75PWl0LVMjaPafuwarsYhWrd/7kqqD5OtMbSxohelnPYjde6+7s45eT/nxkR9l1iI2VuHJxMjxqnzMU7FCgAAAAA0YOOfrYUvbvrlWl6MVi9eq5cpve+yaJE51r5Uvab/r3eIv/y5L5NmH1X//PKoy3xcrt6P+xvph5I5zrL9Wn1RYIw+Pq/IOAZtqeO82MjUaY5169Ulsihg+y7nGPs+1860dzJ2w/F2z+eKa5kaR7vv3msYGYf12At/0UeJ9UvJn9OyWGUXWuT/63rWC3rb192em9dve36yc2ZMUguIys46fJ/e3xLno5yMFQAAAABoQPYvj/QLk/Oyo15+7AvTxr7mZWp+oc0d67xUyQvj85V4+VP7OS9/dofUS1mu3o/7G+mHyB1nZfv74RgVug4e8+K8nHOwT65Oc6zXT29/h21nevF+xRYOcu1E2zX9DM/L7ZP6zjk3t6PZ9oywbhHbJpzt29cwPg7rRQuh+7Zr8WjrnOy4zQtXdlEksm+uLnsOc3H6krg24ZjI5+XcdT/ma7SzDl+sj06/zp6P2BpX4fQTAAAAAFqSXzwyn/VLqfPy5hb7che+iJmXLV3XxrHeS5VuU76Pv3zZFzRH6qUsV+/H/Y30Y+s4K9rfQmNU7Do43DrUPm67G3V67Rup8fP2dWNPbI2t3n9pxzk+PK/V+RS+lrFtwtu+dQ0T4xCrOznGYR/2nJOzj6lQL1jJNnecDlx31Renf+o707fMmKT+uie3eBTW4dPfLdvdsf3gfLaOtaLXBAAAAADuL7945L4kuS9rlrvN+968THkvXpljw5eq1cuZobZHXvRTL2W5ej/ub2rxI3OcFevvrjad791zcXn1fHgdLLXP0pb6ixB7/lt1BscqO8dPLx649WTa2Yzd5TtvUWJnX5TYttiYpcYx3O720RW0442DOU/3L6T8v9AxYn3YeU52sWhuQu3jt7lZlznGHpKMmeyYhOdq4lk+767D9en9bcf5WLFtYT8BAAAAoBEb/82j5S8eoi+oq5c551hn381jVy9V+rv5Jc9S+0Ve9M1LoFuif3Hi1vtxf9f92DzOWvVrb5s7xujj83KOnYoaR1Pn0pY5ZhqD51adq2MnifHT+67PcVc7mdhVgnOb+5PoyyfXMmxLymY8ujLjoHcNznXVz0mkX7vPSbXvjsm6n5t1ZWLm2z2/zTEJr6vZ91Ad1uf3t9j5bMemEbkmAAAAANCCYPEIAAAAAAAAWLB4BAAAAAAAgCQWjwAAAAAAAJDE4hEAAAAAAACSWDwCAAAAAABAEotHAAAAAAAASGLxCAAAAAAAAEksHgEAAAAAACCJxSMAAAAAAAAksXgEAAAAAACApGDx6Pv9/HpO/zdl/f3P6/F+uhu+n8vnn9f78fX1/rJl+eL9enwl93u8fswXVtCu7O81KvU93qvDUu2H26ei2jzSj0gdukzfy3eP19Qry+nfkTaE7H/iXOd6D/RluT7WzvNVdTrX0zXFw5fT/vfTPWd93FyX3S/bluWPk8Sh1383Did+uyIyzk5bYR/numQ/70TD6yH1LvUsux6tx0j2y9p7HtbeawoAAAAAgHZs8UgWAqaXS/edV17K3Zd277O8nM4LB/rlWb/MRl6k5/2kD8FLdNiueeldms28eMfa97Y7jvbDkIUL7yV9Vb/Tv6NtyP6Hz9Wpd3dfnPGx9p6vOTbcT5E6bBvuz5H21AKQ19dYW0ZsnJxj/bjcOc7z8e7+wbnJfsnrIce55zR9Xg48UI8j2S9j93kY4f5GcpwBAAAAAMM7tHj0/ZxeRF/Ty2fwQuwtCEz7zC+v3ous+4IaeZF263C/m6zaVftPdc193fPiLR9N+8H22dF+GKsX71X9Tv+OtqH2P3quznXc3Rf56J/H7vNVda4XJJR5wShYyFj1S6zrWbelxWPR1j+1JWNm64+0FR9nu4/bj6BPar/49ZC+hmO12F+Px+vXOj/3n4e2/5oCAAAAAKDtXzyaX0rdffTP8jKq3jtln+dremE13ydfZGMv0u4LsvMSHWvXbpOFCVXJnhdvp03Z/iX/PEeX+aX5aD+M1Yu3V49w+ne0Dbttz7nO5+T0b3dfnPFRHyN9Mc4sHr3CfzY2n48v/Odl0UWNRN9kX3t9n9MxaqFkOjR9fSLjrHZwr0tiXCLXY/1P41z76/HIfrHrKg6dxyS2vxEdZwAAAAAAJrsXj9yXy/kl2b6MTv9fXtbty7t9aVffzy++U1nenNcv0s5+8/ZJtl3z/fN7z4u3075zvOdoPwz3O2VVv9O/o204dW2e69ymXEdT94G+uIs5h85X1emfy0wWR6a6H6/vaR8ntj5YPEr2Tc5nqvPHLmZObeh2/fGKHh+MxdK14Nyc8ZR63Ovh911fg6+tRSj144nrOjl2Hon9Dfc7AAAAAABcOxeP9IuvfSFVRV5o3b+akL84eupj5xdR78XXlX6Rlpfa5YU30a5Xr91n68XbsWP7rn7Yb8MX71X9crzp39E2vLr2n6v0SV2bnX3xHTxfs/9yLg6JEXusu2AUbXtdT6qteN8kfqf950Wq6fN0ruvzjxzv9Cd2XebPXr9tXWY8Vwti8r29VgfqcQXjNF/X+Rin7DiP1f7229U4AwAAAACg7Vs8Cl5g9YuovJQvf70gL6rzi7N9iV4dZ+VepJ0+JNp9fQfbZb/Ci0e7+mE2rF+8w3FM1XXVuerx1X3a2xdHqi9mw+nFI68et4+aWhgJ+rNqa6NvXhxOws/J471xdscsOLfwePk8X49gX1t37LtsPQ5vP12HGo/wePVd5jxS+5sNLB4BAAAAAFIii0fmrxKcv05Y/fdqJupFf/p+fhn+Dv4n+uVFdfXCam28SJsFh2S700tvWO/8T5VcqfZle3Ce0RfyrX6YE4i+eMuxTv1VztVpz1sw2dsXQxZcjp1vcD1d5vzmvb3P+ri5b5G+hG1t9U3q9/oRfE4eH47z3M+NWJ2E10MtWKlzcheDjtejJK7r0fM4FcMAAAAAAEyCxSMAAAAAAABgweIRAAAAAAAAklg8AgAAAAAAQBKLRwAAAAAAAEhi8QgAAAAAAABJLB4BAAAAAAAgicUjAAAAAAAAJLF4BAAAAAAAgCQWjwAAAAAAAJDE4hEAAAAAAACS/MWjn9f78fw2H8TP+/V4vF8/5qP6/PX++jLl8Zq2LH5ej7d3+PfT/5w7Xtq226fyWBo9TPoxt2GL7cjUp9U2K+jDUp7vZc/v99P9LMd44+CM2dZ32bG2pD3bj+D73LnIccH1UdQx7vkAAAAAAACkrRePvr6cBR93QUMv/LiLOmqRxlmg+H76ixj+543jvYUWWTCJLaSc4dSlzs8unOj+rNZcDOlbdAHLLNgsp7WxQJT7LjnWjqm9uR/StjteqXMxdYeLe0vff94/YTsAAAAAAAAR68WjxyNYlEgthAh30UL/vCxYTJ+fU13uYkfueO97t94U015+p6nax7xPuCCUXCCapL77ns7p9Xou7a7OS/q1c/EoNdYpzuJR+lyknqnOVdv6L5g2hgsAAAAAAMATWTx6vX9kkUKtMjgLGvM2n/x1kV60kMWJp1pY0WsYU13Pl17I0Dvmj/cWO/b85ZH0bWvxyK9n6asmCy6p46OLR3Mf9bmqI2Wb/JWPV9wFIncBJ1w8Sox1gvTfdnfzXMK2pR23j9lxAwAAAAAA0OKLR+pH+e8XHVg8ssdO//85fdbHy/cHjncWN0qsbYQLKuGC0GrBxRHuK9xtq/NWW0VkgUh/MYl/J/V6Yx0j4+fUtXkuQdvq+/kzf4UEAAAAAAD2SS4eTR/e6i97kn9FI/Q+ahHC/Qsa+Yujp/7LnHmRY+t453v3L2zOWy/GhAss4QKMa/2dHQ+nSH9X5+W0u/s7W3dq8Wj9l1ib5xK0HX4f/uUSAAAAAABATGbxaCKf50ULvcDhLjioBQyzv7sYIT/PCxvuolLmeL9t55+FJZkFl9QqU3guQvqy878xtLUYMx//ndiuNoTnEfxzN/c4b6x90YWerXOJ1T9/1mOXGjoAAAAAAAArv3g0+bH/DSNFLzp4f3njbJ8XI76d/4n+yKLF+vhJ2LYsjgR98Zm6Eisgqb+sUQtWpv3Y99aev9TRi1/Tfl4/pV/OQo5a5FnOOT4umj/WRnC8FNuP7LlE69937gAAAAAAAJa/eAQAAAAAAAA4WDwCAAAAAABAEotHAAAAAAAASGLxCAAAAAAAAEksHgEAAAAAACCJxSMAgMf+rzJSKKMWAAAA+HhCAgB4eHnGyIh/AACANZ6QAAAeXp4xMuIfAABgjSckAICHl2eMjPgHAABY4wkJAODh5RkjI/4BAADWliekn9f7MT0wyUOTX57vb7PLe/rp6X6WY57Lt9OG9+vxeL9+zEe1/1KXt2v2OxG0tfp81lTP4zX1NPD9DM7VEY6Ne3xk3B5qAGQsgm3JfeWrhz8GU3/mz8Fx9phkfan9Pe74u9dsosbCfLe6MCfGD0BTJPeBURH/AAAAa9EnJFnIiC44mEWFcFFjWV9wF4/04sRSz/R53jH3nRG2pY75cHHCLqqEix9zWz/vn8hp6+OWtmV85oUS+S6ymKL2Cc8psa/4fvoLNd5n7zgZBzPGqfpS+7umc57HX87f7i/HzueqF8DmbqnvTowfgKbw8oyREf8AAABrhxaPvp+P9+v1DBY1pn29xQa9UBFdPDFy31mrttQiyNbikflrn2jd8t10/GrBRep1F6kivAUVTRZ31DGr+rRji0em3/N30+fp/Od9veOccUjV523XdWfPz1k8Cq/98vmD8QPQFF6eMTLiHwAAYG3/4tG8aBBZvJDFB72S8raLR7K4EluAErnvlFhb3s8pZhEmt5IRLn6Yv5qZS+xYOSZoex4j9d1y/HJeelHF+6dc2X2farFMbZL9ni+9WCNfe8ftqE+2z+codUf+8sgxL4SZn91rs1oE8+qe7Bk/AE2RXAZGRfwDAACs7V48crfNCwzOQoJ8L/9sKb54ZBdStr8T0bbUfluLRzsEix9qccRbaIn8FY1apMksHrmLKSF17Hq8PHb79P+f0356LOXczZh4xzl9zNWnxlSX7HqO81dHwh17ocbHrSBoc9f4AWgKL88YGfEPAACwtnPxyPxFj1mMUEUWDLyFBLuPWfCY/xrJWhaWst+l2lILE9csHrnnGv7ljaIWY9y2dR/VKQT1Rdl9UvvO4zHVK39x9NRtzX2L9Fntn6rP2S7n4w21R8Z0WbQTc93GKhYifdkcPwBNkfsuMCriHwAAYG3f4tFqkcIs9nwH22W/eTHCWWBRzDFb36Xa+tmzeKTr9RemAmH93uewX4Y6r6VttcBij1n1N8L+dU9iX3fBRX6e+28XlSJ9VPun2va2p8ctutAjbc77O9fFCtuM9C03/ADuj5dnjIz4BwAAWNu1eBRbZNALKNN+weLFj/3v9hhqMUT+eij4CxcR+y7Z1vOp/kmU3t+U1cLJicWjiarf1LlaTBFyTKrd8LupSB1und7/Mpuzn973219w+Q7+J/qlrfA4u0Oi7fk4vZeqczVWapEocuwkOx5h3ZPN8QPQFMllYFTEPwAAwBpPSAAADy/PGBnxDwAAsMYTEgDAw8szRkb8AwAArPGEBADw8PKMkRH/AAAAazwhAQA8vDxjZMQ/AADAGk9IAAAPL88YGfEPAACwxhMSAMDDyzNGRvwDAACs8YQEAPDw8oyREf8AAABrPCEBADy8PGNkxD8AAMAaT0gAAA8vzxgZ8Q8AALDGExIAwMPLM0ZG/AMAAKwtT0g/r/djemCShya/PN/fke8erx99zPPbVCB+3q/H4/36ztRl9nxPPz3dz6m6pmY02X+py9vVCvtpd8r1P9zmcfoYqUMXMz6P19Rjy+n7kTaE7J8dByNV74G+zOMz23m+qs7MNcj2Xx871+f1VQ5/+PV+P4N2MsenxsQh9c/H2mIbmNpabbOy42FFrmXuWuS+y46h5eZE8H3uXOS4YNwVdYx7PhiVxA0wKuIfAABgLfqEJC/Y3ov36kXXMC/Uy7vp+iV3VZdlXm7nY7N16ZfkpZ7p8+qFeOL1U443x+T6P2+XNuIv4GFT2+MTLAQcaUP2T46DI1Xv7r4442PtPV9zbOwS5Pu/blMt5jj9/X76ix3+543jU2OSFIzbvHCSOb/JoZjedS1E8N2eGJjam/shbbvjkDoXU3e4aLf0/ef9E7aD4UgsAKMi/gEAANY+XzyaXmr9F1X/JTf1ov39nPZ7TS+s9q02U5daIEi9ybuCfs5tZ/tvtwcv2ZNVH43t8XHG4Wgbav/8mCpevbIIYvbf3Rf56J/H7vNVdfrnMcv1f9U34dalf14WNqbPU5/mY7aO9753641z4yo8x/U5L1Lfxa+l21/pU2oswu8SY5jiLB6lz0XqmepctS3xkx8rjIWXZ4yM+AcAAFjbv3g0PUzJA5WU+Tv7EiovrvrtffWSG33Rnl9eI4sekbrkr09WdcR4L8VyvLOokOu/+hD8pUqsj0Z0fOZ6hDMOR9uw2zJjqsh+8zk5/dvdF2d81MdIX4z1NQyOdeX6P2/zLddXty0LMHN/n6/peNOfreO98wvGesX/PowxOedYW+JQTDtxp0vsWojIddqKAYf033Z381zCtqUdt4+J88Y4JA6AURH/AAAAa/sXj7wXXcPZLsfIP3kJX3JjL9ruttiLf1iX/zIsL+fOS7hL6rAvwFLsS3Cu/87+/vt1pI/G6pxW9QcLAUfacOoKx8HjtanHRNV9oC/uIsGh81V1biweqR+D/u9d/Jn+/3P6rI+X7w8c75xftH+G1J06fxF+7wr3Fe622LXUgmux4zupNxkDloyLU9fmuQRtq+/nz04sYViSP8CoiH8AAIC1YotH0we1oBAu6qzqmvdzitSRq2u1aJB4md7VT4ezXV74lyYSfbTfbo6P07+jbXh1BePgCtqcFwh29sV38HzN/tEFhlz/o+07dc3Xedomf3H01H/BM7e/dbzzvT/WITnGH9N5/Iz1OS9S47Eav1V/nXZ3f2fr9vu78P+CSmyeS9B2+P28+IVhSQwDoyL+AQAA1gouHk3kc+RFNl+XeWmW/4W2ZF36BXp5H3ZetF17+2l524N/bhTro9mwOif3WCVV1442suPg8I7X46P7tLcvjlRfzIb1+er2ooszYV1e/91+alK3XahyFy3k53kBxF1Uyhzvtx2OgyM2DtLGvL9//qHTMe3Ve+A6eWPoiy70bJ1LrP75sx7j6LXFMHh5xsiIfwAAgLX9i0fy1xROUd+HL6GTH/vfqzHCumIvu3oBYNpvoy61oKDaT7zYR/qj7O2/vHRPn1+pPpo36tX4CPXCvtQ/v3wfbWPHOCjhOblv+3v7YiSvSfJ8MwsMkTb8/utj5/7N+wZ1TucQ73fq+ElirOfPRnTBZaLO2dQb+94Kx2N/TEvfndg9cJ2iMRAcL8X2I3su0fr3nTvGIHFwf8u9wLsXSXybWNbbg3tGULL7eBUHVDuJuSjVt5xsfXHLnOiXbJs7++2VrZPYU2esDnsPi9ynf5P0CQAAAD6ekAAAnjZenmMLNMu2ZRE0sSBiij42t0/8Lxjtwk18sTXWt7x8fXGpxSMpqXb39ntd0gtbp8eCxSMAAIBm8IQEAPC08fK8XpSY/4LOW6Ww++X+qie2j/xTUr/+mfPXTfHFpcyCScxmfXF20cZtY15QijW8u9/uOCznEl3kOTAWq2vA4hEAAEAzeEICAHjaeHkOFmiSCxGxBZFQfB+7GBX+RY3ePu370m2u/+Im6NuG7fricotH8bWjvf0Oxyq9kHZkLHRxFphYPAIAAGgGT0gAAE8bL8/uAo1d3AgXPUS4eGGKtwoSWzRJLZiY7WrBw/3Z5fbNbEraU1/c/FdGXomNgTjS73Udti1/cehIndNYvMxfKdlBYfEIAACgGTwhAQA8bbw8xxeF1os18f3ii0eREixshH+NZD/77ToLJhuLR/vqi4svHpkSVHCs3/sWj47VababBSN1DItHAAAAzeAJCQDgaePl2V/weTz0wsV64SO9ILLw65rLagUnUpf9b/54CyDBgknS3vriov9Ebf5vELnne7Tf67Fat3V+LOa6niweAQAAtIInJACAp42X52VRwi7yzH+Js3NBZLFnn4n9S5lEWS+sBAs7od31xUUXjyar7Yf7HYxDbEHqo7FYtqnC4hEAAMDt8YQEAPC08fIcW5Qw/92dqSz/vCqxIOI5sk+mzB2J9S10pL646OLRaqHnTL/dcVjGdFnkKTEWsXrvQfoEAAAAH09IAABPGy/PiUWJg4sn+tjYoklgrjfyP0d/sE21sHWovrj5L61ixQ5KsX47x5+s0188mth9WTwCAAC4PZ6QAACeNl6e04sS9j/crBc38gs5exeP5oWa1QqI5n+fb1MWj47VF5daPHIPKdHv5a+4tLN1xnZX14rFIwAAgNtbnpDm3xaGZXr4jnw3/+bUexqUh8Tp4fs7U5fZc3q8fD/D32TG6pqfKJ0/cZ9K9JlV/hsMzkOoPMD6v+FdSnS7+wAr33kPtE5/zh6X6t/s6JikyH5L/+ZzTfXr6P67+vXJufj9WY2v3W77OtuKkSN90n1Yvvb7lL1u0X3D+hKC81vKVL98l7yG4uD5Z89pEvbFa9s6MqaO2HnKcblz3Kw7df6x89Tb1mMEIeMEjIr4BwAAWIs+IclvAr0XydULnWFeAP2XNP9FcVWXZf5jm/Ox2br0S+FSz/Q59tbnLs64P2f7v7z46t9Wm8+rY5z+nD0u1T/r0Jikqf6E45Pp19H9d/Xr9LnIdn8hQ/XP9sXrl8SFrWNHjBzqk+6H/S46RlZQb3xfv749pJ58Hrr9PX7+2XMSanwScW6dvc5B3bPcOWbrTp9/qesxErmmwKiIfwAAgLXPF4+ml7flJXD9oriqy/h+Tvu9phdP+/aWqWvzJdeSF1nVT3mRdPqR63/wAit/DaSaWh0TvsSeOC7VP+PImORExyvTr6P77+nX6XNZtStk39j4Ltv3xMixPjltyqdM/WG98X39+vaQevYuHp05/81jpL1UnBsfXeegbiVzjrm6c+dS6nqMhJdnjIz4BwAAWNu/eDQ9TMkDlZT5O/uiJ4si6i1s/aK4qkvML4iyiGJeBDN1rf95V4JZnHmF++f6H7zAzv2d+2gFL7Fnjkv1T8zH7RuTPKlDztXpY65fR/ff6tcn5zJ/75tjwOuX1K/r2IyRw32Sn93FhcgYiVi90X3D+rZF83A+d7H099T5R/vpkGOC77w+xeq027aus6pb2jbFDsxcp+Ucn6k7f/5lrsdI5JoAoyL+AQAA1vYvHnkvdIazXY55fq9fFFd1TdxtsUWBsC7/xdC+CEZeSOWlcvru8fqejnVeFHP9T70cr44JXmLPHJfq3+TomOyi+rmuS4vUtXf/jX59dC7zwoDPq0ddf13srlsxcrxP8nNkccG0b+uK1mt5+ybqy3DrVpz+akt/T52/FZzTTG1Px2m0TqeP8n3yOkfqVjLnmKt76/wV1abd7/j1GAkvzxgZ8Q8AALBWbPHIvoyFL2yruub9nCJ15OpaLSgsL40e2c/W4R6T63/qrxFWxzhtnj0u1T9z/KEx2cvWkeuXa8/+2X59eC6rdoXePxxfWTCYhzAbI2f65LQZmo9L1Gt2U4J9o/UlbOeh1Gn6e+b81X7Gqu6JbEvFeapOrx67T+I6f7B4tKo7e/6OuQ59/JHrMRK5nsCoiH8AAIC1gotHE/kcvCjufgGW/4W2ZF3hi17ixdBdnHH3yfXfeYGVvi7Hy18vuC+3zuezx+3u3/4x8V+YI+Y2M/1y7dk/7K/br1Pn4tLn5caMN75e/W4f9XHRGDnVp7A+hx2jVL3LBmc8Y/3LX79V7njnKz48/2g/HWo8lvbS10GYOvde56Duxcm4U+0fuUeE+8PFyzNGRvwDAACs7V88kr8scIr6PnyZm/y8nt4LW1jX6p/MTPRL6bTfRl1yrG4/8lIowhdg9yU/1X93e9C+Ot75fn7RPHtcon+x/wbSrjEJ6zPUsXP7zot4ol9H91fnn+jXJ9d3oV/s57bd48K2gzGIxci5PvmLC7ExStY777fsG9anJK6fJXWF9atjnPq9+iaHzt+px//vARmZOD83pg5Vd6RNcSLurNj5x67dtJXFowwZK2BUxD8AAMAaT0gtm16yV4sLaAfXDzfFyzNGRvwDAACs8YTUKvkrDP5sol1cP9wYL88YGfEPAACwxhMSAMDDyzNGRvwDAACs8YQEAPDw8oyREf8AAABrPCEBADy8PGNkxD8AAMAaT0gAAA8vzxgZ8Q8AALDGExIAwMPLM0ZG/AMAAKzxhAQA8PDyjJER/wAAAGs8IQEAPLw8Y2TEPwAAwBpPSAAADy/PGBnxDwAAsOY/If283o/poUkenKQ8Xj/mC+v7/fx6Tv/XCPb/erze4RHRY57zp8nP+/V4vFdNhXXbY8LtU1H9zNV7tp/uMV7dwtk/0iddnPpi9Z8ZhzN9n8o8RuE2T6Qer71EH7+fXr++n2Hd+f7N++baC8/p7PWIfLcZP67sucoxTt12v1x/1HHCH6Of18M/x6ld/+PJMTZ9nOvaPG+pd6ln2fVoPUayX9be87B2Xne1L/aScQNGRfwDAACsrReP5hdjeSkLXv7kxXl6qPJeGMMX3vBFLXpM+BKaeMmc+6JfVNWLo7fdkav3bD9j7Vvh/obUvX7BnXwyDh/13eFt33l9vXoSfZTj7H7uz1a2Xqcfufa87z64Hqs2DNm+53okz3XdJ3WtgrYOxYdzrCwWLYtJR6+du7/up7df8rzlOPecps/LgQfqcST7Zew+DyPc30iOM3aRMQVGRfwDAACsbSwe+YsU38/pxe01vazZNzX1whju47/IRY+ZXiqX4/a8ZMpH8zIYbJ/l6j3dz0j7xmp/I/XS+tE4fNj3mbc9ePmfbNeT6OO8iBJ5uZ/k63XiLNde8N3p67Fqw1Dbd1yP1LlG612P8e748Nqf2pK+2fojbeXH2O1H0KfMeUtfw/Fc7K/H4/Vr5z0meh7a7uuOQ3h5xsiIfwAAgLX14tH00CQPTqu/bplf4oKX/WA/76UtdYxsk5dw9cK35yXTeWlUbdo+On8VkatXHXOyn3oH/6U1tr8RfWnN1b9nHM70PTdG6oPs77S3ox5dIn00Cyqv1T+lmmzW65yb1z8RXMP5uw+uh9f2zvhxpc51Ps4X/vOy3fGhNj/mmH9Ox6iFkunQ6DmFx8/bhGy355IYu8h5h3337a/HI/vFrr04dB6T2P5GdJyxm1wfYFTEPwAAwNp68ch7UVteDt2XsfmlUr0Ipl/akseYNuT75/eel8ypLB1x+ujI1Xu2n7H2J9H9Dfc7K1n/7nE4P8ae4JycU9pZT6KPsmAw1fd4fU/fp/sar9eJs1x7Qd9PX49VG4azXY5JXo/UuX6weJTsv/RpqvPn9dT9mNrQ7fr9ih4fjNfSNTmvyKKP+tE/b7/v+joti4f76/E4+9k6bR3HziOxv+F+h+NkrIFREf8AAABrmcUj/QK2/BXB8gKniuynXuzcxQL3hTJzzNyG3WfrJdOxa3tQ78f9dCX2t9+uXlr31B/011Wy7852edleXsT31iP7RfooCyd2P28RZV+9c5zl2kudU6oN+214PVL1eNttnQfONVqvrmcZZ9ltZ3yo72RxZdp/XqSaPk/jsR6jyPFOf2LXerlEmfP2rqWQ7+2YHKjHFYzTrntM5jxW+9tvV+OMI2Q8gVER/wAAAGuZxSP9cqZewIIXPv3d9GL4PW13FjbUi6DdL3eMu1322/GSOdu73a1X/fxhP63U/mbD6qV1b/1uf12X9V0WJky9u+vxz3XmLqi4++yqV7bZvxpx+qTk+mik2jAbtq+HEW6Xz7HrkTpX9fP6r1+WfbXd8WE2yIKJu4ATfk4e742xO666n/FFn4l33sG+tu7Yd9l6HN5+ug41HuHx6rvMeaT2NxtW44xDeHnGyIh/AACAtfXikfwG3xbzZigvrOGLmH4xnl7Q3P2dl7nsMd5Ln2w3/yzHtXo5NMI+TiX+8unUGx5zsp8iub8Zq/Cltcg4nOm7e8xU1H7hGJmFkNh/qyjeR/8FfeYtqEz21Ov2b1md0Mc63yUXJ4yj10PV49QvJTo2k+j1SJyr/qwXQ+a6I/0N+7PVf6nfHZ7w8+74mvup+5gb1/C8pQ19Tu61P16PEo6/qeDoeSRjy9QXjjOOkWsDjIr4BwAAWOMJCQDg4eUZIyP+AQAA1nhCAgB4eHnGyIh/AACANZ6QAAAeXp4xMuIfAABgjSckAICHl2eMjPgHAABY4wkJAODh5RkjI/4BAADWeEICAHjk5ZlCGbkAAADAxxMSAAA3d8WCBgslAAAA2IunRgAAbo7FIwAAAPwmnhoBABgQi0cAAADYi6dGAAAGxOIRAAAA9uKpEQCAm7tikYfFIwAAAOzFUyMAADfH4hEAAAB+E0+NAAAMiMUjAAAA7MVTIwAAA2LxCAAAAHvx1AgAwM1dscjD4hEAAAD24qkRAICbY/EIAAAAv4mnRgAABsTiEQAAAPbiqREAgAGxeAQAAIC9eGoEAODmrljkYfEIAAAAe/HUCADAzbF4BAAAgN/EUyMAAANi8QgAAAB78dQIAMCAWDwCAADAXjw1AgBwc1cs8rB4BAAAgL14agQA4OZYPAIAAMBv4qkRAIABsXgEAACAvXhqBABgQCweAQAAYC+eGgEAuLkrFnlYPAIAAMBePDUCAHBzLB4BAADgN/HUCADAgFg8AgAAwF48NQIAMCAWjwAAALAXT40AANzcFYs8LB4BAABgL54aAQC4ORaPAAAA8Jt4agQAYEAsHgEAAGAvnhoBABgQi0cAAADYi6dGAABu7opFHhaPAAAAsBdPjQAA3NyVi0cUCoVCKV8AoDfc2YAP2QcE/j//n//P/7/q/19B6qZQKBTKNQUAesOdDfgADwcAWsULDgCUx70VQK+4swEAMCBecACgPO6tAHrFnQ34AA8HAFrFCw4AlMe9FUCvuLMBADAgXnAAoDzurQB6xZ0N+AAPBwBaxQsOAJTHvRVAr7izAQAwIF5wAKA87q0AesWdDfgADwcAWsULDgCUx70VQK+4swEAMCBecACgPO6tAHrFnQ34AA8HAFrFCw4AlMe9FUCvuLMBADAgXnAAoDzurQB6xZ0N+AAPBwBaxQsOAJTHvRVAr7izAQAwIF5wAKA87q0AetXdnc3esCkUCqXHApRCTAHtcucFCqW3AuCeulw8AoAecX9DSTykA+0id9ErYhu4r+6ykxsOgF5xf0NJEk/EFNAmche9IraB++ouO7nhAOgV9zeUJPFETAFtInfRK2IbuK/uspMbDoBecX9DSRJPxBTQJnIXvSK2gfvqLju54QDoFfc3lCTxREwBbSJ30StiG7iv7rKTGw6AXnF/Q0kST8QU0CZyF70itoH76i47ueEA6BX3N5Qk8URMAW0id9ErYhu4r+6ykxtO4Of1fnw93q8f8/n7qcboIRvMz25R28NjPD/v18M95vn+Nt+In9dD12F4n8P2nt6R6Xpz55CSOIfvZ+y47/dzbldK/NzXx4Z9dsrjNX27iLYr5+Hup/r89faGRUSukzpObXf66h6/Wbffd7dN6evSlrkOq/HMXK+d5xUdk1U7cMlYA6XY/AXQHnIXvSK2gfvqLju54QS8l3G9SDK/sIeLD1byBd4sGDirAHqhYVk4UJ/nhYNgf9Weuxhh+7JRb+4cUqLnYBeJnIUORW+3zcuCl7f4oaSONVJjqSSOVcc452LGJFxkSdYdbnePz9Ydjrd8Nn1Tx9l+TtufZhy88dy4XrvOKzEmXjsIcX9DSRJPxBTQJnIXvSK2gfvqLju54QScl3F/YWeSWpRIvcBH93cXXmRR4fF+2IUI+U59Nm2GCxN2AWKr3tw5pETOQS0KPV+q3dhCht0WWzxKH2ukxnKSPFYdIy9v5jjV50j9qbrD7e7xubrVd8GijaH6GmvLHc9of5wxzLVtJMfEbQcr3N9Qks5TYgpoEbmLXhHbwH11l53ccAL2ZfwVWTCYX/J1mV/iEy/wemEhXHTQiwb6r0zkZ92WXUR4vF7TNnOMu2hhFhTkuM16c+eQsjoHvVgl/VLteYtDuq1lLMI2csca0QUVkTnWHiPnpXdQY6LGzhVcp7mOsE33+Ezd+QU4ZyzcjjjjuXm9Ns8rMyZOO1iT6wKUYu8pANpD7qJXxDZwX91lJzecgHlxlxf014d/ebS5aKCOm76X///8nj4+pzpkocDUFS6CmNWEffUmziElPAfVtm1D170sZgSfDx1rpMYyd6xzjCzoPF/6PI/V7Ww346SOz9TtLx7phRz3eijqeNlu+u6Mye7Fo0jbelf5PjEm4djDI9cEKMXeiwG0h9xFr4ht4L66y05uOAF3QcF9wVcfg8UHK/UCH9s/XLBQixI/79fz+X6qf762/JWJv2jg2Ko3dw4p3jk4CyRumRdQggUMr42tY43oWG4c6x2j25Tvl34Y0bon4fbwWiTqXi/+mH6uG17GwR3PWH92tj23ZbbNxY6Jd90QkrECSrH5B6A95C56RWwD99VddnLDCQQv43rhILMIIJIv8GYhwFlkcP+KRdVtflbbzX7y8/IXKZHFo416s+cw04sSdlHJWyCJnY+3Tbc/N6/6aT5vHmskF1Qy28Jj3HZdsbqFqmvZ3zvnbN3heDuLR1Odz+WgZVy8c9m4Xrm2t8Yk9j1m3N9QksQTMQW0idxFr4ht4L66y05uOIHVy7j9y4/n+9u81Ltl+SsTf/uyUGAWDmyxCwbyjbNgJAsGy49mu2ovtngk0vVmz8FsUYJ+e+279SnOgknY9lTsItT2sUa4YDLZPHZ1jP7OrVaJ1D1T3+k+S5mP3aw7PGezb1CfHYf1Nchcr0zbm2OSjT3IeACl2BwD0B5yF70itoH76i47ueEA6BX3N5Qk8URMAW0id9ErYhu4r+6ykxsOgF5xf0NJEk/EFNAmche9IraB++ouO7nhAOgV9zeUJPFETAFtInfRK2IbuK/uspMbDoBecX9DSRJPxBTQJnIXvSK2gfvqLju54QDoFfc3lCTxREwBbWond9P/oyTz/2BG8L0t+n8vY9nH+9/PcP4HNsL9/BL+D45stZn/fi6x/zGP1f+4iGtv/+x4Jf4HZpJtfNDvm5F+Arin7rKTGw6AXnF/Q0n2ZQJAe9rI3fXCkSrz/+rq1kKOv8+y7rFsO7YQJbb2y38/l8gijPpflJ2+W/rk2tu//OJRuo3z/b4b6SeAe+ouO7nhAOgV9zeUZF8mALSnidz9fur7zLxYNJm2rRd8Un+tI5ZFEbvu8fN66Hq9hZD1fmLed7VglWvTMf+FU+IvgSznL6Hi++7tX2bxaLMNx95+35CcH4B76i47ueEA6BX3N5SkX0CIKaBFTeSuXTxKLmCcWDyKLUgp8cWZeRHl4sUjvQg01fnS/cv9ZVC+f+nFo+02HCweAbhAd9nJDQdAr7i/oSSJJ2IKaFMbubssmKiSWfDxir+64iy62IWV2MJPfHHG/jOvpc49bTp2LcKYfqnzc3927e1favFoTxsOFo8AXKC77OSGA6BX3N9QknphIaaAJrWUu/MCiSnrf7YWFG8hJ77Peq0nUZcq7mJT+cUj+0/P7HnZz36Ve/sXXzza14aDxSMAF+guO5cbMYVCofRXgFKIKaBdLebusohkF0vsgkrsL4ksf9Hl8dCLJutjEoszyb8AyrXp2FyEidRnj/Ha3tu/2OLR3jYcLB4BuADZCXyACQ5Aq+zLC4D2tJq7/v9a2J6FHGfRxfyZzbwIlVic0bvZRZjUXwAVWjya/7tO8bK0vbd/kcWj3W04WDwCcAGyE/gAExyAVtkXDwDtaSN3ZcEk9hc0dsFjz0JOeIxYFl5i/wRu3m9eQHHr39OmI7sIs7SZLEtndvYvXDw60oaDxSMAFyA7gQ8wwQFolX3xANCeJnJ3XsAIy76FEb0mEll0EauFl/h+9r8NdKxNR24RZtd3R/sXLB4dasORO+7mZDwA3BPZCXyACQ5Aq/QLC/cwoEWt5G74H8ve9d8BMiW7eDTxF17S+y198PeLlSOLR3O9q4M0//u9/fMXj4614WDxCMAFyE7gA0xwAFqlX1a4hwEtInfRK2IbuC+yE/gAExyAVrF4BLSL3EWviG3gvshO4ANMcABaxeIR0C5yF70itoH7IjuBDzDBAWgVi0dAu8hd9IrYBu6L7AQ+wAQHoFUsHgHtInfRK2IbuC+yE/gAExyAVrF4BLSL3EWviG3gvshO4ANMcABaxeIR0C5yF70itoH7IjuBDzDBAWgVi0dAu8hd9IrYBu6ru+y0D8MUCoXSYwFKIaaAdpG76BWxDdxXl4tHNdDOObRzDu2cQztAmsQTMQW0idxFr4ht4L66y85aNxzaOYd2zqGdc2gHSJN4IqaANpG76BWxDdxXd9lZ64ZDO+fQzjm0cw7tAGkST8QU0CZyF70itoH76i47a91waOcc2jmHds6hHSBN4omYAtpE7qJXxDZwX91lZ60bDu2cQzvn0M45tAOkSTwRU0CbyF30itgG7qu77Kx1w6Gdc2jnHNo5h3aANIknYgpoE7mLXhHbwH11l521bji0cw7tnEM759AOkCbxREwBbSJ30StiG7iv7rKz1g2Hds6hnXNo5xzaAdIknogpoE3kLnpFbAP31V121rrh0M45tHMO7ZxDO0CaxBMxBbSJ3EWviG3gvrrLzlo3HNo5h3bOoZ1zaAdIk3gipoA2kbvoFbEN3Fd32VnrhkM759DOObRzDu0AaRJPxBTQJnIXvSK2gfvqLjtr3XBo5xzaOYd2zqEdIE3iiZgC2kTuolfENnBf3WWnfRimUCiUHgtQCjEFtMudFyiU3gqAe+py8agG2jmHds6hnXNoB0iTeCKmAKAs7q0AetXdna3WzZp2zqGdc2jnHNoB0iSeiCkAKIt7K4BedXdnq3Wzpp1zaOcc2jmHdoA0iSdiCgDK4t4KoFfd3dlq3axp5xzaOYd2zqEdIE3iiZgCgLK4twLoVXd3tlo3a9o5h3bOoZ1zaAdIk3gipgCgLO6tAHrV3Z2t1s2ads6hnXNo5xzaAdIknogpACiLeyuAXnV3Z6t1s6adc2jnHNo5h3aANIknYgoAyuLeCqBX3d3Zat2saecc2jmHds6hHSBN4omYAoCyuLcC6FV3d7ZaN2vaOYd2zqGdc2gHSJN4IqYAoCzurQB61d2drdbNmnbOoZ1zaOcc2gHSJJ6IKQAoi3srgF51d2erdbOmnXNo5xzaOYd2gDSJJ2IKAMri3gqgV93d2WrdrGnnHNo5h3bOoR0gTeKJmOqHvZ4UCoVCoVxVMLbuIiAW5BQKhdJLAUohpvri3icoFAqFQrmiYGxdLh7VQDvn0M45tHMO7QBpEk/EVD+4nsA9kIvoEXEN0V0E1Apq2jmHds6hnXNoB0iTeCKm+sH1BO6BXESPiGuI7iKgVlDTzjm0cw7tnEM7QJrEEzHVD64ncA/kInpEXEN0FwG1gpp2zqGdc2jnHNoB0iSeiKl+cD2BeyAX0SPiGqK7CKgV1LRzDu2cQzvn0A6QJvFETPWD6wncA7mIHhHXEN1FQK2gpp1zaOcc2jmHdoA0iSdiqh9cT+AeyEX0iLiG6C4CagU17ZxDO+fQzjm0A6RJPBFT/eB6AvdALqJHxDVEdxFQK6hp5xzaOYd2zqEdIE3iiZjqB9cTuAdyET0iriG6i4BaQU0759DOObRzDu0AaRJPxFQ/uJ7APZCL6BFxDdFdBNQKato5h3bOoZ1zaAdIk3gipvrB9QTugVxEj4hriO4ioFZQ0845tHMO7ZxDO0CaxBMx1Q+uJ3AP5CJ6RFxDdBcBtYKads6hnXNo5xzaAdIknoipfnA9gXsgF9Ej4hqiuwiwgU2hUCg9FqCU1mLKzQMK5c6lJ7Hzo1B6LzXE2qVQzpZaeBMBAGBAtR84PiV9/a///N8Uyq1LSzm1B3lHGa3UymFyi1Kq1Jx3WDwCAGBA8rBR84HjUzxoU1ooLeXUHuQdZbRSK4fJLUqpUnPeYfEIAIABycNGzQeOT/GgTWmhtJRTe5B3lNFKrRwmtyilSs15h8UjAAAGJA8bNR84PjXGg/Z/vP/lD/q6SPmnv/7H+7/+7W/vf/rD397/7u3zx/e//Nv0s3xn9p339+rbs8+/vv/89Zf3P1bblmP+/He7Xfdv+WxLUIe0+ad/db53+jxv67PIePVEzid2nmMW8nOEImNag7QTa//+hTy4W5Hzr4XFIwAABmQfuFohfY09NPVU/v2vf3x/eQ+zU9l6KJ+/kwfjyIPv1j5//0vw4K0fyJeH9+nz3KfEQ3lYh3kR8B/m23woP1payqk9Rsi7vYX8HKPUyuFWc4s8uF+pFbOCxSMAAAYkDxs1Hzg+JX2NPTT1VD5/KA9/M7u9zz/+NNX11+mh2rQb7cNc4g/lYR26zT9OD+a2rXYfyo+WlnJqjxHybm8hP8cotXK41dwiD+5Xas47LB4BADAgedio+cDxqVYftI8VeWiW6+I8OG89lJvr6B3jltw+c93Lw/o//pT4ZwWqRB7KI3XM2+Q3vepBvd2H8qOlpZzaY4y821vIzxFKrRxuN7fIg7uVWjErWDwCAGBA+iGNxaNbFvMgrR6O54de+33wUD5/px/ow9+25vaR397aB3D7MO4/lNuXBPtAvX4oj9Xhtinf//nv7T6UHy0t5dQeQ+Xd3kJ+dl1q5XDzuUUe3KbUnHdYPAIAYEDysFHzgeNTzT9oHy32wXb3Q7l+AF79KX9yH/2AbeNAFdlv/i2srcN9oA4fyhN1eG3afdp8KD9aZAx6IucTO8/hC/nZbZFxqkHaibXfVCEPblHknGph8QgAgAHZh6hWSF9jD03dFnk4Vg+28ltV98/4nc+Rh9/lt7GmpPYJHtb1d/LgrPfxH7wTD+WpOv4ebJf9WDxq0nB5t7eQn92WWjncRW6RB7coNecdFo8AABiQPGzUfOD4VBcP2htF/bbVXBfvv/sgD+jz9uCh2Nm++m1uZh//z/51cX8jLN/rY9yHaf+hPFnH9GDuP6zL9r8M8XIqY9YTOZ/YeY5YyM8xioxrDdJOrP27F/LgfkXGoBYWjwAAGJB9SGuF9DX20ESh3Km0lFN7kHeU0UqtHCa3KKVKzXmHxSMAAAYkDxs1Hzg+xYM2pYXSUk7tQd5RRiu1cpjcopQqNecdFo8AABiQPGzUfOD4FA/alBZKSzm1B3lHGa3UymFyi1Kq1Jx3WDwCAGBA8rBR84HjUzxoU1ooLeXUHuQdZbRSK4fJLUqpUnPeYfEIAIABycNGzQeOT/GgTWmhtJRTe5B3lNFKrRwmtyilSs15h8UjAAAGJA8bNR84PsWDNqWF0lJO7UHeUUYrtXKY3KKUKjXnHRaPAAAYkDxs1Hzg+BQP2pQWSks5tQd5Rxmt1MphcotSqtScd1g8AgBgQPKwUfOB41Mt9RXj6i1OyTuMplbMk1sopWYsEbUAAAxIHjZaenjlQRst6C1OyTuMplbMk1sopWYsEbUAAAxIHjZaenjlQRst6C1OyTuMplbMk1sopWYsEbUAAAxIHjZaenjlQRst6C1OyTuMplbMk1sopWYsEbUAAAxIHjZaenjd7OvP6/0w56TK89t8AfH9/Hp/PCQ3H+P8Of68X4/H+/Vjfy4wHhGbcdqY3s6nqO/n++vxmqLJfvx6P3SAoWG1Yv5UO3IPdmLOv6/9gm5y4Jo5oci8u0PN+zQzAgAAA5KHjZZeDDf76j1U6wdBXuQWxRaPbjzG+xePrtNSTu3R2/kU5b44By/RaFetmD/Vzp0Xj8iBFRaPAABAF+Rho6UXw82+Bg/VP6/HvLAhP9vzXR5uv9/Pr+mh+3s6btpuH/Ci+8pD8fM57W+2u0+D8t1qu9Rttk1lXmBJ1pPY/3A9AbdvU9G7fVDn0THO1CkP1fP+U7FffTT+U0mf4/KSpdu2L1wfjnFA9u1Jb+dTlMSIilGJIf8FPhnHTv5IHOp4Ox6D8fwpG8ujkjGq4VQ7ucWj6To/X3o++/p6vr/lutufZddU/G0dlzPXeSAH7DZ3+yS6/2R/rAcLaXJe9gTmcYvnSNE5ITjHGrkpx9bCjAAAwIDsQ0wrNvvqPVTLQ6R5aFMPZ8sDmTyg6gc3/bD7eC4Pqcl91cOgfZDWx6kqVospL7OPS/Z3Ht5j9Xic/T0H6wn6Jg/H6+fST+rcMcaJOtVLwry/09bBeo6do/tiEbxkzOy+8uOO8YhoKaf26O18ipIYmeLvZV/CrVwcB/HqHadsx2Ayfzyfx/KoasX8qXaCe553L3Piy42ROc5S8bd1XI459lwO6Dlk3h7Z/2isz+3I1qlP9lh3+8LJEdWXAnl0xby7Q837NDMCAAADkoeNll4MN/sqD23mnFQxT2zqAdLdPn8nD2r+g11y39UDr37Aiz+Qauqhd67HPBAm6lGfYvuntmfqscK+uQ+xZ+ucDjw2xplxs8dOO80PzJ+Of/4c3XPyz+/0eERIHT3p7XyKkhiZxufx+p7iw74A7o1jvZ+N3yMxmMofUTKWRyVjV8OpdrYWj0xcSBzY2JrjLBV/W8flyLHTeZzNAdt2av/DsT6Pz7Svqlf36fu5cZw7jvIpto/X93geueMn5Lxs98/WuYfUWQszAgAAA7IPMa3Y7OvqoVpLPwD7D6IiuW/iAS98UJx5+zvtpB4Ud+2/ox5H8iH2gzoPj3GyTvnZPkQvx346/vlzDNs3P38yHhEt5dQevZ1PUW6MyM/mLXFfHDv7HY5B+XmdP8frQUytmD/Vzh0Xj2ydTj17c8Aek27rYKyr/aefX+afrE37PV6v99PtY+44+Tm1j7c9nkeXzLs71IpZwYwAAMCA7MNYKzb7mljYUA9n85+Fu9wHRyO1b+oBL2jT/rM17wFS1Zl/UEztf7Qej7ePfNQPsR/VeXSMU3WWqsfbLh9z5yjHmQd5p46PxiOipZzao7fzKSoXl6k4nrfL/Ue/DB+OwUT+lI7lUdWK+XPtSNy4seV8luusb3BeLMwLMyom1vG3eZyKmel5wezjScWW15ZjtX+sb46jsT6R76JtyKfkcUvfk/us+r60OfP2kY8F5t0dat6nmREAABiQPGy09GK42dfUgsREPUya8/UfntcPatF9Mw943v7zw7V+MLfbvIf3aD2J/Q/X45P9575NRXfvgzqPjvGe81UlMZ47xv/IOeq65dgdY79zjENST096O5+ivBiZOJ+jcTxZ4nV6mZxfao/GYCp/ysbyqGT8ajjdjlxPe52nou95Zrv5IPGnY0s22zjTP+vjnPjbcZzax411K9zufE7ey+dtU5k7n9j/aKyLaZ562p8nP6+nE++p48rNCcsY66JP8drclHprYUYAAGBA9sGmFS31FVumB2nnhUO9NOgn7Ob1Fqfk3R31mz93UCvmm8qtb/nnXydWNULeYskexPoeNWOJGQEAgAHJw0ZLD69NPWgj70deCMxvYVVZ/jKjdb3FKXl3Qx3nzx3Uivlmckv+4rTUgs3RxSNifZeascSMAADAgOzDWCta6ivG1VuckncYTa2YJ7dQSs1YImoBABiQPGy09PDKgzZa0FuckncYTa2YJ7dQSs1YImoBABiQPGy09PDKgzZa0FuckncYTa2YJ7dQSs1YImoBABiQPGy09PDKgzZa0FuckncYTa2YJ7dQSs1YImoBABiQPGy09PDKgzZa0FuckncYTa2YJ7dQSs1YImoBABiQPGy09PDKgzZa0FuckncYTa2YJ7dQSs1YImoBABiQPGy09PDKgzZa0FuckncYTa2YJ7dQSs1YImoBABiQPGy09PDKgzZa0FuckncYTa2YJ7dQSs1YImoBABiQPGy09PA6/IP2z+v9MNdMlee3+QJ30lucDp93GE6tmG8itz6ed37er8fj/foxH0tQfXq+mQEXNWOJGQEAgAHZh8FWtNTXS8gD8+M1PYqrD9MD+df7UfSJHCX0FqfD5x2GUyvmm8itj+cdFo9qqBlLzAgAAAxIHjZaejFsqa+X8B7i5ePDPMR/v59f08P5tzxQf73tL4ble3uN3d8Wfz/NNlP0V7E6ZNuy3/zC8P18P196vy95gJ8+6314mBcyFj3p7XyALbVivoncSs47wRwz7xPOJf7iUfQYmUOe07xit2/NV97ikZ6ntuYstw6/r4n9E/25K+lnLcwIAAAMyD4wtaKlvl4i8hvg5YF5eqB/Lg/46uE3sq96cJ8fhM1DvtopUodHvjcP607dbn3ykD8/fA+stzgdPu8wnFox30RupeYdtcBi5xI9F+j7fziXOItHqWNku7cYpPdPzlfz4pE7D4bic5buT2yuCvaP9OfOasYSMwIAAAOSh42WXgxb6usl1AOz+U2oFO+h2n+ADhdy7EN48mE8UodQ+89trl8A5of/CYtHWm9x2tv5AFtqxXwTuZWYd8K/CFq+C+eSZfEoecxqcUfvv7V49JT6loaU5Jw11z8J5rD8/s7i141J/2thRgAAYED2gakVLfX1Et5vgF3uIpCWWjzSD8L2QdndZ12H/wDtfB88eLN45OstTofPOwynVsw3kVuJeSd9vw/nEn/xKHpMcrEmMV+ZBS39T+KcharcnOWeg53Ddu3v9ue+asYSMwIAAAOyD2StaKmvlziweLR++DUP2AfqcBeGVH3ugzWLR0m9xenweYfh1Ir5JnIrNWeoOcH+0y5XbPHIzD+pY1bzlTk+1bZaPHL/WZn+OTtnBfOh7Ld/f/d87qlmLDEjAAAwIHnYaOnFsKW+XuLI4tFEFnPsNbaLPXpfZ/t8XKwOZ9/p+HlxSB6sWTxKkvHqSW/nA2ypFfNN5FZy3tH3fzU/qJL+bwTp/fS26DHJxZrEfOUtHk3m4zNzlltPbD4M92fxKIkZAQCAAdkHqVa01Nf7mh6WnRcB9SA/P0ijhN7ilLzDaGrFPLm1pdB85S0G9almLBG1AAAMSB42Wnp45UG7gB95GDe/aVXF+e0tiugtTsk7jKZWzJNbG0rNVyweFUXUAgAwIPtA1oqW+opx9Ran5B1GUyvmyS2UUjOWiFoAAAYkDxstPbzyoI0W9Ban5B1GUyvmyS2UUjOWiFoAAAYkDxstPbzyoI0W9Ban5B1GUyvmyS2UUjOWiFoAAAYkDxstPbzyoI0W9Ban5B1GUyvmyS2UUjOWiFoAAAYkDxstPbzyoI0W9Ban5B1GUyvmyS2UUjOWiFoAAAYkDxstPbzyoI0W9Ban5B1GUyvmyS2UUjOWiFoAAAYkDxstPbza/lIody89iZ0fhdJ7qSHWLoVyttTC4hEAAAOq/cABAACAdvHUCADAgFg8AgAAwF48NQIAMCAWjwAAALAXT40AAAyIxSMAAADsxVMjAAADYvEIAAAAe/HUCADAgFg8AgAAwF48NQIAMCAWjwAAALAXT40AAAyIxSMAAADsxVMjAAADYvEIAAAAe/HUCADAgFg8AgAAwF48NQIAMCAWjwAAALAXT40AAAyIxSMAAADsxVMjAAADYvEIAAAAe/HUCADAgFg8AgAAwF48NQIAMCAWjwAAALAXT40AAAyIxSMAAADsxVMjAAADYvEIAAAAe/HUCADAgFg8AgAAwF48NQIAMCAWjwAAALAXT40AAAyIxSMAAADsxVMjAAADYvEIAAAAe/HUCADAgFg8AgAAwF48NQIAMCAWjwAAALAXT40AAAyIxSMAAADsxVMjqrEvKhQKhUKhUCgUSs8FAHrDnQ3VxCZWCoVCoVAoFAqltwIAveHOhmqYTIH7I0+BNpG7wD2QiwB6xZ0N1TCZAvdHno6Fa90Pche4B3IRQK+4s6EaJlPg/shToE3kLnAP5CKAXnFnQzVMpsD9kadj4Vr3g9wF7oFcBNAr7myohskUuD/yFGgTuQvcA7kIoFfc2VANkylwf+TpWLjW/SB3gXsgFwH0ijsbqmEyBe6PPAXaRO4C90AuAugVdzZUw2QK3B95OhaudT/IXeAeyEUAveLOhmqYTIH7I0+BNpG7wD2QiwB6xZ0N1TCZAvdHno6Fa90Pche4B3IRQK+4s6EaJlPg/shToE3kLnAP5CKAXnFnQzVMpsD9kadj4Vr3g9wF7oFcBNAr7myohskUuD/yFGgTuQvcA7kIoFfc2VANkylwf+TpWLjW/SB3gXsgFwH0ijsbqmEyBe6PPAXaRO4C90AuAugVdzZUw2QK3B95OhaudT/IXeAeyEUAveLOhmqYTIH7I0+BNpG7wD2QiwB6xZ3tJDsxUCi1Co6LjSOFUrq0rKX+h+NOody19CR2fhRK7wVAHNlxktxY/r//5/+mUKoUJrJzyFPK1YXcrId8prRQersnkHeU0QrzOpBGdpzEZEqpWZjIziFPKVeX1nOzpf6Tz5QWSm/zNXlHGa30lsNASWTHSUymlJqFiewc8pRydSE36yGfKS2U3u4J5B1ltMK8DqSRHScxmVJqFiayc8hTytWl9dxsqf/kM6WF0tt8Td5RRiu95TBQEtlx0jCT6f/87+9/ns5VzleX//b+v/6n/93/+D/l8/P9P7z9vt7//H/8za/LlP/3//hv6+9UXf/8/l/ONtnPa0/K//nPXhtff3ou33Vc5FxxnIxbbDy7KeTnrxc5T9QhYx27BkMVcv72RcahJ3I+sfOkBIXc7KbIeAGIIztOkhtL7IbTXfEmvP/7/b/+NE1Af/jv7/939Z2eDJdJzkyOq8nKTpr+xDdPus7+4WS4mhwjE2ivhYnsnO7zlPz89dJ6brbU/2Hm3Vwh529fepuvybudhdzspvSWw0BJZMdJw0ymwWSoJyQzAWUnw6mY33rYY91tq+12MnS2e5Of+d6rf6Ai44LjZNxi49lNIT9/vciYoA4Z69g1GKqQ87cvMmY9kfOJnSclKORmN0XGFkAc2XGS3FhiN5zuijfh/e39f/1hmrB2/SYlvk3/Juaf3/9D6nF/y2Inuz/98/T/9WTrTobexGiPGagwkZ3TfZ6Sn79eWs/Nlvo/zLybK+T87Utv8zV5t7OQm92U3nIYKInsOGmYydRMUnK+upjfojjf7Z4Mnf1Xk5udDKd91XfTRLmeDJ22BytMZOd0n6fk568XcrOe7vN5TyHnb196uyeQdzsLudlN6S2HgZLIjpOGmUy9CS/4vDUZOhOcfPYmtOA7/7Op6w/hZLiePOU6eG12WpjIzuk+T70cDD5735GfV5XWc7Ol/nefz3uKl9fBZ+87cv63Suv3hBB5t7N4+Rd89r4jN+9eesthoCSy46RhJlNvwpPiTHred+vJ0J/A9Pcybn5JTI7zv/U2x4ffqxKZgDstMhY4TsYtNp7dFPLz14uMA+qQsY5dg6EKOX/7IuPUEzmf2HlSgkJudlNkPAHEkR0nyY0ldsPproSToZmk1OfcZGi+m/+dtnucrTtSlzuxqX/vPU+m688jTYZMZOd0n6fk56+X1nOzpf5LX2PXYKhCzt++tJRTe5B3Owu52U3pLYeBksiOk4aZTO2k5pRwwnMnQ3e/ZeIz/+FA+1uTuTj/QcHIZKjrdCc/+9sZt52wzj6LnCuOk3GLjWc3hfz89SLniTpkrGPXYKhCzt++yDj0RM4ndp6UoJCb3RQZLwBxZMdJcmOJ3XAolCsKE9k55Cnl6tJ6brbUf/KZ0kLpbb4m7yijld5yGCiJ7DiJyZRSszCRnUOeUq4u5GY95DOlhdLbPYG8o4xWmNeBNLLjJCZTSs3CRHYOeUq5urSemy31n3ymtFB6m6/JO8popbccBkoiO05iMqXULExk55CnlKsLuVkP+UxpofR2TyDvKKMV5nUgjew4icmUUrMwkZ1DnlKuLq3nZkv9J58pLZTe5mvyjjJa6S2HgZLIjpOYTCk1CxPZOeQp5epCbtZDPlNaKL3dE8g7ymiFeR1IIztOYjKl1CxMZOeQp5SrS+u52VL/yWdKC6W3+Zq8o4xWesthoCSy4yRuLKiJeDuHccPViLF6GGu0oLc4Je8wGmIeSCM7TuLGgpqIt3MYN1yt9Rhrqf/kM1rQW5ySdxgNMQ+kkR0ncWNBTcTbOYwbrkaM1cNYowW9xSl5h9EQ80Aa2XESNxbURLydw7jhaq3HWEv9J5/Rgt7ilLzDaIh5II3sOOn0jeX7qY6dy/PbfFHK9/s51btZ7dyPx/v1I8fI/zff/aZbjM+NxsOQscBxh8etVPz9vN6PVAyp72wM3i/Wsi7PT1cbYyPjgDoOjXXVWI1w8zx3PyjtqvNW9T6nrDxqz5xbQ71+yLj35NT5/Hb+teAuY7R6JrlDvobq9kuuB4A4suOk0zcW9wHM3LAfRZ8o99xgf96vh9NuzQfbLXcYnzuNh8FEds7hcSsVf3tj6IaxlnV5fjoaGZvWc7Ol/h/qa81Y3VIzlq86b7feQ+7yMlqvH73N16fO5075d1e3HKO75Guobr96y2GgJLLjpNM3Fu8BTC/izL9tMJOH1K1KavtUvAkm8r0+1NRvtulj/G1fz1fwWQ6MHSebpZ3H+/l87Nx32e59l3O78cmcY+lzz5A6cNzhcTsdf0FeJGNgiZnndyTWTF1LiDmfk3WKoK75u8IxWiM/1XGRsUmdi+qTs+9UbNPJY86ef4QcizoOjXXxWE3EUhir6rvveZsO3RP1no3RM+ctxzjPAm7/vx6vqRazj3ts6j61+uy/9H0/9fHLuKSOKzAukX11PwqPeUCO68mp87k4/5bv4tdSxZmN3cnP6+FdS/W9DsJ4LJi41dsLxqTrqlwV2TF2n1eWuvQumXw9MiYlxiqyr+5H4esQIccCiCM7Tjp9Y3EnC3OjUzc3c0Nf7nP6Jr18t9zMdR12X2c/xb/xzyLHzPus2na4x7n9ner4zrWR7XOG2u+Xx2fVliN1HqvjT5x7BhPZOYfHTV2rc/GnY8zkRTQG/HrVd2G9uc/ROvV33oNyuJ9VIkY/GJ9oW5vHJfrk1uH2SaTOZdXuifOPaD03W+r/ob66cWGu90ex6nK2J3Mv9bNw++Zy29vbl5Bbt6lj87zVMRv9Dvu86qtTt/dZt7Msljt1ZI9zrNraMy7O+Tmf5+OsU3XntX5PCJ06HzV25fLv3Bxn29Ft+P2JXFfneFls8hZjQqu2TsSN2u+CXM0d77YzbV8/r9g8WefroTFxnRorp8/O5/k461Td23rLYaAksuOk0zcWdTOTG7Ip5i6nbsrudvf7cCJwP6v6nIex4AZrf2ugi60juAmH9U+ix0X2E7v2TRy7cofxifQ1Po47j9977hlSP447PG4l4k/kYkD9bGIwt1/4OfOdisPEg/Wd8/PouEbPpUSO7z3/CKkXdRwa69JzySQWS8ncW/18rN6tY5LOnLebQ147Ti4l8iz6shupw7ZnuqNtnONH4/KL9wWpsyenzqdw/iXzbBK9ltOeavFDdpK+TMe+pv10M1MfnEWQdCy49WlF4+bMGLlx7bWzxPehMRbmXHXzW/kq2/3j4+Of2B62H+uP+MX8FVIvgDiy46TTN5bVDVHTN/v1diVzQ1wfF04g+UlGCepPHhf2Y7J738ixUXcYn6C+1H7Fzz2Dieycw+NWIv5ELgbUzyb/cvuFnze/cx6mTHIXj9HL89MRHJc8l8xDZvHzj2g9N1vq/6G+Xhmrq1ha557dvpXnxWP0zHm7x3jtOM8KmTzL91XvZ1/evRfPzHGfjsv6fOvdF1q/J4ROnU/h/NM/r/MseS2F6sPj/Xg4MfyYPk/Hm8Pzx0/sIoUskhaPmzNj5B7jtZOI71Csb2qbHZNMvhp7xuTTsVqfQ738FadiHhgE2XHS6RtLYrLQNznnTzTls/dblsQNMTwuvMHaOlS7/uSgf56YOuzn5HFhPya79/U+m98I2eNcdxgfc8zWeBQ/9wypG8cdHrcS8SdyMWDqisWaG5/6o/THPS5RZ6LfzeXn6rhlLJLnEvZJfdb1nzv/Y8jNeg6NdeFY3R1/lmlnK95ukaPuMV47zv0orNf7nLlved/pn5d+p4/7OHfD83Xauvq+cChOG3DqfLz4cITXRT7ba5Eb/0R9yWuphPFm8sapJ3+8MMdMG5vJ1dzx3jGG2V+ft5uT4fhZ22PycY6ZPv1G/gqpE0Ac2XHS6RtLarIQ5iaoS+YGGH72jtNF3UjtjV+K+Y/i6RuxOzloy5+ASt8Sx0VvxDv3DT6r9lYT0uQW42P6p77LjEfpc8+QNnDc4XErEX8iFwPq5yX//FiTr6cHo7kdp61cnUdjPFfX5Pfy09k+2ZWHyXMX587/CKm7ZS31/1Bfi8fqwfhTx7o/H6w3e4w0O+1fKkfdYyJ9U+ewOk/n+EnyvuXWIVT90/dmw9ZxatvZ3E1dmxJ1Z0i9PTl1PmfiMDf+R6+lIfE1x56Y6nG/Tx3vx6U9j3NxUz1X1cedYyzUNn8853rMd/NC0Vznxpiktoftx/pjeeegi9vHj+reIHUDiCM7TuLG8hk1Cc2z01jOnDvxds4w4yYPSc7DqXpY/SC/msrP3AN4BeRmPbcc68K5t9fIc+jd9XZPuMX5/FKelUCutod5HUgjO07ixvKB4CFgKCfPnXg7Z5Rx+/l+6t8OzuWDxZTW8pPFo4+01P879rVo7u018hzagNbvCaE7nM+v5FkJ5GqTesthoCSy4yRuLKiJeDuHccPViLF6GGu0oLc4Je8wGmIeSCM7TuLGgpqIt3MYN1yt9Rhrqf/kM1rQW5ySdxgNMQ+kkR0ncWNBTcTbOYwbrkaM1cNYowW9xSl5h9EQ80Aa2XESNxbURLydw7jhaq3HWEv9J5/Rgt7ilLzDaIh5II3sOIkbC2oi3s5h3HA1Yqwexhot6C1OyTuMhpgH0siOk7ixoCbi7RzGDVdrPcZa6j/5jBb0FqfkHUZDzANpZMdJ3FhQE/F2DuOGqxFj9TDWaEFvcUreYTTEPJBGdpzEjQU1EW/nMG64Wusx1lL/yWe0oLc4Je8wGmIeSCM7Trr0xvL9VPXP5fltvqjo5/V+fD3erx/zOWbPPmepup0x+Hq+t0fh+/38uD9Sx9f7N4Y8R8YAx7UxbgVjLsxJcy95bCWF2m9Pjn2iRH7eD7lZzzBjfeYZ4Mr5GIf0FqdNnc8dnp/3qDLnFrBjPL+fkWeMU8/w9yF9BhBHdpx06Y3FnVTMDXjz5a+0PQ+iVz6sBnXL5LQ5+RTpD4tHPWlj3K5aPNL17rp31HiQvfJ+8Ytaz82W+j/MffDMM0Cn+dWi3uK0qfO5w/PzHjXm3BI2x1M/Z6zOJbgf7XqGv5Fh5hrgBLLjpEtvLN6k8vN+Paabrn2zNDdvaV+V1PapLDd4U0fsu+A4f7u98fvHp471vjt1jMNrX7gvwrG6g3NU45LoQyjSJz2sH55DQdIGjrvtuF0Vc07eqIe1x2uq0RHWY+8f6p7jbA9fQlPHmfaez4ez/cP83HuuNyH9Qx3DjPXpZ4CNOXuV58GidVC3n5N78tzuu2z3vhuEnHNPmjqfX3x+nuuTPjxf83GP1/dSh52Ts3PumdxK9fPDPM2N5+TnNd0TzLk6m039qWf4+5PxABBHdpx06Y3FvVmbG7y64eZuxma/+eat6tD7ei+R7n7hMavv3LYMp954m3aScWweE2ln1b6etFYTz6ruSF0i1c5qQtOf5/5ZZ86hICayc+45bhfGnM2Bl3wf5OIqP5x+qPqc/VdtJ44z/dLn8v3+3jwHtx7HmXO9idZzs6X+D3MfdPPRzbHNXIzkiZs/br0il3fuZ7cPu/K8nfy9Qm9x2tT5uDHuxq362Y3D9TwWi9n883OiPnW8nzurn91+ilSeuNvP9NO1sy6P2h4ZT0U/l0sdahHJ/WWV2tetL/EMf1PDzDXACWTHSZfeWNTNWv8mQBVzd1c3Z3e7+314o3Y+pyYVXZ8zebk396A+VcfcptmeaVOcOWa22u5PPLvqnkT3c7kTo6IfAOyE+tE5FCTt47hbjtuVMae2T8dN+f6SepyHuez9I9EnybdD9x3jo/zce643IX1HHcOMdYFngGhuZe49R54HrB7y9woyHj1p6nwK5I77WcV48vk5qEuKfOnmmVe3M9dn5lz16WBupfopPsrTxHgq3jn4zzHr+lg8AnpBdpx06Y1lNalo64c7R24iUD+vb/57Hxa9/Vb1xts8c4wnul/kITdTd3I/x3oMEg/TmXZSdZfERHbOHcft0phT2+0DnP8wum7XkXiQXfUpFOnH3nP4+FxvovXcbKn/w9wHP3wGSObWoTzPLx71kr9X6C1OmzqfX3t+drh98Ope8m13Lq768kE/99blSozndIC6P8zt2eItYIX122eT+5NzARBHdpx06Y0ldbM2N9955V4+Z2/U5vNGffPN3P3s/KwmH9uOqsvdJ97mmWM83nYzSZn68nWbn9XHxH4uc8zy25BgAv/kHAqStnHcLcftypgLtqv67OewXfnsteXcI9zPueOC9sTH+RnWGWnjTsjNeoYZ6zAfLZND6VzcuI+E9arP/v3B5qf3Wf18JM/byd8r9BanTZ3P6dxJxOyZ+txjvLozi0fO51O5lejnx3maqDe6v7vN+95/hm9BbzkMlER2nHTpjSV1sxbm5q9L5sbvfvaO0UVNHiL4bt4+tS6TnP7thflZfdb/YTw1YebaPHWMQ203x0vxJp1E3fLN/Oe5Mn7p/TzJ8fnwHAqSPuC4247bVTG32m5/O2juJ6n7x6o/Qd2p46L9+DA/957rTUj/W9ZS/1sf691Uvh18BrD5lJuzV3nuzvmT4Pv5uyN53lj+XkHGpCdNnc+Z3MnFbC5nUvW5ffDq1jmjUzSs1zn+TG4l+/lhnrrn4vD+mdzMPG9Iw6o+066UhhaOhPQZQBzZcVJTNxa5iTs3bnXTn2c/tICJ7BzGDVcjxuphrD+UeBFEWb3F6dB518rzM8/5RTHXAGlkx0kt3Vh+pgdG7zcAPDw2h4nsHMYNV2s9xlrqP/n8IRaPqugtTkfOu1aen3nOL2vkmAe2kB0ncWNBTcTbOYwbrkaM1cNYowW9xSl5h9EQ80Aa2XESNxbURLydw7jhaq3HWEv9J5/Rgt7ilLzDaIh5II3sOIkbC2oi3s5h3HA1Yqwexhot6C1OyTuMhpgH0siOk7ixoCbi7RzGDVdrPcZa6j/5jBb0FqfkHUZDzANpZMdJ3FhQE/F2DuOGqxFj9TDWaEFvcUreYTTEPJBGdpzEjQU1EW/nMG64Wusx1lL/yWe0oLc4Je8wGmIeSCM7TuLGgpqIt3MYN1yNGKuHsUYLeotT8g6jIeaBNLLjJG4sqIl4O4dxw9Vaj7GW+k8+owW9xSl5h9EQ80Aa2XFSczeWn9f7MfVZ+q3L8/1tvsr7fj+n/Z/7dq5E+vR4v37MxyPMONzrfLYxkZ3DuOFqxFg9jDVa0FuckncYDTEPpJEdJzV3Y1GLJsuCy/dz7wLSDRePgnMZARPZOYwbrtZ6jLXUf/IZLegtTsk7jIaYB9LIjpOau7GsFlz0otDDbgj+MmneHiwe6UUn8zms0/38/Xx/PV/v18PW9z3//PV4veduBO1+2YbC7VPRffpZ6pGiO+JtC/uut0u/lv1utRi2g/QZxzFuuBoxVg9jjRb0FqfkHUZDzANpZMdJzd1YwoUes+CiFlrMQs28oOJ9totHduHF+WulsE73sywe2Trc+lY/u31yFrTc/dRXUp/Zd3Wcw9nv5/XwF6pEWG8jmMjOYdxwtdZjrKX+k89oQW9xSt5hNMQ8kEZ2nNTcjWW14LIsHqlFFu+fsDkLS95f7wSLLmGd7me1iGPq9PZb/pJJt7vUPRf9Zbru8LuJ/YsoXdz9nM/CbGPxaAyMG65GjNXDWKMFvcUpeYfREPNAGtlxUnM3luhijLuIs7F49Hi9X2qBJqwj8Xn34pHbriNXd/CdV0943MQuLEX/oqkRTGTnMG64Wusx1lL/yWe0oLc4Je8wGmIeSCM7TmruxuItqph/gmb/SVe4oOJ9XhZ73IUkvTbjficfnX9atmPxyLajF6nMZ69PziKQ+9kcZ9tVi0f2ONWu0yfFnK9sDI5tBRPZOYwbrkaM1cNYowW9xSl5h9EQ80Aa2XFSczcWs2gi/VZlXgAyzKKLLcviSrBAZOsxG9b/9OzA4pH66LabWCyKfF7+mZq0YRa1pJj/SPfyz/Hc/Sam/ywejYFxw9Vaj7GW+k8+owW9xSl5h9EQ80Aa2XESNxbURLydw7jhasRYPYw1WtBbnJJ3GA0xD6SRHSdxY0FNxNs5jBuu1nqMtdR/8hkt6C1OyTuMhpgH0siOk7ixoCbi7RzGDVcjxuphrNGC3uKUvMNoiHkgjew4iRsLaiLezmHccLXWY6yl/pPPaEFvcUreYTTEPJBGdpzEjQU1EW/nMG64GjFWD2ONFvQWp+QdRkPMA2lkx0lyY6FQahYcFxtHCqV0aVlL/Q/HnUK5a+lJ7PwolN4LgDiyA9VwQwbujzwF2kTuAvdALgLoFXc2VMNkCtwfeToWrnU/yF3gHshFAL3izoZqmEyB+yNPgTaRu8A9kIsAesWdDdUwmQL3R56OhWvdD3IXuAdyEUCvuLOhGiZT4P7IU6BN5C5wD+QigF5xZ0M1TKbA/ZGnY+Fa94PcBe6BXATQK+5sqIbJFLg/8hRoE7kL3AO5CKBX3NlQDZMpcH/k6Vi41v0gd4F7IBcB9Io7G6phMgXujzwF2kTuAvdALgLoFXc2VMNkCtwfeToWrnU/yF3gHshFAL3izoZqmEyB+yNPgTaRu8A9kIsAesWdDdUwmQL3R56OhWvdD3IXuAdyEUCvuLOhGiZT4P7IU6BN5C5wD+QigF5xZ0M1TKbA/ZGnY+Fa94PcBe6BXATQK+5sqIbJFLg/8hRoE7kL3AO5CKBX3NlQDZMpcH/k6Vi41v0gd4F7IBcB9Io7G6phMgXujzwF2kTuAvdALgLoVVd3Nnuzpty7YGyxmKDcr2AMV17rMKYodQr6F7vulPsVAOhNV3e22I2bcr+CscVignK/AnwqFleU6wv6F7vulPsVAOhNl4tHAO6LPAXu48pcJNeBa5BbAIDf0NXMw2QK3B95CoyBXAeuQW4BAH4Di0cAqiJPgfu4MhfJdeAa5BYA4Dd0NfMwmQL3R54CYyDXgWuQWwCA38DiEYCqyFPgPq7MRXIduAa5BQD4DV3NPEymwP2Rp8AYyHXgGuQWAOA3sHgEoCryFLiPK3ORXAeuQW4BAH5DVzMPkylwf+QpMAZyHbgGuQUA+A0sHgGoijwF7uPKXCTXgWuQWwCA39DVzMNkCtwfeQqMgVwHrkFuAQB+A4tHAKoiT4H7uDIXyXXgGuQWAOA3dDXzMJkC90eeAmMg14FrkFsAgN/A4hGAqshT4D6uzEVyHbgGuQUA+A1dzTxMpsD9kafAGMh14BrkFgDgN7B4BKAq8hS4jytzkVwHrkFuAQB+Q1czD5MpcH/kKTAGch24BrkFAPgNLB4BqIo8Be7jylwk14FrkFsAgN/Q1cxTczK1bVEotUovap6PO34UylUFcaXHxx1zCuXO5Wq12hHueVEooxQAcV1lR82El3b+6z//N4VSpfQ0kZGnlJ5K67l5Zf9L5zr5TGmh1LgnlM6tHPKOMlqplVtAi7rKDiZTSq+lp4mMPKX0VHrKzdJK5zr5TGmh1LgnlM6tHPKOMlqplVtAi7rKDiZTSq+lp4mMPKX0VFrPzSv7XzrXyWdKC6XGPaF0buWQd5TRSq3cAlrUVXYwmVJ6LT1NZOQppafSU26WVjrXyWdKC6XGPaF0buWQd5TRSq3cAlrUVXYwmZ4p//H+lz/ocZPyT3/9j/d//dvf3v/0h7+9/93b54/vf/m36Wf5zuw77+/Vl9tHt/Xnv8f3+/LatOVf33/++sv7H/azHPOnf3W+d/o277/UObfVeJFz6YW9NjVIO7HxbKeQn3cvch4tu7L/9jqXInXFrkFfhZxvvch5Xs2OZw3STuw8xyvk5ihFxgNAXFfZYW+ANUg7sRtOa+Xf//rH95c3uUxlazKcv5OJx52ITEnuE5sMl4lO9cWd+KT8/S9qrMMJdJnk3MlQT4TL5Dt9Ds+t0dLTREae7i/k5/1LT7lZWulcbz2f9xRyvv1S455QOrdyRsi7PYXcHKfUyi2gRV1lB5Pp8fL5ZBhMXlKS++QnQyn/+JM70cnnqd2/ThOi7aOq+4/OcUvfoufSSelpIiNP9xfy8/6l9dy8sv+lc731fN5TyPn2S417Quncyhkh7/YUcnOcUiu3gBZ1lR1MpmeKTFYybs6ktDUZmnFe/dbDluQ+25OhTGjzb0LmfjgTqt0mv2FRE9/SN5lIl9+i9FVqxXUNOi7I032F/Lx7qRXLLdIxVm58pK7YNeirkPOtl5Ixn6KvJfNo3UJujlJq5RbQoq6yQ998mUxPFTOBqclknoTs98FkOH+nJ1L3Nx9zXdF9jk2G7s/zROfULd//+e+pydBO8qbfalu7paeJjDw9UVSukJ93LK3n5pX9L53r3eTznqLyj5xvsdS4J5TOrZyh8m5PUXlCbvZcauUW0KKusoPJ9MNiJxpvMpOSmgz1hLT609fkPluTofu9/tleU1VWfbP7mL7Nv11x6+tjMqwV1zXY61mDtBMbzyaLjf0gv7w4T+ae3Te3j5t/Zj/yc7PUiuUW2dgoReqKXYNui82nIGe93Enms903t4+b02Y/cv7jUjLmU+w1qEHaiZ3n0MXGfZBbXown887um9vHzT2zH7lZrdTKLaBFXWWHvWnWIO3EbjhNF5lM1EQjv4VwJynnc2QyWn57YUpyH/1zajJUk6Y9zqtDipnY/h5sV3XYCS+o3x7TwWTY00RGnp4s5OctS+u5eWX/S+d6V/m8p5DzTZYa94TSuZUzXN7tKeRm16VWbgEt6io7mEyPFzUBmXHz/r21TIzz9nACW7avfouS3Sc2GTr7OZOc/ye1uujJ8o/BJCnb/+JNeHKsrrOfiVDOpxf2etcg7cTGs5VCft6/yLkgzsZOKVJX7Br0VMj59ouc69XsNapB2omd52iF3BynyJgAiOsqO+xNtQZpJ3bDoVCuKD1NZOQppafSem5e2f/SuU4+U1ooNe4JpXMrh7yjjFZq5RbQoq6yg8mU0mvpaSIjTyk9lZ5ys7TSuU4+U1ooNe4JpXMrh7yjjFZq5RbQoq6yg8mU0mvpaSIjTyk9ldZz88r+l8518pnSQqlxTyidWznkHWW0Uiu3gBZ1lR1MppReS08TGXlK6an0lJullc518pnSQqlxTyidWznkHWW0Uiu3gBZ1lR1MppReS08TGXlK6am0nptX9r90rpPPlBZKjXtC6dzKIe8oo5VauQW0qKvsYDKl9Fp6msjIU0pPpafcLK10rpPPlBZKjXtC6dzKIe8oo5VauQW0qKvsqD2ZArX0FG/kKXrSeoxd2f/SuU4+owU14rR0buWQdxgNMQ+kdZUdTKboVU/xRp6iJ8RYWulcZ6zRghpxWjq3csg7jIaYB9K6yg4mU/Sqp3gjT9GT1mPsyv6XznXyGS2oEaelcyuHvMNoiHkgravsYDJFr3qKN/IUPSHG0krnOmONFtSI09K5lUPeYTTEPJDWVXbcZjL9eb0fpi+qPL/NF5/5fn69N6v6fpp2n2/ZVY55vH70d7/pF8fkN8Zg17U6QMasFzYGathsp0RcSs49Xu91hP28X48lDm6TixlF4vaiXHfdaSw3Y+zmruy/jYFSqox1hfiNc+4XyXvKB86cl3qe0M8SSarePfs4bZc+t5QrxnEHOcer2bGs4dN2fl4Pf16Zrsv5tCo5r/p1ldLKPHprm+f//X66953C9xipA0BcV9lhbxo1ZNuRm9h849KTU4kXne0JaT0R3uYl69fG5HfGoMjDg6Oniew2eSpKxOXOF5Tb5GJGkbi9KNdddxrLnnKztNK5XmWsK8TvpqsWj644L6n34AKTLCZsLkqVwOJREZ+2I/dr9+U//PyJO86rrcyjt7Z1/pLbU1zO41z4HlMrt4AWdZUdt5lMvZuevonZm56+oel+zg81chN8Pt9Pu92ddcwN0hb9lay4L9ts3WpCdvfzjrU30fixevvj/fqWG3C+nWx/U35pTI6PQWT73vM92q8DYyj79MKORQ2b7RyNS3U9gzyRa/mYjrP7mmup83HaVw6MxaE6bml7fgjOxIab41KKxJjXtwJ1Fsz15P5221ZOB+fmX8fE/gfyUsh+Lbuy/3Z8S6ky1sXiNxFjk1ger+4XkXtKss49cZs8r63c2cgxqTfYZ9W8t48m56v3S49TdLzV/geeVaLjeC1p62r2XGv4rB394r9cv+nzc7omQSza85mvUSKmV3liv3fiK1pfJG5WdSVyKJavK15fdsTl6XwNzs/uc7j/sb7JtTLjIaY652rm/iTOSR1rt9s6rjl/8T3F0Osl9Zhjs/eY46RfAOK6yg57g6oh245309M3VHUDUzfM5U423wxlu/fwZW68wc0zfiOU/e2xwY1/Ise4N1yfe6xu9/Fc2vM5+6b6m/NrY6L32TcGroPne7RfB8ewp4nsNnkqjsaluVZennjX0qlD/bxc11UcynFBzORiXz0wzn1KxczBGLsibkvlemr/ye6cljqCvqyPO3h+gZ5ys7TSuV5lrEvFr2eJsXQeO/cLr06nD56DcZs6L49Tp5Bj3M8zZ795n1Sdk0g9bj4vgnOKjbfaZ/r58LNKpn+F1YjT0rmV81k7+nrIi766fBIL07V7PZxrlMq3+drpa67DRfZZ4js/r7rXPBY3Tl2J9tL56vByS6rayK3kuTm8OoNxcSrfug/t6r/aro9181LOwx7rbl/Ej4tb9v3o/NVH+51Tp2yb69S2+5RWK7eAFnWVHbeZTNVNTPdFFXPHUzdid7v9Tm6k3k1yuem7Nz53QpLvlnrsjdeZCI3VxDqJHys34fWEF9030d+sXxsTvc++MUhs33G+h/t1cAzl2F7Ysahhs52jcRnLE+9autfev66rOIwcp77PxL7t37ST98B1q7gtlOvJ/SersZxs93cin00dp88vIMe37Mr+2/EtpcpYF4pf9SkSY2qbqXM62Mlj59ggbq/MSxGtU38xHbO8kEX3M/s8ZXycOj1BPULqsjkcqzed/2efVfxxvJL04Wr2XGv4qB259nINpv//nC6KXCe5BuovR8x1du/l6jrqHRIx7cd3eHyyvmjcpHJu2b4cL/x515J9wj7YQ9Tx5lqVyFepe6nP+e5E/6N9s9dL9p2O+37qvLXXS0SPU22avjhKn7+QOu14z9db7c/iEVBDV9lhbzI1ZNuZb76+cFKbZW760QnJ29+dDNY34VWbyWMjk2Jq3z03/tCvjUmkjV3ndex8D/drR52uniay2+SpOBqXQWwp3rXUx+rnHP+65uPQ+T4ZG/Lz8jA11/VBjF0St4VyPbn/JD+Wqf5O5LOc4K79E+cXqBXLLbKxWkqVsS4Uv8kYU/tE8jh5rG77qrxM93Mix9gXstR+ah/pnz4vfe8LuPUozr6JetP5H/RR7BoXXWe0f4XViFMbPzV81I5cAzXo0zWXvzgyixF27gmvs2xf36PdmPbjOzw+WV8sbty6su3F8nVx53k02f9U3+yxL/NP1qb9Hq/X+2n3TR5nyPdTO1edf3g+qsh+uXvMCVIvgLiussPeSGrItpO66ambqr8yrqRupN52+ahvhN5Epeq0N971TTg2kcaPXU8CyX1T/VU/T9cgdrf+tTHR++wZg+Pn6zjarz11OnqayG6Tp+JoXEbyJHXtw+saxqHfhtRrvk/FRqKvt4vbkrkevQby1c772qpufVypvBSt5+aV/S+d61XGulD8JmMsVb8bb16d8vG6vEzWKeQYc87Z8/HuY5Ex8vbRddn+Zs8pVpdqwz+/feMiH+29+Vo14rR0buV80o57r5af5+dDuTbys3eN9D1a7bLabq+5H9/hXJCsLxI3Xl2p9pL56vCO1X2SNvfFpX8+s1S7qp5IXhzsf7JvEzc/dV3LGOeOs+w1ueT8V9vN8eq/ZRW/x5xRK7eAFnWVHbeZTDOTjbqhmX7OE0DmRqom23l/dxI026YNy+QZuQlLX3YdG5tYE/vmbvzed45fG5PJoTGIbN8z0U0O9WtnnZYc3ws7PjVstnM0LtX1DK5VEPNyjfW1D67rKg71vrr+6SHLPqAlY8OJJXPMavsd4rZgrsevwWRvTkvddrv5TvtszFxSB+LsuJdSZayLxW8ixtztqtj9nWO9OuXjlXmZ6udE5Zl774vs5+0zCfquOPmqivd9uv3d9+BUHclxvJb042p2XGo4347EojPm0/WYf3biUa6LPR+5fkoypoP4dmJrOTRSXzRuUjnntuHEliphHZrX5lR0s3viMjgfyxmf0On7kCqR7W7fxNS2/BND68f+96qU+HFen7wxX+/7yfl7/TRU21Md6XvMcVIHgLiussPeNGrgxpIwTQrhjR2f6yneyNOzpgcx54FIPTDND2mI8h5Sr9F6jF3Z/9K53kc+N5THFfKnRzXitHRu5fSRd2e1Pu/y3HDG2DEP5HWVHUymv0x+U8CkdIme4o08PelHHgL12Oni/LYfcSwe/Sobq6V0MdZN5LH8NYD0Lf5XFsirEac2fmqo1c4ttT7v8txwytAxD2zoKjvszbEGbiyoqad4I0/Rk9Zj7Mr+l8518hktqBGnpXMrh7zDaIh5IK2r7GAyRa96ijfyFD0hxtJK5zpjjRbUiNPSuZVD3mE0xDyQ1lV2MJmiVz3FG3mKnrQeY1f2v3Suk89oQY04LZ1bOeQdRkPMA2ldZYedTGsVoJae4i3Mo6sLcCViLC3MxRIFuLsacRrmxdUFGAkxD6R1lR3hZHd1AWrpKd7CPLq6AFdqPcau7L+bh6UKcHc14jTMi6sLMBJiHkjrKjtqTnLcWFBTT/FGnqInxFha6VxnrNGCGnFaOrdyyDuMhpgH0rrKDiZT9KqneCNP0ZPWY+zK/pfOdfIZLagRp6VzK4e8w2iIeSCtq+xgMkWveoo38hQ9IcbSSuc6Y40W1IjT0rmVQ95hNMQ8kNZVdjCZFvbzej/MmKry/DZfHPXzfj2+3s9v+f+P9+vHbI6y+5qPUHqKNxtPNdxi3M7k0fdz2vf5zu6p6t2zj9P24zVlWAXS/4NtfT/bzPtbxNgHruy/jbtSWh9rjKFGnJbOrRzyDqMh5oG0rrKDybQwefGcXwD1os4jv/KzYc/iEWJ6irfh8rR4Hhm7F4+WfX5ej2lMNo4pgcUjTErnOmONFtSI09K5lUPeYTTEPJDWVXYwmRbmvfTqF0/70qtfQvV4zy+J8sL4fL6fdrt5E5SXwq8vWTRyF4++l/2mYutd9k3vk2qnZ3KevbDXs4ZbjFsyjzLxrbbZRZ7Eft7CkN5nlQqRBaZlkSZR7ySa32r/KTe/pc6NOtTi0XSe9rtUjs7nqstmnTfMe+lLy67sv72GpbQ+1hhDjTgtnVs55B1GQ8wDaV1lB5NpYd5Lr/PPydRL3PLiNr8MqxdB92XWLgLZRaPUXx7Jvva4Hfsk2+lXT/E2XJ6m8sjj5sAksuijOfvN+6TqnETqcReBF0F+xfJb7TP9/FwWwnxBHXO7if554yKHbIzLTfN+iLngpNK5zlijBTXitHRu5ZB3GA0xD6R1lR1MpoWpF089pqqYNzv910FBke/k5c57SbYvd/Znd5tU7/x1w2qhSe0S3yfZTr/k/Hthr2cNtxi3RB6JeA6oL7xFn+h+Zp+n5ON61UUL6hHLYlC83mR+qwWb9QLPdo7qOmPHuYtY7j7bdd4n76WPLbuy//YaltL6WGMMNeK0dG7lkHcYDTEPpHWVHUymhcmLp/MCaMlL3vovFyZHFo+8fd2/Itixz01fIq/UU7wNl6eJPErnwMRd9Entp/aRBRfJgdhf7UxWi0fOvol6k/kd9lHsylFd5+7Fo1113ifvh5gLTiqd64w1WlAjTkvnVg55h9EQ80BaV9nBZFpY7qU3+GsGJflyZ39eXly9F0dVX7hvZp+bvkReqad4Gy5PE3mUzoGJs+iT3M9bGJJFlkhOBotHUpfNnWx+xepSbfi5ti9H5WNkcSuxz74675P3refmlf0vneutjzXGUCNOS+dWDnmH0RDzQFpX2cFkWlhq8WiiXkLNeM8vmsmXO39BSL8MyouoOX56Y1z+2sE9LrHPTV8ir9RTvNm4qeEW45bMo1QOTFYLQ5H9goUhPy8MtY85Vor3fbr9aH6r/cNc25Oj8jGyeDSR7Us7dp89dbJ41AJ7XUthrNGCGnFaOrdyyDuMhpgH0rrKDiZT9KqneCNPd4gtBOGWWs/NK/tfOtdbH2uMoUacls6tHPIOoyHmgbSusoPJFL3qKd7I0xz5ixoZn/7/mq4XzAVppXOdsUYLasRp6dzKIe8wGmIeSOsqO5hM0aue4o08RU9aj7Er+18618lntKBGnJbOrRzyDqMh5oG0rrKDyRS96ineyFP0hBhLK53rjDVaUCNOS+dWDnmH0RDzQFpX2cFkil71FG/kKXrSeoxd2f/SuU4+owU14rR0buWQdxgNMQ+kdZUdTKboVU/xRp6iJ8RYWulcZ6zRghpxWjq3csg7jIaYB9K6yg4mU/Sqp3gjT9GT1mPsyv6XznXyGS2oEaelcyuHvMNoiHkgravsYDJFr3qKN/IUPSHG0krnOmONFtSI09K5lUPeYTTEPJDWVXYwmaJXPcUbeYqetB5jV/a/dK6Tz2hBjTgtnVs55B1GQ8wDaV1lB5MpetVTvJGn6AkxllY61xlrtKBGnJbOrRzyDqMh5oG0rrKDyRS96inehs3Tn9f7Yc5dyuP1Y75I+37u2e/n/Xp8vZ/f04/fz/fX4zVt+YRTnyvo/+ft7FTknK5zqxg74cr+21gppfWxxhhqxGnp3Moh7zAaYh5I6yo7mEzRq57ibdg8lcWXeRHk+/38ery31oX2LR45rlxoUYtHz6nn9uNjGt/l82VYPGpW6VxnrNGCGnFaOrdyyDuMhpgH0rrKDiZT9KqneBs2T73FI/+ve/RCjB6XeaFEFk3stnmRxiw6feu/ApLjZYHpyy5EecdMxTYQLMAsi1K6H3p/XYdXnytYPBKyr25C+mXr8Re8oucWOY9kHarvj+Wvnuw53YT0qWVX9t9ey1JaH2uMoUacls6tHPIOoyHmgbSusoPJFL3qKd6GzdPUXx7J4oizICKLLXbhZP2XR/q4x3NZCNILQE5dsUWixHa3rYVTnyuyeBQ/Xvpo9kueW+w8XEEdc7uJf1L3i5gL0krnOmONFtSI09K5lUPeYTTEPJDWVXYwmaJXPcXbsHmqFl/0uUuxCyD6L32CYr6MLx6FiyfpxaN58SaxeGQXY/y/5jm3eCQ/L+egj0+fW+w84nXE+h4e95ukry27sv/2WpbS+lhjDDXitHRu5ZB3GA0xD6R1lR1MpuhVT/E2bJ46f3nkLoCsF4gW6+/0X+z4u3+yeGTI99NY6T7tXTzSC0/qGK/+pY/pc4ucR6IOf7uuk8WjNpTOdcYaLagRp6VzK4e8w2iIeSCtq+xgMkWveoq3YfPUWTzSiyOxf5bl+2zxKFjcmduQOtaLOktb+xaP1F8Jmbbcv0DSbTn9iZ7b+jyydbB4dJkr+18611sfa4yhRpyWzq0c8g6jIeaBtK6yg8kUveop3obNU2/xaOIsivj/XMtZbFELNnq7XjDZXjx6TD/PdTmrLLLoorc/pn30QpHX7rxvbvHI1jEV91xUv5Z63EWv+LnFziNRB4tHzbLXvRTGGi2oEaelcyuHvMNoiHkgravsYDJFr3qKN/IUPWk9xq7sf+lcJ5/RghpxWjq3csg7jIaYB9K6yg4mU/Sqp3gjT9ETYiytdK4z1mhBjTgtnVs55B1GQ8wDaV1lB5MpetVTvJGn6EnrMXZl/0vnOvmMFtSI09K5lUPeYTTEPJDWVXYwmaJXPcUbeYqeEGNppXOdsUYLasRp6dzKIe8wGmIeSOsqO5hM0aue4o08RU9aj7Er+18618lntKBGnJbOrRzyDqMh5oG0rrKDyRS96ineyFP0hBhLK53rjDVaUCNOS+dWDnmH0RDzQFpX2VF7MqVQapZe1Dwfd/wolKtKy67sf+nxsfVRKHcvV6vVjnDPi0IZpQCI6yo7SPj74/qAPL0/rg9KINfrY7zHQG7dH9cHQI+6urMxmd4f1wfk6f1xfcZx5bUm1+tjvMdAbt0f1wdAj7q6szGZ3h/XB+Tp/XF9UAK5Xh/jPQZy6/64PgB61NWdjcn0/rg+IE/vj+szjiuvNbleH+M9BnLr/rg+AHrU1Z2NyfT+uD4gT++P64MSyPX6GO8xkFv3x/UB0KOu7mxMpvfH9QF5en9cn3Fcea3J9foY7zGQW/fH9QHQo67ubEym98f1AXl6f1wflECu18d4j4Hcuj+uD4AedXVnYzK9P64PyNP74/qM48prTa7Xx3iPgdy6P64PgB51dWdjMr0/rg/I0/vj+qAEcr0+xnsM5Nb9cX0A9KirOxuT6f1xfUCe3h/XZxxXXmtyvT7Gewzk1v1xfQD0qKs7G5Pp/XF9QJ7eH9cHJZDr9THeYyC37o/rA6BHXd3ZmEzvj+sD8vT+uD7juPJak+v1Md5jILfuj+sDoEdd3dmYTO+P6wPy9P64PiiBXK+P8R4DuXV/XB8APerqzsZken9cH5Cn98f1GceV15pcr4/xHgO5dX9cHwA96urOxmR6f1wfkKf3x/VBCeR6fYz3GMit++P6AOhRV3c2JtP74/qAPL0/rs84rrzW5Hp9jPcYyK374/oA6FFXdzYm0/vj+oA8vT+uD0og1+tjvMdAbt0f1wdAj7q6s9WcTG1bFEqt0oua5+OOH4VyVWnZlf0vPT62Pgrl7uVqtdoR7nlRKKMUAHFdZUfNhJd2/us//zeFUqX0NJGRp5SeSk+5WVrpXCefKS2UGveE0rmVQ95RRiu1cgtoUVfZwWRK6bX0NJGRp5SeSuu5eWX/S+c6+UxpodS4J5TOrRzyjjJaqZVbQIu6yg4mU0qvpaeJjDyl9FR6ys3SSuc6+UxpodS4J5TOrRzyjjJaqZVbQIu6yg4mU0qvpaeJjDyl9FRaz80r+18618lnSgulxj2hdG7lkHeU0Uqt3AJa1FV2MJmeKf/x/pc/6HGT8k9//Y/3f/3b397/9Ie/vf/d2+eP73/5t+ln+c7sO+/v1ZfbR7f157/H9/vy2rTlX99//vrL+x/2sxzzp391vnf6Nu+/1Dm31XiRc+mFvTY1SDux8WynkJ93L3IeiLPXuRSpK3YN+irkfOtFzvNqdjxrkHZi5zleITdHKTIeAOK6yg57A6xB2ondcFor//7XP76/vMllKluT4fydTDzuRGRKcp/YZLhMdKov7sQn5e9/UWMdTqDLJOdOhnoiXCbf6XN4bo2WniYy8nR/IT/vX1rPzSv7L3WXrF/qil2Dngo53365Mqes0rmVM0Le7Snk5jilVm4BLeoqO5hMj5fPJ8Ng8pKS3Cc/GUr5x5/ciU4+T+3+dZoQbR9V3X90jlv6Fj2XTkpPExl5ur+Qn/cvPeVmaaVzvfV83lPI+fZLjXtC6dzKGSHv9hRyc5xSK7eAFnWVHUymZ4pMVjJuzqS0NRmacV791sOW5D7bk6FMaPNvQuZ+OBOq3Sa/YVET39I3mUiX36L0VWrFdQ06LsjTfYX8vHupFctXubL/OsbK1S91xa5BX4Wcb72UjPkUfS2ZR+sWcnOUUiu3gBZ1lR365stkeqqYCUxNJvMkZL8PJsP5Oz2Rur/5mOuK7nNsMnR/nic6p275/s9/T02GdpI3/Vbb2i09TWTk6YmicoX8vGPpKTdLK53r3eTznqLyj5xvsdS4J5TOrZyh8m5PUXlCbvZcauUW0KKusoPJ9MNiJxpvMpOSmgz1hLT609fkPluTofu9/tleU1VWfbP7mL7Nv11x6+tjMqwV1zXY61mDtBMbzyaLjf0gv7w4T+ae3Te3j5t/Zj/yc7PUiuWrXNl/GxulSF2xa9BtsfkU5KyXO8l8tvvm9nFz2uxHzn9cSsZ8ir0GNUg7sfMcuti4D3LLi/Fk3tl9c/u4uWf2IzerlVq5BbSoq+ywN80apJ3YDafpIpOJmmjktxDuJOV8jkxGy28vTEnuo39OTYZq0rTHeXVIMRPb34Ptqg474QX122M6mAx7msjI05OF/Lxl6Sk3Syud613l855CzjdZatwTSudWznB5t6eQm12XWrkFtKir7GAyPV7UBGTGzfv31jIxztvDCWzZvvotSnaf2GTo7OdMcv6f1OqiJ8s/BpOkbP+LN+HJsbrOfiZCOZ9e2Otdg7QTG89WCvl5/yLn0rIr+29jpxSpK3YNeirkfPtFzvVq9hrVIO3EznO0Qm6OU2RMAMR1lR32plqDtBO74VAoV5SeJjLylNJT6Sk3Syud6+QzpYVS455QOrdyyDvKaKVWbgEt6io7mEwpvZaeJjLylNJTaT03r+x/6VwnnyktlBr3hNK5lUPeUUYrtXILaFFX2cFkSum19DSRkaeUnkpPuVla6VwnnyktlBr3hNK5lUPeUUYrtXILaFFX2cFkSum19DSRkaeUnkrruXll/0vnOvlMaaHUuCeUzq0c8o4yWqmVW0CLusoOJlNKr6WniYw8pfRUesrN0krnOvlMaaHUuCeUzq0c8o4yWqmVW0CLusoOJlNKr6WniYw8pfRUWs/NK/tfOtfJZ0oLpcY9oXRu5ZB3lNFKrdwCWtRVdtSeTIFaeoo38hQ9IcbSSuc6Y40W1IjT0rmVQ95hNMQ8kNZVdjCZolc9xRt5ip60HmNX9r90rpPPaEGNOC2dWznkHUZDzANpXWUHkyl61VO8kafoCTGWVjrXGWu0oEacls6tHPIOoyHmgbSusoPJFL3qKd7IU/Sk9Ri7sv+lc518RgtqxGnp3Moh7zAaYh5I6yo7upxMf17vhzkvVZ7f5osr/bxfj8f79WM+xnw/31+P17RnQeG57qz/+/n1fmQ7u03qqDK0J1WLtwrs9a1hs50S+ZXMBcmjJa5KxOnViuRBhXvWncZyM8YGZmOglNN1/co8Kpx7QI05055XsH3OldT2UHK8vt/PefvGM4JxxT0ldT7e+XtjbZ5pvoP95/J8f+8dmx3k+KvZftZwup1kHNXg5J7qx3SN9Re/Q/LfjEM2ti4fM/+5xLXk6sVjd+Yc1fht9KNgX6VfAOK6yg57I6qh2o1FbobzQ5C+oX/yULOPedDKNXPZg/By4/95PbYni0mJF8kiD7gX6mkiu1WelsivnblwpwWPlGIvehffs+40lq3n5pX9L53rp+v6lXk0cNWcGTsvb7ss+Jj5PLU9lKp3Ood53A7c98reU1Lnkzp/Id/55yrPF14M7B2bHWrcE0rnVk7TeSekH6UXQA5x42kah6cbm4FfHLNorl4xdledY8G+1sotoEVdZUcTk+lR3k1WPi4PPHpxRZ/z/BAnD3TP5zRRme3OTCATw7z/VOxXXj1qo/ugJZPecsx8g1cPjlNfvOPUF+n9E/2aRW78y2SWqXfebo9N7Bvyjt3Rzlb/LyRt9sKObQ2b7RzNLxUfU26Y32CrMEjkgs43k0exOFXHLW3L/qrtTKzFc/jDmPX6VqDOgves5P5221bOB+fmX8fE/gfzXPZDnB3fUk7XVSwmE3EzieXm6h5wxZwZOy9vu9Rv8iS1PZQZr5k6n2Ufj3znnJPueqnzTJ2P089g+7ThxOJRYmx2kPO4mh3HGk63k4kj+dmeg5t3z9d0jNo+jf8cR8u1iB6nrpc/L3u5J/2Y69BxqMNML1zo+mx8JOI0tX1P/Nr+zVXJMbH9JifGLNa+2tdpw9bjjYuYx1gXOWTf2H0wHslzzNSptvn9WO2X7OtxUi+AuK6yw95Iaqh2Y/FusnqiUzfDYPKZb77qJuvePPUE4E8ky3a1/6r+9YOWJseZur12nH55Uvs77bu8G7+2TCoup175NE10630sf99ZMHlJHR/3/0I9TWS3ytOj+WWu/cP9zWEyF/w8WsWpHBfE4Kkc9hyM2SvyoNA9K7n/ZHfOSx1BX9bHHTy/QOu5eWX/S+f66bpKxaRniZt0bjr3AK9Opw+eg7GYOi/ZbsZ+qWOS2h5K1euI3ysm3rGp/U6cZ+p8Ns/fFr9e936ipNo4Qeq4mj2vGk63k7o+ubwz+7s55c2NseNM3HjzsmrPXHN1beWa+rG8ioEVJ049B+NXqP1sfGXi7MyYRdt3++6MRTgu0VzdHjvfwfFInaPH7f9k7kfI2W9XX/c5HfPAALrKDntDrqHajUXdDJ3JxtwN5SY/b3O/kxu3d1PWN253InZv6POkbOj9XsvEYbfN7ZjtXju6Hlv99v7OxOSKTA7zBGl+XtU7Cc9BpPa13HpFkf5fSPrSCzu2NWy2czS/VO4EDyXJXPDjZBWnkePU94lYU3E5N+w/lJ2N2UvyoNA9K7n/ZDWWk+3+TuSzqeP0+QXkeMTZ8S3ldF2FYlJ9isSN2mbqnA52ctM5NojFK3NNbZ+Pde5Zue2qDtNGql4rOBeXnMsl95Rsv4N+evuLdb1hP5NtnCB9uZo95xpOt5O4Ptm8M/u418fe75PHRa+Xc81VP57vpxzv7ST7hNt020sbS9ycjl/VP+fZ1jnPlTNjlmh/zj0vtpd9whyY93frSY6dPn7pz4HxSJyjSI297YfdM7pfpq9HSb0A4rrKDnsjqaHajcW76S/sZLqSvHHLz/ZGuxwb1qNuyO7ikVefTICx7boeda/etX9uQnEXj3Sfs/XKp3AsMvtayUnzk/5fqFq8VWBjsIbNdo7mVyyevNjQx+rnFj9O8nHqfJ+MNZ0Pdvzmuj6I2UvyoNA9K7n/JD+Wqf5O5LOc4K79E+cXqBXLV7my/zZWSzldV6l5NBU3ap9IbiaP1W1flWvhdj2vT42ltodS9Sruea/VuKekzme22r6uN+xnso0TSsZ8io21Gk63k7g+2bwzY+5eH7t/8rhoTDrXXPoxnYP+q3o7PzukXfW9+XnucypOP8xT5zxXzoxZqn2pa2pHxnJpbtknzIE5V916UmNXcjysVJ1C9cO8H6T2S/X1hFq5BbSoq+xoYjI9KneT9RZajNSNe9fN2t5wlxuvN7moNmMThHw8sn9mQgl/s2COSdarPvqTam7f2RX9v1BPE9mt8vRofrkPKlYilsI4CePUb0PqNd+nYi3R149iNtH3j+osdc9K7T/ZnfOruvVxJfO8p9wsrXSun66rUEwm4yZVvxtDXp3y8cJc87YvcZ/cHkqej+539Bjr8vNMnY9jtX1dr9cfsXdsdqhxTyidWzlV805Pnt71mWMuddy0ZTUvu9dc+jEfJ/uu67BtpOL0o/gN++ec58qZMUu2rz8/Hu5xzj7esfIx8uySGLty+bxI1imcfiT323Gd96qVW0CLusqOJibTo1ITyURuoPac50kleeOWG6ndV8pyU5YJY97uTNx6H+e46TtvEo9OOnv2z0wo9lgp3nkn6hXOcdk+BGT73NbWsXv6fyHpTy/seNew2c7R/FLxEVz7ZC4EcbKKU72vrl8e8LZizYlNc8xq+4mYLZ4Hxe5Zif3F3pyXuu1285322Zi5pI6WXdl/O+6lnK6rWEwm4sbdrord3znWq1M+XphrTn7YerPbQ6l6w3yaij5/n5yDu0+R83Tq884ndf7e9nW9ct29vu8dmx3k+KvZftZwup3U9Zkk886Mu3t9ljxLHKdiK4wb55qra+vMHybmvt265uu9I8fd7Xvid+L12xzvPofPzoxZpn11zHxuwt9H92Mpetftsfv5ZDyS55ga+4nXj8R+yb4eJ3UDiOsqO9SNpFLCt3djmW62zk10PaHgznqayMjTs8jhwz54eNyrrxgrq3Su33esyU0sasRp6dzKuW/eYYssrHRxK6owl7uIeSCtq+xgMs2QvyB66PHRxVmdx+01F28ZNgZr6GncyOETWDzadGX/bayWctuxJjfhqBGnNtZqqNUOSpruSRIjza8cyV8vSaxH/oLpQsQ8kNZVdjCZolc9xRt5ip4QY2mlc52xRgtqxGnp3Moh7zAaYh5I6yo7mEzRq57ijTxFT1qPsSv7XzrXyWe0oEacls6tHPIOoyHmgbSusoPJFL3qKd7IU/SEGEsrneuMNVpQI05L51YOeYfREPNAWlfZwWSKXvUUb+QpetJ6jF3Z/9K5Tj6jBTXitHRu5ZB3GA0xD6R1lR1MpuhVT/FGnqInxFha6VxnrNGCGnFaOrdyyDuMhpgH0rrKDiZT9KqneCNP0ZPWY+zK/pfOdfIZLagRp6VzK4e8w2iIeSCtq+xgMkWveoo38hQ9IcbSSuc6Y40W1IjT0rmVQ95hNMQ8kNZVdjCZolc9xRt5ip60HmNX9r90rpPPaEGNOC2dWznkHUZDzANpXWUHk+kOP6/3w4yTKo/X+8d8laSOeb6/zcff8P38ej9emz1N+Hm/Hl/v52+ewIeajbcIG3s13GLcwpyzgRhsn+M7tT2UqnfK1Oe8/fHekzaSXx/nx87z8c7fu/9Ink79/Q72n8t0D9o7NhVJPxBnr1MpjDVaUCNOS+dWDnl3Qjjv7XnWLuH7Wa+tjhHzQFpX2WFv0jU0e2NRE9qyEPTzekznsrEwFBzzGz5bPGpfTxPZcHnqLZLohUwVy952WfAxCz2p7aFUvdPD45wrOx8kiy0ebZ5P6vyFWTxaNky7PPy83zs2FbWem1f2v3Sutz7WGEONOC2dWznk3QkyVx191i6BxaMiiHkgravsYDLdIZjQhPviqCc4PY7+X0jYY/RfNaivgklqXuCZtj9fcozUMx0n+9mfzb5eO7YO2e85HWu32/bn46X4/bDblxdM/XKqt+sXS+nX3r/AuCs5n17Ya1bDLcYtWCSZF0RWCyEmtlPbQ6l6XUGOery8sveARF6lctO163ycfgbbpw0nFo8SY1ORjAfibByVwlijBTXitHRu5ZB3J8hcFcxPy7N2Yp6dRJ+N1f7T3Gj+Kndzrp7m0fmvnmJzNTbJ2AGI6yo77E20hmZvLJEJbX5B81409SKMmnfmY5xtIngxdReP7HY1EZoDvO+dCc1rf+6bmSxN5fOxUctL5OplU1m/lLamp4lsuDz1FjzCvDIPeG5OpraHUvU6lofVgHdsar8lr3K5Ocudz+b527Jj8WjeNzM2FUlfWnZl/+11LaX1scYYasRp6dzKIe9OUHOVP0fFn0+DedaZiJf99Zz7eC5zti81V8efC7CNmAfSusoOJtMdMhNauEAj29VEZo55Tt97v8WQSSp4AVXHOxOgO1na7+X/22s1F9nfq89f8An7JlT/5jrsvnqy9PoZ1NUiOcde2GtWwy3GLVwksbEp2+d4l4c/Z1EltV3VYWI5Va8V5KfLzUsh+bV0K5JXmdycbfY76Ke3v1jXG/Yz2cYvknNCnL3mpTDWaEGNOC2dWznk3Qlq3ksvHsXm2eSzcWKu256rdZ2/PUe2SMYUQFxX2WFvojU0e2NZTWh6sUUml3CBRk1M8oV5+Xt+L/sqkUlKHS/bzU7uZGm/D9uZZV5QV8d4+8rEGrzMyveqz/Ih8bLbkJ4msuHydLVIYgTbvXyLbQ+l6lUiOeFw81JIfqkmUnmVyc3ZzvOZrbav6w37mWzjF7Wem1f2v3Sutz7WGEONOC2dWznk3QkyVyWetVPzbPLZOPWMG6nD367r/OUpsknEPJDWVXYwme4QTGjq5ctONKsXRDPpeMfIJGV+lv297fsWj/zjHKv2l8kynFS9l0pVXzCxTpZjEi+7DelpIhsuT1eLJIa3XeebitfU9lCq3kn6IdTwck3vLymbzKtMbs52nY9jtX1dr9cfsXdsKuopN0srneuMNVpQI05L51YOeXeCzFWJZ+3sPBt7NlbP15m5MTlXy0cWj84g5oG0rrKDyXQHNaHpcVIleKmTiWb+zs44wSToTk7L/tNEZl/k5Pvc4tFETaS2HVu3N+kFL5JOv3XVerHK9tPW7dU7z5iJl92GyPn0wl6fGm4xbt6ChyPMRS/fIttDqXrVg6Rz/FTmh0yHl+tTyeVVNjet3Pmkzt/bvnPxKNbGL5J+tOzK/tvrVErrY40x1IjT0rmVQ96dEM5V3lyXmGcn0WdjtX845+6Zq+Uji0dnyLgCiOsqO+wNtwZuLKipp3gjT9ETYiytdK4z1mhBjTgtnVs55B1GQ8wDaV1lB5MpetVTvJGn6EnrMXZl/0vnOvmMFtSI09K5lUPeYTTEPJDWVXYwmaJXPcUbeYqeEGNppXOdsUYLasRp6dzKIe8wGmIeSOsqO5hM0aue4o08RU9aj7Er+18618lntKBGnJbOrRzyDqMh5oG0rrKDyRS96ineyFP0hBhLK53rjDVaUCNOS+dWDnmH0RDzQFpX2VF7MqVQapZe1Dwfd/wolKtKy67sf+nxsfVRKHcvV6vVjnDPi0IZpQCI6yo7SHjg/shTYAzkOnANcgsA8BtYPAJQFXkK3MeVuUiuA9cgtwAAv6GrmYfJFLg/8hQYA7kOXIPcAgD8BhaPAFRFngL3cWUukuvANcgtAMBv6GrmYTIF7o88BcZArgPXILcAAL+BxSMAVZGnwH1cmYvkOnANcgsA8Bu6mnmYTIH7I0+BMZDrwDXILQDAb2DxCEBV5ClwH1fmIrkOXIPcAgD8hq5mHiZT4P7IU2AM5DpwDXILAPAbWDwCUBV5CtzHlblIrgPXILcAAL+hq5mHyRS4P/IUGAO5DlyD3AIA/AYWjwBURZ4C93FlLpLrwDXILQDAb+hq5mEyBe6PPAXGQK4D1yC3AAC/gcUjAFWRp8B9XJmL5DpwDXILAPAbupp5mEyB+yNPgTGQ68A1yC0AwG9g8QhAVeQpcB9X5iK5DlyD3AIA/IauZh4mU+D+yFNgDOQ6cA1yCwDwG1g8AlAVeQrcx5W5SK4D1yC3AAC/oauZh8kUuD/yFBgDuQ5cg9wCAPwGFo9Osm1RKLVKL2qejzt+FMpVpWVX9r/0+Nj6KJS7l6vVake450WhjFIAxHWVHTUTXtr5r//83xRKldLTREaeUnoqPeVmaaVznXymtFBq3BNK51YOeUcZrdTKLaBFXWUHkyml19LTREaeUnoqrefmlf0vnevkM6WFUuOeUDq3csg7ymilVm4BLeoqO5hMKb2WniYy8pTSU+kpN0srnevkM6WFUuOeUDq3csg7ymilVm4BLeoqO5hMKb2WniYy8pTSU2k9N6/sf+lcJ58pLZQa94TSuZVD3lFGK7VyC2hRV9nBZHqm/Mf7X/6gx03KP/31P97/9W9/e//TH/72/ndvnz++/+Xfpp/lO7PvvL9XX24f3daf/x7f78tr05Z/ff/56y/vf9jPcsyf/tX53unbvP9S59xW40XOpRf22tQg7cTGs51Cft69yHkgzl7nUqSu2DXoq5DzrRc5z6vZ8axB2omd53iF3BylyHgAiOsqO+wNsAZpJ3bDaa38+1//+P7yJpepbE2G83cy8bgTkSnJfWKT4TLRqb64E5+Uv/9FjXU4gS6TnDsZ6olwmXynz+G5NVp6msjI0/2F/Lx/aT03r+y/1F2yfqkrdg16KuR8++XKnLJK51bOCHm3p5Cb45RauQW0qKvsYDI9Xj6fDIPJS0pyn/xkKOUff3InOvk8tfvXaUK0fVR1/9E5bulb9Fw6KT1NZOTp/kJ+3r/0lJullc711vN5TyHn2y817gmlcytnhLzbU8jNcUqt3AJa1FV2MJmeKTJZybg5k9LWZGjGefVbD1uS+2xPhjKhzb8JmfvhTKh2m/yGRU18S99kIl1+i9JXqRXXNei4IE/3FfLz7qVWLF/lyv7rGCtXv9QVuwZ9FXK+9VIy5lP0tWQerVvIzVFKrdwCWtRVduibL5PpqWImMDWZzJOQ/T6YDOfv9ETq/uZjriu6z7HJ0P15nuicuuX7P/89NRnaSd70W21rt/Q0kZGnJ4rKFfLzjqWn3CytdK53k897iso/cr7FUuOeUDq3cobKuz1F5Qm52XOplVtAi7rKDibTD4udaLzJTEpqMtQT0upPX5P7bE2G7vf6Z3tNVVn1ze5j+jb/dsWtr4/JsFZc12CvZw3STmw8myw29oP88uI8mXt239w+bv6Z/cjPzVIrlq9yZf9tbJQidcWuQbfF5lOQs17uJPPZ7pvbx81psx85/3EpGfMp9hrUIO3EznPoYuM+yC0vxpN5Z/fN7ePmntmP3KxWauUW0KKussPeNGuQdmI3nKaLTCZqopHfQriTlPM5Mhktv70wJbmP/jk1GapJ0x7n1SHFTGx/D7arOuyEF9Rvj+lgMuxpIiNPTxby85alp9wsrXSud5XPewo532SpcU8onVs5w+XdnkJudl1q5RbQoq6yg8n0eFETkBk3799by8Q4bw8nsGX76rco2X1ik6GznzPJ+X9Sq4ueLP8YTJKy/S/ehCfH6jr7mQjlfHphr3cN0k5sPFsp5Of9i5xLy67sv42dUqSu2DXoqZDz7Rc516vZa1SDtBM7z9EKuTlOkTEBENdVdtibag3STuyGQ6FcUXqayMhTSk+lp9wsrXSuk8+UFkqNe0Lp3Moh7yijlVq5BbSoq+xgMqX0WnqayMhTSk+l9dy8sv+lc518prRQatwTSudWDnlHGa3Uyi2gRV1lB5MppdfS00RGnlJ6Kj3lZmmlc518prRQatwTSudWDnlHGa3Uyi2gRV1lB5MppdfS00RGnlJ6Kq3n5pX9L53r5DOlhVLjnlA6t3LIO8popVZuAS3qKjuYTCm9lp4mMvKU0lPpKTdLK53r5DOlhVLjnlA6t3LIO8popVZuAS3qKjuYTCm9lp4mMvKU0lNpPTev7H/pXCefKS2UGveE0rmVQ95RRiu1cgtoUVfZwWRK6bX0NJGRp5SeSk+5WVrpXCefKS2UGveE0rmVQ95RRiu1cgtoUVfZwWRK6bX0NJGRp5SeSuu5eWX/S+c6+UxpodS4J5TOrRzyjjJaqZVbQIu6yo7akylQS0/xRp6iJ8RYWulcZ6zRghpxWjq3csg7jIaYB9K6yg4mU/Sqp3gjT9GT1mPsyv6XznXyGS2oEaelcyuHvMNoiHkgravsYDJFr3qKN/IUPSHG0krnOmONFtSI09K5lUPeYTTEPJDWVXYwmaJXPcUbeYqetB5jV/a/dK6Tz2hBjTgtnVs55B1GQ8wDaV1lB5MpetVTvDWbpz+v98P0XZXnt/liAN/P99fj9f6ZP369Hy/7KSLYv7TN9iuqFcstsrlSyiVjXTqvj8a+av/5/o27ieTRx7exQ+P38349Hu+bpO5lLonTgB3vGj5qJ4yPqRy/d5eOG6lvO/br50dFF8/RH9kcs+/3071nhvsXOC+pB0BcV9lhbxw1cGNBTT3FW7N5Kg8o80OJfvi8ywLG5dwHzT0PnSweNePK/pfO9Uv6WjqvR1w82j1+pRcB7qnGPaF0buV81I4XH2f9TtzUz4+K7r54lBsz6fsUk/O1Ce6hP6/H9P1n99RauQW0qKvsaGYyBQ7qKd6azdPgIVgeUOwDjX5Y0eflLbI8n++n3T4/6chvzcy2qcwPRcn95atlfyn6q6P1HG93Jvuo85I6/If45LnbbUGd0f1tvd/yELjj/Obt9gHxg3P7kNSLOHs9SrlkrE/k9fOl41TF3xyPJhZTsS/bnXbmBVDvxUfHsToktf+nsR70bzPXtuo8NH7uIsCe/LbHaV59c1+kngP3jovvB0Lqvpo9txo+aieID0tdS2f88/HtxI1cwyN5oY6123Udej61cRj41fyYZOrU/V6K/SpVz6n71IlzjffrmjET38/p2r2kHnOsdw/VpE+xqveSvgGI6yo77E2qBm4sqKmneGs2T70HGv1Aqh5O1MPQ8pQyP+iohzP3pTD2sCrb3Ye59f7qwXCu/3w9voP7yz7Tub/mB3VDtqfO3Xn4mx/wU/ubdh/P5Rif01/5FPbDc/DcPtR6bl7Z/9K5fklfz+S12d/NTS/GU7Ef2z6/+Dht6x3i+3sOxnrwUiZ1OqdofFLn1vh9T9/vuB8F9a3Hzx0r3a9d944951NAjXtC6dzK+agdFd+6r1KWGHaui/ezy26X622ulRcH8jGfF8sc43Lqc/16fkx7Jep07zVeW7l6TLvusfN4Od972z3b55rsl2e7Hk9qzNRH+51Tp4oxW6c2j8NJtXILaFFX2WEnpxq4sZygbvByjWKTC3J6irdm83SOX1Och7F5m/ud93DmP6yqB655f7M9sX/u4exIPerTwf1n6oFPHi7l5W95SNt37hP5PG1P7q/Oa/2gHu3vJPage/rcPiTtIc5ej1IuGeszeW32kZizcTjHZCL2w+3z/ubF5yntuQmQ2n9yNtbd/gqp0zZ5On8Ojd/Lq2O7zYl8nup0z1+oY1VbB+4de86nAGnzavbcavioHYkP93o65vibrsvq2prz09fOuVZBfGzmhbrO02cvQO6aH9N3iTpV+/M5LM8B2XrM/u55zeMVjGO4/1Jf/lxT/RKlx0xEz8XcQ5e9/P3OkHYBxHWVHfZGUwM3FtTUU7w1m6eJh+D5ASaUekjytjsPW8mHKvl5eZCa2zpaz+F2He4+8rN5mNt37hNzTHL/4KFT8erwv1/Vk9rX2544tw/ViuWrXNl/G7OlXNLXM3ntvGTZfeb9vZib2P2D7fP+5kXp+a3zfH5PSu3vbT8W6+ELldSp2vugzmPj59Sxq82JfI7cO9SLqe68d29QPjmfAmrcE0rnVs5H7STiQ1HfPd9P9zpEr10qbuTjRl5Y8r3KM/lw1/yYJOuUn9fPAdl69Ml65xUfr4nd39u+51zj/TpejyMZM35bqsh+sr+3eKT3M6d/itQNIK6r7LA3kxq4saCmnuKt2TxNPdDIw1DwWy8l8ZDkPaCqYzceqhLtHq3ncLuu1D6qnj3n7jyEx/Z3Hy6NZH/VR/+B+aNz+xBzQVrpXL9krM/k9e6XslTsS7yb/b0XH9lufk7sXy6P5aPu20d1Hhq/ZTz2t7kxrmps/H795v1A1LgnlM6tnI/aScWHoq+jzScRv3bO9VbbDuSFY5k3Etfeiw+9f938mKTqrHSfOnyuiX5dMmar7eZ49d87W8ZA2l7aOKdWbgEt6io7mplMgYN6irdm8zT1QDNRDyvmvOYHueRDkn7gVftOT2vph7nI/qoUqGfX/g5vn4nzOXXuj6meebt5iBXR/VXfwnYT/RVyLcx3uuoPzu1D0mbLruy/uh4F67+kr2fy2sSzfG9j0o25VOzLPnr7dJxdFFGxvLz4uDEb3f/DWF/q1OXj/Dk4fnqb1JNpcz5Gf2d5fZ+3Sz077x07x+hT0u7V7DjU8FE7zr3aFh3HxnRNnEs8iV+7JW7kEFvXdl54MTg3dN/8SNfp9EGVpa1kPeZ85Xs75m7f4/epo+ea6lf5MZvrcKhzn+rwYiwx3kdIPQDiusoOe+OogRsLauop3sjTo6aHMOdhSD0szQ96+G3MBWmlc52xHoD3ctmmGnFaOrdyyLs7uOtzQJ/PJ8Q8kNZVdjCZolc9xRt5epD8hv6hx0wX568U8Otaj7Er+29jtpTWxxo7sHi0S+ncyiHvbuCuzwGdPp8Q80BaV9lhb141cGNBTT3FG3mKnhBjaaVznbFGC2rEaencyiHvMBpiHkjrKjuYTNGrnuKNPEVPWo+xK/tfOtfJZ7SgRpyWzq0c8g6jIeaBtK6yg8kUveop3shT9IQYSyud64w1WlAjTkvnVg55h9EQ80BaV9nBZIpe9RRv5Cl60nqMXdn/0rlOPqMFNeK0dG7lkHcYDTEPpHWVHUym6FVP8UaeoifEWFrpXGes0YIacVo6t3LIO4yGmAfSusoOO5nWKkAtPcVbmEdXF+BKrcfYlf1387BUAe6uRpyGeXF1AUZCzANpXWVHONldXYBaeoq3MI+uLsCViLG0MBdLFODuasRpmBdXF2AkxDyQRnacxI0FNRFv5zBuuFrrMdZS/8lntKC3OCXvMBpiHkgjO07ixoKaiLdzGDdcjRirh7FGC3qLU/IOoyHmgTSy4yRuLKiJeDuHccPVWo+xlvpPPqMFvcUpeYfREPNAGtlxEjcW1ES8ncO44WrEWD2MNVrQW5ySdxgNMQ+kkR0ncWNBTcTbOYwbrtZ6jLXUf/IZLegtTsk7jIaYB9LIjpO4saAm4u0cxg1XI8bqYazRgt7ilLzDaIh5II3sOIkbC2oi3s5h3HC11mOspf6Tz2hBb3FK3mE0xDyQRnacxI0FNRFv5zBuuBoxVg9jjRb0FqfkHUZDzANpZMdJ3FhQE/F2DuOGq7UeYy31n3xGC3qLU/IOoyHmgTSy4yRuLKiJeDuHccPViLF6GGu0oLc4Je8wGmIeSCM7TuLGgpqIt3MYN1yt9Rhrqf/kM1rQW5ySdxgNMQ+kkR0ncWNBTcTbOYwbrkaM1cNYowW9xSl5h9EQ80Aa2XESNxbURLydw7jhaq3HWEv9J5/Rgt7ilLzDaIh5II3sOIkbC2oi3s5h3HA1Yqwexhot6C1OyTuMhpgH0siOk7ixoCbi7RzGDVdrPcZa6j/5jBb0FqfkHUZDzANpZMdJ3FhQE/F2DuOGqxFj9TDWaEFvcUreYTTEPJBGdpzEjQU1EW/nMG64Wusx1lL/yWe0oLc4Je8wGmIeSCM7TuLGgpqIt3MYN1yNGKuHsUYLeotT8g6jIeaBNLLjJG4sqIl4O4dxw9Vaj7GW+k8+owW9xSl5h9EQ80Aa2XGS3FgolJoFx8XGkUIpXVBHbOwplDuWnsTOj0LpvQCIIzsAABgUD8kAAADYg6dGAAAAAAAAJLF4BADAoPjLIwAAAOzBUyMAAAAAAACSWDwCAGBQ/OURAAAA9uCpEQAAAAAAAEksHgEAMCj+8ggAAAB78NQIAAAAAACAJBaPAAAYFH95BAAAgD14agQAAAAAAEASi0cAAAyKvzwCAADAHjw1AgAAAAAAIInFIwAABsVfHgEAAGAPnhoBAAAAAACQxOIRAACD4i+PAAAAsAdPjQAAAAAAAEhi8QgAgEHxl0cAAADYg6dGAAAAAAAAJLF4BADAoPjLIwAAAOzBUyMAAAAAAACSWDwCAGBQ/OURAAAA9uCpEQAAAAAAAEksHgEAMCj+8ggAAAB78NQIAAAAAACAJBaPAAAYFH95BAAAgD14agQAAAAAAEASi0cAAAyKvzwCAADAHjw1AgAAAAAAIInFIwAABsVfHgEAAGAPnhoBAAAAAACQxOIRAGAo8tc2FEquAAAAwMcTEgBgKCwOIIf4AAAAWOMJCQAwFBYHkEN8AAAArPGEBAAYCosDyCE+AAAA1nhCAgAMhcUB5BAfAAAAazwhAQCGwuIAcogPAACANZ6QAABDYXEAOcQHAADAGk9IAIChsDiAHOIDAABgjSckAMBQWBxADvEBAACwxhMSAGAoLA4gh/gAAABY4wkJADAUFgeQQ3wAAACs8YQEABhKV4sD38/pfB7v14/5/PN6P6bze36bz5ba72spqx1+3q+H871bHq/pW9f3++nt47TfATknAAAA+HhCAgAMpavFgUOLR8+32mz2eaRWfMI6V/TikW3j5/WILDC1i8UjAACANZ6QAABDGX7xyP6V0Wong8Uj8xMAAAAsnpAAAEPhL4/K/OWRjKMudlGqDyweAQAArPGEBAAYyriLR86CT+qvjsTOxaO5CtVmbv+2sHgEAACwxhMSAGAo4y4e7fwLoaOLR+Zz8i+ZGsPiEQAAwBpPSACAoXS1OBAsFqn//lBskejKxSO1f2TBqlEsHgEAAKzxhAQAGEp3iwNm8caW6CLOBYtHbpu9/NWRkPMBAACAjyckAMBQWBxADvEBAACwxhMSAGAoLA4gh/gAAABY4wkJADAUFgeQQ3wAAACs8YQEABgKiwPIIT4AAADWeEICAAyFxQHkEB8AAABrPCEBAIbC4gByiA8AAIA1npAAAENhcQA5xAcAAMAaT0gAgKGwOIAc4gMAAGCNJyQAwFBYHEAO8QEAALDGExIAYCgsDiCH+AAAAFjjCQkAMBQWB5BDfAAAAKzxhAQAGAqLA8ghPgAAANZ4QgIADEUWByiUXAEAAICPJyQAAAAAAAAksXgEAAAAAACAJBaPAAAAAAAAkMTiEQAAAAAAAJJYPAIAAAAAAEASi0cAAAAAAABIYvEIAAAAAAAASSweAQAAAAAAIInFIwAAAAAAACSxeAQAAAAAAIAkFo8AAAAAAACQxOIRAAAAAAAAklg8AgAAAAAAQML7/f8DvdOMAzLEYcUAAAAASUVORK5CYII=\" style=\"height:579px; width:774px\" /></span></span></p>\r\n', 4, '', 4);
INSERT INTO `menu` (`idMenu`, `menuName`, `parentId`, `slug`, `status`, `content`, `userId`, `link`, `orderMenu`) VALUES
(37, 'Kelompok Jabatan Fungsional', 23, 'kelompok-jabatan-fungsional', '1', '&lt;p style=&quot;margin-left:0in; margin-right:0in&quot;&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;strong&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;KELOMPOK JABATAN FUNGSIONAL&lt;/span&gt;&lt;/span&gt;&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in&quot;&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;strong&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Bagian Kesatu&lt;/span&gt;&lt;/span&gt;&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in&quot;&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;strong&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Jabatan Fungsional Tertentu&lt;/span&gt;&lt;/span&gt;&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in&quot;&gt;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in&quot;&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;strong&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Pasal&amp;nbsp; &lt;/span&gt;&lt;/span&gt;&lt;/strong&gt;&lt;strong&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;39&lt;/span&gt;&lt;/span&gt;&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;ol&gt;\r\n	&lt;li&gt;&lt;span style=&quot;font-size:8pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Times New Roman&amp;quot;,serif&quot;&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Kelompok Jabatan Fungsional Tertentu, terdiri atas sejumlah tenaga dalam jenjang Jabatan Fungsional yang terbagi dalam berbagai kelompok sesuai dengan bidang keahliannya.&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/li&gt;\r\n&lt;/ol&gt;\r\n\r\n&lt;p style=&quot;margin-left:0.25in; margin-right:0in&quot;&gt;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;ol start=&quot;2&quot;&gt;\r\n	&lt;li&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Kelompok Jabatan Fungsional Tertentu sebagaimana dimaksud pada ayat (1), dipimpin oleh seorang tenaga fungsional senior yang ditunjuk dan bertanggung jawab kepada Kepala &lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Badan&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;.&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/li&gt;\r\n&lt;/ol&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in&quot;&gt;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;ol start=&quot;3&quot;&gt;\r\n	&lt;li&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Jumlah Jabatan Fungsional Tertentu sebagaimana dimaksud pada ayat (2), ditentukan berdasarkan kebutuhan dan beban kerja.&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/li&gt;\r\n&lt;/ol&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in&quot;&gt;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;ol start=&quot;4&quot;&gt;\r\n	&lt;li&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Jenis Jabatan Fungsional Tertentu sebagaimana dimaksud pada ayat (3), ditetapkan sesuai dengan ketentuan peraturan perundang&amp;ndash;undangan.&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/li&gt;\r\n&lt;/ol&gt;\r\n\r\n&lt;p style=&quot;margin-left:0.5in; margin-right:0in&quot;&gt;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in&quot;&gt;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in&quot;&gt;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in&quot;&gt;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in&quot;&gt;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in&quot;&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;strong&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Bagian Kedua&lt;/span&gt;&lt;/span&gt;&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in&quot;&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;strong&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Jabatan Fungsional Umum&lt;/span&gt;&lt;/span&gt;&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in&quot;&gt;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in&quot;&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;strong&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Pasal&amp;nbsp; &lt;/span&gt;&lt;/span&gt;&lt;/strong&gt;&lt;strong&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;40&lt;/span&gt;&lt;/span&gt;&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;ol&gt;\r\n	&lt;li&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Penamaan jabatan fungsional umum dirumuskan berdasarkan hasil analisis jabatan.&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/li&gt;\r\n	&lt;li&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Nama-nama jabatan fungsional umum di lingkungan &lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Badan Pendapatan Daerah &lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;ditetapkan sesuai dengan ketentuan peraturan perundang&amp;ndash;undangan&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;.&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/li&gt;\r\n	&lt;li&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Nama-nama jabatan fungsional umum sebagaimana dimaksud pada ayat (&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;2&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;) dapat disesuaikan dengan kebutuhan organisasi perangkat daerah.&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/li&gt;\r\n	&lt;li&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Penetapan nama-nama jabatan fungsional umum di lingkungan &lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Badan Pendapatan Daerah&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt; dengan &lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Keputusan Bupati&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;.&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/li&gt;\r\n	&lt;li&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Dalamhalterjadi&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt; perubahan nama-nama jabatan fungsional umum sebagaimana dimaksud pada ayat (4) ditetapkan dengan&lt;/span&gt;&lt;/span&gt; &lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Keputusan &lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Bupati&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;.&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/li&gt;\r\n	&lt;li&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Setiap ASN yang belum menduduki Jabatan Struktural dan Jabatan Fungsional Tertentu, diangkat dalam jabatan fungsional umum.&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/li&gt;\r\n	&lt;li&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Pengangkatan ASN dalam jabatan fungsional umum sebagaimana dimaksud pada ayat (6) di lingkungan &lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Badan Pendapatan Daerah&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt; dilakukan oleh Sekretaris Daerah dan ditetapkan dengan Keputusan Bupati dengan berpedoman pada ketentuan peraturan perundang-undangan.&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/li&gt;\r\n	&lt;li&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Pemindahan ASN dalam jabatan fungsional umum di lingkungan &lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Badan Pendapatan Daerah&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt; dilakukan oleh Sekretaris Daerah dan ditetapkan dengan Keputusan Bupati.&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/li&gt;\r\n	&lt;li&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Uraian tugas Jabatan Fungsional Umum (JFU) pada &lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Badan Pendapatan Daerah ditetapkan oleh Kepala Badan&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;.&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/li&gt;\r\n&lt;/ol&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in&quot;&gt;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in&quot;&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;strong&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;BAB VII&lt;/span&gt;&lt;/span&gt;&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in&quot;&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;strong&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;UNIT PELAKSANA TEKNIS BADAN&lt;/span&gt;&lt;/span&gt;&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in&quot;&gt;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in&quot;&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;strong&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Pasal 41&lt;/span&gt;&lt;/span&gt;&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;ol&gt;\r\n	&lt;li&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;UPTB mempunyai kedudukan sebagai unsur pelaksana teknis operasional Badan Pendapatan Daerah.&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/li&gt;\r\n	&lt;li&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;UPTB dipimpin oleh seorang Kepala yang berada di bawah dan bertanggung jawab kepada Bupati melalui Kepala Badan.&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/li&gt;\r\n&lt;/ol&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in&quot;&gt;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in&quot;&gt;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in&quot;&gt;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in&quot;&gt;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in&quot;&gt;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in&quot;&gt;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in&quot;&gt;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in&quot;&gt;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in&quot;&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;strong&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;BAB VIII&lt;/span&gt;&lt;/span&gt;&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in&quot;&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;strong&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;TATA KERJA&lt;/span&gt;&lt;/span&gt;&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in&quot;&gt;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in&quot;&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;strong&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Pasal 42&lt;/span&gt;&lt;/span&gt;&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;ol&gt;\r\n	&lt;li&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Dalam melaksanakan tugasnya Kepala Badan, Sekretaris, Kepala Bagian, Kepala Unit Pelaksana Teknis Badan Pendapatan Daerah, Kepala Sub Bagian, Kepala &lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Sub Bidang &lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;dan pemegang Jabatan Fungsional wajib menerapkan prinsip koordinasi, integrasi, simplikasi dan sinkronisasi secara vertikal serta horisontal baik dalam lingkungan Badan maupun instansi lain sesuai dengan tugas pokok masing-masing.&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/li&gt;\r\n	&lt;li&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;span style=&quot;font-size:12.0pt&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Bookman Old Style&amp;quot;,serif&quot;&gt;Setiap pimpinan satuan organisasi wajib mengikuti, memenuhi petunjuk dan bertanggung jawab kepada atasannya masing-masing serta menyampaikan laporan tepat pada waktunya.&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/li&gt;\r\n&lt;/ol&gt;\r\n\r\n&lt;p style=&quot;margin-left:0in; margin-right:0in&quot;&gt;&amp;nbsp;&lt;/p&gt;\r\n', 4, '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `nilaivolumeairtanah`
--

CREATE TABLE `nilaivolumeairtanah` (
  `idNilaiVolumeAirTanah` int(11) UNSIGNED NOT NULL,
  `peruntukanAirTanahId` int(11) UNSIGNED NOT NULL,
  `volumeAirTanahId` int(11) UNSIGNED NOT NULL,
  `hargaNilaiVolumeAirTanah` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `nilaivolumeairtanah`
--

INSERT INTO `nilaivolumeairtanah` (`idNilaiVolumeAirTanah`, `peruntukanAirTanahId`, `volumeAirTanahId`, `hargaNilaiVolumeAirTanah`) VALUES
(1, 2, 1, 250),
(4, 2, 2, 275),
(5, 2, 3, 300),
(6, 2, 4, 325),
(7, 2, 7, 350),
(8, 3, 1, 400),
(9, 3, 2, 500),
(10, 3, 3, 600),
(11, 3, 4, 700),
(12, 3, 7, 800),
(13, 4, 1, 700),
(14, 4, 2, 1000),
(15, 4, 3, 1300),
(16, 4, 4, 1600),
(17, 4, 7, 1900),
(18, 5, 2, 100),
(19, 5, 1, 100),
(20, 5, 3, 100),
(21, 5, 4, 100),
(22, 5, 7, 100),
(23, 6, 1, 100),
(24, 6, 2, 100),
(25, 6, 3, 100),
(26, 6, 4, 100),
(27, 6, 7, 100);

-- --------------------------------------------------------

--
-- Table structure for table `pajakairtanah`
--

CREATE TABLE `pajakairtanah` (
  `idPajakAirTanah` int(11) UNSIGNED NOT NULL,
  `persentase` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pajakairtanah`
--

INSERT INTO `pajakairtanah` (`idPajakAirTanah`, `persentase`) VALUES
(1, 20);

-- --------------------------------------------------------

--
-- Table structure for table `pajakhiburan`
--

CREATE TABLE `pajakhiburan` (
  `idPajakHiburan` int(11) UNSIGNED NOT NULL,
  `persentase` int(11) DEFAULT NULL,
  `labelPersentaseHiburan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pajakhiburan`
--

INSERT INTO `pajakhiburan` (`idPajakHiburan`, `persentase`, `labelPersentaseHiburan`) VALUES
(1, 35, 'Pagelaran Busana, Kontes Kecantikan, Diskotik, Karaoke, Klab Malam, Permainan Ketangkasan, Panti Pijat, dan Mandi Uap / SPA'),
(4, 10, 'Tontonan Film'),
(5, 0, 'Pagelaran Kesenian, Musik dan Tari (Berkelas Lokal / Tradisional)'),
(6, 5, 'Pagelaran Kesenian, Musik dan Tari (Berkelas Nasional)'),
(7, 15, 'Pagelaran Kesenian, Musik dan Tari (Berkelas Internasional)'),
(8, 0, 'Pameran (Non Komersial)'),
(9, 10, 'Pameran (Komersial)'),
(10, 0, 'Sirkus, Akrobat, dan Sulap (Berkelas Lokal / Tradisional)'),
(11, 10, 'Sirkus, Akrobat, dan Sulap (Berkelas Nasional dan Internasional)'),
(12, 10, 'Bilyar dan Bowling'),
(13, 5, 'Pacuan Kuda (Berkelas Lokal / Tradisional)'),
(14, 15, 'Pacuan Kuda (Berkelas Nasional dan Internasional)'),
(15, 15, 'Balap Kendaraan Bermotor'),
(16, 0, 'Pertandingan Olahraga (Berkelas Lokal / Tradisional)'),
(17, 5, 'Pertandingan Olahraga (Berkelas Nasional)'),
(18, 15, 'Pertandingan Olahraga (Berkelas Internasional)');

-- --------------------------------------------------------

--
-- Table structure for table `pajakhotel`
--

CREATE TABLE `pajakhotel` (
  `idPajakHotel` int(11) UNSIGNED NOT NULL,
  `persentase` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pajakhotel`
--

INSERT INTO `pajakhotel` (`idPajakHotel`, `persentase`) VALUES
(1, 10);

-- --------------------------------------------------------

--
-- Table structure for table `pajakmblb`
--

CREATE TABLE `pajakmblb` (
  `idPajakMBLB` int(11) UNSIGNED NOT NULL,
  `persentase` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pajakmblb`
--

INSERT INTO `pajakmblb` (`idPajakMBLB`, `persentase`) VALUES
(1, 20);

-- --------------------------------------------------------

--
-- Table structure for table `pajakparkir`
--

CREATE TABLE `pajakparkir` (
  `idPajakParkir` int(11) UNSIGNED NOT NULL,
  `persentase` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pajakparkir`
--

INSERT INTO `pajakparkir` (`idPajakParkir`, `persentase`) VALUES
(1, 20);

-- --------------------------------------------------------

--
-- Table structure for table `pajakrestoran`
--

CREATE TABLE `pajakrestoran` (
  `idPajakRestoran` int(11) UNSIGNED NOT NULL,
  `persentase` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pajakrestoran`
--

INSERT INTO `pajakrestoran` (`idPajakRestoran`, `persentase`) VALUES
(1, 10);

-- --------------------------------------------------------

--
-- Table structure for table `pajaksarangburungwalet`
--

CREATE TABLE `pajaksarangburungwalet` (
  `idPajakSarangBurungWalet` int(11) UNSIGNED NOT NULL,
  `persentase` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pajaksarangburungwalet`
--

INSERT INTO `pajaksarangburungwalet` (`idPajakSarangBurungWalet`, `persentase`) VALUES
(1, 10);

-- --------------------------------------------------------

--
-- Table structure for table `pengumuman`
--

CREATE TABLE `pengumuman` (
  `idPengumuman` int(11) UNSIGNED NOT NULL,
  `judulPengumuman` varchar(200) DEFAULT NULL,
  `isiPengumuman` text,
  `tanggalPengumuman` datetime DEFAULT NULL,
  `file` varchar(100) DEFAULT NULL,
  `slug` varchar(200) DEFAULT NULL,
  `tanggalInputPengumuman` datetime DEFAULT NULL,
  `userId` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pengumuman`
--

INSERT INTO `pengumuman` (`idPengumuman`, `judulPengumuman`, `isiPengumuman`, `tanggalPengumuman`, `file`, `slug`, `tanggalInputPengumuman`, `userId`) VALUES
(4, 'PAJAK BUMI DAN BANGUNAN TAHUN 2019', '&lt;p&gt;SPTT PBB-P2 TAHUN 2019 DAPAT DI AMBIL DI KELURAHAN/ DESA/ RT SETEMPAT&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p&gt;UNTUK MENGHINDARI DENDA BAYARLAH PAJAK TEPAT PADA WAKTUNYA&lt;/p&gt;\r\n\r\n&lt;p&gt;JATUH TEMPO PEMBAYARAN PBB-P2 30 SEPTEMBER 2019&lt;/p&gt;\r\n', '2019-07-26 13:37:14', '699b66ca6c5401f6d216e32a0130174c.pdf', 'pajak-bumi-dan-bangunan-tahun-2019', '2019-07-26 13:37:14', 1);

-- --------------------------------------------------------

--
-- Table structure for table `peruntukanairtanah`
--

CREATE TABLE `peruntukanairtanah` (
  `idPeruntukanAirTanah` int(11) UNSIGNED NOT NULL,
  `labelPeruntukanAirTanah` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `peruntukanairtanah`
--

INSERT INTO `peruntukanairtanah` (`idPeruntukanAirTanah`, `labelPeruntukanAirTanah`) VALUES
(2, 'Non Niaga'),
(3, 'Niaga'),
(4, 'Industri dengan Bahan Baku Air'),
(5, 'PDAM'),
(6, 'Pertamina');

-- --------------------------------------------------------

--
-- Table structure for table `quotes`
--

CREATE TABLE `quotes` (
  `idQuotes` int(11) UNSIGNED NOT NULL,
  `descQuotes` text,
  `isActive` enum('0','1') DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `quotes`
--

INSERT INTO `quotes` (`idQuotes`, `descQuotes`, `isActive`) VALUES
(3, '&lt;p style=&quot;text-align:center&quot;&gt;&lt;span style=&quot;font-family:Georgia,serif&quot;&gt;&lt;span style=&quot;font-size:26px&quot;&gt;&amp;quot;SEGA&amp;quot;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;div style=&quot;text-align:center&quot;&gt;SEGA dalam bahasa Mendawai yaitu Ungkapan sesuatu yang Cantik, Bagus, Indah atau sesuatu yang menarik.&lt;/div&gt;\r\n\r\n&lt;div style=&quot;text-align:center&quot;&gt;SEGA&amp;nbsp;dapat pula diartikan sebagai motto pelayanan BAPENDA&amp;nbsp;yaitu Sopan, Efektif, Gratis dan Amanah untuk memberikan kepuasan pelayanan kepada masyarakat luas khususnya di Kabupaten Kotawaringin Barat.&lt;/div&gt;\r\n', '1');

-- --------------------------------------------------------

--
-- Table structure for table `satuanwaktu`
--

CREATE TABLE `satuanwaktu` (
  `idSatuanWaktu` int(11) UNSIGNED NOT NULL,
  `labelSatuanWaktu` varchar(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `satuanwaktu`
--

INSERT INTO `satuanwaktu` (`idSatuanWaktu`, `labelSatuanWaktu`) VALUES
(1, 'Bulan'),
(2, 'Hari');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `idUser` int(11) UNSIGNED NOT NULL,
  `namaUser` varchar(100) DEFAULT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `levelUserId` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`idUser`, `namaUser`, `username`, `password`, `levelUserId`) VALUES
(1, 'handri', 'handri', '$2y$10$tiTs5BvJ2cuaf9yBvp/j0e9lef.zUOQrjBBqqUAnvSLRO598LPMeO', 1),
(4, 'sayid', 'sayid', '$2y$10$952gz4Ar.JsdL75R1kr7U.YrHVrYXe7JQE.rhUAC/HBVBJPIYxir6', 1),
(9, 'deri', 'deri', '$2y$10$bn47yAh.S//OvMfv4gXXj.mv9/vraWqjXEv5.8q2d.Z5H1xJeKNuq', 1);

-- --------------------------------------------------------

--
-- Table structure for table `volumeperuntukanairtanah`
--

CREATE TABLE `volumeperuntukanairtanah` (
  `idVolumePeruntukan` int(11) UNSIGNED NOT NULL,
  `nilaiVolumePeruntukanAwal` int(11) DEFAULT NULL,
  `nilaiVolumePeruntukanAkhir` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `volumeperuntukanairtanah`
--

INSERT INTO `volumeperuntukanairtanah` (`idVolumePeruntukan`, `nilaiVolumePeruntukanAwal`, `nilaiVolumePeruntukanAkhir`) VALUES
(1, 50, 100),
(2, 101, 500),
(3, 501, 1000),
(4, 1001, 2500),
(7, 2501, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `albumfoto`
--
ALTER TABLE `albumfoto`
  ADD PRIMARY KEY (`idAlbum`),
  ADD KEY `userId` (`userId`);

--
-- Indexes for table `bannerhome`
--
ALTER TABLE `bannerhome`
  ADD PRIMARY KEY (`idBannerHome`);

--
-- Indexes for table `berita`
--
ALTER TABLE `berita`
  ADD PRIMARY KEY (`idBerita`),
  ADD KEY `userId` (`userId`);

--
-- Indexes for table `biodinas`
--
ALTER TABLE `biodinas`
  ADD PRIMARY KEY (`idDinas`);

--
-- Indexes for table `detailfilepublikasi`
--
ALTER TABLE `detailfilepublikasi`
  ADD PRIMARY KEY (`idDetailFilePublikasi`),
  ADD KEY `kategoriFilePublikasi` (`kategoriFilePublikasiId`);

--
-- Indexes for table `foto`
--
ALTER TABLE `foto`
  ADD PRIMARY KEY (`idFoto`),
  ADD KEY `albumIdAlbum` (`albumIdAlbum`);

--
-- Indexes for table `jenisreklame`
--
ALTER TABLE `jenisreklame`
  ADD PRIMARY KEY (`idJenisReklame`),
  ADD KEY `satuanWaktuId` (`satuanWaktuId`);

--
-- Indexes for table `kategorifilepublikasi`
--
ALTER TABLE `kategorifilepublikasi`
  ADD PRIMARY KEY (`idKategoriFilePublikasi`);

--
-- Indexes for table `kategorimblb`
--
ALTER TABLE `kategorimblb`
  ADD PRIMARY KEY (`idKategoriMBLB`);

--
-- Indexes for table `kategorisr`
--
ALTER TABLE `kategorisr`
  ADD PRIMARY KEY (`idKategoriSR`);

--
-- Indexes for table `kepaladaerah`
--
ALTER TABLE `kepaladaerah`
  ADD PRIMARY KEY (`idKepalaDaerah`);

--
-- Indexes for table `komponenkl`
--
ALTER TABLE `komponenkl`
  ADD PRIMARY KEY (`idKomponenKL`);

--
-- Indexes for table `komponenlr`
--
ALTER TABLE `komponenlr`
  ADD PRIMARY KEY (`idKomponenLR`);

--
-- Indexes for table `komponennjt`
--
ALTER TABLE `komponennjt`
  ADD PRIMARY KEY (`idKomponenNJT`),
  ADD KEY `komponenSRid` (`komponenSRid`),
  ADD KEY `komponenPLid` (`komponenPLid`),
  ADD KEY `komponenKLid` (`komponenKLid`);

--
-- Indexes for table `komponenpl`
--
ALTER TABLE `komponenpl`
  ADD PRIMARY KEY (`idKomponenPL`);

--
-- Indexes for table `komponensr`
--
ALTER TABLE `komponensr`
  ADD PRIMARY KEY (`idKomponenSR`),
  ADD KEY `kategoriIdKategoriSR` (`kategoriIdKategoriSR`);

--
-- Indexes for table `leveluser`
--
ALTER TABLE `leveluser`
  ADD PRIMARY KEY (`idLevelUser`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`idMenu`),
  ADD KEY `pejabatId` (`userId`);

--
-- Indexes for table `nilaivolumeairtanah`
--
ALTER TABLE `nilaivolumeairtanah`
  ADD PRIMARY KEY (`idNilaiVolumeAirTanah`),
  ADD KEY `peruntukanAirTanahId` (`peruntukanAirTanahId`),
  ADD KEY `volumeAirTanahId` (`volumeAirTanahId`);

--
-- Indexes for table `pajakairtanah`
--
ALTER TABLE `pajakairtanah`
  ADD PRIMARY KEY (`idPajakAirTanah`);

--
-- Indexes for table `pajakhiburan`
--
ALTER TABLE `pajakhiburan`
  ADD PRIMARY KEY (`idPajakHiburan`);

--
-- Indexes for table `pajakhotel`
--
ALTER TABLE `pajakhotel`
  ADD PRIMARY KEY (`idPajakHotel`);

--
-- Indexes for table `pajakmblb`
--
ALTER TABLE `pajakmblb`
  ADD PRIMARY KEY (`idPajakMBLB`);

--
-- Indexes for table `pajakparkir`
--
ALTER TABLE `pajakparkir`
  ADD PRIMARY KEY (`idPajakParkir`);

--
-- Indexes for table `pajakrestoran`
--
ALTER TABLE `pajakrestoran`
  ADD PRIMARY KEY (`idPajakRestoran`);

--
-- Indexes for table `pajaksarangburungwalet`
--
ALTER TABLE `pajaksarangburungwalet`
  ADD PRIMARY KEY (`idPajakSarangBurungWalet`);

--
-- Indexes for table `pengumuman`
--
ALTER TABLE `pengumuman`
  ADD PRIMARY KEY (`idPengumuman`),
  ADD KEY `userId` (`userId`);

--
-- Indexes for table `peruntukanairtanah`
--
ALTER TABLE `peruntukanairtanah`
  ADD PRIMARY KEY (`idPeruntukanAirTanah`);

--
-- Indexes for table `quotes`
--
ALTER TABLE `quotes`
  ADD PRIMARY KEY (`idQuotes`);

--
-- Indexes for table `satuanwaktu`
--
ALTER TABLE `satuanwaktu`
  ADD PRIMARY KEY (`idSatuanWaktu`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`idUser`),
  ADD KEY `levelUserId` (`levelUserId`);

--
-- Indexes for table `volumeperuntukanairtanah`
--
ALTER TABLE `volumeperuntukanairtanah`
  ADD PRIMARY KEY (`idVolumePeruntukan`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `albumfoto`
--
ALTER TABLE `albumfoto`
  MODIFY `idAlbum` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `bannerhome`
--
ALTER TABLE `bannerhome`
  MODIFY `idBannerHome` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `berita`
--
ALTER TABLE `berita`
  MODIFY `idBerita` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `biodinas`
--
ALTER TABLE `biodinas`
  MODIFY `idDinas` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `detailfilepublikasi`
--
ALTER TABLE `detailfilepublikasi`
  MODIFY `idDetailFilePublikasi` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT for table `foto`
--
ALTER TABLE `foto`
  MODIFY `idFoto` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `jenisreklame`
--
ALTER TABLE `jenisreklame`
  MODIFY `idJenisReklame` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `kategorifilepublikasi`
--
ALTER TABLE `kategorifilepublikasi`
  MODIFY `idKategoriFilePublikasi` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `kategorimblb`
--
ALTER TABLE `kategorimblb`
  MODIFY `idKategoriMBLB` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT for table `kategorisr`
--
ALTER TABLE `kategorisr`
  MODIFY `idKategoriSR` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `kepaladaerah`
--
ALTER TABLE `kepaladaerah`
  MODIFY `idKepalaDaerah` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `komponenkl`
--
ALTER TABLE `komponenkl`
  MODIFY `idKomponenKL` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `komponenlr`
--
ALTER TABLE `komponenlr`
  MODIFY `idKomponenLR` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `komponennjt`
--
ALTER TABLE `komponennjt`
  MODIFY `idKomponenNJT` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `komponenpl`
--
ALTER TABLE `komponenpl`
  MODIFY `idKomponenPL` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `komponensr`
--
ALTER TABLE `komponensr`
  MODIFY `idKomponenSR` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `leveluser`
--
ALTER TABLE `leveluser`
  MODIFY `idLevelUser` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `idMenu` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `nilaivolumeairtanah`
--
ALTER TABLE `nilaivolumeairtanah`
  MODIFY `idNilaiVolumeAirTanah` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `pajakairtanah`
--
ALTER TABLE `pajakairtanah`
  MODIFY `idPajakAirTanah` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `pajakhiburan`
--
ALTER TABLE `pajakhiburan`
  MODIFY `idPajakHiburan` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `pajakhotel`
--
ALTER TABLE `pajakhotel`
  MODIFY `idPajakHotel` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `pajakmblb`
--
ALTER TABLE `pajakmblb`
  MODIFY `idPajakMBLB` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `pajakparkir`
--
ALTER TABLE `pajakparkir`
  MODIFY `idPajakParkir` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `pajakrestoran`
--
ALTER TABLE `pajakrestoran`
  MODIFY `idPajakRestoran` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `pajaksarangburungwalet`
--
ALTER TABLE `pajaksarangburungwalet`
  MODIFY `idPajakSarangBurungWalet` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `pengumuman`
--
ALTER TABLE `pengumuman`
  MODIFY `idPengumuman` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `peruntukanairtanah`
--
ALTER TABLE `peruntukanairtanah`
  MODIFY `idPeruntukanAirTanah` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `quotes`
--
ALTER TABLE `quotes`
  MODIFY `idQuotes` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `satuanwaktu`
--
ALTER TABLE `satuanwaktu`
  MODIFY `idSatuanWaktu` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `idUser` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `volumeperuntukanairtanah`
--
ALTER TABLE `volumeperuntukanairtanah`
  MODIFY `idVolumePeruntukan` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `albumfoto`
--
ALTER TABLE `albumfoto`
  ADD CONSTRAINT `albumfoto_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `user` (`idUser`);

--
-- Constraints for table `berita`
--
ALTER TABLE `berita`
  ADD CONSTRAINT `berita_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `user` (`idUser`);

--
-- Constraints for table `detailfilepublikasi`
--
ALTER TABLE `detailfilepublikasi`
  ADD CONSTRAINT `detailfilepublikasi_ibfk_1` FOREIGN KEY (`kategoriFilePublikasiId`) REFERENCES `kategorifilepublikasi` (`idKategoriFilePublikasi`);

--
-- Constraints for table `foto`
--
ALTER TABLE `foto`
  ADD CONSTRAINT `foto_ibfk_1` FOREIGN KEY (`albumIdAlbum`) REFERENCES `albumfoto` (`idAlbum`);

--
-- Constraints for table `jenisreklame`
--
ALTER TABLE `jenisreklame`
  ADD CONSTRAINT `jenisreklame_ibfk_1` FOREIGN KEY (`satuanWaktuId`) REFERENCES `satuanwaktu` (`idSatuanWaktu`);

--
-- Constraints for table `komponennjt`
--
ALTER TABLE `komponennjt`
  ADD CONSTRAINT `komponennjt_ibfk_1` FOREIGN KEY (`komponenSRid`) REFERENCES `komponensr` (`idKomponenSR`),
  ADD CONSTRAINT `komponennjt_ibfk_2` FOREIGN KEY (`komponenPLid`) REFERENCES `komponenpl` (`idKomponenPL`),
  ADD CONSTRAINT `komponennjt_ibfk_3` FOREIGN KEY (`komponenKLid`) REFERENCES `komponenkl` (`idKomponenKL`);

--
-- Constraints for table `menu`
--
ALTER TABLE `menu`
  ADD CONSTRAINT `menu_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `user` (`idUser`);

--
-- Constraints for table `nilaivolumeairtanah`
--
ALTER TABLE `nilaivolumeairtanah`
  ADD CONSTRAINT `nilaivolumeairtanah_ibfk_1` FOREIGN KEY (`peruntukanAirTanahId`) REFERENCES `peruntukanairtanah` (`idPeruntukanAirTanah`),
  ADD CONSTRAINT `nilaivolumeairtanah_ibfk_2` FOREIGN KEY (`volumeAirTanahId`) REFERENCES `volumeperuntukanairtanah` (`idVolumePeruntukan`);

--
-- Constraints for table `pengumuman`
--
ALTER TABLE `pengumuman`
  ADD CONSTRAINT `pengumuman_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `user` (`idUser`);

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_ibfk_1` FOREIGN KEY (`levelUserId`) REFERENCES `leveluser` (`idLevelUser`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
