<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PersentaseParkir extends CI_Controller {

	public function __construct()
        {
                parent::__construct();
        }

    public function index()
    {
        if($this->session->userdata('logged_in') == true){
            $this->editPersentaseParkir();
        }else{
            $this->load->view('login');
        }
    }
    
    public function editPersentaseParkir(){
        $data['list'] = $this->PersentaseParkir_model->getPersentaseParkir();
        $data['title'] = 'Ubah Persentase Pajak Parkir';
        $data['labelpersentase'] = 'Pajak Parkir';
        $data['act'] = 'district/PersentaseParkir/ubahPersentaseParkirAct';
        $data['content'] = "district/PersentasePajak/formPersentase";
        $this->load->view('district/main',$data);
    }

    public function ubahPersentaseParkirAct(){
        $persentase = $this->input->post('persentase');
        $notifInput = $this->PersentaseParkir_model->updatePersentaseParkir($persentase);
        
        if($notifInput){
            $this->session->set_flashdata('message', 'Success');
        }else{
            $this->session->set_flashdata('message', 'Gagal');
        }
		redirect('district/PersentaseParkir/editPersentaseParkir');
    }
}

?>