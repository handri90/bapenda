<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/misc/favicon.png">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>BAPENDA KOBAR - Badan Pengelola Pendapatan Daerah
    </title>
    <!-- Bootstrap Core CSS-->
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">

    <link rel='stylesheet' href='<?php echo base_url(); ?>assets/css/unite-gallery.css' type='text/css' /> 
    <!-- Custom CSS-->
    <link href="<?php echo base_url(); ?>assets/css/universal.css" rel="stylesheet">
  </head>
  <body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top" class="top">
    <!-- Preloader-->
    <div id="preloader">
      <div id="status"></div>
    </div>
    <!-- Navigation-->
    <nav class="navbar navbar-custom navbar-fixed-top <?php echo !empty($classNav)?$classNav:'navbar-universal'; ?>">
      <div class="container">
        <div class="navbar-header">
          <button type="button" data-toggle="collapse" data-target=".navbar-main-collapse" class="navbar-toggle"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button><a href="<?php echo base_url(); ?>" class="navbar-brand page-scroll">
            <!-- Text or Image logo--><img src="<?php echo base_url(); ?>assets/img/logo.png" alt="Logo" class="logo"><img src="<?php echo base_url(); ?>assets/img/logodark.png" alt="Logo" class="logodark"></a>
        </div>
        <div class="collapse navbar-collapse navbar-main-collapse">
          <?php echo $menu; ?>
        </div>
      </div>
    </nav>