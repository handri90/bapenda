<?php
class PersentaseSarangBurungWalet_model extends CI_Model {

        public function __construct()
        {

        }

        public function getPersentaseSarangBurungWalet(){
          $query = $this->db->query("select * from pajaksarangburungwalet");
    		  return $query->row();
        }

        public function updatePersentaseSarangBurungWalet($persentase){
          $query = $this->db->query("update pajaksarangburungwalet set persentase=".$persentase);
          return $query;
        }
}
?>