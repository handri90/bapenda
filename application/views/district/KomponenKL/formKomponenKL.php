<!-- page content -->
<div class="right_col" role="main">
          <div class="">

            <div class="row">

              <div class="col-md-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo $title; ?></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                    <form enctype="multipart/form-data" class="form-horizontal form-label-left" method="post" action="<?php echo base_url().$act; ?>">

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Nilai Awal</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <input type="hidden" name="idkomponen" class="form-control onlynumber" value="<?php echo !empty($list->idKomponenKL)?$list->idKomponenKL:""; ?>">
                          <input required type="text" name="nilaiawal" class="form-control" placeholder="Nilai Awal" value="<?php echo !empty($list->nilaiAwalKomponenKL)?$list->nilaiAwalKomponenKL:""; ?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Nilai Akhir</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                        <input required type="text" name="nilaiakhir" class="form-control onlynumber" placeholder="Nilai Akhir" value="<?php echo !empty($list->nilaiAkhirKomponenKL)?$list->nilaiAkhirKomponenKL:""; ?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Bobot</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <input required type="text" name="bobot" class="form-control onlynumber" placeholder="Bobot" value="<?php echo !empty($list->bobotKomponenKL)?$list->bobotKomponenKL:""; ?>">
                        </div>
                      </div>
                      
                      
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                          <button type="button" class="btn btn-primary" id="backButton">Cancel</button>
                          <button type="reset" class="btn btn-primary">Reset</button>
                          <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                      </div>

                    </form>
                  </div>
                </div>
              </div>
            </div>


            
          </div>
        </div>
        <!-- /page content -->