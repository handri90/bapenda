<!-- page content -->
<div class="right_col" role="main">
          <div class="">

            <div class="row">

              <div class="col-md-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo $title; ?></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                    <form class="form-horizontal form-label-left" method="post" action="<?php echo base_url().$act; ?>">
                        <div class="form-group">
                        <input type="hidden" name="idnilaivolume" class="form-control" value="<?php echo !empty($list->idNilaiVolumeAirTanah)?$list->idNilaiVolumeAirTanah:""; ?>">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Peruntukan
                        </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <select name="peruntukan" class="form-control">
                            <?php
                            for($i=0;$i<count($peruntukan);$i++){
                                $select="";
                                if(!empty($list->peruntukanAirTanahId)){
                                    if($list->peruntukanAirTanahId==$peruntukan[$i]['idPeruntukanAirTanah']){
                                        $select="selected";
                                    }
                                }
                                ?>
                                <option <?php echo $select; ?> value="<?php echo $peruntukan[$i]['idPeruntukanAirTanah']; ?>"><?php echo $peruntukan[$i]['labelPeruntukanAirTanah']; ?></option>
                                <?php
                            }
                            ?>
                          </select>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Volume
                        </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <select name="volume" class="form-control">
                            <?php
                            for($i=0;$i<count($volume);$i++){
                                $select="";
                                if(!empty($list->volumeAirTanahId)){
                                    if($list->volumeAirTanahId==$volume[$i]['idVolumePeruntukan']){
                                        $select="selected";
                                    }
                                }
                                ?>
                                <option <?php echo $select; ?> value="<?php echo $volume[$i]['idVolumePeruntukan']; ?>"><?php $nilaiAkhir = !empty($volume[$i]['nilaiVolumePeruntukanAkhir'])?$volume[$i]['nilaiVolumePeruntukanAkhir']:'&#8734;';
                            echo $volume[$i]['nilaiVolumePeruntukanAwal']." - ".$nilaiAkhir; ?> m<sup>3</sup></option>
                                <?php
                            }
                            ?>
                          </select>
                        </div>
                      </div>
                      
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Nilai Perolehan</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <input required type="text" name="nilaiperolehan" class="form-control" placeholder="Nilai Perolehan" value="<?php echo !empty($list->hargaNilaiVolumeAirTanah)?$list->hargaNilaiVolumeAirTanah:""; ?>">
                        </div>
                      </div>
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                          <button type="button" class="btn btn-primary" id="backButton">Cancel</button>
                          <button type="reset" class="btn btn-primary">Reset</button>
                          <button type="submit" class="btn btn-success btnTambahFilePublikasi">Submit</button>
                        </div>
                      </div>

                    </form>
                  </div>
                </div>
              </div>
            </div>


            
          </div>
        </div>
        <!-- /page content -->