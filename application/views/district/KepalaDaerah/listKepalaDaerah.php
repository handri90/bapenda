<!-- page content -->
<div class="right_col" role="main">
          <div class="">

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Kepala Daerah</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li><a href="<?php echo base_url(); ?>district/KepalaDaerah/tambahKepalaDaerah" class="close-link badge bg-green"><i class="fa fa-plus"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <table id="datatable" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>No</th>
                          <th>Foto</th>
                          <th>Nama</th>
                          <th>Jabatan</th>
                          <th>Status</th>
                          <th>Action</th>
                        </tr>
                      </thead>


                      <tbody>
                      <?php
                      $no=1;
                        for($i=0;$i<count($list);$i++){
                          ?>
                          <tr>
                            <td><?php echo $no++; ?></td>
                            <td><img src="<?php echo base_url().'kepaladaerah/'.$list[$i]['filenameKepalaDaerah']; ?>" width="250" ></td>
                            <td><?php echo $list[$i]['namaKepalaDaerah']; ?></td>
                            <td><?php echo $list[$i]['jabatanKepalaDaerah'];
                            ?></td>
                            <td><?php
                            if($list[$i]['isActive'] == '0'){
                              echo 'hidden';
                            }else{
                              echo 'show';
                            }
                            ?></td>
                            <td><a class="btn btn-primary pull-right" style="margin-right: 5px;" href="<?php echo base_url().'district/KepalaDaerah/changeStatusKepalaDaerah/'.$list[$i]['idKepalaDaerah']; ?>"><i class="fa fa-pencil"></i> Ubah Status</a><a class="btn btn-primary pull-right" style="margin-right: 5px;" href="<?php echo base_url().'district/KepalaDaerah/editKepalaDaerah/'.$list[$i]['idKepalaDaerah']; ?>"><i class="fa fa-pencil"></i> Ubah</a><a class="btn btn-primary pull-right" style="margin-right: 5px;" data-toggle="modal" data-target="#confirm-delete" href="#" data-href="<?php echo base_url().'district/KepalaDaerah/deleteKepalaDaerah/'.$list[$i]['idKepalaDaerah']; ?>"><i class="fa fa-eraser"></i> Hapus</a></td>
                          </tr>
                          <?php
                        }
                      ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>