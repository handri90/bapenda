<?php
class KepalaDaerah_model extends CI_Model {

        public function __construct()
        {

        }

        public function getListKepalaDaerah(){
                $query = $this->db->query("select * from kepaladaerah");
                return $query->result_array();
        }
        
        public function inputKepalaDaerah($nama,$jabatan,$file,$ordermenu){
            $query = $this->db->query("insert into kepaladaerah (namaKepalaDaerah,jabatanKepalaDaerah,filenameKepalaDaerah,orderKepalaDaerah) values ('".$nama."','".$jabatan."','".$file."',".$ordermenu.")");
    		return $query;
        }

        public function getDataKepalaDaerahById($id){
          $query = $this->db->query("select * from kepaladaerah where idKepalaDaerah = ".$id);
    		  return $query->row();
        }

        public function updateKepalaDaerah($nama,$jabatan,$file,$ordermenu,$idKepalaDaerah){
          $query = $this->db->query("update kepaladaerah set namaKepalaDaerah='".$nama."',jabatanKepalaDaerah='".$jabatan."',filenameKepalaDaerah='".$file."',orderKepalaDaerah=".$ordermenu." where idKepalaDaerah=".$idKepalaDaerah);
          return $query;
        }

        public function deleteKepalaDaerah($id){
          $query = $this->db->query("select filenameKepalaDaerah from kepaladaerah where idKepalaDaerah = ".$id);
          $result = $query->row();
          unlink("kepaladaerah/".$result->filenameKepalaDaerah);
          $query = $this->db->query("delete from kepaladaerah where idKepalaDaerah = ".$id);
          return $query;
        }

        public function changeStatusKepalaDaerah($id){
          $query = $this->db->query("select isActive from kepaladaerah where idKepalaDaerah=".$id);
          $checkStatus = $query->row();
          $val = 0;
          if($checkStatus->isActive == '0'){
            $val = '1';
          }else{
            $val = '0';
          }
          $query = $this->db->query("update kepaladaerah set isActive='".$val."' where idKepalaDaerah=".$id);
          return $query;
        }
}
?>