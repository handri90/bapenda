<?php
class PeruntukanAirTanah_model extends CI_Model {

        public function __construct()
        {

        }

        public function getListPeruntukanAirTanah(){
                $query = $this->db->query("select * from peruntukanairtanah");
                return $query->result_array();
        }
        
        public function inputPeruntukanAirTanah($labelperuntukan){
            $query = $this->db->query("insert into peruntukanairtanah (labelPeruntukanAirTanah) values ('".$labelperuntukan."')");
    		return $query;
        }

        public function getPeruntukanAirTanahById($id){
          $query = $this->db->query("select * from peruntukanairtanah where idPeruntukanAirTanah = ".$id);
    		  return $query->row();
        }

        public function updatePeruntukanAirTanah($labelperuntukan,$idkategori){
          $query = $this->db->query("update peruntukanairtanah set labelPeruntukanAirTanah='".$labelperuntukan."' where idPeruntukanAirTanah=".$idkategori);
          return $query;
        }

        public function deletePeruntukanAirTanah($id){
          $query = $this->db->query("delete from peruntukanairtanah where idPeruntukanAirTanah = ".$id);
          return $query;
        }
}
?>